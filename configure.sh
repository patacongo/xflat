#!/bin/sh
########################################################################
# configure.sh
# Manage various buildroot configurations.
#
# Copyright (c) 2006, Cadenux, LLC.  All rights reserved.
# Copyright (c) 2006, Gregory Nutt.  All rights reserved.
# Author: Gregory Nutt <spudmonkey@racsa.co.cr>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name Cadenux nor the names of its contributors may be
#    used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
########################################################################

WD=`pwd`

function showusage ()
{
    echo ""
    echo "USAGE: ${0} [--debug] <action>"
    echo ""
    echo "WHERE:"
    echo ""
    echo "  --debug enables script debug. [default: no debug]"
    echo ""
    echo "and <action> is one of the following:"
    echo ""
    echo "  --save <config-name>.  Save the current configuration"
    echo "    in config/<config-name>/.  [default: none]"
    echo ""
    echo "  --restore <config-name>.  Restore the configuration in"
    echo "    config/<config-name>/.  [default: none]"
    echo ""
    echo "  --diff <config-name>.  Compare the current configuration with"
    echo "    the configuration in config/<config-name>/.  [default: none]"
    echo ""
    echo "  --linux <linux-dir>.  Use the linux the configuration in"
    echo "    config/<config-name>/<linux-dir>.config to save/restore"
    echo "    from <linux_dir>/.config.  Use <linux-dir> = none is"
    echo "    is not linux kernel. [default: linux-2.6.14]"
    exit 1
}

function readans ()
{
    echo -n "${1} (${2}): "
    IFS='@@' read ans || exit 1
    [ -z "$ans" ] && ans=${2}
}

function readyn ()
{
    while :; do
        readans "${1} [Y/N]" ${2}
        case "$ans" in
	  [yY] | [yY]es )
            ans=y
	    break ;;
	  [nN] | [nN]o )
	    ans=n
            break ;;
	  * )
            echo "Please answer Y or N"
            ;;
        esac
    done
}

function check_file ()
{
    if [ ! -f ${1} ]; then
	echo ""
	echo "ERROR: ${1} does not exist"
	if [ ! -z "${2}" ]; then
	    echo "${2}"
	fi
	echo ""
	exit 1
    fi
}

function compare_files ()
{
    echo ""
    echo "********************************************************************"
    echo "* Comparing ${1} to ${2}"
    echo "********************************************************************"
    echo ""
    diff -u ${1} ${2}
}

function make_buildroot_config ()
{
    echo "#!/bin/sh" >${2}
    grep "[:space;]*=[:space:]*y" ${1} | \
	sed -e "s/[:space:]//g" >>${2}
    chmod 755 ${2}
}

function do_config_save ()
{
    echo "Saving current configuration to config/${config_name}"

    # Verify that the minimum source configuration files exist

    check_file ${setenv_config} ""
    check_file ${buildroot_config}  ""
    check_file ${uclibc_config} ""

    # Get the buildroot settings

    make_buildroot_config ${buildroot_config} tmp-buildroot.config
    source tmp-buildroot.config
    rm tmp-buildroot.config

    # Verify that all additional source configuration files exist

    if [ "X${BR2_XFLAT}" = "Xy" ]; then
	check_file ${uclibc_xflat_config} ""
	check_file ${xflat_config} ""
    fi

    if [ "X${BR2_PACKAGE_BUSYBOX}" = "Xy" ]; then
	check_file ${busybox_config} ""
    fi

    # The linux directory may or may not be present.  The source
    # configuration file need exist only if the linux directory is
    # present

    if [ -d ${linux_dir} ]; then
	check_file ${linux_config} ""
    fi

    # Create the configuration directory

    if [ -d config/${config_name} ]; then
	echo "Configuration directory config/${config_name} already exists"
	readyn "Replace configuration" n
	if [ "X${ans}" = "Xn" ]; then
	    echo "Aborting..."
	    exit 1
	fi
	rm -rf config/${config_name}
    fi

    mkdir -p config/${config_name}

    # And copy the configuration files

    cp ${setenv_config} config/${config_name}/${setenv_save} || \
    { \
      echo "Failed to copy ${setenv_config} to config/${config_name}"; \
      exit 1; \
    }

    cp ${buildroot_config} config/${config_name}/${buildroot_save} || \
    { \
      echo "Failed to copy ${buildroot_config} to config/${config_name}"; \
      exit 1; \
    }

    cp ${uclibc_config} config/${config_name}/${uclibc_save} || \
    { \
      echo "Failed to copy ${uclibc_config} to config/${config_name}"; \
      exit 1; \
    }

    if [ "X${BR2_XFLAT}" = "Xy" ]; then
	cp ${uclibc_xflat_config} config/${config_name}/${uclibc_xflat_save} || \
	    { \
	    echo "Failed to copy ${uclibc_xflat_config} to config/${config_name}"; \
	    exit 1; \
	    }

	cp ${xflat_config} config/${config_name}/${xflat_save} || \
	    { \
	    echo "Failed to copy ${xflat_config} to config/${config_name}"; \
	    exit 1; \
	    }
    fi

    if [ -d ${linux_dir} ]; then
	cp ${linux_config} config/${config_name}/${linux_save} || \
	{ \
	    echo "Failed to copy ${linux_config} to config/${config_name}"; \
	    exit 1; \
	}
    fi

    if [ "X${BR2_PACKAGE_BUSYBOX}" = "Xy" ]; then
	cp ${busybox_config} config/${config_name}/${busybox_save} || \
	    { \
	    echo "Failed to copy ${busybox_config} to config/${config_name}"; \
	    exit 1; \
	    }
    fi

    echo "*********************************************"
    echo "Configuration config/${config_name} saved"
    echo "*********************************************"

}

function do_config_restore ()
{
    echo "Retrieving configuration to config/${config_name}"

    # Verify that the source directory exists

    if [ ! -d config/${config_name} ]; then
	echo "Configuration directory config/${config_name} does not exist"
	exit 1
    fi

    # Verify that the minimum source configuration files exist

    check_file config/${config_name}/${setenv_save} ""
    check_file config/${config_name}/${buildroot_save} ""
    check_file config/${config_name}/${uclibc_save} ""

    # Get the buildroot settings

    make_buildroot_config config/${config_name}/${buildroot_save} tmp-buildroot.config
    source tmp-buildroot.config
    rm tmp-buildroot.config

    # Verify that the additional source configuration files exist

    if [ "X${BR2_XFLAT}" = "Xy" ]; then
	check_file config/${config_name}/${uclibc_xflat_save} ""
	check_file config/${config_name}/${xflat_save} ""
    fi

    if [ "X${BR2_PACKAGE_BUSYBOX}" = "Xy" ]; then
	check_file config/${config_name}/${busybox_save} ""
    fi

    # A linux kernel is optional.  But we will declare an error
    # if the linux directory is ppresent and there is no saved
    # configuration file.

    if [ -d "${linux_dir}" ]; then
	check_file config/${config_name}/${linux_save} "Did you mean to use --linux none"
    fi

    # Then copy the configuration files

    cp -f config/${config_name}/${setenv_save} ${setenv_config}
    cp -f config/${config_name}/${buildroot_save} ${buildroot_config}
    cp -f config/${config_name}/${uclibc_save} ${uclibc_config}

    if [ "X${BR2_XFLAT}" = "Xy" ]; then
	cp -f config/${config_name}/${uclibc_xflat_save} ${uclibc_xflat_config}
	cp -f config/${config_name}/${xflat_save} ${xflat_config}
    fi

    if [ "X${BR2_PACKAGE_BUSYBOX}" = "Xy" ]; then
	cp -f config/${config_name}/${busybox_save} ${busybox_config}
    fi

    if [ -d ${linux_dir} ]; then
	cp -f config/${config_name}/${linux_save} ${linux_config}
    fi

    # Source the setenv file so that it refers to the correct toolchain
    # (This is probably futile since the toolchain has not been built yet)

    source ${setenv_config} || \
	{ echo "Failed to source ${setenv_config}" ; exit 1 ; }

    # Then make sure that the new configurations are usable

    make -C ${buildroot_dir} oldconfig || \
	{ echo "make oldconfig in ${buildroot_dir} failed" ; \
	  echo "Please 'make oldconfig' in ${buildroot_dir}" ; }

    if [ -d ${linux_dir} ]; then
	make -C ${linux_dir} oldconfig || \
	    { echo "make oldconfig in ${linux_dir} failed" ; \
	      echo "Please 'make oldconfig' in ${linux_dir}" ; }
    fi

    echo "**********************************************"
    echo "Configuration config/${config_name} restored"
    echo "          Remember to source setenv!"
    echo "**********************************************"
}

function do_config_diff ()
{
    echo "Comparing current configuration to config/${config_name}"

    # Verify that the configuration directory exists

    if [ ! -d config/${config_name} ]; then
	echo "ERROR: Configuration directory config/${config_name} does not exist"
	exit 1
    fi

    # Compare the setenv script

    if [ ! -f ${setenv_config} ]; then
	echo "ERROR: ${setenv_config} does not exist"
    else
	if [ ! -f config/${config_name}/${setenv_save} ]; then
	    echo "ERROR: config/${config_name}/${setenv_save} does not exist"
	else
	    compare_files ${setenv_config} config/${config_name}/${setenv_save}
	fi
    fi

    # Compare the buildroot configuration file

    if [ ! -f ${buildroot_config} ]; then
	echo "ERROR: ${buildroot_config} does not exist"
	BR2_XFLAT=n
	BR2_PACKAGE_BUSYBOX=n
    else
	if [ ! -f config/${config_name}/${buildroot_save} ]; then
	    echo "ERROR: config/${config_name}/${buildroot_save} does not exist"
	else
	    compare_files ${buildroot_config} config/${config_name}/${buildroot_save}
	fi

        # Get the buildroot settings

	make_buildroot_config ${buildroot_config} tmp-buildroot.config
	source tmp-buildroot.config
	rm tmp-buildroot.config
    fi

    # Compare the uclibc configuration file

    if [ ! -f ${uclibc_config} ]; then
	echo "ERROR: ${uclibc_config} does not exist"
	BR2_XFLAT=n
	BR2_PACKAGE_BUSYBOX=n
    else
	if [ ! -f config/${config_name}/${uclibc_save} ]; then
	    echo "ERROR: config/${config_name}/${uclibc_save} does not exist"
	else
	    compare_files ${uclibc_config} config/${config_name}/${uclibc_save}
	fi
    fi

    # Compare XFLAT configuration files

    if [ "X${BR2_XFLAT}" = "Xy" ]; then

	# Compare the uclibc-xflat configuration files

	if [ ! -f ${uclibc_xflat_config} ]; then
	    echo "ERROR: ${uclibc_xflat_config} does not exist"
	    BR2_XFLAT=n
	    BR2_PACKAGE_BUSYBOX=n
	else
	    if [ ! -f config/${config_name}/${uclibc_xflat_save} ]; then
		echo "ERROR: config/${config_name}/${uclibc_xflat_save} does not exist"
	    else
		compare_files ${uclibc_xflat_config} config/${config_name}/${uclibc_xflat_save}
	    fi
	fi

	# Compare the xflat configuratin files

	if [ ! -f ${xflat_config} ]; then
	    echo "ERROR: ${xflat_config} does not exist"
	    BR2_XFLAT=n
	    BR2_PACKAGE_BUSYBOX=n
	else
	    if [ ! -f config/${config_name}/${xflat_save} ]; then
		echo "ERROR: config/${config_name}/${xflat_save} does not exist"
	    else
		compare_files ${xflat_config} config/${config_name}/${xflat_save}
	    fi
	fi

    else
	# XFLAT is not enabled, there should be no configuration files

	if [ -f config/${config_name}/${uclibc_xflat_save} ]; then
	    echo "WARNING: config/${config_name}/${uclibc_xflat_save} exists but XFLAT is disabled"
	fi

	if [ -f config/${config_name}/${xflat_save} ]; then
	    echo "WARNING: config/${config_name}/${xflat_save} exists but XFLAT is disabled"
	fi
    fi

    # Compare the busybox configuration files

    if [ "X${BR2_PACKAGE_BUSYBOX}" = "Xy" ]; then
	if [ ! -f ${busybox_config} ]; then
	    echo "ERROR: ${busybox_config} does not exist"
	    BR2_XFLAT=n
	    BR2_PACKAGE_BUSYBOX=n
	else
	    if [ ! -f config/${config_name}/${busybox_save} ]; then
		echo "ERROR: config/${config_name}/${busybox_save} does not exist"
	    else
		compare_files ${busybox_config} config/${config_name}/${busybox_save}
	    fi
	fi
    else
	# Busybox is not enabled, there should be no configuration files

	if [ -f config/${config_name}/${busybox_save} ]; then
	    echo "WARNING: config/${config_name}/${busybox_save} exists but XFLAT is disabled"
	fi
    fi

    # Compare the linux configuration files

    if [ -d ${linux_dir} ]; then
	if [ ! -f ${linux_config} ]; then
	    echo "ERROR: ${linux_config} does not exist"
	    BR2_XFLAT=n
	    BR2_PACKAGE_LINUX=n
	else
	    if [ ! -f config/${config_name}/${linux_save} ]; then
		echo "ERROR: config/${config_name}/${linux_save} does not exist"
	    else
		compare_files ${linux_config} config/${config_name}/${linux_save}
	    fi
	fi
    else
	# The linux directory does not exist, there should be no configuration files

	if [ -f config/${config_name}/${linux_save} ]; then
	    echo "WARNING: config/${config_name}/${linux_save} exists but XFLAT is disabled"
	fi
    fi
}

# Parse command arguments

config_op="none"
config_diff="n"
config_name=
linux_dir=linux-2.6.14

while [ ! -z "${1}" ]; do
    case "${1}" in
        --debug )
	    set -x
	    ;;
	--save )
	    config_op="save"
	    config_name=${2}
	    shift
	    ;;
	--restore )
	    config_op="restore"
	    config_name=${2}
	    shift
	    ;;
	--diff )
	    config_op="diff"
	    config_name=${2}
	    shift
	    ;;
	--linux )
	    linux_dir=${2}
	    shift
	    ;;
        *)
	    echo "Unrecognized option"
	    showusage
	    ;;
    esac
    shift
done

# Verify commands

if [ -z "${config_name}" ]; then
    echo "No configuration name specified"
    showusage
fi

# Setup directories

setenv_save=setenv
buildroot_save=buildroot.config
uclibc_save=uClibc.config
uclibc_xflat_save=uClibc-xflat.config
xflat_save=xflat.config
linux_save=${linux_dir}.config
busybox_save=busybox.config

setenv_dir=.
buildroot_dir=buildroot
uclibc_dir=${buildroot_dir}/toolchain/uClibc
uclibc_xflat_dir=${buildroot_dir}/toolchain/uClibc-xflat
xflat_dir=${buildroot_dir}/toolchain/xflat
busybox_dir=${buildroot_dir}/package/busybox

setenv_config=${setenv_dir}/setenv
buildroot_config=${buildroot_dir}/.config
uclibc_config=${uclibc_dir}/uClibc.config
uclibc_xflat_config=${uclibc_xflat_dir}/uClibc-xflat.config
xflat_config=${xflat_dir}/xflat.config
linux_config=${linux_dir}/.config
busybox_config=${busybox_dir}/busybox.config

# Perform action

case "${config_op}" in
    "save" )
	do_config_save
	;;
    "restore" )
	do_config_restore
	;;
    "diff" )
	do_config_diff
	;;
    *)
	echo "You must specify an action"
	showusage
	;;
esac
