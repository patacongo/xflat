/***********************************************************************
 * xflat/kernel/binfmt/linux-2.6/binfmt_main.c
 *
 * References:
 *  linux/fs/binfmt_aout.c:
 *      Copyright (C) 1991, 1992, 1996  Linus Torvalds
 *  linux/fs/binfmt_flat.c for 2.0 kernel
 *	Copyright (C) 1998  Kenneth Albanowski <kjahds@kjahds.com>
 *      JAN/99 -- coded full program relocation (gerg@lineo.com)
 *	Copyright (C) 2000, 2001 Lineo, by David McCullough <davidm@lineo.com>
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 ***********************************************************************/

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include <linux/module.h>
#include <linux/sched.h>
#include <linux/kernel.h>
#include <linux/mm.h>
#include <linux/mman.h>
#include <linux/a.out.h>
#include <linux/errno.h>
#include <linux/signal.h>
#include <linux/string.h>
#include <linux/fs.h>
#include <linux/file.h>
#include <linux/stat.h>
#include <linux/fcntl.h>
#include <linux/ptrace.h>
#include <linux/user.h>
#include <linux/slab.h>
#include <linux/binfmts.h>
#include <linux/personality.h>
#include <linux/init.h>
#include <linux/flat.h>
#include <linux/config.h>

#include <asm/byteorder.h>
#include <asm/system.h>
#include <asm/uaccess.h>
#include <asm/pgalloc.h>
#include <asm/unaligned.h>

#include "xflatlib.h"

/***********************************************************************
 * Definitions
 ***********************************************************************/

/* CONFIG_XFLAT_DEBUG is defined if the "debug" target is selected in the
 * Makefile for this module.
 */

#ifndef dbg
#ifdef CONFIG_XFLAT_DEBUG

#undef KERN_DEBUG
#define KERN_DEBUG

# define dbg(format, arg...) \
   printk(KERN_DEBUG "BINFMT_XFLAT: " format "\n" , ## arg)

#else

# define dbg(format, arg...) \
   do {} while (0)

#endif
#endif

#define err(format, arg...) \
   printk(KERN_ERR "BINFMT_XFLAT: " format "\n" , ## arg)

#define info(format, arg...) \
   printk(KERN_INFO "BINFMT_XFLAT: " format "\n" , ## arg)

#define warn(format, arg...) \
   printk(KERN_WARNING "BINFMT_XFLAT: " format "\n" , ## arg)

/***********************************************************************
 * Private Function Prototypes
 ***********************************************************************/

static void *xflat_ldr_map(bin_handle_t handle,
			   unsigned long nbytes);
static void *xflat_prog_map(bin_handle_t handle,
			    unsigned long nbytes);
static void xflat_unmap(bin_handle_t address,
			unsigned long nbytes);
static void *xflat_alloc(unsigned long nbytes);
static file_handle_t xflat_ldr_open(bin_handle_t handle,
				    const char *filename);
static file_handle_t xflat_prog_open(bin_handle_t handle,
				     const char *filename);
static int  xflat_ldr_read(bin_handle_t bin_handle,
			   file_handle_t file_handle,
			   char *dest, unsigned long nbytes,
			   unsigned long fpos);
static int xflat_prog_read(bin_handle_t bin_handle,
			   file_handle_t file_handle,
			   char *dest, unsigned long nbytes,
			   unsigned long fpos);
static void xflat_close(file_handle_t handle);
static void xflat_ldr_close(file_handle_t handle);

static int xflat_load_binary(struct linux_binprm *, struct pt_regs * regs);
static int xflat_load_library(struct file*);
static int xflat_core_dump(long signr, struct pt_regs *regs, struct file *file);

/***********************************************************************
 * Global Function Prototypes
 ***********************************************************************/

extern void dump_thread(struct pt_regs *, struct user *);

/***********************************************************************
 * Private Constant Data
 ***********************************************************************/

static struct linux_binfmt xflat_format =
{
  .module       = THIS_MODULE,
  .load_binary  = xflat_load_binary,
  .load_shlib   = xflat_load_library,
  .core_dump    = xflat_core_dump,
  .min_coredump = PAGE_SIZE,
};


static const struct xflat_vtbl prog_vtbl =
{
  xflat_prog_map,
  xflat_unmap, 
  xflat_alloc,
  xflat_unmap,
  xflat_prog_open,
  xflat_prog_read,
  xflat_close
};

static const struct xflat_vtbl ldr_vtbl =
{
  xflat_ldr_map,
  xflat_unmap, 
  xflat_alloc,
  xflat_unmap,
  xflat_ldr_open,
  xflat_ldr_read,
  xflat_ldr_close
};

/***********************************************************************
 * Private Functions
 ***********************************************************************/

/***********************************************************************
 * These are the functions that are exported to the xflatlib
 ***********************************************************************/

static void *
xflat_ldr_map(file_handle_t handle, unsigned long nbytes)
{
  struct file *file = (struct file*)handle;
  void *retval;

  dbg("Map %ld bytes for loader", nbytes);

  down_write(&current->mm->mmap_sem);
  retval = (void*)do_mmap(file, 0, nbytes, PROT_READ|PROT_EXEC,
			  MAP_PRIVATE, 0);
  up_write(&current->mm->mmap_sem);

  dbg("Returning address=0x%p", retval);
  return retval;
}

static void *
xflat_prog_map(file_handle_t handle, unsigned long nbytes)
{
  struct linux_binprm *bprm = (struct linux_binprm*)handle;
  void *retval;

  dbg("Map %ld bytes for program", nbytes);

  down_write(&current->mm->mmap_sem);
  retval = (void*)do_mmap(bprm->file, 0, nbytes, PROT_READ|PROT_EXEC,
			  MAP_PRIVATE, 0);
  up_write(&current->mm->mmap_sem);

  dbg("Returning address=0x%p", retval);
  return retval;
}

static void
xflat_unmap(void *address, unsigned long nbytes)
{
  dbg("Unmap %ld bytes at 0x%p", nbytes, address);
  do_munmap(current->mm, (unsigned long)address, nbytes);
}

static void *
xflat_alloc(unsigned long nbytes)
{
  void *retval;

  dbg("nbytes=%ld", nbytes);

  down_write(&current->mm->mmap_sem);
  retval = (void*)do_mmap(0, 0, nbytes,  PROT_READ | PROT_EXEC | PROT_WRITE,
			  MAP_PRIVATE | MAP_ANONYMOUS, 0);
  up_write(&current->mm->mmap_sem);

  dbg("Returning address=0x%p", retval);
  return retval;
}

static file_handle_t
xflat_ldr_open(bin_handle_t handle, const char *filename)
{
  struct file *stream = open_exec(filename);
  if (IS_ERR(stream))
    {
      /* This is not really an error because we may try to open
       * several pathes to the loader before we decide we cannot
       * open the loader.
       */

      dbg("Failed to open loader %s, error=%ld",
	  filename, -((long)stream));
      stream = NULL;
    }

  dbg("Returning file=0x%p", stream);
  return stream;
}

static file_handle_t
xflat_prog_open(bin_handle_t handle, const char *filename)
{
  dbg("This should never be called, returning NULL");
  return NULL;
}

static int
xflat_ldr_read(bin_handle_t bin_handle, file_handle_t file_handle,
		char *dest, unsigned long nbytes,
		unsigned long fpos)
{
  struct file *stream  = (struct file*)file_handle;
  int retval;

  dbg("Reading %ld bytes from loader stream=0x%p", nbytes, stream);

  retval = kernel_read(stream, fpos, dest, nbytes);

  /* The return value from kernel_read is the number of bytes
   * read or an error value.  The read failed if either the
   * the number of bytes requested is not the number read (unexpected
   * "short" read OR an error code).
   */

  if ((unsigned long)retval == nbytes) 
    {
      /* If the number of bytes read is the number requested,
       * then the read succeeded.
       */

      retval = 0;
    }
  else
    {
      /* A read failure from the loader is always a error! */

      err("Error reading ld-xflat.so (read size=%ld bytes, error=%d)!",
	  nbytes, retval);

      /* Make sure that the return value is non-zero */

      retval = -1;
    }
  return retval;
}

static int
xflat_prog_read(bin_handle_t bin_handle, file_handle_t file_handle,
		char *dest, unsigned long nbytes,
		unsigned long fpos)
{
  struct linux_binprm *bprm = (struct linux_binprm*)bin_handle;
  int retval;
  loff_t lpos = fpos;

  dbg("Reading %ld bytes from program stream=0x%p", nbytes, bprm->file);

  retval = bprm->file->f_op->read(bprm->file, dest, nbytes, &lpos);

  /* The return value from kernel_read is the number of bytes
   * read or an error value.  The read failed if either the
   * the number of bytes requested is not the number read (unexpected
   * "short" read OR an error code).
   */

  if ((unsigned long)retval == nbytes) 
    {
      /* If the number of bytes read is the number requested,
       * then the read succeeded.
       */

      retval = 0;
    }
  else
    {
      /* We will not claim an error an any event because
       * we don't anything about this file.  This will just end
       * up being another "normal" -ENOEXEC.
       */

      dbg("Read of %ld bytes failed, error=%d", nbytes, retval);

      /* Make sure that the return value is non-zero */

      retval = -1;
    }
  return retval;
}

static void
xflat_close(file_handle_t handle)
{
  /* We need to keep the binary file open.  It will be close
   * by the exec infrastructure.
   */

  dbg("xflat_close: Do nothing");
}

static void
xflat_ldr_close(file_handle_t handle)
{
  struct file *stream  = (struct file*)handle;

  /* But, after mapping loader, it will be necessary to close
   * the loader file.  Return the inode.
   */

  dbg("xflat_ldr_close");

  allow_write_access(stream);
  if (stream)
    fput(stream);
}

/***********************************************************************
 * Some Debug Functions
 ***********************************************************************/
 
/***********************************************************************
 * xflat_dump_memory
 ***********************************************************************/

#ifdef CONFIG_XFLAT_DEBUG
static void
xflat_dump_memory(unsigned long begin_stack, unsigned long end_stack)
{
  unsigned long *p = (unsigned long*)(begin_stack & ~3);
  unsigned long *pend = (unsigned long*)((end_stack+3) & ~3);

  dbg("  ADDRESS    VALUE");
  for (; p != pend; p++)
    {
      dbg("  0x%p 0x%08lx", p, *p);
    }
}
#else /* CONFIG_XFLAT_DEBUG */
# define xflat_dump_memory(b,e)
#endif /* CONFIG_XFLAT_DEBUG */

/***********************************************************************
 * xflat_dump_load_info
 ***********************************************************************/

#ifdef CONFIG_XFLAT_DEBUG
static void
xflat_dump_load_info(struct xflat_load_info *load_info)
{
  unsigned long dspace_size =
    XFLAT_DATA_OFFSET +
    load_info->data_size +
    load_info->bss_size +
    load_info->stack_size;

  dbg("LOAD_INFO:");
  dbg("  ISPACE:");
  dbg("    ispace       = 0x%08lx", load_info->ispace);
  dbg("    entry_offset = 0x%08lx", load_info->entry_offset);
  dbg("    ispace_size  = 0x%08lx", load_info->ispace_size);

  dbg("  DSPACE:");
  dbg("    dspace       = 0x%08lx", load_info->dspace);
  dbg("      (ldso)     = 0x%08x",  XFLAT_DATA_OFFSET);
  dbg("    data_size    = 0x%08lx", load_info->data_size);
  dbg("    bss_size     = 0x%08lx", load_info->bss_size);
  dbg("      (pad)      = 0x%08lx", load_info->dspace_size - dspace_size);
  dbg("    stack_size   = 0x%08lx", load_info->stack_size);
  dbg("    dspace_size  = 0x%08lx", load_info->dspace_size);

  dbg("  ARGUMENTS:");
  dbg("    arg_start    = 0x%08lx", load_info->arg_start);
  dbg("    env_start    = 0x%08lx", load_info->env_start);
  dbg("    env_end      = 0x%08lx", load_info->env_end);

  dbg("  RELOCS:");
  dbg("    reloc_start  = 0x%08lx", load_info->reloc_start);
  dbg("    reloc_count  = 0x%08lx", load_info->reloc_count);

  dbg("  HANDLES:");
  dbg("    bin_handle   = 0x%p",    load_info->bin_handle);
  dbg("    file_handle  = 0x%p",    load_info->file_handle);

  dbg("  xFLT HEADER:");
  dbg("    header       = 0x%p",    load_info->header);

  dbg("  ALLOCATIONS:");
  dbg("    alloc_start  = 0x%08lx", load_info->alloc_start);
  dbg("    alloc_size   = 0x%08lx", load_info->alloc_size);
}
#else /* CONFIG_XFLAT_DEBUG */
# define xflat_dump_load_info(i)
#endif /* CONFIG_XFLAT_DEBUG */

/***********************************************************************
 * These are the functions that are exported to linux
 ***********************************************************************/
 
/***********************************************************************
 * xflat_core_dump
 ***********************************************************************/

/* Routine writes a core dump image in the current directory.
 * Currently only a stub-function.
 */

static int xflat_core_dump(long signr, struct pt_regs *regs,
			  struct file *file)
{
  info("Process %s:%d received signr %d and should have core dumped",
       current->comm, current->pid, (int) signr);
  return(1);
}

/***********************************************************************
 * xflat_create_tables
 ***********************************************************************/

/* xflat_create_tables() parses the env- and arg-strings in new user
 * memory and creates the pointer tables from them, and puts their
 * addresses on the "stack", returning the new stack pointer value.
 */

static inline unsigned long
xflat_create_tables(struct xflat_load_info *prog_info,
		   struct xflat_load_info *ldr_info,
		   unsigned long pp,
		   struct linux_binprm *bprm)
{
  int retval;

  retval = xflat_init_stack(prog_info, ldr_info,
			    bprm->argc, bprm->envc, (char*)pp);

  current->mm->arg_start = prog_info->arg_start;
  current->mm->arg_end   = prog_info->env_start;
  current->mm->env_start = prog_info->env_start;
  current->mm->env_end   = prog_info->env_end;

  return retval;
}

/***********************************************************************
 * xflat_load_binary
 ***********************************************************************/

/* These are the functions used to load xflat style executables and shared
 * libraries.  There is no binary dependent code anywhere else.
 */

static int xflat_load_binary(struct linux_binprm *bprm, struct pt_regs *regs)
{
  struct xflat_hdr      *prog_header;
  struct xflat_hdr       ldr_header;
  struct xflat_load_info prog_info;
  struct xflat_load_info ldr_info;
  unsigned long          extra_stack_size;
  unsigned long          entry_point;
  unsigned long          rlim;
  unsigned long          p = bprm->p;
  struct inode          *inode;
  int                    result;
  int                    i;

  dbg("BINFMT_XFLAT: Loading file: %s", bprm->filename);

  prog_header = ((struct xflat_hdr*)bprm->buf);		/* exec-header */
  inode = bprm->file->f_dentry->d_inode;

  /* Initialize the xflat library to load the program binary. */

  result = xflat_init((void*)bprm, (void*)bprm, 
		      prog_header, &prog_vtbl, &prog_info);
  xflat_dump_load_info(&prog_info);
  if (result != 0)
    {
      dbg("BINFMT_XFLAT: Failed to initialize for load of xFLT program");
      return result;
    }

  /* We have to add the size of our arguments to our stack size
   * otherwise it's too easy for users to create stack overflows
   * by passing in a huge argument list.  And yes,  we have to be
   * pedantic and include space for the argv/envp array as it may have
   * a lot of entries.
   */

  /* Increment the stack size for the strings */

#define TOP_OF_ARGS (PAGE_SIZE*MAX_ARG_PAGES-sizeof(void*))
  extra_stack_size = TOP_OF_ARGS - bprm->p;

  /* Adjust the stack size to include space for the
   * parameters, environment, etc.
   */

  xflat_adjust_stack_size(&prog_info, bprm->argc,
			  bprm->envc, extra_stack_size);

  /* Check initial limits. This avoids letting people circumvent
   * size limits imposed on them by creating programs with large
   * arrays in the data or bss.
   */

  rlim = current->signal->rlim[RLIMIT_DATA].rlim_cur;
  if (rlim >= RLIM_INFINITY)
    rlim = ~0;
  if (XFLAT_DATA_OFFSET + prog_info.data_size + prog_info.bss_size > rlim)
    return -ENOMEM;

  /* Flush all traces of the currently running executable */

  result = flush_old_exec(bprm);
  if (result)
    return result;

  /* Load the program binary */

  result = xflat_load(&prog_info);
  xflat_dump_load_info(&prog_info);
  if (result != 0)
    {
      dbg("BINFMT_XFLAT: Failed to load xFLT program binary");
      xflat_uninit(&prog_info);
      return result;
    }

  /* Initialize the xflat library to load the program binary. */

  result = xflat_init_interpreter(NULL, &prog_info, &ldr_info,
				  &ldr_header, &ldr_vtbl);
  xflat_dump_load_info(&ldr_info);
  if (result != 0)
    {
      dbg("Failed to initialize for load of xFLT interpreter, "
	       " result=%d",  result);
      xflat_unload(&prog_info);
      xflat_uninit(&prog_info);
      return result;
    }

  /* Make sure that there is not stack associated with the interpreter. */

  ldr_info.stack_size = 0;

  /* Load the intrepeter binary */

  result = xflat_load(&ldr_info);
  xflat_dump_load_info(&ldr_info);
  if (result != 0)
    {
      dbg("Failed to load xFLT interpreter, result=%d",
	       result);
      xflat_unload(&prog_info);
      xflat_uninit(&prog_info);
      xflat_uninit(&ldr_info);
      return result;
    }

  /* We can now close the loader file.  After it is mapped to
   * memory, we no longer need its file descriptor.
   */

  xflat_ldr_close(ldr_info.file_handle);

  /* OK, This is past the point of no return */

  set_personality(PER_LINUX_32BIT);

  current->mm->start_code = ldr_info.ispace;
  current->mm->end_code   = ldr_info.ispace + ldr_info.ispace_size;
  current->mm->start_data = ldr_info.dspace + XFLAT_DATA_OFFSET;
  current->mm->end_data   = ldr_info.dspace + XFLAT_DATA_OFFSET + ldr_info.data_size;

  /* Set up the brk stuff (uses any slack left in data/bss/stack allocation
   * We put the brk after the bss (between the bss and stack) like other
   * platforms.
   *
   * - brk/stark_brk point to the first address after BSS in the DSpace
   *   allocation.
   * - end_brk points to the end of the allocation minus the stack size.
   */

  current->mm->start_brk       = XFLAT_DATA_OFFSET + prog_info.dspace +
                                 prog_info.data_size + prog_info.bss_size;
  current->mm->brk             = (current->mm->start_brk + 3) & ~3;
  current->mm->context.end_brk = prog_info.alloc_start +
                                 ksize((void *)prog_info.alloc_start) -
                                 prog_info.stack_size;
#if defined(set_mm_counter)
  set_mm_counter(current->mm, rss, 0);
#else
  current->mm->rss       = 0;
#endif

  dbg("Load %s:\n", bprm->filename);
  dbg("\tTEXT=0x%08x-0x%08x",
      (int)current->mm->start_code, (int)current->mm->end_code);
  dbg("\tDATA=0x%08x-0x%08x",
      (int)current->mm->start_data, (int)current->mm->end_data);
  dbg("\tBSS=0x%08x-0x%08x",
      (int)current->mm->end_data,   (int)current->mm->brk);
  dbg("\tBRK=0x%08x-%08x",
      (int)current->mm->start_brk,  (int)current->mm->context.end_brk);

  compute_creds(bprm);
  current->flags &= ~PF_FORKNOEXEC;

  flush_icache_range(current->mm->start_code, current->mm->end_code);

  set_binfmt(&xflat_format);

  /* The "bottom" of the stack is then the end_brk value plus size
   * of the stack.
   */

  p = ((current->mm->context.end_brk + prog_info.stack_size + 3) & ~3) - 4;
  dbg("Bottom of stack=0x%lx", p);

  /* Copy the arg pages onto the stack */

  for (i = TOP_OF_ARGS - 1; i >= bprm->p; i--)
    * (char *) --p =
      ((char *) page_address(bprm->page[i/PAGE_SIZE]))[i % PAGE_SIZE];

  current->mm->start_stack = (unsigned long)
    xflat_create_tables(&prog_info, &ldr_info, p, bprm);

  /* And set up to exec the interpreter */

  entry_point = ldr_info.ispace + ldr_info.entry_offset;

  dbg("start_thread(regs=0x%p, entry=0x%lx, start_stack=0x%lx)",
      regs, entry_point, current->mm->start_stack);

  start_thread(regs, entry_point, current->mm->start_stack);

  dbg("LDR ENTRY CODE:");
  xflat_dump_memory(entry_point, entry_point + 16*sizeof(unsigned long));

  if (current->ptrace & PT_PTRACED)
    send_sig(SIGTRAP, current, 0);

  return 0;
}

/***********************************************************************
 * xflat_load_library
 ***********************************************************************/

static int xflat_load_library(struct file *file)
{
  return(-ENOEXEC);
}

/***********************************************************************
 * xflat_init_binfmt
 ***********************************************************************/

static int __init xflat_init_binfmt(void)
{
  int retval;

  /* Say hello */

  info("Extended flat loader");

  /* Register ourselves as a binfmt loader */

  retval = register_binfmt(&xflat_format);
  if (retval != 0)
    {
      err("Failed to register binfmt (%d)", retval);
    }
  return retval;
}

/***********************************************************************
 * xflat_exit_binfmt
 ***********************************************************************/

#ifdef MODULE
  static void __exit xflat_exit_binfmt(void)
  {
    unregister_binfmt(&xflat_format);
  }
#endif

/***********************************************************************
 * Public Functions
 ***********************************************************************/
 
/***********************************************************************
 * Module Interfaces
 ***********************************************************************/

#ifndef MODULE
  core_initcall(xflat_init_binfmt);
#else
  module_init(xflat_init_binfmt);
  module_exit(xflat_exit_binfmt);

  MODULE_AUTHOR("Gregory Nutt <spudmonkey@racsa.co.cr>");
  MODULE_DESCRIPTION("XFLAT Loader");
  MODULE_LICENSE("Proprietary");
#endif
