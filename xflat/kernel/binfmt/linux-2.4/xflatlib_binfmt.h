/***********************************************************************
 * xflat/kernel/binfmt/linux-2.4/xflatlib_binfmt.h
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#ifndef _XFLATLIB_BINFMT_H_
#define _XFLATLIB_BINFMT_H_

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include <linux/kernel.h>      /* printk */
#include <linux/string.h>      /* memset, strncmp */

#include <asm/uaccess.h>       /* put_user, get_user */
#include <asm/errno.h>         /* ENOEXEC */

#include <asm/byteorder.h>     /* ntohs, ntohl */

/***********************************************************************
 * Definitions
 ***********************************************************************/

#define XFLAT_NTOHL   ntohl
#define XFLAT_NTOHS   ntohs
#define XFLAT_PRINT   printk
#define XFLAT_STRLEN  strlen
#define XFLAT_STRCPY  strcpy
#define XFLAT_STRCAT  strcat
#undef  KERN_DEBUG
#define KERN_DEBUG
#define XFLAT_FD

/* Use string.h utilities */

#define xflat_memset  memset
#define xflat_strncmp strncmp

/* Adapt asm/uaccess.h (linux 2.4) */

#define XFLAT_PUT_USER(x,p) put_user(x,p)
#define XFLAT_GET_USER(x,p) get_user(x,p)

/***********************************************************************
 * Public Types
 ***********************************************************************/

/***********************************************************************
 * Public Functions
 ***********************************************************************/

#endif /* XFLATLIB_BINFMT_H_ */
