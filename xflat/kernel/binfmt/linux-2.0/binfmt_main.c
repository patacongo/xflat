/***********************************************************************
 * xflat/kernel/binfmt/linux-2.0/binfmt_main.c
 *
 * References:
 *  linux/fs/binfmt_aout.c:
 *      Copyright (C) 1991, 1992, 1996  Linus Torvalds
 *  linux/fs/binfmt_flat.c for 2.0 kernel
 *	Copyright (C) 1998  Kenneth Albanowski <kjahds@kjahds.com>
 *      JAN/99 -- coded full program relocation (gerg@lineo.com)
 *	Copyright (C) 2000, 2001 Lineo, by David McCullough <davidm@lineo.com>
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 ***********************************************************************/

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include <linux/module.h>

#define abort() break      /* To keep macro __xchg happy */

#include <linux/mm.h>      /* For do_mmap, etc. */
#include <linux/mman.h>    /* For PROT_READ, etc. */
#include <linux/binfmts.h> /* For struct linux_binprm */

#ifndef MODULE
#include <linux/malloc.h>  /* For ksize() -- not available to modules? */
#endif

#include <asm/segment.h>   /* For get_fs() */
#include <asm/pgtable.h>   /* For flush_cache_mm */

#include "xflatlib.h"      /* For xflat library APIs */

/***********************************************************************
 * Compilation Switches
 ***********************************************************************/

/***********************************************************************
 * Definitions
 ***********************************************************************/

#ifndef dbg
#ifdef CONFIG_XFLAT_DEBUG

#undef KERN_DEBUG
#define KERN_DEBUG

# define dbg(format, arg...) \
   printk( KERN_DEBUG "BINFMT_XFLAT: " format "\n" , ## arg)

#else

# define dbg(format, arg...) \
   do {} while (0)

#endif
#endif

#define err(format, arg...) \
   printk(KERN_ERR "BINFMT_XFLAT: " format "\n" , ## arg)

#define info(format, arg...) \
   printk(KERN_INFO "BINFMT_XFLAT: " format "\n" , ## arg)

#define warn(format, arg...) \
   printk(KERN_WARNING "BINFMT_XFLAT: " format "\n" , ## arg)

#ifndef NULL
#define NULL ((void *) 0)
#endif

/***********************************************************************
 * Private Types
 ***********************************************************************/

struct real_file_handle_s
{
  /* For the main program, file_inode is passed to do_xflat_load_binary()
   * as bprm->inode; for the loader, file_inode is created by
   * open_namei().
   */

  struct inode *file_inode;

  /* For both the main program and loader, the file stream is obtained
   * by indexing into current->files->fd[] using the file descriptor
   */

  struct file  *file_stream;

  /* This is the saved file descriptor */

  int           fd;
};
typedef struct real_file_handle_s real_file_handle_t;

/***********************************************************************
 * Private Function Prototypes
 ***********************************************************************/

static void *xflat_map(bin_handle_t handle,
		       unsigned long nbytes);
static void xflat_unmap(void *address,
			unsigned long nbytes);
static void *xflat_alloc(unsigned long nbytes);
static file_handle_t xflat_ldr_open(bin_handle_t handle,
				    const char *filename);
static file_handle_t xflat_prog_open(bin_handle_t handle,
				     const char *filename);
static int xflat_read(bin_handle_t bin_handle,
		      file_handle_t file_handle,
		      char *dest, unsigned long nbytes,
		      unsigned long fpos);
static void xflat_close(file_handle_t handle);
static void xflat_ldr_close(file_handle_t handle);

static int xflat_load_binary(struct linux_binprm *, struct pt_regs * regs);

/***********************************************************************
 * Public Function Prototypes
 ***********************************************************************/

/***********************************************************************
 * Private Data
 ***********************************************************************/

static struct linux_binfmt xflat_format =
{
#ifndef MODULE
  NULL, NULL, xflat_load_binary, NULL, NULL
#else
  NULL, &mod_use_count_, xflat_load_binary, NULL, NULL
#endif
};

static const struct xflat_vtbl prog_vtbl =
{
  xflat_map,
  xflat_unmap, 
  xflat_alloc,
  xflat_unmap,
  xflat_prog_open,
  xflat_read,
  xflat_close
};

static const struct xflat_vtbl ldr_vtbl =
{
  xflat_map,
  xflat_unmap, 
  xflat_alloc,
  xflat_unmap,
  xflat_ldr_open,
  xflat_read,
  xflat_ldr_close
};

/***********************************************************************
 * Private Functions
 ***********************************************************************/

/***********************************************************************
 * These are the functions that are exported to the xflatlib
 ***********************************************************************/

static void *
xflat_map(file_handle_t file_handle, unsigned long nbytes)
{
  /* In all cases, the file_handle is an initialized instance
   * of a real_file_handle_t
   */

  real_file_handle_t *real_file = (real_file_handle_t*)file_handle;
  return (void*)do_mmap(real_file->file_stream, 0, nbytes,
			PROT_READ|PROT_EXEC, 0, 0);
}

static void
xflat_unmap(void *address, unsigned long nbytes)
{
  do_munmap((unsigned long)address, 0);
}

static void *
xflat_alloc(unsigned long nbytes)
{
  return
    (void*)do_mmap(0, 0, nbytes, PROT_READ | PROT_EXEC | PROT_WRITE, 0, 0);
}

static file_handle_t
xflat_ldr_open(bin_handle_t bin_handle, const char *filename)
{
  real_file_handle_t *real_file;
  int old_fs;
  int retval;

  /* In this case, bin_handle is an UNINITIALIZED instance of
   * a real_file_handle_t.
   */

  real_file = (real_file_handle_t*)bin_handle;

  old_fs = get_fs(); /* This could probably be optimized */
  set_fs(get_ds());
  retval = open_namei(filename, 0, 0, &real_file->file_inode, NULL);
  set_fs(old_fs);

  if (retval >= 0)
    {
      real_file->fd = open_inode(real_file->file_inode, O_RDONLY);
      real_file->file_stream = current->files->fd[real_file->fd];
      return (file_handle_t)real_file;
    }
  return (file_handle_t)NULL;
}

static file_handle_t
xflat_prog_open(bin_handle_t bin_handle, const char *filename)
{
  /* In this case, bin_handle is an INITIALIZED instance of
   * a real_file_handle_t. This function should never be
   * called!
   */

  return (file_handle_t)bin_handle;
}

static int
xflat_read(bin_handle_t bin_handle, file_handle_t file_handle,
	   char *dest, unsigned long nbytes,
	   unsigned long fpos)
{
  real_file_handle_t *real_file;
  int nbytesread;
  int retval = -ENOEXEC;

  dbg("Read %ld bytes from offset %ld", nbytes, fpos);

  /* In all cases, the file_handle is an INITIALIZED instance
   * of a real_file_handle_t.
   */

  real_file = (real_file_handle_t*)file_handle;

  /* The return value from read_exec is the number of bytes
   * read.
   */

  nbytesread = read_exec(real_file->file_inode, fpos, dest, nbytes, 0);
  if ((unsigned long)nbytesread == nbytes) 
    {
      /* If the number of bytes read is the number requested,
       * then the read succeeded.
       */

      retval = 0;
    }
  else
    {
      /* We will not claim an error an any event because
       * we don't anything about this file.  This will just end
       * up being another "normal" -ENOEXEC.
       */

      dbg("Read of %ld bytes failed, error=%d", nbytes, nbytesread);
    }
  return retval;
}

static void
xflat_close(file_handle_t handle)
{
  /* We need to keep the binary file open.  It will be close
   * by the exec infrastructure.
   */
}

static void
xflat_ldr_close(file_handle_t handle)
{
  real_file_handle_t *real_file;

  /* But, after mapping loader, it will be necessary to close
   * the loader file. In this case, bin_handle is an INITIALIZED
   * instance of a real_file_handle_t.
   */

  real_file = (real_file_handle_t*)handle;
  sys_close(real_file->fd);
}

/***********************************************************************
 * Some Debug Functions
 ***********************************************************************/
 
/***********************************************************************
 * xflat_dump_memory
 ***********************************************************************/

#ifdef CONFIG_XFLAT_DEBUG
static void
xflat_dump_memory(unsigned long begin_stack, unsigned long end_stack)
{
  unsigned long *p = (unsigned long*)(begin_stack & ~3);
  unsigned long *pend = (unsigned long*)((end_stack+3) & ~3);

  dbg("  ADDRESS    VALUE");
  for (; p != pend; p++)
    {
      dbg("  0x%p 0x%08lx", p, *p);
    }
}
#else /* CONFIG_XFLAT_DEBUG */
# define xflat_dump_memory(b,e)
#endif /* CONFIG_XFLAT_DEBUG */

/***********************************************************************
 * xflat_dump_load_info
 ***********************************************************************/

#ifdef CONFIG_XFLAT_DEBUG
static void
xflat_dump_load_info(struct xflat_load_info *load_info)
{
  unsigned long dspace_size =
    XFLAT_DATA_OFFSET +
    load_info->data_size +
    load_info->bss_size +
    load_info->stack_size;

  dbg("LOAD_INFO:");
  dbg("  ISPACE:");
  dbg("    ispace       = 0x%08lx", load_info->ispace);
  dbg("    entry_offset = 0x%08lx", load_info->entry_offset);
  dbg("    ispace_size  = 0x%08lx", load_info->ispace_size);

  dbg("  DSPACE:");
  dbg("    dspace       = 0x%08lx", load_info->dspace);
  dbg("      (ldso)     = 0x%08x",  XFLAT_DATA_OFFSET);
  dbg("    data_size    = 0x%08lx", load_info->data_size);
  dbg("    bss_size     = 0x%08lx", load_info->bss_size);
  dbg("      (pad)      = 0x%08lx", load_info->dspace_size - dspace_size);
  dbg("    stack_size   = 0x%08lx", load_info->stack_size);
  dbg("    dspace_size  = 0x%08lx", load_info->dspace_size);

  dbg("  ARGUMENTS:");
  dbg("    arg_start    = 0x%08lx", load_info->arg_start);
  dbg("    env_start    = 0x%08lx", load_info->env_start);
  dbg("    env_end      = 0x%08lx", load_info->env_end);

  dbg("  RELOCS:");
  dbg("    reloc_start  = 0x%08lx", load_info->reloc_start);
  dbg("    reloc_count  = 0x%08lx", load_info->reloc_count);

  dbg("  HANDLES:");
  dbg("    bin_handle   = 0x%p",    load_info->bin_handle);
  dbg("    file_handle  = 0x%p",    load_info->file_handle);

  dbg("  xFLT HEADER:");
  dbg("    header       = 0x%p",    load_info->header);

  dbg("  ALLOCATIONS:");
  dbg("    alloc_start  = 0x%08lx", load_info->alloc_start);
  dbg("    alloc_size   = 0x%08lx", load_info->alloc_size);
}
#else /* CONFIG_XFLAT_DEBUG */
# define xflat_dump_load_info(i)
#endif /* CONFIG_XFLAT_DEBUG */

/***********************************************************************
 * putstring
 ***********************************************************************/

static unsigned long putstring(unsigned long p, char * string)
{
  unsigned long l = strlen(string)+1;

  dbg("put_string \"%s\"", string);
  p -= l;
  memcpy((void*)p, string, l);
  return p;
}

/***********************************************************************
 * putstringarray
 ***********************************************************************/

static unsigned long putstringarray(unsigned long p, int count, char ** array)
{
  dbg("putstringarray(%d)", count);
  while(count)
    {
      p = putstring(p, array[--count]);
      dbg("p2 = 0x%lx", p);
    }
  return p;
}

/***********************************************************************
 * stringarraylen
 ***********************************************************************/

static unsigned long stringarraylen(int count, char ** array)
{
  int l = 4;
  while (count)
    {
      l += strlen(array[--count]);
      l++;
      l+=4;
    }
  return l;
}

/***********************************************************************
 * xflat_create_tables
 ***********************************************************************/

/* xflat_create_tables() parses the env- and arg-strings in new user
 * memory and creates the pointer tables from them, and puts their
 * addresses on the "stack", returning the new stack pointer value.
 */

static inline unsigned long
xflat_create_tables(struct xflat_load_info *prog_info,
		   struct xflat_load_info *ldr_info,
		   unsigned long pp,
		   struct linux_binprm *bprm)
{
  int retval;

  retval = xflat_init_stack(prog_info, ldr_info,
			    bprm->argc, bprm->envc, (char*)pp);

  current->mm->arg_start = prog_info->arg_start;
  current->mm->arg_end   = prog_info->env_start;
  current->mm->env_start = prog_info->env_start;
  current->mm->env_end   = prog_info->env_end;

  return retval;
}

/***********************************************************************
 * do_xflat_load_binary
 ***********************************************************************/

/* These are the functions used to load xflat style executables and shared
 * libraries.  There is no binary dependent code anywhere else.
 */

inline int
do_xflat_load_binary(struct linux_binprm * bprm, struct pt_regs * regs)
{
  struct xflat_hdr      *prog_header;
  struct xflat_hdr       ldr_header;
  struct xflat_load_info prog_info;
  struct xflat_load_info ldr_info;
  real_file_handle_t     ldr_handle;
  real_file_handle_t     prog_handle;
  unsigned long          stack_end;
  unsigned long          entry_point;
  unsigned long          pic_base;
  unsigned long          result;
  unsigned long          p = bprm->p;
  int                    relocs = 0;

  current->personality = PER_LINUX;
	
  dbg("Checking file");

  if (flush_old_exec(bprm))
    {
      err("Unable to flush");
      return -ENOMEM;
    }

  /* Initialize the xflat library to load the program binary. */

  prog_header             = (struct xflat_hdr*)bprm->buf;
  prog_handle.file_inode  = bprm->inode;
  prog_handle.fd          = open_inode(bprm->inode, O_RDONLY);
  prog_handle.file_stream = current->files->fd[prog_handle.fd];

  result = xflat_init((bin_handle_t)&prog_handle, (file_handle_t)&prog_handle,
		      prog_header, &prog_vtbl, &prog_info);
  xflat_dump_load_info(&prog_info);
  if (result != 0)
    {
      /* No error... This is the normal situation when
       * the file is not recognized.
       */

      dbg("Failed to initialize xflat library");
      return result;
    }

  relocs                = ntohl(prog_header->reloc_count);

  /* Make room on stack for arguments & environment */

  prog_info.stack_size += strlen(bprm->filename) + 1;
  prog_info.stack_size += stringarraylen(bprm->envc, bprm->envp);
  prog_info.stack_size += stringarraylen(bprm->argc, bprm->argv);

  /* Load the program binary */

  result = xflat_load(&prog_info);
  xflat_dump_load_info(&prog_info);
  if (result != 0)
    {
      dbg("Failed to load xFLT program binary");
      xflat_uninit(&prog_info);
      return result;
    }

  /* Initialize the xflat library to load the program binary. */

  result = xflat_init_interpreter(&ldr_handle, &prog_info, &ldr_info,
				  &ldr_header, &ldr_vtbl);
  xflat_dump_load_info(&ldr_info);
  if (result != 0)
    {
      dbg("Failed to initialize for load of xFLT interpreter, result=%ld",
	  result);
      xflat_unload(&prog_info);
      xflat_uninit(&prog_info);
      return result;
    }

  /* Make sure that there is not stack associated with the interpreter. */

  ldr_info.stack_size = 0;

  /* Load the intrepeter binary */

  result = xflat_load(&ldr_info);
  xflat_dump_load_info(&ldr_info);
  if (result != 0)
    {
      dbg("Failed to load xFLT interpreter, result=%ld", result);
      xflat_unload(&prog_info);
      xflat_uninit(&prog_info);
      xflat_uninit(&ldr_info);
      return result;
    }
  
  /* We can now close the loader file.  After it is mapped to
   * memory, we no longer need its file descriptor.
   */

  xflat_ldr_close(ldr_info.file_handle);

  /* Describe the process to the system */

  current->mm->start_code = ldr_info.ispace;
  current->mm->end_code   = ldr_info.ispace + ldr_info.ispace_size;
  current->mm->start_data = ldr_info.dspace;
  current->mm->end_data   = ldr_info.dspace + ldr_info.data_size;
#ifdef NO_MM
  current->mm->brk        = prog_info.alloc_start +
                            ((prog_info.alloc_size + 3) & ~3);
  current->mm->start_brk  = current->mm->brk;
#ifdef MODULE
  current->mm->end_brk    = prog_info.alloc_start + prog_info.alloc_size;
#else
  current->mm->end_brk    = prog_info.alloc_start +
                            ksize((void *) prog_info.alloc_start);
#endif
#else
  current->mm->brk        = prog_info.dspace + prog_info.data_size +
                            prog_info.bss_size;
#endif

  /* Not sure if we should on do this for XIP ?
   * the coldfire code never did this and was plenty stable
   */

  if (bprm->inode->i_sb->s_flags & MS_SYNCHRONOUS)
    {
      dbg("Retaining inode");
      current->mm->executable = bprm->inode;
      bprm->inode->i_count++;
    }
  else
    current->mm->executable = 0;

  dbg("Load %s: TEXT=%x-%x DATA=%x-%x BSS=%x-%x",
      bprm->argv[0],
      (int) current->mm->start_code, (int) current->mm->end_code,
      (int) current->mm->start_data, (int) current->mm->end_data,
      (int) current->mm->end_data, (int) current->mm->brk);

  current->mm->rss = 0;
  current->suid    = current->euid = current->fsuid = bprm->e_uid;
  current->sgid    = current->egid = current->fsgid = bprm->e_gid;
  current->flags  &= ~PF_FORKNOEXEC;
        
  if (current->exec_domain && current->exec_domain->use_count)
    {
      (*current->exec_domain->use_count)--;
    }

  if (current->binfmt && current->binfmt->use_count)
    {
      (*current->binfmt->use_count)--;
    }

  current->exec_domain = lookup_exec_domain(current->personality);
  current->binfmt      = &xflat_format;

  if (current->exec_domain && current->exec_domain->use_count)
    {
      (*current->exec_domain->use_count)++;
    }

  if (current->binfmt && current->binfmt->use_count)
    {
      (*current->binfmt->use_count)++;
    }

#ifndef NO_MM
  /*set_brk(current->mm->start_brk, current->mm->brk);*/
#endif

  /* Set up the initial stack contents */

  stack_end = ((prog_info.dspace   + prog_info.data_size  +
		prog_info.bss_size + prog_info.stack_size + 0x3) & ~0x3) - 4;
	
  dbg("stack_end:   0x%lx", stack_end);
  p = putstringarray(stack_end, 1, &bprm->filename);

  dbg("filename:    0x%lx", p);
  p = putstringarray(p, bprm->envc, bprm->envp);

  dbg("envp strtab: 0x%lx", p);
  p = putstringarray(p, bprm->argc, bprm->argv);

  dbg("argv strtab: 0x%lx", p);
  p = xflat_create_tables(&prog_info, &ldr_info, p, bprm);

  dbg("stack ptr:   0x%lx", p);
  dbg("arg_start:   0x%lx", current->mm->arg_start);
  dbg("arg_end:     0x%lx", current->mm->arg_end);
  dbg("env_start:   0x%lx", current->mm->env_start);
  dbg("env_end:     0x%lx", current->mm->env_end);

  current->mm->start_stack = p;

  dbg("INITIAL STACK:");
  xflat_dump_memory(p, stack_end);

  /* And set up to exec the interpreter */

  entry_point = ldr_info.ispace + ldr_info.entry_offset;
  pic_base    = ldr_info.dspace + XFLAT_DATA_OFFSET;

  dbg("entry_point: 0x%lx", entry_point);
  dbg("pic_base:    0x%lx", pic_base);

  flush_cache_mm(current->mm);
  start_thread(regs, entry_point, pic_base, p);
	
  dbg("LDR ENTRY CODE:");
  xflat_dump_memory(entry_point, entry_point + 16*sizeof(unsigned long));

  if (current->flags & PF_PTRACED)
    send_sig(SIGTRAP, current, 0);
  return 0;
}

/***********************************************************************
 * xflat_load_binary
 ***********************************************************************/

static int
xflat_load_binary(struct linux_binprm * bprm, struct pt_regs * regs)
{
  int retval;

  MOD_INC_USE_COUNT;
  retval = do_xflat_load_binary(bprm, regs);
  MOD_DEC_USE_COUNT;
  return retval;
}

/***********************************************************************
 * Public Functions
 ***********************************************************************/
 
/***********************************************************************
 * Module Interfaces
 ***********************************************************************/
 
int xflat_init_binfmt(void)
{
  int retval;

  /* Say hello */

  info("Extended flat loader");

  /* Register ourselves as a binfmt loader */

  retval = register_binfmt(&xflat_format);
  if (retval != 0)
    {
      err("Failed to register binfmt (%d)", retval);
    }
  return retval;
}

#ifdef MODULE
int init_module(void)
{
  int retval;
  retval = xflat_init_binfmt();
  return retval;
}

void cleanup_module( void)
{
  unregister_binfmt(&xflat_format);
}
#endif

