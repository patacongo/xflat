#!/bin/sh
########################################################################
# xflat/kernel/patch_kernel.sh
# Script to patch XFLAT into a Linux kernel
#
# Copyright (c) 2006, Cadenux, LLC.  All rights reserved.
# Copyright (c) 2006, Gregory Nutt.  All rights reserved.
# Author: Gregory Nutt <spudmonkey@racsa.co.cr>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name Cadenux nor the names of its contributors may be
#    used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
########################################################################

function show_usage ()
{
    echo ${1}
    echo "USAGE: ${progname} [options] <kernel-path>"
    echo ""
    echo "Recognized options:"
    echo "  --debug.  Enable script debug"
    echo "  --kversion NN.NN.NN Used kernel version=NN.NN.NN"
    exit 1
}

function sanity_check ()
{
    if [ ! -d patches ]; then
	show_usage "Directory patches does not exist.  CD to kernel dir?"
    fi
}

function verify_kdir ()
{
    if [ -z ${kdir} ]; then
	show_usage "No kernel directory was provided on the command line"
    fi
    if [ ! -d ${kdir} ]; then
	show_usage "Kernel directory ${kdir} does not exist"
    fi
    if [ ! -f ${kdir}/Makefile ]; then
	show_usage "Kernel makefile ${kdir}/Makefile does not exist"
    fi
}

function get_kversion ()
{
    version=`head -1 ${kdir}/Makefile | cut -d' ' -f 3`
    patchlevel=`head -2 ${kdir}/Makefile | grep PATCHLEVEL | cut -d' ' -f 3`
    sublevel=`head -3 ${kdir}/Makefile | grep SUBLEVEL | cut -d' ' -f 3`

    if [ -z ${kversion} ]; then
	kversion=${version}.${patchlevel}.${sublevel}
    fi
    kshort=linux-${version}.${patchlevel}

    echo "Using kernel version: ${kversion} (${kshort})"
}

function apply_patch ()
{
    if [ -f ${1} ]; then
	case "${1}" in
	    *.gz)
		type="gzip";
		uncomp="gunzip -dc";
		;; 
	    *.bz)
		type="bzip";
		uncomp="bunzip -dc";
		;; 
	    *.bz2)
		type="bzip2";
		uncomp="bunzip2 -dc";
		;; 
	    *.zip)
		type="zip";
		uncomp="unzip -d";
		;; 
	    *.Z)
		type="compress";
		uncomp="uncompress -c";
		;; 
	    *)
		type="plaintext";
		uncomp="cat";
		;; 
	esac

	echo "Applying ${i} using ${type}: " 
	${uncomp} ${1} | patch -p1 -E -d ${kdir} 
	if [ $? != 0 ] ; then
	    echo "Patch failed!  Please fix ${1}!"
	    exit 1
	fi
    else
	echo "Warning: patch ${1} does not exist"
    fi
}

function copy_binfmt_files ()
{
    echo "Copying binfmt files"
    if [ ! -d ${kdir}/fs/xflat ]; then
	echo "Creating directory ${kdir}/fs/xflat"
	mkdir -p ${kdir}/fs/xflat
    fi
    cp -f binfmt/${kshort}/binfmt_main.c ${kdir}/fs/xflat/.
    cp -f binfmt/${kshort}/xflatlib_binfmt.h ${kdir}/fs/xflat/.
    cp -f ../xflatlib/xflatlib_interp.c ${kdir}/fs/xflat/.
    cp -f ../xflatlib/xflatlib_load.c ${kdir}/fs/xflat/.
    cp -f ../xflatlib/xflatlib_stack.c ${kdir}/fs/xflat/.
    cp -f ../xflatlib/xflatlib_verify.c ${kdir}/fs/xflat/.
    cp -f ../sys-include/xflat.h ${kdir}/fs/xflat/.
    cp -f ../sys-include/xflatlib.h ${kdir}/fs/xflat/.
}

function apply_binfmt_patch ()
{
    # Verify that there is a binfmt loader for this kernel version

    if [ ! -d binfmt/${kshort} ]; then
	echo "There is not binfmt loader for this kernel version"
	echo "(it would be in xflat/kernel/binfmt/linux-${kshort})"
    else
	patchfile=patches/linux-${kversion}-binfmt-xflat.patch
	if [ -f patches/linux-${kversion}-binfmt-xflat.patch ]; then
	    apply_patch ${patchfile}
	    copy_binfmt_files
	else
	    echo "Warning: binfmt patch xflat/kernel/${patchfile} does not exist"
	    echo "It will only be possible to use XFLAT as a kernel module"
	fi
    fi
}

function apply_signal_patch ()
{
    patchfile=patches/linux-${kversion}-xflat-signal.patch
    if [ -f patches/linux-${kversion}-xflat-signal.patch ]; then
	apply_patch ${patchfile}
    else
	echo "Warning: patch ${patchfile} does not exist"
	echo "The kernel may be unstable with XFLAT binaries"
    fi
}

function cleanup ()
{
    # Check for rejects...
    if [ "`find ${kdir}/ '(' -name '*.rej' -o -name '.*.rej' ')' -print`" ] ; then
	echo "FAILED -- Reject files found."
	exit 1
    fi

    # Remove backup files
    find ${kdir}/ '(' -name '*.orig' -o -name '.*.orig' ')' -exec rm -f {} \;
}

# Parse command arguments

progname=${0}
kdir=
kversion=

while [ ! -z "${1}" ]; do
    case "${1}" in
        --debug )
            set -x
            ;;
        --kversion )
	    kversion=${2}
            shift
            ;;
        *)
	    kdir=${1}
            ;;
    esac
    shift
done

sanity_check
verify_kdir
get_kversion
apply_binfmt_patch
apply_signal_patch
cleanup
