static int myfunc(int i)
{ return i+1; }

extern int dofunc(int (*func)(int));

int main(void)
{
  return dofunc(myfunc);
}
