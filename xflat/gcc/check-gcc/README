README
^^^^^^

This directory contains a simple test to see if you GCC
compiler is XFLAT ready.  This directory contains the
following files:

test-case.c:
  This is a very simple test file.  It simply calls some
  external function, dofunc(), and passes a pointer to
  a provide function, myfunc():

    return dofunc(myfunc);

Makefile:
  My be used to compile test-case.c.  This Makefile will
  probably require customization for your environment.
  When it successfully compiles test-case.c, it will
  generate a test-case.s.  Compare test-case.s to the
  following files to see if your compiler is XFLAT-ready:

test-case.s.BAD (gcc-3.2.3, no patches)
  If your compiler is NOT XFLAT ready, the test will
  generate an assembly file like this one:

    ...
    ldr   r0, .L3        <-- Loads the GOT relative address
                             of myfunc() into r0
    add   r0, sl, r0     <-- Adds the PIC address bad to
                             the GOT relative address and
                             produces garbage
  .L3:
   .word  myfunc(GOTOFF) <-- The GOT relative address of
                             myfunc()

test-case.s.GOOD-1 (gcc-3.2.3, testfix patch)
test-case.s.GOOD-2 (gcc-3.2.3, testfix patch + disable-got patch)
  If your compiler is XFLAT ready, then it should generate
  something like the contents of this file:

    ...
    .section .rodata....
    .align   2
  .LC0:
    .word    myfunc      <-- Absolute address of myfunc in
                             the data section.  This will be
                             relocated to the correct address
                             at run time.
    ...
    ldr     r3, .L3      <-- Loads the GOT offset address of .LCO
    ldr     r0, [sl, r3] <-- Correctly adjusts the GOT offset and
                             loads the correct address of myfunc()
    ...
  .L3:
    .word   .LC0(GOTOFF)
    ...

  With -membedded-pic, the output is the same except that the GOTOFF
  label is removed from .LC0.
