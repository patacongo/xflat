########################################################################
# xflat/tests/hello++/Makefile.flat
#
# Copyright (c) 2004, 2006, Cadenux, LLC.  All rights reserved.
# Copyright (c) 2004, 2006, Gregory Nutt.  All rights reserved.
# Author: Gregory Nutt <spudmonkey@racsa.co.cr>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name Cadenux nor the names of its contributors may be
#    used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
########################################################################

WD			= ${shell pwd}
TOPDIR			= $(WD)/../..

include	$(TOPDIR)/.config
include	$(TOPDIR)/Make.defs

BIN1			= fhello++1
BIN2			= fhello++2
BIN3			= fhello++3
#BIN4			= fhello++4

SRCS1			= hello++1.c
OBJS1			= $(SRCS1:.c=.o1)

SRCS2			= hello++2.c
OBJS2			= $(SRCS2:.c=.o1)

SRCS3			= hello++3.c
OBJS3			= $(SRCS3:.c=.o1)

#SRCS4			= hello++4.c
#OBJS4			= $(SRCS4:.c=.o1)

CXXOBJS			= $(OBJS1) $(OBJS2) $(OBJS3) # $(OBJS4)

# Unless you need it, turn off run-time type information by compiling
# with "gcc -fno-rtti ...".

CXXFLAGS		+= -fno-rtti -D_STLP_NO_WCHAR_T

all: $(BIN1) $(BIN2) $(BIN3) # $(BIN4)

$(CXXOBJS): %.o1: %.cpp
	arm-uclinux-g++ -c $(CXXFLAGS) $< -o $@

# BIN1 and BIN2 link just like FLAT C code because they contain no
# static constructors.  BIN1 is equivalent to a C hello world;
# BIN2 contains a class that implements hello world, but it is
# not statically initialized.

$(BIN1).fr1: $(OBJS1)
	arm-uclinux-gcc -o $@ -Wl,-r -Wl,-d -Wl,-warn-common \
	-L$(ARCHLIBDIR) -nostartfiles -Wl,-T,$(ARCHFLATSCRIPT) \
	$(ARCHFLATSTARTUP) $^

$(BIN1): $(BIN1).fr1
	arm-uclinux-ld -elf2flt="-s 16384" -o $@ $^
	rm -f $^

$(BIN2).fr1: $(OBJS2)
	arm-uclinux-gcc -o $@ -Wl,-r -Wl,-d -Wl,-warn-common \
	-L$(ARCHLIBDIR) -nostartfiles -Wl,-T,$(ARCHFLATSCRIPT) \
	$(ARCHFLATSTARTUP) $^

$(BIN2): $(BIN2).fr1
	arm-uclinux-ld -elf2flt="-s 16384" -o $@ $^
	rm -f $^

# BIN3 and BIN4 require that we do things the long way.
#
# BIN3 is equivalent to BIN2 except that is uses static initializers

$(BIN3).fr1: $(OBJS3)
	arm-uclinux-gcc -o $@ -Wl,-r -Wl,-d -Wl,-warn-common \
	-L$(ARCHLIBDIR) -nostartfiles -Wl,-T,$(ARCHFLATSCRIPT) \
	$(ARCHFLATSTARTUP1) $^ $(ARCHFLATCTORS)

$(BIN3): $(BIN3).fr1
	arm-uclinux-ld -elf2flt="-s 16384" -o $@ $^
	rm -f $^

# BIN4 is similar to BIN3 except that it uses the streams code from libstdc++
#
# NOTE:  libstdc++ is not available for FLAT as of this writing
#
#$(BIN3).fr1: $(OBJS3)
#	arm-uclinux-gcc -o $@ -Wl,-r -Wl,-d -Wl,-warn-common \
#	-L$(ARCHLIBDIR) -nostartfiles -Wl,-T,$(ARCHFLATSCRIPT) \
#	$(ARCHFLATSTARTUP1) $^ $(ARCHFLATCTORS)
#
#$(BIN3): $(BIN3).fr1
#	arm-uclinux-ld -elf2flt="-s 16384" -o $@ $^
#	rm -f $^

clean: 
	rm -f $(BIN1) $(BIN2) $(BIN3) $(BIN4) *.o1 *.fr1 *~ core

install: $(BIN1) $(BIN2) $(BIN3) # $(BIN4)
	install -D $(BIN1) $(USER_TESTS_DIR)/$(BIN1)
	install -D $(BIN2) $(USER_TESTS_DIR)/$(BIN2)
	install -D $(BIN3) $(USER_TESTS_DIR)/$(BIN3)
#	install -D $(BIN4) $(USER_TESTS_DIR)/$(BIN4)
