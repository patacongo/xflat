########################################################################
# xflat/tests/stdcpp/Makefile
#
# Copyright (c) 2004, 2006, Cadenux, LLC.  All rights reserved.
# Copyright (c) 2004, 2006, Gregory Nutt.  All rights reserved.
# Author: Gregory Nutt <spudmonkey@racsa.co.cr>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name Cadenux nor the names of its contributors may be
#    used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
########################################################################

WD			= ${shell pwd}
TOPDIR			= $(WD)/../..

include $(TOPDIR)/.config
include	$(TOPDIR)/Make.defs

BIN1			= libstdc++.a
BIN2			= libstdc++.so

SRCS			= stdcpp-0001.cpp
OBJS			= $(SRCS:.cpp=.o)

# Unless you need it, turn off run-time type information by compiling
# with "gcc -fno-rtti ...".

ARCHCXXFLAGS		= $(ARCHWARNINGS) $(ARCHOPTIMIZATION) \
			  $(ARCHCPUFLAGS) -fno-rtti -fno-exceptions $(ARCHPICFLAGS)

all: $(BIN1) $(BIN2)

$(OBJS): %.o: %.cpp
	$(XFLATCXX) -c $(ARCHCXXFLAGS) $< -o $@

$(BIN1): $(OBJS)
	$(ARCHAR) -rc $@ $^

$(BIN2): $(OBJS)
	$(XFLATLD) -shared -o $@ $^

clean: 
	rm -f $(BIN1) $(BIN2) *.debug *.o *.so *.a *~ core

user_install:

root_install: $(BIN1) $(BIN2)
	install -D $(BIN1) $(USER_LIB_DIR)/$(BIN1)
	install -D $(BIN2) $(ARCH_LIB_DIR)/$(BIN2)
	ln -sf $(BIN2) $(ARCH_LIB_DIR)/$(BIN2).0
