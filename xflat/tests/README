README
^^^^^^
Contents
^^^^^^^^
  - Building and Installing XFLAT Tests
  - Test Descriptions
  - Test Status

Building and Installing XFLAT Tests
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To build all tests

  cd tests
  make build

To install all tests (assuming that you have write privileges
in the buildroot/build/root/bin directory).

  cd tests
  make user_install

Test executables will be installed in /bin/xflat in the target
root filesystem.

Test Descriptions
^^^^^^^^^^^^^^^^^

This directory contains a set of tests for the Shared library facility:

1. thunk

   This test builds a *FLAT* executable but forces an XFLAT-style
   thunk layer between two flat functions.  This allows you to use
   the the flat tool chain to debug the xflat thunk assembly
   language.

   Since this test depends on the presence of FLAT, it is not
   automatically built by the tests/Makefile.

2. hello-nolibc

   This test generates its output via kernel syscalls.  As a result,
   it can be used before you have a working libc-xflat.so.
   This test will verify that:

   o binfmt_xflat and ld-xflat.so can load and start an xflat program.

   o That the stack received by the application is valid (argc, argv,
     envp), and

   o It attempts one very highly instrumented shared library call
     (into a function exported by ld-xflat.so).  The call, however,
     is redirected to other logic that dumps all of the registers
     and provides diagnostic information.

   Test execution:

     cd /bin/xflat
     ./hello-nolibc

3. hello

   The standard hello world program that talks to the console via
   libc-xflat.so using printf statements.  This demonstrates:

   o Basic shared library operations, and

   o That the stack received by the application is valid (argc, argv,
     envp).

   Test execution:

     cd /bin/xflat
     ./hello

4. hello++

   This is an excessively complex version of "Hello, World" design to
   illustrate some basic properties of C++ using XFLAT (and FLAT)

   - Building a C++ program
   - Static constructor and destructors

   NOTE:  At present, C++ support (and this example) are "under
   construction."  Future extensions to this example will demonstrate:

   - Stream IO

   However, stream IO is not supported as of this writing.

5. errno

   Provides a tests of access to global variables exported from the
   libc shared library:  stdout, stderr, errno.

   Test execution:

     cd /bin/xflat
     ./errno

6. fcnptr

   Provides of test of the logic to pass function pointers between
   modules and to perform callbacks using these function pointers from
   within a shared library.

   Test execution:

     cd /bin/xflat
     ./fcnptr_main

7. pthread

   This test performs instrumented pthread creation

   Test execution:

     cd /bin/xflat
     ./pthread

8. dlfcn

   Provides an example of how to load and access shared libraries
   from user code using the interfaces defined in dlfcn.h:  dlopen()
   and dlsym().

   Test execution:

     cd /bin/xflat
     ./dlfcn_main

9. resolver

   This example is very similar to dlfcn.  I also loads a shared
   library and makes a call into a function in the shared library.
   However, it relies on the dynamic symbol binding using the XFLAT
   resolver.

   Test execution:

     cd /bin/xflat
     ./resolver_main

10. execv

   An example of the use of vfork and execv in the uClinux environment.

   Test execution:

     cd /bin/xflat
     ./parent

11. pthread

   A simple example of the use Linux 2.x capabilities to create multi-
   threaded processes using XFLAT shared libraries.

   Test execution:

     cd /bin/xflat
     ./pthread

12. signal

   A simple example of the use of Linux signal handlers.

   Test execution:

     cd /bin/xflat
     ./signal

13. longjmp

    One limitation of the XFLAT libc shared library is that is cannot
    export the setjmp and longjmp functions.  The reason for this
    limitation has to do with the fact that setjmp must be called at
    point where you wish to take the stack snapshot.  Since XFLAT
    uses a "thunk" function to get to the actual call, it is not
    possible to directly call setjmp from the application code with
    violating the basic assumption of the longjmp model.

    A special library, liblongjmp.a, has been provided with the BSP
    to support the use of longjmp and setjmp.  This example in this
    directory illustrates how to use liblongjmp.a

   Test execution:

     cd /bin/xflat
     ./longjmp

33. mutex

    This is a pthread stress test.  It creates many threads and
    controls them with mutexes.

   Test execution:

     cd /bin/xflat
     ./mutex

34. stdcpp

    Minimal C++ library.  VERY tiny.  You must edit Make.config
    to build this library.

    This is not really a test.  It is just provided here for
    general interest.

Test Status
^^^^^^^^^^^
Test Summary as of May 4, 2006

Test Environment:

  Host Build Machine:      32-bit AMD Athlon 2.4GHz with SuSE 10.0
  Target Test Machine:     Big-endian ARM946 running linux-2.6.14
  Cross Build tools:       gcc-3.4.5 (config/rosedale)

Test Overview:

  Number of tests:     13  dlfcn, errno, execv, fcnptr, hello,
                           hello++, hello-nolibc, longjmp, mutex,
                           pthread, resolver, signal, thunk.
  Number that build:   12  dlfcn, errno, execv, fcnptr, hello,
                           hello++, hello-nolibc, longjmp,
                           mutex, pthread, resolver, signal.
  Number that pass:    10  dlfcn, errno, execv, fcnptr, hello,
                           hello++, hello-nolibc, longjmp, resolver,
                           pthread, signal
  Number that fail:     0
  Number uncertain      1  mutex

Test: tests/dlfcn
^^^^^^^^^^^^^^^^^
test name:    dlfcn
test result:  PASS
test output:

# ./dlfcn_main 
handle1=0x6197178c
handle2=0x6197178c
getglobal is at 0x60330514 in libdlfcn1.so
getglobal is at 0x60330308 in /bin/xflat/libdlfcn2.so
setglobal is at 0x603304f4 in libdlfcn1.so
setglobal is at 0x603302e8 in /bin/xflat/libdlfcn2.so
getglobal(1): Returning myglobal=1
getglobal(2): Returning myglobal=2
Initially, global1=1 global2=2
setglobal(1): Set myglobal=47
setglobal(2): Set myglobal=938
Set global1=47 global2=938
getglobal(1): Returning myglobal=47
getglobal(2): Returning myglobal=938
Current global1=47 global2=938
#

Test: tests/errno
^^^^^^^^^^^^^^^^^
test name:    errno
test result:  PASS
test output:

# ./errno
Hello, World on stdout
Hello, World on stderr
We failed to open "aflav-sautga-ay!" errno is 2

Test: tests/execv
^^^^^^^^^^^^^^^^^
test name:    execv
test result:  PASS
test output:

# ./parent 
Parent: Started, pid=604
Parent: Calling vfork()
Child: vfork-ed from 604, calling execv()
Parent: Child has been exec'ed, pid=605
Child: execv was successful!
Child: argc=2
Child: argv[0]="/bin/xflat/child"
Child: argv[1]="Hello from your parent!"
Child: Exit-ting with status=0
Parent: Child pid=605 has exit-ted with status=0
vfork() and execv() were successful
#

Test: tests/fcnptr
^^^^^^^^^^^^^^^^^^
test name:    fcnptr
test result:  PASS
test output:

# ./fcnptr_main 
main:  Calling do_callback with s1="Knock, knock, ..."
do_callback: Received s1="Knock, knock, ..."
             Calling back with s2="Who's there?
callback: s1="Knock, knock, ..."
          s2="Who's there?"
# 

Test: tests/hello
^^^^^^^^^^^^^^^^^
test name:    hello
test result:  PASS
test output:

# ./hello
Hello, world!
argc	= 1
argv	= 0x0x619fbe04
argv[0]	= (0x0x619fbe73) "./hello"
argv[1]	= 0x(nil)
envp	= 0x0x619fbe0c
envp[0]	= (0x0x619fbe7b) "DMALLOC_OPTIONS=debug=0x34f47d83,inter=100,log=logfile"
envp[1]	= (0x0x619fbeb2) "INPUTRC=/etc/inputrc"
envp[2]	= (0x0x619fbec7) "EDITOR=/bin/vi"
envp[3]	= (0x0x619fbed6) "PAGER=/bin/more "
envp[4]	= (0x0x619fbee7) "HISTFILESIZE=1000"
envp[5]	= (0x0x619fbef9) "HISTSIZE=1000"
envp[6]	= (0x0x619fbf07) "HOSTNAME=rosedale"
envp[7]	= (0x0x619fbf19) "LOGNAME=root"
envp[8]	= (0x0x619fbf26) "USER=root"
envp[9]	= (0x0x619fbf30) "SHELL=/bin/sh"
envp[10]	= (0x0x619fbf3e) "PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/bin/X11:/usr/local/bin"
envp[11]	= (0x0x619fbf7d) "debug=0"
envp[12]	= (0x0x619fbf85) "rdnetmask=ffffff00"
envp[13]	= (0x0x619fbf98) "rdmacaddr=00:60:CE:12:FF:41"
envp[14]	= (0x0x619fbfb4) "rdipaddr=192.168.0.150"
envp[15]	= (0x0x619fbfcb) "rgipaddr=192.168.0.101"
envp[16]	= (0x0x619fbfe2) "TERM=vt102"
envp[17]	= (0x0x619fbfed) "HOME=/"
envp[18]	= 0x(nil)
Goodbye, world!

Test: tests/hello++
^^^^^^^^^^^^^^^^^^^
test name:    hello++
test result:  Builds but tests involving static initializers fail
test output:

# ./hello++1
Hello, World!

# ./hello++2
main: Started.  Creating MyThingSayer
CThingSayer::CThingSayer: I am!
main: Created MyThingSayer=0x619e3d88
main: Calling MyThingSayer->Initialize
CThingSayer::Initialize: When told, I will say 'Hello, World!'
main: Calling MyThingSayer->SayThing
CThingSayer::SayThing: I am now saying 'Hello, World!'
main: Destroying MyThingSayer
CThingSayer::~CThingSayer: I cease to be
CThingSayer::~CThingSayer: I will never say 'Hello, World!' again
main: Returning

# ./hello++3
CThingSayer::CThingSayer: I am!
main: Started.  MyThingSayer should already exist
main: Calling MyThingSayer.Initialize
CThingSayer::Initialize: When told, I will say 'Hello, World!'
main: Calling MyThingSayer.SayThing
CThingSayer::SayThing: I am now saying 'Hello, World!'
main: Returning.  MyThingSayer should be destroyed
CThingSayer::~CThingSayer: I cease to be
CThingSayer::~CThingSayer: I will never say 'Hello, World!' again
#

Test: tests/hello-nolibc
^^^^^^^^^^^^^^^^^^^^^^^^
test name:    hello-nolibc
test result:  PASS (but with some runtime link failures. this is a problem
              in ld-xflat.so.1)
test output:

# ./hello-nolibc 
LDSO: WARN: Exporter of symbol "__udivsi3" needed by "ld-xflat.so.1" not found
LDSO: WARN: Exporter of symbol "__umodsi3" needed by "ld-xflat.so.1" not found
Hello, world!
My PIC base: 0x6193a004
__dyninfo0000
  function_name: 0x602c14d8
  function_address: 0x60246e8c
  data_segment: 0x61896004
__dynstack addr: 0x6193a004
__dynstack_ptr: 0x48656c6c
argc:        0x1
argv:        0x6193bdbc
  ./hello-nolibc
  NULL
envp:        0x6193bdc4
  LD_LIBRARY_PATH=/lib:/usr/lib:/usr/local/lib:/bin/xflat
  DMALLOC_OPTIONS=debug=0x34f47d83,inter=100,log=logfile
  INPUTRC=/etc/inputrc
  EDITOR=/bin/vi
  PAGER=/bin/more 
  HISTFILESIZE=1000
  HISTSIZE=1000
  HOSTNAME=rosedale
  LOGNAME=root
  USER=root
  SHELL=/bin/sh
  PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/bin/X11:/usr/local/bin
  debug=0
  rdnetmask=ffffff00
  rdmacaddr=00:60:CE:12:FF:41
  rdipaddr=192.168.0.150
  rgipaddr=192.168.0.101
  TERM=vt102
  HOME=/
  NULL
Calling dlmodule(NULL)
asm_regs:    0x6193a200
thunk_regs:  0x6193a200
  0x0
  0x6193a0d8
  0x17
  0x1
  0x6193a164
  0x6193a004
  0x6193a170
  0x60246e8c
  0x1
  0x0
  0x61896004
  0x0
  0x6193a200
  0x6193bda0
  0x602c1584
  0x0
__dynstack addr: 0x6193a004
  0x48656c6c
  0x6f2c2077
  0x6f726c64
  0x210a0000
  0x4d792050
__dynstack_ptr: 0x48656c6c
^^^^^^^^^^^^^^^^^^^^^^^^^^^
test name:    longjmp
test result:  PASS
test output:

# ./longjmp
main: Calling setjmp
main: setjmp returned 0
main: Normal setjmp return
main: Calling function with 47
function: received 47
function: Calling leaf() with 139
leaf: received 139
leaf: Calling longjmp() with 302
main: setjmp returned 302
main: SUCCESS: setjmp return from longjmp call
#

Test: tests/mutex
^^^^^^^^^^^^^^^^^
test name:    mutex
test result:  Probably sucessful but difficult to determine.  Control-C
              does not stop the test on my kernel.  It is likely that
              this is because that platform uses /dev/console and not a
              tty. for the console.
test output:

# ./mutex 
Starting threads
Press control-C to terminate the example

Test: tests/pthread
^^^^^^^^^^^^^^^^^^^
test name:    pthread
test result:  FAIL
test output:

#
# ./pthread 
PARENT: started
PARENT: initial frame state:
  00 00 00 00 00 00000000 00000000 00000000 00000000 00000000 
  01 00 00 00 00 00000000 00000000 00000000 00000000 00000000 
  02 00 00 00 00 00000000 00000000 00000000 00000000 00000000 
  03 00 00 00 00 00000000 00000000 00000000 00000000 00000000 
  04 00 00 00 00 00000000 00000000 00000000 00000000 00000000 
  05 00 00 00 00 00000000 00000000 00000000 00000000 00000000 
  NR 00 00 00 00 00000000 00000000 00000000 00000000 00000000 
PARENT: initializing attributes
PARENT: Before pthread_attr_init:
  00 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  01 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  02 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  03 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  04 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  05 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  NR 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
PARENT:After pthread_attr_init:
  00 00 00 ff ff 6024bdb0 61960004 6024830c 619714d8 61971534 
  01 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  02 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  03 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  04 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  05 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  NR 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
PARENT: calling pthread_start with arg=0x12345678
CHILD: started with arg=0x12345678
CHILD: Before pthread_create
  00 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  01 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  02 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  03 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  04 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  05 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  NR 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
CHILD: After pthread_create
  00 01 01 ff ff 6024bdb0 00000000 6024830c 602483b4 61971634 
  01 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  02 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  03 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  04 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  05 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  NR 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
CHILD: returning 0x87654321
PARENT: Before pthread_create:
  00 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  01 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  02 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  03 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  04 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  05 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  NR 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
PARENT: After pthread_create:
  00 00 00 ff ff 6024bdb0 00000000 6024830c 602483b4 619715e0 
  01 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  02 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  03 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  04 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  05 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  NR 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
PARENT: Before pthread_join:
  00 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  01 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  02 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  03 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  04 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  05 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  NR 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
PARENT: After pthread_join:
  00 00 00 ff ff 6024bdb0 00000000 6024830c 602483b4 61971634 
  01 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  02 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  03 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  04 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  05 00 ff ff ff ffffffff ffffffff ffffffff ffffffff ffffffff 
  NR 00 ff ff ff 602485ac 60248654 12345678 12345678 6197149c 
PARENT child exitted with 0x87654321
PARENT returning success
# 

Test: tests/resolver
^^^^^^^^^^^^^^^^^^^^
test name:    resolver
test result:  FAIL
test output:

# ./resolver_main 
main: Started! This is an example to show how a symbol is
main: resolved dynamically by the XFLAT loader.
main: Loading the shared library
load_library: loading 'libresolver.so'
load_library: handle=0x6197178c
main: calling 'unresolved_function' with 47
unresolved_function: Received 47
unresolved_function: SUCCESS -- returning 92
main: 'unresolved_function' returned 92
main: SUCCESS
#

Test: tests/signal
^^^^^^^^^^^^^^^^^^
test name:    signal
test result:  PASS
test output:

# ./signal
Setting up signal handlers from pid=606
Old SIGUSR1 sighandler at (nil)
New SIGUSR1 sighandler at 0x61971070
Old SIGUSR2 sighandler at (nil)
New SIGUSR2 sighandler at 0x6197109c
Raising SIGUSR1 from pid=606
sigusr1_sighandler: Received SIGUSR1, signo=10
SIGUSR1 raised from pid=606
Killing SIGUSR2 from pid=606
sigusr2_sigaction: Received SIGUSR2, signo=12 siginfo=0x619fdbb0 arg=0x619fdc30
SIGUSR2 killed from pid=606
Resetting SIGUSR2 signal handler from pid=606
Old SIGUSR2 sighandler at 0x6197109c
New SIGUSR2 sighandler at 0x619710d8
Killing SIGUSR2 from pid=606
sigusr2_sighandler: Received SIGUSR2, signo=12
SIGUSR2 killed from pid=606
# 

Test: tests/stdcpp
^^^^^^^^^^^^^^^^^^
test name:    stdcpp (this is not really a test)

Test: tests/thunk
^^^^^^^^^^^^^^^^^^^^^^^^^^^
test name:    thunk
test result:  This test cannot be built because it depends upon have the
              FLAT development environment
test output:  N/A
