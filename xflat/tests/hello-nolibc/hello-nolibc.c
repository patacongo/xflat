/***********************************************************************
 * xflat/tests/hello-nolibc/hello-nolibc.c
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "dlfcn.h"
#include "xflat.h"

static inline void do_write_trap(int fd, const void *buf, unsigned long count)
{
  __asm__ __volatile__ (
  "mov\tr0,%0\n\t"
  "mov\tr1,%1\n\t"
  "mov\tr2,%2\n\t"
  "swi\t0x900004\n\t"
        :
        : "r" ((long)(fd)),"r" ((long)(buf)),"r" ((long)(count))
        : "r0","r1","r2","lr");
}

void deb_puts(const char *string)
{
  unsigned long count;
  for(count = 0; string[count]; count++);
  do_write_trap(1, string, count);
}

static inline char *ltoahex(char *ptr, unsigned long n)
{
  *ptr-- = '\0';
  do
    {
      char nibble = n & 0x0f;
      if (nibble <= 9)
	*ptr-- = '0' + nibble;
      else
	*ptr-- = 'a' + nibble - 10 ;
      n >>= 4;
    }
  while (n > 0);
  *ptr-- = 'x';
  *ptr   = '0';
  return ptr;
}

void deb_putn(unsigned long n)
{
  char tmp[22];
  char *ptr;
  int i;
  for (i = 0; i < 22; i++) tmp[i] = '\0';
  ptr = ltoahex(&tmp[21], n);
  deb_puts(ptr);
}

void deb_putc(char c)
{
  do_write_trap(1, &c, 1);
}

extern struct xflat_import __dyninfo0000;
extern unsigned long __dynstack[];
extern unsigned long __dynstack_ptr;

static inline unsigned long
xflat_get_picbase(void)
{
  unsigned long picbase;
  __asm__ ("\tmov %0, r10\n\t" : "=r"(picbase));
  return picbase;
}

int main(int argc, char **argv, char **envp)
{
  void *handle;
  int i;

  /* Mandatory "Hello, world!" */

  deb_puts("Hello, world!\n");

  /* These are the thunks that I will be using... */

  deb_puts("My PIC base: ");
  deb_putn(xflat_get_picbase());
  deb_putc('\n');

  deb_puts("__dyninfo0000\n");

  deb_puts("  function_name: ");
  deb_putn(__dyninfo0000.function_name);
  deb_putc('\n');

  deb_puts("  function_address: ");
  deb_putn(__dyninfo0000.function_address);
  deb_putc('\n');

  deb_puts("  data_segment: ");
  deb_putn(__dyninfo0000.data_segment);
  deb_putc('\n');

  deb_puts("__dynstack addr: ");
  deb_putn((unsigned long)__dynstack);
  deb_putc('\n');

  deb_puts("__dynstack_ptr: ");
  deb_putn(__dynstack_ptr);
  deb_putc('\n');

  /* Print arguments */

  deb_puts("argc:        "); deb_putn(argc); deb_putc('\n');
  deb_puts("argv:        "); deb_putn((unsigned long)argv); deb_putc('\n');

  for (i = 0; i <= argc; i++)
    {
      if (argv[i])
	{
	  deb_puts("  "); deb_puts(argv[i]); deb_putc('\n');
	}
      else
	{
	  deb_puts("  NULL\n");
	}
    }

  /* Print environment variables */

  deb_puts("envp:        "); deb_putn((unsigned long)envp); deb_putc('\n');

  for (i = 0; envp[i] != NULL; i++)
    {
      deb_puts("  "); deb_puts(envp[i]); deb_putc('\n');

    }
  deb_puts("  NULL\n");

  /* No... try a call into ld-xflat.so */

  deb_puts("Calling dlmodule(NULL)\n");

  handle = dlmodule(NULL);

  deb_puts("dlmodule returned: ");
  deb_putn((unsigned long)handle);
  deb_putc('\n');

  deb_puts("Goodbye, world!\n");
  return 0;
}
