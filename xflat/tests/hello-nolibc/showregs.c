/***********************************************************************
 * xflat/tests/hello-nolibc/showregs.c
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "dlfcn.h"
#include "xflat.h"

static inline void do_exit_trap(int status)
{
  __asm__ __volatile__ (
  "mov\tr0,%0\n\t"
  "swi\t0x900001\n\t"
        :
        : "r" ((long)(status))
        : "r0","lr");
}

extern void deb_puts(const char *string);
extern void deb_putn(unsigned long n);
extern void deb_putc(char c);

extern unsigned long thunk_regs[16];
extern unsigned long __dynstack[];
extern unsigned long __dynstack_ptr;

void
show_regs(unsigned long *asm_regs)
{
  int i;

  deb_puts("asm_regs:    "); deb_putn((unsigned long)asm_regs); deb_putc('\n');
  deb_puts("thunk_regs:  "); deb_putn((unsigned long)thunk_regs); deb_putc('\n');

  for (i = 0; i < 16; i++)
    {
      deb_puts("  "); deb_putn(thunk_regs[i]); deb_putc('\n');
    }

  deb_puts("__dynstack addr: ");
  deb_putn((unsigned long)__dynstack);
  deb_putc('\n');

  for (i = 0; i < 5; i++)
    {
      deb_puts("  "); deb_putn(__dynstack[i]); deb_putc('\n');
    }

  deb_puts("__dynstack_ptr: ");
  deb_putn(__dynstack_ptr);
  deb_putc('\n');

  do_exit_trap(0);
}
