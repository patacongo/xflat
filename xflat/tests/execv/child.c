/***********************************************************************
 * xflat/tests/execv/child.c
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#include <stdio.h>  /* For printf() and friends */
#include <stdlib.h> /* For exit() */
#include <string.h> /* For strcmp() */

static char my_path[]     = CHILD_PATH;
static char parent_arg[]  = "Hello from your parent!";

int main(int argc, char **argv, char **envp)
{
  printf("Child: execv was successful!\n");
  printf("Child: argc=%d\n", argc);

  if (argc != 2)
    {
      printf("Child: expected argc to be 2\n");
      printf("Child: Exit-ting with status=2\n");
      exit(2);
    }

  printf("Child: argv[0]=\"%s\"\n", argv[0]);

  if (strcmp(argv[0], my_path) != 0)
    {
      printf("Child: expected argv[0] to be \"%s\"\n", my_path);
      printf("Child: Exit-ting with status=3\n");
      exit(3);
    }

  printf("Child: argv[1]=\"%s\"\n", argv[1]);

  if (strcmp(argv[1], parent_arg) != 0)
    {
      printf("Child: expected argv[1] to be \"%s\"\n", parent_arg);
      printf("Child: Exit-ting with status=4\n");
      exit(4);
    }

  printf("Child: Exit-ting with status=0\n");
  return 0;
}
