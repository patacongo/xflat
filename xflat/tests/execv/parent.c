/***********************************************************************
 * xflat/tests/execv/parent.c
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#include <stdio.h>      /* For printf() and friend */
#include <stdlib.h>     /* For exit() */
#include <errno.h>      /* For errno */
#include <unistd.h>     /* For execv */
#include <sys/types.h>  /* (needed by execv and wait) */
#include <sys/wait.h>   /* For wait */

static char child_path[] = CHILD_PATH;
static char child_arg[]  = "Hello from your parent!";

int main(int argc, char **argv, char **envp)
{
  pid_t parent_pid = getpid();
  pid_t child_pid;
  pid_t exit_pid;

  printf("Parent: Started, pid=%d\n", parent_pid);
  printf("Parent: Calling vfork()\n");

  /* vfork the child process. */

  child_pid = vfork();
  if (child_pid == 0)
    {
      char *child_argv[3];
      int child_retval;

      /* I am the child process! Per 'man vfork,' th rules are: (1) The
       * child  shares all memory with its parent, including the stack,
       * until execve() is issued by the child, and (2) parent will be
       * suspended until child calls execve(), or _exit().
       */

      printf("Child: vfork-ed from %d, calling execv()\n", parent_pid);

      child_argv[0] = child_path;
      child_argv[1] = child_arg;
      child_argv[2] = NULL;

      child_retval = execv(child_path, child_argv);

      /* execv should not return */

      printf("Child: execv failed, status=%d errno=%d\n",
	     child_retval, errno);
      printf("Child: Exit-ting with status=1\n");
      _exit(1);
    }
  else if (child_pid != -1)
    {
      int child_status;

      /* I am the parent. */

      printf("Parent: Child has been exec'ed, pid=%d\n", child_pid);

      /* Wait for the child to exit */

      exit_pid = wait(&child_status);

      if (exit_pid != -1)
	{

	  printf("Parent: Child pid=%d has exit-ted with status=%d\n",
		 exit_pid, child_status);

	  /* A child has exit-ted */

	  if (exit_pid != child_pid)
	    {
	      printf("But this is not our child?\n");
	      exit(1);
	    }
	  else if (child_status != 0)
	    {
	      printf("The child has exit-ted with an error!\n");
	      exit(2);
	    }
	}
      else
	{
	  printf("wait() failed, errno=%d\n", errno);
	  exit(3);
	}
    }
  else
    {
      printf("vfork() failed, errno=%d\n", errno);
      exit(4);
    }
  printf("vfork() and execv() were successful\n");
  return 0;
}
