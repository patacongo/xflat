/***********************************************************************
 * xflat/tests/resolver/resolver_main.c
 *
 * Copyright (c) 2004, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2004, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#include <stdio.h>
#include <dlfcn.h>
#include "resolver.h"

/* These are used to load the libresolver.so shared library */

static void *handle;
static const char libresolver_name[] = "libresolver.so";

/* This is an unresolved external symbol.  An warning will be reported
 * when the program is loaded because the symbol is not resolved.
 * However, we expect the symbol to be dynamically resolved when the
 * libresolver.so shared library is loaded.
 */

extern int unresolved_function(int some_arg);

static int load_library(void)
{
  int ret = 0;

  printf("load_library: loading '%s'\n", libresolver_name);

  handle = dlopen(libresolver_name, RTLD_NOW);
  if (handle == NULL)
    {
      printf("load_library: ERROR -- Failed to load %s\n", libresolver_name);
      ret = -1;
    }
  printf("load_library: handle=%p\n", handle);
  return ret;
}

int main(int argc, char **argv, char **envp)
{
  int value;
  int ret;

  printf("main: Started! This is an example to show how a symbol is\n");
  printf("main: resolved dynamically by the XFLAT loader.\n");

  /* Load the shared libraries */

  printf("main: Loading the shared library\n");
  ret = load_library();
  if (ret != 0)
    {
      printf("Exitting: Could not load shared library\n");
      return 1;
    }

  printf("main: calling 'unresolved_function' with %d\n", MAIN_ARG);

  value = unresolved_function(MAIN_ARG);
  printf("main: 'unresolved_function' returned %d\n", value);

  if (value != LIB_RETURN)
    {
      printf("main: ERROR -- expected %d\n", LIB_RETURN);
      return 1;
    }

  printf("main: SUCCESS\n");
  return 0;
}
