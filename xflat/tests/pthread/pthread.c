/***********************************************************************
 * xflat/tests/pthread/pthread.c
 *
 * Copyright (c) 2003, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2003, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#include <stdlib.h>
#include <pthread.h>

#define CHILD_ARG ((void*)0x12345678)
#define CHILD_RET ((void*)0x87654321)

enum exit_values_e
{
  TESTRESULT_SUCCESS = 0,
  TESTRESULT_PTHREAD_ATTR_INIT_FAIL,
  TESTRESULT_PTHREAD_CREATE_FAIL,
  TESTRESULT_PTHREAD_JOIN_FAIL,
  TESTRESULT_CHILD_ARG_FAIL,
  TESTRESULT_CHILD_RETVAL_FAIL,
};

struct frame_s
{
  char flags[4];
  unsigned long regs[5];
};

#define NFRAMES 6
struct all_frames_s
{
  struct frame_s frames[NFRAMES];
  struct frame_s nrframe;
};

extern struct frame_s __dynframes[NFRAMES];
extern struct frame_s __dynnrframe;

static struct all_frames_s parent_snapshot[4];
static struct all_frames_s child_snapshot[2];

static inline void do_write_trap(int fd, const void *buf, unsigned long count)
{
#ifdef __arm__
  __asm__ __volatile__ (
  "mov\tr0,%0\n\t"
  "mov\tr1,%1\n\t"
  "mov\tr2,%2\n\t"
  "swi\t0x900004\n\t"
        :
        : "r" ((long)(fd)),"r" ((long)(buf)),"r" ((long)(count))
        : "r0","r1","r2","lr");
#else
  (void)write(fd, buf, count);
#endif
}

void deb_puts(const char *string)
{
  unsigned long count;
  for(count = 0; string[count]; count++);
  do_write_trap(1, string, count);
}

static inline unsigned char deb_nibbletohex(unsigned char nibble)
{
  nibble &= 0x0f;
  if (nibble < 10)
    nibble += '0';
  else
    nibble += ('a' - 10);
  return nibble;
}

static inline void deb_bytetohex(unsigned char ch, char *buffer)
{
  buffer[0] = deb_nibbletohex(ch >> 4);
  buffer[1] = deb_nibbletohex(ch);
}

static inline void deb_longtohex(unsigned long val, char *buffer)
{
  deb_bytetohex((unsigned char)(val >> 24),          &buffer[0]);
  deb_bytetohex((unsigned char)((val >> 16) & 0xff), &buffer[2]);
  deb_bytetohex((unsigned char)((val >> 8) & 0xff),  &buffer[4]);
  deb_bytetohex((unsigned char)(val & 0xff),         &buffer[6]);
}

void deb_putbyte(unsigned char ch)
{
  char tmp[2];
  deb_bytetohex(ch, tmp);
  do_write_trap(1, tmp, 2);
}

void deb_putlong(unsigned long val)
{
  char tmp[8];
  deb_longtohex(val, tmp);
  do_write_trap(1, tmp, 8);
}

void deb_putc(unsigned char c)
{
  do_write_trap(1, &c, 1);
}

static void get_frames(struct all_frames_s *snapshot)
{
  int i;
  for (i = 0; i < NFRAMES; i++)
    {
      snapshot->frames[i] = __dynframes[i];
    }
  snapshot->nrframe = __dynnrframe;
}

static void set_frame(struct frame_s *frame)
{
  int i;
  for (i = 1; i < 4; i++)
    {
      frame->flags[i] = 0xff;
    }
  for (i = 0; i < 5; i++)
    {
      frame->regs[i] = 0xffffffff;
    }
}
static void set_frames(void)
{
  int i;
  for (i = 0; i < NFRAMES; i++)
    {
      if (__dynframes[i].flags[0] == 0)
	{
	  set_frame(&__dynframes[i]);
	}
    }
  set_frame(&__dynnrframe);
}

static void dump_frame(struct frame_s *frame)
{
  int i;
  for (i = 0; i < 4; i++)
    {
      deb_putbyte(frame->flags[i]);
      deb_putc(' ');
    }
  for (i = 0; i < 5; i++)
    {
      deb_putlong(frame->regs[i]);
      deb_putc(' ');
    }
  deb_putc('\n');
}

static void dump_frames(struct all_frames_s *snapshot)
{
  int i;
  for (i = 0; i < NFRAMES; i++)
    {
      deb_puts("  ");
      deb_putbyte((unsigned char)i);
      deb_putc(' ');
      dump_frame(&snapshot->frames[i]);
    }
  deb_puts("  NR ");
  dump_frame(&snapshot->nrframe);
}

static void *child_start_routine(void *arg)
{
  deb_puts("CHILD: started with arg=0x");
  deb_putlong((unsigned long)arg);
  deb_putc('\n');

  get_frames(&child_snapshot[1]);
  set_frames();
  deb_puts("CHILD: Before pthread_create\n");
  dump_frames(&child_snapshot[0]);
  deb_puts("CHILD: After pthread_create\n");
  dump_frames(&child_snapshot[1]);

  if (arg != CHILD_ARG)
    {
      deb_puts("CHILD: expected arg=0x");
      deb_putlong((unsigned long)CHILD_ARG);
      deb_putc('\n');
      return (void*)TESTRESULT_CHILD_ARG_FAIL;
    }

  deb_puts("CHILD: returning 0x");
  deb_putlong((unsigned long)CHILD_RET);
  deb_putc('\n');

  pthread_exit(CHILD_RET);
}

int main(int argc, char **argv, char **envp)
{
  pthread_attr_t attr;
  pthread_t      thread;
  void          *retval;
  int            status;

  deb_puts("PARENT: started\n");
  deb_puts("PARENT: initial frame state:\n");
  dump_frames(&parent_snapshot[0]);
  deb_puts("PARENT: initializing attributes\n");
  get_frames(&parent_snapshot[0]);

  set_frames();
  get_frames(&parent_snapshot[0]);
  status = pthread_attr_init(&attr);
  if (status != 0)
    {
      deb_puts("PARENT: pthread_attr_init() returned 0x");
      deb_putlong((unsigned long)status);
      deb_putc('\n');

      exit(TESTRESULT_PTHREAD_ATTR_INIT_FAIL);
    }
  get_frames(&parent_snapshot[1]);
  set_frames();
  deb_puts("PARENT: Before pthread_attr_init:\n");
  dump_frames(&parent_snapshot[0]);
  deb_puts("PARENT:After pthread_attr_init:\n");
  dump_frames(&parent_snapshot[1]);

  deb_puts("PARENT: calling pthread_start with arg=0x");
  deb_putlong((unsigned long)CHILD_ARG);
  deb_putc('\n');

  get_frames(&parent_snapshot[0]);
  get_frames(&child_snapshot[0]);
  status = pthread_create(&thread, &attr,
			  child_start_routine, CHILD_ARG);
  get_frames(&parent_snapshot[1]);
  set_frames();
  if (status != 0)
    {
      deb_puts("PARENT: pthread_create() returned 0x");
      deb_putlong((unsigned long)status);
      deb_putc('\n');

      exit(TESTRESULT_PTHREAD_CREATE_FAIL);
    }

  get_frames(&parent_snapshot[2]);
  status = pthread_join(thread, &retval);
  if (status != 0)
    {
      deb_puts("PARENT pthread_join() returned 0x");
      deb_putlong((unsigned long)status);
      deb_putc('\n');

      exit(TESTRESULT_PTHREAD_JOIN_FAIL);
    }

  get_frames(&parent_snapshot[3]);
  set_frames();
  deb_puts("PARENT: Before pthread_create:\n");
  dump_frames(&parent_snapshot[0]);
  deb_puts("PARENT: After pthread_create:\n");
  dump_frames(&parent_snapshot[1]);
  deb_puts("PARENT: Before pthread_join:\n");
  dump_frames(&parent_snapshot[2]);
  deb_puts("PARENT: After pthread_join:\n");
  dump_frames(&parent_snapshot[3]);

  deb_puts("PARENT child exitted with 0x");
  deb_putlong((unsigned long)retval);
  deb_putc('\n');

  if (retval != CHILD_RET)
    {
      deb_puts("PARENT child thread did not exit with 0x");
      deb_putlong((unsigned long)CHILD_RET);
      deb_putc('\n');

      exit(TESTRESULT_CHILD_RETVAL_FAIL);
    }

  deb_puts("PARENT returning success\n");
  return TESTRESULT_SUCCESS;
}
