########################################################################
# xflat/tests/thunk/Makefile
#
# Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
# Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
# Author: Gregory Nutt <spudmonkey@racsa.co.cr>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name Cadenux nor the names of its contributors may be
#    used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
########################################################################
#
# Directories.  Here is where we expect to find things and how we expect
# them to be named.

WD			= ${shell pwd}
TOPDIR			= $(WD)/../..

include $(TOPDIR)/.config
include	$(TOPDIR)/Make.defs

ARCHCFLAGS		= $(ARCHWARNINGS) $(ARCHOPTIMIZATION) \
			  $(ARCHCPUFLAGS) $(ARCHPICFLAGS) \
			  $(XFLAT_INCLUDES)

BIN			= thunk

DUMMY_SRCS		= thunk-dummy.c
DUMMY_OBJS		= $(DUMMY_SRCS:.c=.o)

BASE_SRCS		= thunker.c thunkee.c
BASE_OBJS		= $(BASE_SRCS:.c=.o)

DERIVED_SRCS		= $(BIN)-thunk.S
DERIVED_OBJS		= $(DERIVED_SRCS:.S=.o)

all: $(BIN) $(BIN).gdb

$(DERIVED_OBJS): $(DERIVED_SRCS)
	$(ARCHCC) -c $(ARCHCFLAGS) $(INCLUDES) $(DEFINES) $< -o $@

$(DERIVED_SRCS): $(BIN).r1
	$(LDELFLIB) -o $(BIN).tmp $(BIN).r1
	sed s/local/global/ $(BIN).tmp >$(BIN)-thunk.S
	rm -f $(BIN).tmp

$(DUMMY_OBJS): %.o: %.c
	$(ARCHCC) -c $(ARCHCFLAGS) $(INCLUDES) $(DEFINES) $< -o $@

$(BASE_OBJS): %.o: %.c
	$(ARCHCC) -c $(ARCHCFLAGS) $(INCLUDES) $(DEFINES) $< -o $@

$(BIN).r1: $(DUMMY_OBJS)
	$(ARCHCC) -o $(BIN).r1 $(ARCHLDFLAGS1) $(DUMMY_OBJS)

$(BIN).r2: $(BIN).r1 $(DERIVED_SRCS) $(DERIVED_OBJS) $(BASE_OBJS)
	$(ARCHCC) -o $(BIN).r2 $(ARCHLDFLAGS_FLAT) $(ARCH_STARTUP_FLAT) \
		$(BASE_OBJS) $(DERIVED_OBJS) -lc -lgcc

$(BIN): $(BIN).r2
	$(LDELF2FLT) -o $@ $@.r2
	#rm -rf $(BIN).r1 $(BIN).r2

$(BIN).gdb: $(BIN).r2
	$(ARCHLD) -o $(BIN).elf $(ARCHLLDFLAGS_FLAT) $(BIN).r2
	$(ARCHLD) -o $(BIN).gdb $(ARCHLLDSCRIPT_FLAT) $(BIN).elf
	rm -rf $(BIN).elf

clean: 
	rm -f $(BIN) $(BIN).r1 $(BIN).r2 $(BIN).elf $(BIN).gdb *.o \
		$(BIN)-thunk.S $(BIN).tmp core

user_install:
	install -D $(BIN) $(USER_TESTS_DIR)/$(BIN)
