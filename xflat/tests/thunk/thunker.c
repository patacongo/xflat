/***********************************************************************
 * xflat/tests/thunk/thunker.c
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#include <stdio.h>
#include "xflat.h"

#define COMPILER_PROBLEM

extern struct xflat_import __dyninfo0000;

extern int thunk(int number);
extern int thunkee(int number);

static inline unsigned long
xflat_get_picbase(void)
{
  unsigned long picbase;
  __asm__ ("\tmov %0, r10\n\t" : "=r"(picbase));
  return picbase;
}

#ifdef COMPILER_PROBLEM
static const unsigned long thunkee_address = (unsigned long)thunkee;
#else
#define thunkee_address (unsigned long)thunkee
#endif

int main(int argc, char **argv, char **envp)
{
  int retval;

  __dyninfo0000.function_address = thunkee_address;
  __dyninfo0000.data_segment     = xflat_get_picbase;

  printf("__dynimport001: function_address=0x%0lx data_segment=0x%0lx\n",
	 __dyninfo0000.function_address, __dyninfo0000.data_segment);
  printf("Calling thunk with 5\n");
  retval = thunk(5);
  printf("thunkee returned %d through thunk\n", retval);
  return 0;
}
