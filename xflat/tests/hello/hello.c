/***********************************************************************
 * xflat/tests/hello/hello.c
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#include <stdio.h>
#include <stdlib.h>

/* #define CHEAT */

#ifdef CHEAT
/* ARM ONLY! */
static inline void do_write_trap(int fd, const void *buf, unsigned long count)
{
  __asm__ __volatile__ (
  "mov\tr0,%0\n\t"
  "mov\tr1,%1\n\t"
  "mov\tr2,%2\n\t"
  "swi\t0x900004\n\t"
        :
        : "r" ((long)(fd)),"r" ((long)(buf)),"r" ((long)(count))
        : "r0","r1","r2","lr");
}

/* A version of puts that doesn't use libc. */

void deb_puts(const char *string)
{
  unsigned long count;
  for(count = 0; string[count]; count++);
  do_write_trap(1, string, count);
}
#else
#define deb_puts(s)
#endif

int main(int argc, char **argv, char **envp)
{
  int i;

  /* Mandatory "Hello, world!" */

  deb_puts("Getting ready to say \"Hello, world\"\n");
  printf("Hello, world!\n");
  deb_puts("It has been said.\n");

  /* Print arguments */

  printf("argc\t= %d\n", argc);
  printf("argv\t= 0x%p\n", argv);

  for (i = 0; i < argc; i++)
    {
      printf("argv[%d]\t= ", i);
      if (argv[i])
	{
	  printf("(0x%p) \"%s\"\n", argv[i], argv[i]);
	}
      else
	{
	  printf("NULL?\n");
	}
    }

  printf("argv[%d]\t= 0x%p\n", argc, argv[argc]);

  /* Print environment variables */

  printf("envp\t= 0x%p\n", envp);

  for (i = 0; envp[i] != NULL; i++)
    {
      printf("envp[%d]\t= (0x%p) \"%s\"\n", i, envp[i], envp[i]);
    }
  printf("envp[%d]\t= 0x%p\n", i, envp[i]);

  printf("Goodbye, world!\n");
  return 0;
}
