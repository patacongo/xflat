/***********************************************************************
 * xflat/tests/dlfcn_main.c
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

typedef int (*getglobal_t)(void);
typedef void (*setglobal_t)(int);

/* These global variables can only be accessed in the program's
 * address space
 */

static void *handle1;
static void *handle2;

static getglobal_t getglobal1;
static getglobal_t getglobal2;
static setglobal_t setglobal1;
static setglobal_t setglobal2;

struct dlfcndesc getglobal1_desc;
struct dlfcndesc getglobal2_desc;
struct dlfcndesc setglobal1_desc;
struct dlfcndesc setglobal2_desc;

static int global1_value;
static int global2_value;

static const char libdlfcn1_name[] = "libdlfcn1.so";
static const char libdlfcn2_name[] = USER_TESTS_DIR "/libdlfcn2.so";
static const char getglobal_name[] = "getglobal";
static const char setglobal_name[] = "setglobal";

static void do_setglobal1(int value)
{
  (void)dlfcncall(&setglobal1_desc, (void*)value);
}

static void do_setglobal2(int value)
{
  (void)dlfcncall(&setglobal2_desc, (void*)value);
}

static int do_getglobal1(void)
{
  return (int)dlfcncall(&getglobal1_desc, NULL);
}

static int do_getglobal2(void)
{
  return (int)dlfcncall(&getglobal2_desc, NULL);
}

static void create_wrappers(void)
{
  dlinitdesc(getglobal1, &getglobal1_desc);
  dlinitdesc(getglobal2, &getglobal2_desc);
  dlinitdesc(setglobal1, &setglobal1_desc);
  dlinitdesc(setglobal2, &setglobal2_desc);
}

static int get_symbols(void)
{
  int ret = 0;
  getglobal1 = (getglobal_t)dlsym(handle1, getglobal_name);
  if (getglobal1 == NULL)
    {
      fprintf(stderr, "Failed to get %s from %s\n",
	      getglobal_name, libdlfcn1_name);
      ret = -1;
    }
  printf("%s is at %p in %s\n", getglobal_name, getglobal1, libdlfcn1_name);

  getglobal2 = (getglobal_t)dlsym(handle2, getglobal_name);
  if (getglobal2 == NULL)
    {
      fprintf(stderr, "Failed to get %s from %s\n",
	      getglobal_name, libdlfcn2_name);
      ret = -1;
    }
  printf("%s is at %p in %s\n", getglobal_name, getglobal2, libdlfcn2_name);

  setglobal1 = (setglobal_t)dlsym(handle1, setglobal_name);
  if (setglobal1 == NULL)
    {
      fprintf(stderr, "Failed to set %s from %s\n",
	      setglobal_name, libdlfcn1_name);
      ret = -1;
    }
  printf("%s is at %p in %s\n", setglobal_name, setglobal1, libdlfcn1_name);

  setglobal2 = (setglobal_t)dlsym(handle2, setglobal_name);
  if (setglobal2 == NULL)
    {
      fprintf(stderr, "Failed to set %s from %s\n",
	      setglobal_name, libdlfcn2_name);
      ret = -1;
    }
  printf("%s is at %p in %s\n", setglobal_name, setglobal2, libdlfcn2_name);
  return ret;
}

static int load_libraries(void)
{
  int ret = 0;

  handle1 = dlopen(libdlfcn1_name, RTLD_NOW);
  if (handle1 == NULL)
    {
      fprintf(stderr, "Failed to load %s\n", libdlfcn1_name);
      ret = -1;
    }
  printf("handle1=%p\n", handle1);

  handle2 = dlopen(libdlfcn2_name, RTLD_NOW);
  if (handle2 == NULL)
    {
      fprintf(stderr, "Failed to load %s\n", libdlfcn2_name);
      ret = -1;
    }
  printf("handle2=%p\n", handle1);
  return ret;
}


int main(int argc, char **argv, char **envp)
{
  int ret;

  /* Load the shared libraries */

  ret = load_libraries();
  if (ret != 0)
    {
      printf("Exitting: Could not load shared libraries\n");
      exit(1);
    }

  /* Get the addresses of the symbols that we need from the
   * shared libraries.
   */

  ret = get_symbols();
  if (ret != 0)
    {
      printf("Exitting: Could not get symbols\n");
      exit(1);
    }

  /* Create XFLAT wrappers for each symbol */

  create_wrappers();

  /* Get the current state of the global variables in the shared
   * libraries address space.
   */

  global1_value = do_getglobal1();
  global2_value = do_getglobal2();

  printf("Initially, global1=%d global2=%d\n", global1_value, global2_value);

  /* Set the globals to a new value */

  do_setglobal1(47);
  do_setglobal2(938);

  printf("Set global1=%d global2=%d\n", 47, 938);

  /* Read the globals to see if they we set as expected */

  global1_value = do_getglobal1();
  global2_value = do_getglobal2();

  printf("Current global1=%d global2=%d\n", global1_value, global2_value);

  /* Check that they were set as expected */

  if (global1_value != 47 || global2_value != 938)
    {
      fprintf(stderr, "ERROR: value written does not match value read\n");
      exit(1);
    }
  return 0;
}
