/***********************************************************************
 * xflat/ldso/xflat-ldso/arm/xflat_sysdeps.h
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#ifndef _XFLAT_SYSDEPS_H_
#define _XFLAT_SYSDEPS_H_

/***********************************************************************
 * Conditional Compilation Switches
 ***********************************************************************/

/* Defines behavior of ntohl and ntohs. */

/* #define NETWORK_IS_HOST_ORDER */

/* This flag is set to indicate that the code is compiled with
 * -msingle-pic-base and the the PIC base register need not be
 * saved/restored.
 */

#define SINGLE_PIC_BASE

/* This switch changes the way the xflat_loader returns parameters.
 * If r10 is clobber on return from xflat_loader, then we have to do
 * something different.  We really should fix the compiler, it should
 * not be saving a restoring the PIC based register if -msingle-pic-base
 * is supplied.
 */

#define PIC_BASE_IS_RESTORED

/***********************************************************************
 * Included Files
 ***********************************************************************/

#ifndef XFLAT_ASM_CODE
# include "xflatlib.h"  /* For XFLAT_DATA_OFFSET  */
#endif /* XFLAT_ASM_CODE */

/***********************************************************************
 * Definitions
 ***********************************************************************/

/* This identifies the register the is used by the processor as the
 * PIC base register.  It is uually r9 or r10
 */

#define PIC_REG         r10
#define PIC_REG_STRING "r10"

#define RINFO		r4	/* A pointer to the DYN info structure */
#define HISPIC		r5	/* Caller's PIC base register value */
#define FRAME		r6	/* Pointer to dynamic frame */

#define WORK		r7	/* Thunk layer working registers */
#define TMP1		ip
#define TMP2		lr

/* This is an assembly language representation of the struct xflat_import
 * structure:
 */

#define RINFO_FCNNAME	0	/* Offset to the function name */
#define	RINFO_FCNADDR	4	/* Offset to the function address */
#define RINFO_PICBASE	8	/* Offset to the PIC base value */

/* Get a pointer to the argv array.  On many platforms this can be just
 * the address if the first argument, on other platforms we need to
 * do something a little more subtle here.
 */

#define GET_ARGV(ARGVP, ARGS) ARGVP = ((u_int32_t*)ARGS)

/* Transfer control to the user's application, once the dynamic loader
 * is done.  This routine has to exit the current function returning
 * the program entry point (_start?) which which will be called via
 * assembly language logic.
 */

#ifdef PIC_BASE_IS_RESTORED

#ifdef SINGLE_PIC_BASE
# define START(e,d) \
  { \
    u_int32_t picbase = (d) + XFLAT_DATA_OFFSET; \
    __asm__ __volatile__ ("\tmov r1, %0\n\t"::"r"(picbase)); \
    return (e); \
  }    
#else /* SINGLE_PIC_BASE */
# define START(e,d) \
  { \
    u_int32_t picbase = (d) + XFLAT_DATA_OFFSET; \
    __asm__ __volatile__ ("\tmov r1, %0\n\t" ::"r"(picbase):PIC_REG_STRING); \
    return (e); \
  }    
#endif /* SINGLE_PIC_BASE */

#else /* PIC_BASE_IS_RESTORED */

# define START(e,d) \
  xflat_set_dspace(d); \
  return (e);      

#endif /* PIC_BASE_IS_RESTORED */

/***********************************************************************
 * Public Types
 ***********************************************************************/

/***********************************************************************
 * Inline Functions
 ***********************************************************************/

#ifndef XFLAT_ASM_CODE
/* We insist that the DSpace section be linked to virtual address zero.
 * as a consequence, the raw dspace address is very close to the
 * PIC base address -- it just off by the memory that we reserved
 * for use by the loader (XFLAT_DATA_OFFSET).
 */

static inline void
xflat_set_dspace(u_int32_t dspace)
{
  u_int32_t adjusted_dspace = dspace + XFLAT_DATA_OFFSET;
#ifdef SINGLE_PIC_BASE
  __asm__
  (
    "\tmov " PIC_REG_STRING ", %0\n\t"
    : : "r"(adjusted_dspace)
  );
#else /* SINGLE_PIC_BASE */
  __asm__
  (
    "\tmov " PIC_REG_STRING ", %0\n\t"
    : : "r"(adjusted_dspace) : PIC_REG_STRING
  );
#endif /* SINGLE_PIC_BASE */
}

static inline u_int32_t
xflat_get_dspace(void)
{
  u_int32_t unadjusted_dspace;
  __asm__
  (
    "\tmov %0, " PIC_REG_STRING "\n\t"
    : "=r"(unadjusted_dspace)
  );
  return unadjusted_dspace - XFLAT_DATA_OFFSET;
}

/* And here are the obvious conversions */

static inline u_int32_t
xflat_dspace_to_picbase(u_int32_t dspace)
{
  return dspace + XFLAT_DATA_OFFSET;
}

static inline u_int32_t
xflat_picbase_to_dspace(u_int32_t picbase)
{
  return picbase - XFLAT_DATA_OFFSET;
}
#endif /* XFLAT_ASM_CODE */

/***********************************************************************
 * Global Functions
 ***********************************************************************/

#ifndef XFLAT_ASM_CODE
/* This is the address of the entry point to the linux resolver.
 * This is not the "real" prototype.  The real function signature is
 * not expressable as a C prototype.  But this will give us the address.
 */

extern void
xflat_linux_resolve(void);
#endif /* XFLAT_ASM_CODE */

#endif /* _XFLAT_SYSDEPS_H_ */
