/***********************************************************************
 * xflat/ldso/xflat-ldso/arm/xflat_syscalls.h
 * This file contains the system call macros and syscall numbers used
 * by the shared library loader.
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#ifndef _XFLAT_SYSCALLS_H_
#define _XFLAT_SYSCALLS_H_

/***********************************************************************
 * Definitions
 ***********************************************************************/

#define __NR_SYSCALL_BASE	0x900000

#define __NR_xflat_exit		(__NR_SYSCALL_BASE+  1)
#define __NR_xflat_read		(__NR_SYSCALL_BASE+  3)
#define __NR_xflat_write	(__NR_SYSCALL_BASE+  4)
#define __NR_xflat_open		(__NR_SYSCALL_BASE+  5)
#define __NR_xflat_close	(__NR_SYSCALL_BASE+  6)
#define __NR_xflat_lseek	(__NR_SYSCALL_BASE+ 19)
#define __NR_xflat_getuid	(__NR_SYSCALL_BASE+ 24)
#define __NR_xflat_geteuid	(__NR_SYSCALL_BASE+ 49)
#define __NR_xflat_getgid	(__NR_SYSCALL_BASE+ 47)
#define __NR_xflat_getegid	(__NR_SYSCALL_BASE+ 50)
#define __NR_xflat_readlink	(__NR_SYSCALL_BASE+ 85)
#define __NR_xflat_mmap		(__NR_SYSCALL_BASE+ 90)
#define __NR_xflat_munmap	(__NR_SYSCALL_BASE+ 91)
#define __NR_xflat_stat		(__NR_SYSCALL_BASE+106)
#define __NR_xflat_mprotect	(__NR_SYSCALL_BASE+125)


/* Here are the macros which define how this platform makes
 * system calls.  This particular variant does _not_ set 
 * errno (note how it is disabled in __syscall_return) since
 * these will get called before the errno symbol is dynamicly 
 * linked. */

/* These are Erik's versions of the syscall routines.  His were
 * cleaner than mine, so I adopted them instead with some
 * reformating.  Shane Nay.
 */

#define __sys2(x) #x
#define __sys1(x) __sys2(x)

#ifndef __syscall
#define __syscall(name) "swi\t" __sys1(__NR_##name) "\n\t"
#endif

#undef __syscall_return
#define __syscall_return(type, res)					\
do {									\
	if ((u_int32_t)(res) >= (u_int32_t)(-125)) {			\
		/*errno = -(res);*/					\
		res = -1;						\
	}								\
	return (type) (res);						\
} while (0)

#define _syscall0(type,name)						\
type name(void) {							\
  int32_t __res;							\
  __asm__ __volatile__ (						\
  __syscall(name)							\
  "mov %0,r0"								\
  :"=r" (__res) : : "r0","lr");						\
  __syscall_return(type,__res);						\
}

#define _syscall1(type,name,type1,arg1)					\
type name(type1 arg1) {							\
  int32_t __res;							\
  __asm__ __volatile__ (						\
  "mov\tr0,%1\n\t"							\
  __syscall(name)							\
  "mov %0,r0"								\
        : "=r" (__res)							\
        : "r" ((int32_t)(arg1))						\
	: "r0","lr");							\
  __syscall_return(type,__res);						\
}

#define _syscall2(type,name,type1,arg1,type2,arg2)			\
type name(type1 arg1,type2 arg2) {					\
  int32_t __res;							\
  __asm__ __volatile__ (						\
  "mov\tr0,%1\n\t"							\
  "mov\tr1,%2\n\t"							\
  __syscall(name)							\
  "mov\t%0,r0"								\
        : "=r" (__res)							\
        : "r" ((int32_t)(arg1)),"r" ((int32_t)(arg2))			\
	: "r0","r1","lr");						\
  __syscall_return(type,__res);						\
}


#define _syscall3(type,name,type1,arg1,type2,arg2,type3,arg3)		\
type name(type1 arg1,type2 arg2,type3 arg3) {				\
  int32_t __res;							\
  __asm__ __volatile__ (						\
  "mov\tr0,%1\n\t"							\
  "mov\tr1,%2\n\t"							\
  "mov\tr2,%3\n\t"							\
  __syscall(name)							\
  "mov\t%0,r0"								\
        : "=r" (__res)							\
        : "r" ((int32_t)(arg1)),"r" ((int32_t)(arg2)),"r" ((int32_t)(arg3))	\
        : "r0","r1","r2","lr");						\
  __syscall_return(type,__res);						\
}

#undef _syscall4
#define _syscall4(type,name,type1,arg1,type2,arg2,type3,arg3,type4,arg4)\
type name(type1 arg1, type2 arg2, type3 arg3, type4 arg4) {		\
  int32_t __res;							\
  __asm__ __volatile__ (						\
  "mov\tr0,%1\n\t"							\
  "mov\tr1,%2\n\t"							\
  "mov\tr2,%3\n\t"							\
  "mov\tr3,%4\n\t"							\
  __syscall(name)							\
  "mov\t%0,r0"								\
  	: "=r" (__res)							\
  	: "r" ((int32_t)(arg1)),"r" ((int32_t)(arg2)),                  \
	  "r" ((int32_t)(arg3)),"r" ((int32_t)(arg4))	                \
  	: "r0","r1","r2","r3","lr");					\
  __syscall_return(type,__res);						\
}
  
#endif /* _XFLAT_SYSCALLS_H_ */
