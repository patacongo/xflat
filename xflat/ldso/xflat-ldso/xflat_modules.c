/***********************************************************************
 * xflat/ldso/xflat-ldso/xflat_modules.c
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include <errno.h>
#include "xflat_loader.h"
#include "xflat_modules.h"
#include "xflat_read.h"
#include "xflat_util.h"
#include "xflat_syscall.h"

/***********************************************************************
 * Definitions
 ***********************************************************************/

#define XFLAT_MAX_MODULES    XFLAT_MAXLIB_NAMES

#define XFLAT_HDR_SIZE       sizeof(struct xflat_hdr)

/***********************************************************************
 * Private Variables
 ***********************************************************************/

/* This is an array of pre-allocated module structures.  We avoid using a
 * wasteful page allocator and, instead, require an a priori, fixed
 * maximum number of modules that a single process has to deal with.
 */

static struct xflat_module xflat_alloc_modules[XFLAT_MAX_MODULES];

/* This is a list of available module structures */

static struct xflat_module *xflat_free_modules = NULL;

/***********************************************************************
 * Public Variables
 ***********************************************************************/

/* This is the start of the linked list that describes all of the files
 * loaded into the process.
 */

struct xflat_module *xflat_loaded_modules = NULL;

/***********************************************************************
 * Private Functions
 ***********************************************************************/

/***********************************************************************
 * Public Functions
 ***********************************************************************/

/***********************************************************************
 * xflat_initialize_allocators
 ***********************************************************************/

void xflat_initialize_allocators(void)
{
  int i;

  /* Put all of the pre-allocated module structures on the free
   * list.
   */

  xflat_free_modules = &xflat_alloc_modules[0];

  for (i = 0; i < XFLAT_MAX_MODULES; i++)
    {
      if (i == 0)
	{
	  xflat_alloc_modules[i].prev    = NULL;
	}
      else
	{
	  xflat_alloc_modules[i].prev    = &xflat_alloc_modules[i-1];
	}

      if (i < XFLAT_MAX_MODULES-1)
	{
	  xflat_alloc_modules[i].next    = &xflat_alloc_modules[i+1];
	}
      else
	{
	  xflat_alloc_modules[i].next = NULL;
	}
    }
}

/***********************************************************************
 * xflat_allocate_module
 ***********************************************************************/

struct xflat_module *xflat_allocate_module(void)
{
  struct xflat_module *retval = xflat_free_modules;
  if (retval)
    {
      xflat_free_modules = retval->next;
      retval->next       = NULL;
    }
  return retval;
}

/***********************************************************************
 * xflat_free_module
 ***********************************************************************/

void
xflat_free_module(struct xflat_module *module)
{
  /* The module at the beginning at the list should point back
   * to the new one.
   */

  if (xflat_free_modules)
    {
      xflat_free_modules->prev = module;
    }

  /* The new one will be positioned at the beginning of the list */

  module->next       = xflat_free_modules;
  xflat_free_modules = module;
  module->prev       = NULL;
}

/***********************************************************************
 * xflat_initialize_module
 ***********************************************************************/

void xflat_initialize_module(struct xflat_module *module,
			     const char *module_name,
			     enum libtype_e module_type,
			     u_int32_t ispace_start, u_int32_t ispace_size,
			     u_int32_t dspace_start, u_int32_t dspace_size)
{
  if (module)
    {
      /* Clear the structure */

      xflat_memset(module, 0, sizeof(struct xflat_module));

      /* Copy the parameters into the structure. */

      module->module_type  = (unsigned char)module_type;
      module->flags        = 0;
      module->ispace_start = ispace_start;
      module->ispace_size  = ispace_size;
      module->dspace_start = dspace_start;
      module->dspace_size  = dspace_size;

      /* Copy the first bytes of the module basename into the structure.
       * We don't keep the whole path (really should)
       */

      if (module_name)
	{
	  const char *module_basename = xflat_basename(module_name);
	  int namelen                 = xflat_strlen(module_basename)+1;

	  if (namelen > MAX_MODULE_NAME_SIZE)
	    namelen = MAX_MODULE_NAME_SIZE;

	  xflat_strncpy(module->name, module_basename, namelen);
	  module->name[MAX_MODULE_NAME_SIZE-1] = '\0';
	}
      else
	{
	  module->name[0] = '\0';
	}
    }
}

/***********************************************************************
 * xflat_add_module
 ***********************************************************************/

/* We call this function when we have just read an ELF library or
 * executable.  It adds the module to the xflat_loaded_module list
 * so that it can be found later.
 */

void xflat_add_module(struct xflat_module *module)
{
  struct xflat_module *prev = xflat_loaded_modules;
  
  if (!prev)
    {
      /* First case: The module list is empty */

      xflat_loaded_modules = module;
      module->prev = NULL;
    }
  else
    {
      /* The module list is not empty -- find the end (Hmmm..
       * should really create a tail pointer).
       */

      for (; prev->next ; prev = prev->next);

      /* Then insert the new module at the end of the list. */

      prev->next = module;
      module->prev = prev;
    }
  module->next = NULL;
}

/***********************************************************************
 * xflat_find_module_by_name
 ***********************************************************************/

struct xflat_module *xflat_find_module_by_name(const char *module_name)
{
  struct xflat_module *module;
  const char           *module_basename = xflat_basename(module_name);
  int                  len             = xflat_strlen(module_basename);

  /* Check to see if a library has already been added loaded
   * module list.
   */

  if (len > MAX_MODULE_NAME_SIZE-1) len = MAX_MODULE_NAME_SIZE-1;

  for (module = xflat_loaded_modules; module; module = module->next)
    {
      if (xflat_strncmp(module->name, module_basename, len) == 0 &&
	  (module->name[len] == '\0' ||
	   module->name[len] == '.'))
	{
	  return module;
	}
    }
  return NULL;
}

/***********************************************************************
 * xflat_find_module_by_address
 ***********************************************************************/

struct xflat_module *
xflat_find_module_by_address(u_int32_t address)
{
  struct xflat_module *module;

  for (module = xflat_loaded_modules; module; module = module->next)
    {
      if (((address >= module->ispace_start) &&
	   (address <  module->ispace_start + module->ispace_size)) ||
	  ((address >= module->dspace_start) &&
	   (address <  module->dspace_start + module->dspace_size)))
	{
	  return module;
	}
    }
  return NULL;
}

/***********************************************************************
 * xflat_traverse_loaded_modules
 ***********************************************************************/

void
traverse_loaded_modules(void *arg1, func_type fn)
{
  struct xflat_module *module;

  for (module = xflat_loaded_modules; module; module = module->next)
    {
      if (fn(module, arg1) != 0)
	return;
    }
}
 
/***********************************************************************
 * xflat_find_symbol_in_module
 ***********************************************************************/

int
xflat_find_symbol_in_module(struct xflat_module *module,
			    const char *symbol_name, u_int32_t *symbol_value)
{
  struct xflat_export *export_symbols;
  char                *export_name;
  u_int32_t            ispace;
  u_int16_t            nexports;
  int                  i;

  /* Get the ISpace load address of the module.  The xFLT header
   * is the first thing at the beginning of the ISpace.
   */

  ispace = module->ispace_start;

  /* From this, we can get the exported symbol list.  The exported
   * symbol list lives in the text segment, so we can get the
   * address of the exported symbol list by simply adding the
   * load address.
   */

  export_symbols = (struct xflat_export*)
    (GET_EXPORT_SYMBOLS(ispace)   /* Files offset to export list */
     + ispace);                   /* + ISpace address of file map */

  DBG_ASSERT(IS_ISPACE(module, export_symbols),
	     "export_symbols(0x%p) not in ISpace",
	     export_symbols);

  nexports = (GET_EXPORT_COUNT(ispace));

  /* Then, search the list of exported symbols to see if the
   * one we are looking for is exported by this module.
   */

  for (i = 0; i < nexports; i++)
    {
      /* Get a pointer to the exported symbol name. The value of
       * the export function name point int he export_symbols
       * list is an offset from the beginning of the text
       * section.
       */

      export_name = (char*)
	(export_symbols[i].function_name /* TEXT offset to name */
	 + ispace                        /* + ISpace allocation address */
	 + XFLAT_HDR_SIZE);              /* + Offset in ISpace to TEXT */

      DBG_ASSERT(IS_ISPACE(module, export_name),
		 "export_name(0x%p) not in ISpace",
		 export_name);

      /* And compare this to the symbol we are looking for */

      if (xflat_strcmp(export_name, symbol_name) == 0)
	{
	  dbg("Symbol \"%s\" found in module \"%s\"",
	      symbol_name, module->name);

	  /* We got it... Calculate and return the function address.
	   * The function address in the the export symbol table is
	   * an offset from the beginning of the TEXT section.  The
	   * TEXT section is offset from the beginning of the ISpace
	   * allocation by XFLAT_HDR_SIZE.  So the calculatin is:
	   */

	  *symbol_value =
	    (export_symbols[i].function_address /* TEXT offset to function */
	     + ispace                           /* + ISpace alloc address */
	     + XFLAT_HDR_SIZE);                 /* + ISpace offset to TEXT */

	  DBG_ASSERT(IS_ISPACE(module, *symbol_value),
		     "*symbol_value(0x%x) not in ISpace",
		     *symbol_value);

	  return 0;
	}
    }

  dbg("Could not find symbol \"%s\" in module \"%s\"",
      symbol_name, module->name);

  return -ENOENT;
}

/***********************************************************************
 * xflat_find_symbol
 ***********************************************************************/

int xflat_find_symbol(struct xflat_module *import_module,
		      const char *import_name,
		      u_int32_t *export_value, u_int32_t *exporter_dspace)
{
  struct xflat_module *module;
  int                 status;

  /* Traverse every loaded module.  The program module is the first
   * in the list, followed by modules in the order that they were
   * loaded.  The first module that exports the symbol is the one
   * that we will use.  Hence, programs and first-loaded modules
   * take precedenc.
   */

  for (module = xflat_loaded_modules; module; module = module->next)
    {
      /* Skip this module -- modules don't export symbols to themselves */

      if (module != import_module)
	{

	  /* Find the symbol in the module.  If found, this
	   * function will return the symbol value directly
	   * the caller.
	   */

	  status = xflat_find_symbol_in_module(module, import_name,
					       export_value);
	  if (status == 0)
	    {
	      /* We got it! Return the exporting modules DSpace
	       * address as well.
	       */

	      *exporter_dspace = module->dspace_start;
	      return 0;
	    }
	}
    }

  dbg("Could not find symbol \"%s\" in ANY loaded module", import_name);

  return -ENOENT;
}
