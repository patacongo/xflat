/***********************************************************************
 * xflat/ldso/xflat-ldso/xflat_loader.h
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#ifndef _XFLAT_LOADER_H_
#define _XFLAT_LOADER_H_

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include "xflat.h"         /* struct xflat_hdr */
#include "xflatlib.h"      /* struct xflat_lso_info */
#include "xflat_sysdeps.h" /* xflat_set_dspace */

/***********************************************************************
 * Conditional Compilation
 ***********************************************************************/

/***********************************************************************
 * Definitions
 ***********************************************************************/

/* Macros to access elements of the xFLT header given the load address
 * of the module's ISpace.  This works because the xFLT header resides
 * at the beginning of the module's ISpace.
 */

/* These return file offsets to data embedded in ispace.  You need to
 * add the ispace allocation address to this offset to get a usable
 * address.  But, you might want to check if the offset is NULL before
 * doing that.
 */

#define GET_ENTRY_POINT(i) \
   xflat_ntohl(((struct xflat_hdr*)(i))->entry)
#define GET_LOADER(i) \
   xflat_ntohl(((struct xflat_hdr*)(i))->loader)
#define GET_LIB_PATHES(i) \
   xflat_ntohl(((struct xflat_hdr*)(i))->lib_pathes)
#define GET_LIB_NAMES(i) \
   xflat_ntohl(((struct xflat_hdr*)(i))->lib_names)
#define GET_EXPORT_SYMBOLS(i) \
   xflat_ntohl(((struct xflat_hdr*)(i))->export_symbols)

/* The most complicated of all .. this returns a file offset, but the
 * array lies in dspace.  So the correct calculation is:
 *
 * FileOffset - SizeOfISpace + DSpaceStart + XFLAT_DATA_OFFSET
 *
 */

#define GET_IMPORT_SYMBOLS(i) \
   xflat_ntohl(((struct xflat_hdr*)(i))->import_symbols)

/* These return numbers.  The number are fine just the way they are. */

#define GET_ISPACE_SIZE(i) \
   xflat_ntohl(((struct xflat_hdr*)(i))->data_start)
#define GET_DATA_START(i) \
   xflat_ntohl(((struct xflat_hdr*)(i))->data_start)
#define GET_LPATH_COUNT(i) \
   xflat_ntohs(((struct xflat_hdr*)(i))->lpath_count)
#define GET_LNAME_COUNT(i)\
   xflat_ntohs(((struct xflat_hdr*)(i))->lname_count)
#define GET_IMPORT_COUNT(i) \
   xflat_ntohs(((struct xflat_hdr*)(i))->import_count)
#define GET_EXPORT_COUNT(i) \
   xflat_ntohs(((struct xflat_hdr*)(i))->export_count)

/***********************************************************************
 * Public Types
 ***********************************************************************/

/***********************************************************************
 * Public Data
 ***********************************************************************/

/* The name of the program we are loading and the loader. */

extern const char *xflat_progname;
extern const char *xflat_ldrname;

/***********************************************************************
 * Inline Functions
 ***********************************************************************/

/* This function sets the current address context to the loader's address
 * space.  This function receives current DSpace address as a parameter
 * (obtained via xflat_get_space).  When we instantiated each module, we
 * stuck the loader's DSapce address at the beginning of each module's
 * DSpace.  So we can get it now pretty easily.
 */

static inline void
xflat_set_loader_dspace(u_int32_t caller_dspace)
{
  xflat_set_dspace(((struct xflat_ldso_info *)caller_dspace)->dspace);
}

/***********************************************************************
 * Public Function Prototypes
 ***********************************************************************/

/* Entry point to the dynamic loader.  This function is called from
 * assembly language logic when the process is started.
 */

extern start_entry_t
xflat_loader(u_int32_t args);

/* Make sure that the imported symbols of every loaded module has been
 * bound to the exported symbol.
 */

extern void
xflat_bind_all_modules(void);

/* Call the initialization function of every loaded module (unless
 * it has already been called.  This is called by the loader and
 * also from dlopen.
 */

extern void
xflat_module_run_preparation(void);

#endif /* _XFLAT_LOADER_H_ */
