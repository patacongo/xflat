/***********************************************************************
 * xflat/ldso/xflat-ldso/xflat_util.h
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#ifndef _XFLAT_UTIL_H_
#define _XFLAT_UTIL_H_

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include "xflat.h" /* For correct u_int32_t */

/***********************************************************************
 * Definitions
 ***********************************************************************/

#define XFLAT_FD 2,

#ifndef dbg
#ifdef CONFIG_XFLAT_DEBUG

# define dbg(format, arg...) \
   xflat_printf(XFLAT_FD "LDSO: " format "\n" , ## arg)

#else

# define dbg(format, arg...) \
   do {} while (0)

#endif
#endif

#ifndef err
#define err(format, arg...) \
   xflat_printf(XFLAT_FD "LDSO: ERROR: " format "\n" , ## arg)
#endif

#ifndef info
#define info(format, arg...) \
   xflat_printf(XFLAT_FD "LDSO: INFO: " format "\n" , ## arg)
#endif

#ifndef warn
#define warn(format, arg...) \
   xflat_printf(XFLAT_FD "LDSO: WARN: " format "\n" , ## arg)
#endif

#ifndef NULL
#define NULL ((void *) 0)
#endif

#define IS_ISPACE(m,a) \
  (((u_int32_t)(a) >= (m)->ispace_start) && \
   ((u_int32_t)(a) < ((m)->ispace_start + (m)->ispace_size)))

#define IS_DSPACE(m,a) \
  (((u_int32_t)(a) >= (m)->dspace_start) && \
   ((u_int32_t)(a) < ((m)->dspace_start + (m)->dspace_size)))

#ifdef CONFIG_XFLAT_DEBUG
# define DBG_ASSERT(c, s, arg...) \
  if (!(c)) {err(s, ## arg); xflat_exit(1);}
#else
# define DBG_ASSERT(c, s, arg...)
#endif

/***********************************************************************
 * Public Function Prototypes
 ***********************************************************************/

extern void
xflat_printf(int, const char *, ...)
        __attribute__ ((format (printf, 2, 3)));

/* extern char  *
   xflat_strdup(const char *string); */

extern u_int32_t
xflat_strlen(const char *str);

extern char *
xflat_strcat(char *dst, const char *src);

extern char *
xflat_strcpy(char *dst, const char *src);

extern char *
xflat_strncpy(char *dst, const char *src, u_int32_t n);

extern int
xflat_strcmp(const char *s1, const char *s2);

extern int
xflat_strncmp(const char *s1, const char *s2, u_int32_t len);

/* extern char *
   xflat_strchr(const char *str, int c); 

   extern char *
   xflat_strrchr(const char *str, int c); */

extern void *
xflat_memcpy(void *dst, const void *src, u_int32_t len);

/* extern int
   xflat_memcmp(const void *s1, const void *s2, u_int32_t len); */

extern void *
xflat_memset(void *str, int c, u_int32_t len);

extern const char *
xflat_basename(const char *path);

/* This is included very late in order to order a circular header
 * file dependency:  Some incarnations of the included file
 * contained inline functions that depended on the above
 * definitions.
 */

#include "xflat_sysdeps.h" /* For NETWORK_IS_HOST_ORDER */

#ifdef NETWORK_IS_HOST_ORDER
# define htohl(a) (a)
# define htohs(a) (a)
#else

extern u_int32_t
xflat_ntohl(u_int32_t netlong);

extern u_int16_t
xflat_ntohs(u_int16_t netshort);

#endif

#endif /* _XFLAT_UTIL_H_ */
