/***********************************************************************
 * xflat/ldso/xflat-ldso/xflat_user.c
 * This file contains all of the symbols exported to the user space by
 * the xflat loader.
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include <stdarg.h>       /* Needed in dlfcncall */

/* #include "dlfcn.h"        User space include cannot not be used */

#include "xflat_loader.h"
#include "xflat_read.h"
#include "xflat_util.h"

/***********************************************************************
 * Compilation Switches
 ***********************************************************************/

#undef HAVE_BUILTIN_RETURN_ADDRESS

/***********************************************************************
 * Private Type Declarations
 ***********************************************************************/

/* The following structure is a duplicate of the definition in dlfcn.h.
 * It defines the generic type of the an inter-module function pointer.
 */

typedef void *
(*dlfcnptr_t)(void *arg1, void *arg2, void *arg3, void *arg4);

/* The following structure is a duplicate of the definition in dlfcn.h.
 * It defines a inter-module function pointer. It is used by dlfcncall.
 */

struct dlfcndesc
{
  dlfcnptr_t fcnptr; /* The address of the function */
  void      *module; /* Handle to the module that exports the function */
};

/***********************************************************************
 * Definitions
 ***********************************************************************/

/***********************************************************************
 * dlopen
 ***********************************************************************/

/* dlopen  loads  a  dynamic library from the file named by the null
 * terminated string filename and returns an opaque "handle" for the
 * dynamic library.  If filename is not an absolute path (i.e., it does
 * not begin with a "/"), then the file is searched for in the default
 * search path.
 *
 * __dlopen is the name used if __builtin_return_address does not work.
 * In this case, a small assembly language routine provides the return
 * address.  As one example, GCC's __builtin_return_address does
 * not work on ARM with -fomit_frame_pointer.
 */

#ifdef HAVE_BUILTIN_RETURN_ADDRESS
void *dlopen(const char *libname, int flag)
#else
void *__dlopen(const char *libname, int flag, u_int32_t return_address)
#endif
{
  struct xflat_module *new_module = NULL;

  /* If the libname is null, then return the main program's
   * struct xflat_module as a handle.
   */

  if (!libname)
    {
      /* We know that the program module resides at the very
       * beginning of the loaded modules list
       */

      new_module = (void*)xflat_loaded_modules;
    }
  else
    {
      struct xflat_module *caller_module;
#ifdef HAVE_BUILTIN_RETURN_ADDRESS
      u_int32_t            return_address;

      /* Get the return address of the caller. */

      return_address = (u_int32_t)__builtin_return_address(0);
#endif

      /* Locate the module that we were called from. */

      caller_module = xflat_find_module_by_address(return_address);
      if (!caller_module)
	{
	  err("Could not locate caller's module description");
	}
      else
	{
	  char      *path_list[XFLAT_MAXLIB_PATHES];
	  u_int16_t  npathes;

	  /* Get the search path list from the module */

	  (void)xflat_get_module_path_list(caller_module,
					   path_list, &npathes);

	  /* And use this search path to try to find (and load)
	   * the specified library
	   */

	  new_module =
	    xflat_load_shared_library(libname, path_list, (int)npathes);
	  if (!new_module)
	    {
	      err("Could not load \"%s\"", libname);
	      goto dlopen_exit;
	    }
	  else
	    {
	      /* We have successfully loaded the requested library.
	       * Now load any additional shared libraries that may be
	       * needed by this module.
	       */

	      if (xflat_load_dependent_libraries(new_module) != 0)
		{
		  /* We failed to load one or more of the dependent
		   * libraries. We may have just loaded and stranded
		   * some unusable, garbage shared libraries.  Need
		   * to fix this.
		   */

		  err("Could not libraries needed by load \"%s\"", libname);
		  new_module = NULL;
		}
	      else
		{
		  /* Bind all of the addresses imported by the modules. */

		  xflat_bind_all_modules();

		  /* Get all of the modules ready to run. */

		  xflat_module_run_preparation();
		}
	    }
	}
    }

 dlopen_exit:
  return (void*)new_module;
}

/***********************************************************************
 * dlsym
 ***********************************************************************/

/* dlsym takes a "handle" of a dynamic library returned by dlopen and
 * the null terminated  symbol  name,  returning  the address where that
 * symbol is loaded.  If the symbol is not found, dlsym returns NULL.
 */

void *dlsym(void *handle, const char *symbol)
{
  void *retval = NULL;

  if ((!symbol) || (!handle))
    {
      err("Missing handle or symbol");
    }
  else
    {
      struct xflat_module *module = (struct xflat_module*)handle;
      u_int32_t            symbol_value;
      int                  status;

      /* Find the symbol in the module specified by the caller. */

      status = xflat_find_symbol_in_module(module, symbol, &symbol_value);
      if (status != 0)
	{
	  dbg("Could not find symbol \"%s\"", symbol);
	}
      else
	{
	  retval = (void*)symbol_value;
	}
    }
  return retval;
}

/***********************************************************************
 * dlclose
 ***********************************************************************/

#if 0
int dlclose(void *vhandle)
{
  /* NOT IMPLEMENTED.  If we want to implement this we need:
   * 1. Basic logic to free and deallocate module resources.
   * 2. Some kind of "chaining" logic to keep track of what
   *    additional shared libraries may have been opened by
   *    the one that is being closed (see the use of struct
   *    elf_resolve in the ELF loader). */

  return 0;
}
#endif

/***********************************************************************
 * dlmodule
 ***********************************************************************/

/* Given the raw address of a function (a function pointer), dlmodule
 * returns a handle to the module that exports the symbol.  If the
 * module containing 'function_address' is not found, dlmodule
 * returns NULL.
 */

void *dlmodule(void *fcnptr)
{
  /* Locate and return the module that exports the address range that
   * include fcnptr (which, of course, may be NULL).
   */

  return xflat_find_module_by_address((u_int32_t)fcnptr);

}

/* Give a instance of the dlfcndesc structure, dlfcncall will perform
 * the inter-module function call.  The return value of dlfcncall is the
 * return value of the called called function (dlfnccall will assert
 * any failures.  dlfcncall can only be used with pointers to functions
 * that conform to the requirements of type dlfcnptr_t (see above).
 */

void *dlfcncall(struct dlfcndesc *fcndesc, void *arg, ...)
{
  void *retval = NULL;

  if ((!fcndesc) || (!fcndesc->fcnptr) || (!fcndesc->module))
    {
      err("Null value received in function description");
    }
  else
    {
      va_list ap;
      void *arg1, *arg2, *arg3;
      struct xflat_module *module = (struct xflat_module*)fcndesc->module;

      /* We have the first parameter (arg), Extract the other parameters
       * from the stack */

      va_start(ap, arg);
      arg1 = va_arg(ap, void*);
      arg2 = va_arg(ap, void*);
      arg3 = va_arg(ap, void*);
      va_end(ap);

      /* Set the callee's address context.  Note the rest
       * of this function operates in the callees' address
       * space.  One return, the dyncall call logic that
       * got us here will restore the callers address space.
       * NOTE:  This function cannot be called from within
       * ldso without some wrapper logic to handle this
       * case.
       */

      xflat_set_dspace(module->dspace_start);

      /* Then call the function */

      retval = fcndesc->fcnptr(arg, arg1, arg2, arg3);
    }
  return retval;
}

