/***********************************************************************
 * xflat/ldso/xflat-ldso/xflat_syscall.c
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include <stdarg.h>
#include <asm/mman.h>
#include <linux/types.h>
#include "xflat_syscalls.h"
#define _SYS_STAT_H
#include <asm/stat.h> 

/***********************************************************************
 * Private Functions
 ***********************************************************************/

static _syscall1(void *, xflat_mmap, u_int32_t *, buffer);

/***********************************************************************
 * Public Functions
 ***********************************************************************/

_syscall1(void, xflat_exit, int, status);
_syscall1(int, xflat_close, int, fd);


void *xflat_mmap_frontend(void * addr, u_int32_t size, int prot,
			  int flags, int fd, u_int32_t offset)
{
  u_int32_t buffer[6];

  buffer[0] = (u_int32_t) addr;
  buffer[1] = (u_int32_t) size;
  buffer[2] = (u_int32_t) prot;
  buffer[3] = (u_int32_t) flags;
  buffer[4] = (u_int32_t) fd;
  buffer[5] = (u_int32_t) offset;
  return (void *) xflat_mmap(buffer);
}

_syscall2(int, xflat_open, const char *, fn, int, flags);
_syscall3(u_int32_t, xflat_write, int, fd, 
	  const void *, buf, u_int32_t, count);
_syscall3(u_int32_t, xflat_read, int, fd, 
	  const void *, buf, u_int32_t, count);
_syscall3(u_int32_t, xflat_lseek, int, fd,
	  u_int32_t, offset, int, whence);

#if 0
_syscall3(int, xflat_mprotect, const void *, addr,
	  u_int32_t, len, int, prot);
_syscall2(int, xflat_stat, const char *, file_name, struct stat *, buf);
#endif

_syscall2(int, xflat_munmap, void *, start, u_int32_t, length);

#if 0
_syscall0(uid_t, xflat_getuid);
_syscall0(uid_t, xflat_geteuid);
_syscall0(gid_t, xflat_getgid);
_syscall0(gid_t, xflat_getegid);

/* Not an actual syscall, but we need something in assembly to say whether
 * this is OK or not.
 */

int xflat_suid_ok(void)
{
  uid_t uid, euid, gid, egid;

  uid  = xflat_getuid();
  euid = xflat_geteuid();
  gid  = xflat_getgid();
  egid = xflat_getegid();

  if(uid == euid && gid == egid)
    return 1;
  else
    return 0;
}

_syscall3(int, xflat_readlink, const char *, path,
	  char *, buf, size_t, bufsiz);
#endif
