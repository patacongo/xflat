/***********************************************************************
 * xflat/ldso/xflat-ldso/xflat_read.h
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#ifndef _XFLAT_READ_H_
#define _XFLAT_READ_H_

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include "xflat.h"
#include "xflat_modules.h"

/***********************************************************************
 * Definitions
 ***********************************************************************/

/***********************************************************************
 * Public Types
 ***********************************************************************/

/***********************************************************************
 * Public Data
 ***********************************************************************/

/***********************************************************************
 * Public Function Prototypes
 ***********************************************************************/

extern int
xflat_get_module_path_list(struct xflat_module *import_module,
			   char **path_list, u_int16_t *npathes);

extern int
xflat_get_module_name_list(struct xflat_module *import_module,
			   char **name_list, u_int16_t *nnames);

extern struct xflat_module *
xflat_load_shared_library(const char *full_libname,
			  char **path_list, int npathes);

extern int
xflat_load_dependent_libraries(struct xflat_module *import_module);

#endif /* _XFLAT_READ_H_ */


