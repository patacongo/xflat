/***********************************************************************
 * xflat/ldso/xflat-ldso/xflat_syscall.h
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#ifndef _XLFAT_SYSCALL_H_
#define _XLFAT_SYSCALL_H_

/***********************************************************************
 * Definitions
 ***********************************************************************/

#include <asm/mman.h>
#include "xflat.h"     /* For the correct u_int32_t */

/***********************************************************************
 * Definitions
 ***********************************************************************/

/* Encoding of the file mode */

#define	S_IFMT		0170000	/* These bits determine file type */

/* File types */

#define	S_IFDIR		0040000	/* Directory */
#define	S_IFCHR		0020000	/* Character device */
#define	S_IFBLK		0060000	/* Block device */
#define	S_IFREG		0100000	/* Regular file */
#define	S_IFIFO		0010000	/* FIFO */
#define	S_IFLNK		0120000	/* Symbolic link */
#define	S_IFSOCK	0140000	/* Socket */

/* Protection bits */

#define	S_ISUID		04000	/* Set user ID on execution */
#define	S_ISGID		02000	/* Set group ID on execution */
#define	S_ISVTX		01000	/* Save swapped text after use (sticky) */
#define	S_IREAD		0400	/* Read by owner */
#define	S_IWRITE	0200	/* Write by owner */
#define	S_IEXEC		0100	/* Execute by owner */

/* Arguments to lseek */

# define SEEK_SET	0	/* Seek from beginning of file.  */
# define SEEK_CUR	1	/* Seek from current position.  */
# define SEEK_END	2	/* Seek from end of file.  */

#ifndef MAP_ANONYMOUS
#error MAP_ANONYMOUS not defined and suplementary value not known
#endif

#define O_RDONLY        0x0000

#define xflat_mmap_check_error(__res)	\
	(((int)__res) < 0 && ((int)__res) >= -4096)

/***********************************************************************
 * Public Functions
 ***********************************************************************/

extern void
xflat_exit(int status);

extern int
xflat_close(int fd);

extern void *
xflat_mmap_frontend(void *addr, u_int32_t size, int prot,
		    int flags, int fd, u_int32_t offset);
#define xflat_mmap xflat_mmap_frontend

extern int
xflat_open(const char *fn, int flags);

extern u_int32_t
xflat_write(int fd, const void *buf, u_int32_t count);

extern u_int32_t
xflat_read(int fd, const void *buf, u_int32_t count);

extern u_int32_t
xflat_lseek(int fd, u_int32_t offset, int whence);

#if 0
extern int
xflat_mprotect(const void *addr, u_int32_t len, int prot);

extern int
xflat_stat(const char *file_name, struct stat *buf);
#endif

extern int
xflat_munmap(void *start, u_int32_t length);

#if 0
extern uid_t
xflat_getuid(void);

extern uid_t
xflat_geteuid(void);

extern gid_t
xflat_getgid(void);

extern gid_t
xflat_getegid(void);

extern int
xflat_suid_ok(void);

extern int
xflat_readlink(const char *path, char *buf, u_int32_t bufsiz);
#endif

#endif /* _XLFAT_SYSCALL_H_ */
