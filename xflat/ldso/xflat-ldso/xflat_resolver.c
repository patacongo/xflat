/***********************************************************************
 * xflat/ldso/xflat-ldso/xflat_resolver.c
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include "xflat.h"
#include "xflat_loader.h"
#include "xflat_modules.h"
#include "xflat_util.h"
#include "xflat_sysdeps.h"
#include "xflat_syscall.h"
#include "xflat_resolver.h"

/***********************************************************************
 * Private Types
 ***********************************************************************/

/************************************************************************
 * Private Functions
 ************************************************************************/

/***********************************************************************
 * Public Functions
 ***********************************************************************/

/***********************************************************************
 * xflat_resolver.c
 ***********************************************************************/

/* This function is called from the assembly language logic
 * _dl_linux_resolve (see arm/xflat_sysdeps.S) when the first
 * reference to an unresolved, imported function symbol is
 * made.  It receives the following information from the
 * assembly language:
 *
 * import = Absolute address of dyncall info structure in the
 *    caller's data segment (includes caller's PIC base register
 *    value).
 * picb = PIC base register of caller
 * top_of_dynstack  = Top of dynamic stack (for debug only).
 */

void
xflat_linux_resolver(struct xflat_import *import, u_int32_t picb,
		     u_int32_t *top_of_dynstack)
{
  u_int32_t exported_value;
  u_int32_t exporter_dspace;
  int status;

  dbg("Resolving function name=\"%s\"", (char*)import->function_name);

  /* Find the symbol that satisfies this request.  NOTE:  This
   * call assumes that the function_name value was fixed up when
   * the module was loaded by ld-xflat.so.
   */

  status = xflat_find_symbol(NULL, (char*)import->function_name,
			     &exported_value, &exporter_dspace);
  if (status != 0)
    {
      err("Could not find exporter of symbol \"%s\"",
	  (char*)import->function_name);
      xflat_exit(47);
    }

  import->data_segment     = xflat_dspace_to_picbase(exporter_dspace);
  import->function_address = exported_value;

  dbg("Set function address=0x%08x DSpace=0x%08x picbase=%08x",
      exported_value, exporter_dspace, import->data_segment);
}
