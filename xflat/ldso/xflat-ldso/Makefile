########################################################################
# xflat/ldso/xflat-ldso/Makefile
#
# Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
# Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
# Author: Gregory Nutt <spudmonkey@racsa.co.cr>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name Cadenux nor the names of its contributors may be
#    used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
########################################################################

WD			= ${shell pwd}
TOPDIR			= $(WD)/../..

include ../Make.defs

LDSO_DEFINES		= -DLDSO_BUILD
LDSO_INCLUDES		= -Iarm -I$(ARCH_INC3_DIR)

LIBNAME			= ld-xflat.so
BIN			= $(LIBNAME).1

CSRCS			= xflat_loader.c xflat_modules.c xflat_read.c \
			  xflat_user.c xflat_syscall.c xflat_util.c \
			  xflat_resolver.c xflatlib_load.c xflatlib_verify.c
COBJS			= $(CSRCS:.c=$(ARCHSUFFIX))

ASMSRCS			= xflat_sysdeps.S
ASMOBJS			= $(ASMSRCS:.S=$(ARCHSUFFIX))

BASE_OBJS		= $(ASMOBJS) $(COBJS)
DERIVED_OBJS		= $(BIN)-thunk$(ARCHSUFFIX)

EXPORTS			= -X dlopen -X dlsym -X dlmodule -X dlfcncall

VPATH			+= arm

all: $(BIN)

$(CSRCS):

$(COBJS): %$(ARCHSUFFIX): %.c
	$(ARCHCC) -c $(ARCHCFLAGS) $(LDSO_INCLUDES) $(LDSO_DEFINES) $< -o $@

$(ASMOBJS): %$(ARCHSUFFIX): %.S
	$(ARCHCC) -c $(ARCHCFLAGS) $(LDSO_INCLUDES) $(LDSO_DEFINES) $< -o $@

$(BIN).r1: $(BASE_OBJS)
	$(ARCHCC) -o $(BIN).r1 $(ARCHLDFLAGS1) $(BASE_OBJS)

$(BIN)-thunk.S: $(BIN).r1
	$(LDELFLIB) -v $(EXPORTS) -o $(BIN)-thunk.S $(BIN).r1

$(BIN)-thunk$(ARCHSUFFIX): $(BIN)-thunk.S
	$(ARCHCC) $(ARCHCFLAGS) -c -o $(BIN)-thunk$(ARCHSUFFIX) $(BIN)-thunk.S

$(BIN).r2: $(BASE_OBJS) $(DERIVED_OBJS)
	$(ARCHCC) -o $(BIN).r2 $(ARCHLDFLAGS2) $(BASE_OBJS) $(DERIVED_OBJS)

$(BIN): $(BIN).r2
	$(LDELF2XFLT) -l -e xflat_boot -o $@ $@.r2
	@rm -f $(BIN).r1 $(BIN).r2

clean: 
	rm -f $(BIN) $(BIN).r1 $(BIN).r2 *$(ARCHSUFFIX) $(BIN)-thunk.S core

user_install:
	install -D $(BIN) $(USER_LIB_DIR)/$(BIN)
	ln -sf $(BIN) $(USER_LIB_DIR)/$(LIBNAME)

root_install:
	install -D $(BIN) $(ARCH_LIB_DIR)/$(BIN)
	ln -sf $(BIN) $(ARCH_LIB_DIR)/$(LIBNAME)
