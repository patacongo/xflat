/***********************************************************************
 * xflat/ldso/xflat-ldso/xflat_modules.h
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#ifndef _XFLAT_MODULES_H_
#define _XFLAT_MODULES_H_

/***********************************************************************
 * Conditional Compilation
 ***********************************************************************/

/***********************************************************************
 * Definitions
 ***********************************************************************/

/* (See notes in dlfcn.h or in xflat_user.c) */

#ifndef RTLD_NEXT
#define RTLD_NEXT	((void*)-1)
#endif

/* Types of modules that are supported */

enum libtype_e
{
  XFLAT_LIBTYPE_NONE = 0,
  XFLAT_PROGRAM      = 1,
  XFLAT_LIBRARY      = 2,
  XFLAT_LOADER       = 3
};

/* Module flags */

enum modflag_e
{
  MODFLAG_INIT_FUNCS_CALLED = 0x0001,
  MODFLAG_IMPORTS_BOUND     = 0x0002
};

#define MAX_MODULE_NAME_SIZE      16

/***********************************************************************
 * Public Types
 ***********************************************************************/

/* The following structure describes one loaded module. */

struct xflat_module
{
  /* These fields are used to implement a doubly linked list of
   * of modules descriptors.
   */

  struct xflat_module *next;
  struct xflat_module *prev;

  /* These fields characterize the type/state of the module */

  unsigned char        module_type;    /* See enum libtype_e */
  u_int16_t            flags;          /* See enum modflag_e */

  /* These fields describe the memory organization of the module. */

  u_int32_t            ispace_start;
  u_int32_t            ispace_size;
  u_int32_t            dspace_start;
  u_int32_t            dspace_size;

  /* The last MAX_MODULE_NAME_SIZE bytes of the module name */

  char                 name[MAX_MODULE_NAME_SIZE];
};

typedef int (*func_type)(struct xflat_module *module, void *arg);

/***********************************************************************
 * Public Data
 ***********************************************************************/

/* This is the start of the linked list that describes all of the files
 * loaded into the process.
 */

extern struct xflat_module *xflat_loaded_modules;

/***********************************************************************
 * Public Function Prototypes
 ***********************************************************************/

extern void
xflat_initialize_allocators(void);

extern struct xflat_module *
xflat_allocate_module(void);

extern void
xflat_free_module(struct xflat_module *module);

extern void
xflat_initialize_module(struct xflat_module *module,
			const char *module_name,
			enum libtype_e module_type,
			u_int32_t ispace_start,
			u_int32_t ispace_size,
			u_int32_t dspace_start,
			u_int32_t dspace_size);

extern void
xflat_add_loaded_module(struct xflat_module *module);

extern void
xflat_add_module(struct xflat_module *module);

extern struct xflat_module *
xflat_find_module_by_name(const char *module_name);

extern struct xflat_module *
xflat_find_module_by_address(u_int32_t address);

extern void
traverse_loaded_modules(void *arg1, func_type fn);

extern int
xflat_find_symbol_in_module(struct xflat_module *module,
			    const char *symbol_name,
			    u_int32_t *symbol_value);
extern int
xflat_find_symbol(struct xflat_module *import_module,
		  const char *import_name,
		  u_int32_t *export_value, u_int32_t *exporter_dspace);

#endif /* _XFLAT_MODULES_H_ */


