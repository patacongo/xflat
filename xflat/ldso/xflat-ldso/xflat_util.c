/***********************************************************************
 * xflat/ldso/xflat-ldso/xflat_util.c
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

/***********************************************************************
 * Included files 
 ***********************************************************************/

#include <stdarg.h>

#include "xflat.h"
#include "xflat_syscall.h"
#include "xflat_util.h"

/***********************************************************************
 * Definitions
 ***********************************************************************/

/***********************************************************************
 * Private Data
 ***********************************************************************/

/***********************************************************************
 * Private Function Prototypes
 ***********************************************************************/

/***********************************************************************
 * Private Functions
 ***********************************************************************/

/***********************************************************************
 * Public Functions
 ***********************************************************************/

/***********************************************************************
 * xflat_ltoa
 ***********************************************************************/

static char*
xflat_ltoa(char *local, u_int32_t i)
{
  /* 21 digits plus null terminator, good for 64-bit or smaller ints */

  char *p = &local[22];
  *p-- = '\0';
  do
    {
      *p-- = '0' + i % 10;
      i /= 10;
    }
  while (i > 0);
  return p + 1;
}

/***********************************************************************
 * xflat_ltoahex
 ***********************************************************************/

static char *
xflat_ltoahex(char *local, u_int32_t i)
{
  /* 21 digits plus null terminator, good for 64-bit or smaller ints */

  char *p = &local[22];
  *p-- = '\0';
  do
    {
      char temp = i % 0x10;
      if (temp <= 0x09)
	*p-- = '0' + temp;
      else
	*p-- = 'a' - 0x0a + temp;
      i /= 0x10;
    }
  while (i > 0);
  return p + 1;
}

/***********************************************************************
 * xflat_printf
 ***********************************************************************/

/* Minimal printf which handles only %s, %d, and %x */

void
xflat_printf(int fd, const char *fmt, ...)
{
  int num;
  va_list args;
  char *start, *ptr, *string;
  char buf[2048];

  start = ptr = buf;

  if (!fmt)
    return;

  if (xflat_strlen(fmt) >= (sizeof(buf) - 1))
    xflat_write(fd, "(overflow)\n", 10);

  xflat_strcpy(buf, fmt);
  va_start(args, fmt);

  while (start)
    {
      while (*ptr != '%' && *ptr)
	{
	  ptr++;
	}

      if (*ptr == '%')
	{
	  *ptr++ = '\0';
	  xflat_write(fd, start, xflat_strlen(start));

	  /* Skip over numerics and 'l' following the '%' */

	  while (((*ptr >= '0') && (*ptr <= '9')) || (*ptr == 'l')) ptr++;

	  /* We expect the next character to be the format
	   * specifier.
	   */

	  switch (*ptr++)
	    {
	    case 's':
	      string = va_arg(args, char *);

	      if (!string)
		xflat_write(fd, "(null)", 6);
	      else
		xflat_write(fd, string, xflat_strlen(string));
	      break;

	    case 'i':
	    case 'd':
	      {
		char tmp[22];
		num = va_arg(args, int);

		string = xflat_ltoa(tmp, num);
		xflat_write(fd, string, xflat_strlen(string));
		break;
	      }
	    case 'p':
	    case 'x':
	    case 'X':
	      {
		char tmp[22];
		num = va_arg(args, int);

		string = xflat_ltoahex(tmp, num);
		xflat_write(fd, string, xflat_strlen(string));
		break;
	      }
	    default:
	      xflat_write(fd, "(null)", 6);
	      break;
	    }
	  start = ptr;
	}
      else
	{
	  xflat_write(fd, start, xflat_strlen(start));
	  start = NULL;
	}
    }
  return;
}

/***********************************************************************
 * xflat_str_dup
 ***********************************************************************/

#if 0 /* not used */
char *
xflat_strdup(const char *string)
{
  char *retval;
  int len;

  len = xflat_strlen(string);
  retval = xflat_mallocy(len + 1);
  xflat_strcpy(retval, string);
  return retval;
}
#endif

/***********************************************************************
 * xflat_strlen
 ***********************************************************************/

u_int32_t
xflat_strlen(const char *str)
{
  char *ptr = (char *) str;

  while (*ptr)
    ptr++;
  return (ptr - str);
}

/***********************************************************************
 * xflat_strcat
 ***********************************************************************/

char *
xflat_strcat(char *dst, const char *src)
{
  char *ptr = dst;

  while (*ptr)
    ptr++;

  while (*src)
    *ptr++ = *src++;
  *ptr = '\0';

  return dst;
}

/***********************************************************************
 * xflat_strcpy
 ***********************************************************************/

char *
xflat_strcpy(char *dst,const char *src)
{
  char *ptr = dst;

  while (*src)
    {
      *dst++ = *src++;
    }
  *dst = '\0';

  return ptr;
}
 
/***********************************************************************
 * xflat_strncpy
 ***********************************************************************/

char *
xflat_strncpy(char *dst, const char *src, u_int32_t n)
{
  char *ptr = dst;

  while((*src) && (n > 0))
    {
      *dst++ = *src++; n--;
    }

  if (n > 0)
    {
      *dst = '\0';
    }

  return ptr;
}
 
/***********************************************************************
 * xflat_strcmp
 ***********************************************************************/

int xflat_strcmp(const char *s1, const char *s2)
{
  unsigned char c1, c2;

  do
    {
      c1 = (unsigned char) *s1++;
      c2 = (unsigned char) *s2++;
      if (c1 == '\0')
	return c1 - c2;
    }
  while (c1 == c2);

  return c1 - c2;
}

/***********************************************************************
 * xflat_strncmp
 ***********************************************************************/

int xflat_strncmp(const char *s1, const char *s2, u_int32_t len)
{
  unsigned char c1 = '\0';
  unsigned char c2 = '\0';

  while (len > 0)
    {
      c1 = (unsigned char) *s1++;
      c2 = (unsigned char) *s2++;
      if (c1 == '\0' || c1 != c2)
	return c1 - c2;
      len--;
    }

  return c1 - c2;
}

/***********************************************************************
 * xflat_strchr
 ***********************************************************************/

#if 0 /* Not used */
char *xflat_strchr(const char *str, int c)
{
  char ch;

  do
    {
      if ((ch = *str) == c)
	return (char *) str;
      str++;
    }
  while (ch);

  return 0;
}
#endif

/***********************************************************************
 * xflat_strrchr
 ***********************************************************************/

#if 0 /* Not used */
char *xflat_strrchr(const char *str, int c)
{
  char *prev = 0;
  char *ptr = (char *) str;

  while (*ptr != '\0')
    {
      if (*ptr == c)
	prev = ptr;
      ptr++;  
    }
  if (c == '\0')
    return(ptr);
  return(prev);
}
#endif

/***********************************************************************
 * xflat_memcpy
 ***********************************************************************/

void *xflat_memcpy(void *dst, const void *src, u_int32_t len)
{
  char *a = dst;
  const char *b = src;

  while (len--)
    *a++ = *b++;

  return dst;
}

/***********************************************************************
 * xflat_memcmp
 ***********************************************************************/

#if 0 /* Not used */
int xflat_memcmp(const void *s1,const void *s2, u_int32_t len)
{
  unsigned char *c1 = (unsigned char *)s1;
  unsigned char *c2 = (unsigned char *)s2;

  while (len--)
    {
      if (*c1 != *c2) 
	return *c1 - *c2;
      c1++;
      c2++;
    }
  return 0;
}
#endif

/***********************************************************************
 * xflat_memset
 ***********************************************************************/

void *xflat_memset(void *str, int c, u_int32_t len)
{
  char *a = str;

  while (len--)
    *a++ = c;

  return str;
}

/***********************************************************************
 * xflat_basename
 ***********************************************************************/

const char *
xflat_basename(const char *path)
{
  const char *basename = path;
  const char *ptr;

  /* Get the basename from the full pathname.  We assume that 
   * the basename starts immediately after the last '/'.  NOTE:
   * the string is NOT dup'ed.
   */

  for (ptr = path; *ptr; ptr++)
    {
      if (*ptr == '/')
	basename = ptr + 1;
    }
  return basename;
}

#ifndef NETWORK_IS_HOST_ORDER
/***********************************************************************
 * xflat_ntohl
 ***********************************************************************/

u_int32_t
xflat_ntohl(u_int32_t netlong)
{
#ifdef __LITTLE_ENDIAN
  return ((( netlong        & 0x000000ff) << 24) |
	  (((netlong >> 8)  & 0x000000ff) << 16) |
	  (((netlong >> 16) & 0x000000ff) << 8) |
	  (( netlong >> 24) & 0x000000ff));
#else
  return netlong;
#endif
}

/***********************************************************************
 * xflat_htohs
 ***********************************************************************/

u_int16_t
xflat_ntohs(u_int16_t netshort)
{
#ifdef __LITTLE_ENDIAN
  return (((netshort        & 0x000000ff) << 8) |
	  ((netshort >> 8)  & 0x000000ff));
#else
  return netshort;
#endif
}
#endif

