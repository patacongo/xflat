/***********************************************************************
 * xflat/ldso/xflat-ldso/xflat_loader.c
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

/***********************************************************************
 * Included files 
 ***********************************************************************/

#include "xflatlib.h"
#include "xflat_loader.h"
#include "xflat_modules.h"
#include "xflat_read.h"
#include "xflat_util.h"
#include "xflat_syscall.h"
#include "xflat_sysdeps.h"

/***********************************************************************
 * Definitions
 ***********************************************************************/

/* This value, if defined, will be added to the beginning of the loader's
 * default pathname.
 */

#ifndef XFLAT_TARGET_PREFIX  /* If it is not defined, */
# define XFLAT_TARGET_PREFIX /* Define it to be nothing */
#endif

#define XFLAT_HDR_SIZE sizeof(struct xflat_hdr)

/***********************************************************************
 * Public Data
 ***********************************************************************/

const char *xflat_progname = NULL;
const char *xflat_ldrname   = NULL;

/***********************************************************************
 * Private Data
 ***********************************************************************/

static struct xflat_module    ldr_module;
static struct xflat_module    prog_module;

static start_entry_t          xflat_program_start;

/***********************************************************************
 * Private Constant Data
 ***********************************************************************/

static const char xflat_default_progname[] = "";
static const char xflat_default_ldrname[]  = \
   XFLAT_TARGET_PREFIX XFLAT_DEFAULT_LOADER;

/***********************************************************************
 * Private Function Prototypes
 ***********************************************************************/

/***********************************************************************
 * Private Functions
 ***********************************************************************/

/***********************************************************************
 * xflat_get_progname
 ***********************************************************************/

static inline void
xflat_get_progname(const char *progname)
{
  if (progname)
    xflat_progname = progname;
  else
    xflat_progname = xflat_default_progname;
  dbg("Program name is \"%s\"", xflat_progname);
}

/***********************************************************************
 * xflat_get_ldrname
 ***********************************************************************/

static inline void
xflat_get_ldrname(u_int32_t ispace)
{
  /* Get the file offset to the loader name */

  u_int32_t loader = GET_LOADER(ispace);
  if (loader)
    {
      /* The actual loader string is location in memory at the
       * file offset into into the allocated ispace.
       */

      xflat_ldrname = (const char*)(loader + ispace);
    }
  else
    {
      xflat_ldrname = xflat_default_ldrname;
    }

  dbg("Loader name is \"%s\"", xflat_ldrname);
}

/***********************************************************************
 * xflat_register_module
 ***********************************************************************/

static inline void
xflat_register_module(struct xflat_module *module, const char *module_name,
		      u_int32_t ispace_start, u_int32_t dspace_start,
		      u_int32_t dspace_size, int module_type)
{
  /* The start of ispace is also the address of the xFLT header.
   * so we can use this to get the size of the ispace alloction.
   */

  u_int32_t ispace_size = GET_ISPACE_SIZE(ispace_start);

  /* Initialize the module structure and add it to the loaded
   * modules list.
   */

  xflat_initialize_module(module, module_name, module_type,
			  ispace_start, ispace_size,
			  dspace_start, dspace_size);
  xflat_add_module(module);
}

/***********************************************************************
 * xflat_do_load_library
 ***********************************************************************/

static int
xflat_do_load_library(struct xflat_module *import_module, void *arg)
{
  if (xflat_load_dependent_libraries(import_module))
    {
      err("Could not load shared libraries needed by \"%s\"",
	  import_module->name);
      xflat_exit(16);
    }
  return 0;
}
  
/***********************************************************************
 * xflat_load_initial_libraries
 ***********************************************************************/

static inline void xflat_load_initial_libraries(void)
{
  traverse_loaded_modules(NULL, xflat_do_load_library);
}
  
/***********************************************************************
 * xflat_bind_module
 ***********************************************************************/

static int xflat_bind_module(struct xflat_module *import_module)
{
  struct xflat_import *import_symbols;
  char                *import_name;
  u_int32_t            ispace;
  u_int32_t            import_offset;
  u_int32_t            export_value;
  u_int32_t            exporter_dspace;
  u_int16_t            nimports;
  int                  status;
  int                  retval = 0;
  int                  i;

  /* Get the ISpace load address of the module.  The xFLT
   * header is the first thing at the beginning of the ISpace.
   */

  ispace = import_module->ispace_start;

  /* From this, we can get the offset to the list of symbols imported
   * by this module and the number of symbols imported by this module.
   */

  import_offset = GET_IMPORT_SYMBOLS(ispace);
  nimports      = (GET_IMPORT_COUNT(ispace));

  /* Skip this module if it has no imports */

  if ((import_offset != 0) && (nimports > 0))
    {
      /* If non-zero, the value of the imported symbol list that we
       * get from the header is a file offset.  We will have to
       * convert this to an offset into the DSpace segment to
       * get the pointer to the beginning of the imported symbol
       * list.
       */

      import_symbols = (struct xflat_import*)
	(import_offset                      /* File offset */
	 - import_module->ispace_size       /* - size of text segment */
	 + import_module->dspace_start      /* + DSpace alloc address */
	 + XFLAT_DATA_OFFSET);              /* + ldso reserved memory */

      DBG_ASSERT(IS_DSPACE(import_module, import_symbols),
		 "import_symbols(0x%p) not in DSpace",
		 import_symbols);

      /* Now, search the list of imported symbols and attempt to
       * bind this symbol to the value exported by another module.
       */

      for (i = 0; i < nimports; i++)
	{
	  /* Get a pointer to the imported symbol name.  The name itself
	   * lies in the TEXT segment.  But the reference to the name
	   * lies in DATA segment.  Therefore, the name reference should
	   * have been relocated when the module was loaded.
	   */

	  import_name = (char*)import_symbols[i].function_name;

	  DBG_ASSERT(IS_ISPACE(import_module, import_name),
		     "import_name(0x%p) not in ISpace",
		     import_name);

	  /* Find the exported symbol value for this this symbol name. */

	  status = xflat_find_symbol(import_module, import_name,
				     &export_value, &exporter_dspace);
	  if (status != 0)
	    {
	      warn("Exporter of symbol \"%s\" needed by \"%s\" not found",
		   import_name, import_module->name);

	      /* We'll set things up so that we'll try one more time
	       * the first time that the imported function is called.
	       */

	      export_value = (u_int32_t)xflat_linux_resolve;
	      exporter_dspace = ldr_module.dspace_start;
	    }

	  /* And put this into the import structure.  These values
	   * will be used to perform the "thunk" when an inter-module
	   * call is made.
	   */

	  import_symbols[i].function_address =
	    export_value;

	  import_symbols[i].data_segment =
	    xflat_dspace_to_picbase(exporter_dspace);

	  dbg("Thunk for import \"%s\" is {0x%x, 0x%x}",
	      import_name,
	      import_symbols[i].function_address,
	      import_symbols[i].data_segment);
	}
    }
  return retval;
}

/***********************************************************************
 * xflat_one_time_bind
 ***********************************************************************/

static int
xflat_one_time_bind(struct xflat_module *module, void *arg)
{
  struct xflat_ldso_info *ldso_info;

  /* Check if we have already processed this module on a previous call */

  if ((module->flags & MODFLAG_IMPORTS_BOUND) == 0)
    {
      /* Set up some special information at the beginning of the
       * modules data space.  This will be used by ld.so later
       * to help resolve PID addressing issues.
       */

      /* Get a pointer to the special structure at the beginning
       * of the modules data segment:
       */

      ldso_info = (struct xflat_ldso_info*)module->dspace_start;
      if (ldso_info)
	{
	  /* Put a pointer to ld.so's DSpace at the beginning
	   * of the DSpace of every module.
	   */

	  ldso_info->dspace = ldr_module.dspace_start;
	}

      /* Then bind all of the imported symbols to the address
       * (and pic base) provided by the exporting module.
       */

      if (xflat_bind_module(module) != 0)
	{
	  warn("Module \"%s\" contains uresolved symbols",
	       module->name);
	}

      /* Make sure that we do try the bindings more than once. */

      module->flags |= MODFLAG_IMPORTS_BOUND;
    }
  return 0;
}

/***********************************************************************
 * xflat_one_time_init
 ***********************************************************************/

static int
xflat_one_time_init(struct xflat_module *module, void *arg)
{
  u_int32_t module_ispace;
  u_int32_t ldr_dspace;
  u_int32_t entry_offset;

  /* Keep this on the stack where we can find it when we
   * do not have access to our own pic base register.
   */

  ldr_dspace = ldr_module.dspace_start;

  /* Check if we have already processed this module on a previous call */

  if ((module->flags & MODFLAG_INIT_FUNCS_CALLED) == 0)
    {
      /* Call the module's initialization entry point. But
       * don't call the entry point to the loader (that's us) or
       * to the program (we'll call into the program entry
       * point explicitly).
       */

      if ((module->module_type != XFLAT_LOADER) &&
	  (module->module_type != XFLAT_PROGRAM))
	{
	  /* Get the file offset to the entry point */

	  module_ispace = module->ispace_start;
	  entry_offset  = GET_ENTRY_POINT(module_ispace);

	  /* An invalid (zero) offset means that there is
	   * no entry point for this module.
	   */

	  if (entry_offset)
	    {
	      u_int32_t entry_point;

	      /* Select the module's PIC base.  NOTE: This code
	       * must not attempt to access its global variables
	       * until the loader PIC base is restored.
	       */

	      entry_point = entry_offset + module_ispace;
	      dbg("Calling module \"%s\" entry point at 0x%08x",
		  module->name, entry_point);

	      DBG_ASSERT(IS_ISPACE(module, entry_point),
			 "entry_point(0x%x) not in ISpace",
			 entry_point)

	      xflat_set_dspace(module->dspace_start);

#if !defined(SIMULATION_BUILD)

	      /* Call the init function using the module's PID
	       * base addres.
	       */

	      ((library_entry_t)(entry_offset + module_ispace))();
#endif
	      /* Restore the loader's PIC base */

	      xflat_set_dspace(ldr_dspace);
	    }
	}

      /* Make sure that we don't call this more than once */

      module->flags |= MODFLAG_INIT_FUNCS_CALLED;
    }
  return 0;
}

/***********************************************************************
 * Public Functions
 ***********************************************************************/

/***********************************************************************
 * xflat_loader
 ***********************************************************************/

/* When we enter this piece of code, the program stack looks like this:
 *
 *    TOP->argc                Argument count    (integer)
 *         argv[0...(arc-1)]   Program arguments (pointers)
 *         NULL                Marks end of arguments
 *         env[0...N]          Environment variables (pointers)
 *         NULL                Marks end of environment variables
 *         loader ispace       Address of loader ISpace (xFLT header)
 *         loader dspace       Address of loader DSpace
 *         loader dspace size  Size of the allocated loader DSpace
 *         program ispace      Address of program ISpace (xFLT header)
 *         program dspace      Address of program DSpace
 * BOTTOM->program dspace size Size of the allocated program DSpace
 *
 */

start_entry_t
xflat_loader(u_int32_t args)
{
  u_int32_t     prog_ispace;
  u_int32_t     prog_dspace;
  u_int32_t     prog_dspace_size;
  u_int32_t     ldr_ispace;
  u_int32_t     ldr_dspace;
  u_int32_t     ldr_dspace_size;
  unsigned int  argc;
  char        **argv;
  char        **envp;
  u_int32_t    *sptr;

  /* First obtain the information on the stack that tells us more about
   * what binary is loaded, where it is loaded, etc, etc.  The macro
   * GET_ARGV does this and returns a value that points to the first,
   * valid element of the stack parameters.
   */

  GET_ARGV(sptr, args);

  /* Get the argc count*/

  argc             = *sptr++;

  /* Get the argv pointer */

  argv             = (char**)sptr;
  sptr            += argc;		/* Skip over the argv pointers */
  sptr++;				/* Skip over NULL at end of argv */

  /* Get the envp pointer */

  envp             = (char**)sptr;

  while (*sptr) sptr++;			/* Skip over the envp pointers */
  sptr++;				/* Skip over NULL at end of envp */

  dbg("Loader: argc=%d argv=0x%p envp=0x%p", argc, argv, envp);

  /* Locate the ISpace and DSpace allocations and associate xFLT headers */

  ldr_ispace       = *sptr++;
  ldr_dspace       = *sptr++;
  ldr_dspace_size  = *sptr++;

  prog_ispace      = *sptr++;
  prog_dspace      = *sptr++;
  prog_dspace_size = *sptr;

  dbg("Loader: ispace=0x%x dspace=0x%x size=0x%x",
      ldr_ispace, ldr_dspace, ldr_dspace_size);
  dbg("Program: ispace=0x%x dspace=0x%x size=0x%x",
      prog_ispace, prog_dspace, prog_dspace_size);

  /* Verify that both xFLT headers are valid */

  if ((xflat_verify_program((struct xflat_hdr*)prog_ispace) != 0) ||
      (xflat_verify_library((struct xflat_hdr*)ldr_ispace) != 0))
    {
      err("Invalid xFLT Header, cannot load program");
      xflat_exit(1);
    }

  /* Save the program and loader names (if provided) */

  xflat_get_progname(argv[0]);
  xflat_get_ldrname(ldr_ispace);

  /* Initialize our allocators */

  xflat_initialize_allocators();

  /* Initialize the xflat_module structurs associated with the
   * program and the loader.
   */

  xflat_register_module(&prog_module, xflat_progname,
			prog_ispace, prog_dspace, prog_dspace_size,
			XFLAT_PROGRAM);

  xflat_register_module(&ldr_module, xflat_ldrname,
			ldr_ispace, ldr_dspace, ldr_dspace_size,
			XFLAT_LOADER);

  /* We can now load shared libraries. */

  dbg("Loading dependent libraries");
  xflat_load_initial_libraries();

  /* Bind all of the addresses imported by the modules. */

  dbg("Binding all modules");
  xflat_bind_all_modules();

  /* Get all of the modules ready to run. */

  dbg("Calling module initializers");
  xflat_module_run_preparation();

  /* Get the address of the program entry point */

  xflat_program_start = (start_entry_t)
    (GET_ENTRY_POINT(prog_ispace) + prog_ispace);

  DBG_ASSERT(IS_ISPACE(&prog_module, xflat_program_start),
	     "xflat_program_start(0x%p) not in ISpace",
	     xflat_program_start);

  /* Transfer control to the application. */

  dbg("Calling application _start at 0x%p with picbase=0x%x",
      xflat_program_start,
      xflat_dspace_to_picbase(prog_module.dspace_start));

  START(xflat_program_start, prog_module.dspace_start);
}

/***********************************************************************
 * xflat_bind_all_modules
 ***********************************************************************/

void xflat_bind_all_modules(void)
{
  dbg("Binding shared libraries");
  traverse_loaded_modules(NULL, xflat_one_time_bind);
}

/***********************************************************************
 * xflat_module_run_preparation
 ***********************************************************************/

void xflat_module_run_preparation(void)
{
  dbg("Calling initialization entry for shared libraries");
  traverse_loaded_modules(NULL, xflat_one_time_init);
}
