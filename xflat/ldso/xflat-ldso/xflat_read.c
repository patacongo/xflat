/***********************************************************************
 * xflat/ldso/xflat-ldso/xflat_read.c
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include <errno.h>

#include "xflatlib.h"

#include "xflat_loader.h"
#include "xflat_modules.h"
#include "xflat_syscall.h"
#include "xflat_util.h"
#include "xflat_read.h"

/***********************************************************************
 * Definitions
 ***********************************************************************/

/* This value, if defined, will be added to the loaders added additional
 * pathes to the loader default pathes.
 */

#ifdef XFLAT_TARGET_PREFIX
# define DEFAULT_NUM_PATHES 4
#else
# define DEFAULT_NUM_PATHES 2
#endif

#define XFLAT_HDR_SIZE   sizeof(struct xflat_hdr)

/***********************************************************************
 * Global Variables
 ***********************************************************************/

/***********************************************************************
 * Private Function Prototypes
 ***********************************************************************/

static void *
xflat_vtbl_map(bin_handle_t handle, u_int32_t nbytes);

static void
xflat_vtbl_unmap(bin_handle_t address, u_int32_t nbytes);

static void *
xflat_vtbl_alloc(u_int32_t nbytes);

static file_handle_t
xflat_vtbl_open(bin_handle_t handle, const char *filename);

static int
xflat_vtbl_read(bin_handle_t bin_handle, file_handle_t file_handle,
	   char *dest, u_int32_t nbytes, u_int32_t fpos);

static void
xflat_vtbl_close(file_handle_t handle);

static struct xflat_module * 
xflat_find_library(const char *lib_name, const char * const *path_list,
		   int npathes);

static struct xflat_module *
xflat_load_xFLT_shared_library(const char *lib_pathname);

/***********************************************************************
 * Private Constant Data
 ***********************************************************************/

#ifdef XFLAT_TARGET_PREFIX
static const char lib_path1[] = XFLAT_TARGET_PREFIX "/usr/lib";
static const char lib_path2[] = XFLAT_TARGET_PREFIX "/lib";
#endif

static const char lib_path3[] = "/usr/lib";
static const char lib_path4[] = "/lib";

static const char * const default_path_list[DEFAULT_NUM_PATHES]  =
{
#ifdef XFLAT_TARGET_PREFIX
  lib_path1, lib_path2,
#endif
  lib_path3, lib_path4
};

static const struct xflat_vtbl ldso_vtbl =
{
  xflat_vtbl_map,
  xflat_vtbl_unmap, 
  xflat_vtbl_alloc,
  xflat_vtbl_unmap,
  xflat_vtbl_open,
  xflat_vtbl_read,
  xflat_vtbl_close
};

/***********************************************************************
 * Private Functions
 ***********************************************************************/

/***********************************************************************
 * These are the functions that are exported to the xflatlib
 ***********************************************************************/

static void *
xflat_vtbl_map(bin_handle_t bin_handle, u_int32_t nbytes)
{
  /* The handle is really the file descriptor */

  u_int32_t offset;
  int       fd = (int)bin_handle;
  void     *mapped_addr;

  /* Seek to the beginning of the file.  Hmmm... this seems to be
   * necessary for the "generic_mmap," but I thought the offset
   * argurment of the mmap call would take care of this?
   */

  offset = xflat_lseek(fd, 0, SEEK_SET);
  if (offset != 0)
    {
      err("Seek to begninning of file, offset=%d", offset);
      return NULL;
    }

  /* Perform the mmap call */

  mapped_addr = xflat_mmap(NULL,                       /* start */
			   nbytes,                     /* length */
			   PROT_EXEC|PROT_READ,        /* prot   */
			   MAP_PRIVATE|MAP_EXECUTABLE, /* flags  */
			   fd,                         /* fd     */
			   0);                         /* offset */
  if (xflat_mmap_check_error(mapped_addr))
    {
      err("%s: mmap failed", xflat_progname);
      return NULL;
    }
  return mapped_addr;
}
		     
static void
xflat_vtbl_unmap(void *address, u_int32_t nbytes)
{
  xflat_munmap(address, nbytes);
}

static void *
xflat_vtbl_alloc(u_int32_t nbytes)
{
  void *alloc_addr;

  /* Perform the mmap call */

  alloc_addr = xflat_mmap(NULL,                       /* start */
			  nbytes,                     /* length */
			  PROT_READ|PROT_WRITE,       /* prot   */
			  MAP_PRIVATE|MAP_ANONYMOUS,  /* flags  */
			  -1,                         /* fd     */
			  0);                         /* offset */
  if (xflat_mmap_check_error(alloc_addr))
    {
      err("%s: mmap failed", xflat_progname);
      return NULL;
    }
  return alloc_addr;
}

static file_handle_t 
xflat_vtbl_open(bin_handle_t bin_handle, const char *filename)
{
  int in_fd = xflat_open(filename, O_RDONLY);
  if (in_fd < 0)
    {
      /* Could not open it.  Either the file does not exist
       * or (more likely) this is not the right path.
       */

      dbg("Could not open \"%s\"", filename);
    }
  return (file_handle_t)in_fd;
}

static int
xflat_vtbl_read(bin_handle_t bin_handle, file_handle_t file_handle,
		char *dest, u_int32_t nbytes, u_int32_t fpos)
{
  u_int32_t nread;
  u_int32_t offset;
  int       fd;

  /* The handle is really the file descriptor */

  fd = (int)file_handle;

  /* Seek to the specified position */

  offset = xflat_lseek(fd, fpos, SEEK_SET);
  if (offset != fpos)
    {
      err("Seek to %d failed, offset=%d", fpos, offset);
      return -ENOEXEC;
    }

  /* And read the specified number of bytes */

  nread = xflat_read(fd, dest, nbytes);
  if (nread != nbytes)
    {
      err("Read only %d of %d bytes from fd=%d, offset=%d",
	  nread, nbytes, fd, fpos);
    }
  return nread;
}

static void
xflat_vtbl_close(file_handle_t handle)
{
  /* We'll take care of closing the file */
}

/***********************************************************************
 * search_for_library
 ***********************************************************************/

static struct xflat_module * 
xflat_find_library(const char *lib_name, const char * const *path_list,
		   int npathes)
{
  struct xflat_module *new_module;
  char                 full_pathname[2050];
  int                  i;

  /* Try every path in the path list until either we find the library
   * or we reach the end of the path list.
   */

  for (i = 0; i < npathes; i++)
    {
      /* Construct a file name of the form "path/lib_name" */

      xflat_strcpy(full_pathname, path_list[i]); 
      xflat_strcat(full_pathname, "/"); 
      xflat_strcat(full_pathname, lib_name);

      /* Try to load this library */

      new_module = xflat_load_xFLT_shared_library(full_pathname);
      if (new_module != NULL)
	{
	  /* Return early if successful */

	  return new_module;
	}
    }

  /* Library not found */

  return NULL;
}

/***********************************************************************
 * xflat_load_xFLT_shared_library
 ***********************************************************************/

static struct xflat_module *
xflat_load_xFLT_shared_library(const char *lib_pathname)
{
  struct xflat_load_info load_info;
  struct xflat_hdr       header;
  struct xflat_module   *module = NULL;
  int                    in_fd;
  int                    result;

  dbg("Loading \"%s\"", lib_pathname);

  /* First, check if this module has already been loaded. */

  module = xflat_find_module_by_name(lib_pathname);
  if (module)
    {
      dbg("Library \"%s\" is already loaded", lib_pathname);
      return module;
    }

  /* Try to open the library at this path */

  in_fd = xflat_open(lib_pathname, O_RDONLY);
  if (in_fd < 0)
    {
      /* Could not open it.  Either the library does not exist
       * or (more likely) this is not the right path.
       */

      dbg("Could not open library \"%s\"", lib_pathname);
      goto err_out;
    }

  /* Verify the xFLT header */

  xflat_read(in_fd, &header, sizeof(header));
  if (xflat_verify_library(&header) != 0)
    {
      err("Invalid binary format");
      goto err_out_with_open_file;
    }

  /* Initialize the xflat library */

  result = xflat_init((bin_handle_t)in_fd, (file_handle_t)in_fd,
		      &header, &ldso_vtbl, &load_info);
  if (result != 0)
    {
      err("Failed to initialize xflat library");
      goto err_out_with_open_file;
    }

  /* There should be no stack allocation for a library */

  load_info.stack_size = 0;

  /* Load the flat binary */

  result = xflat_load(&load_info);
  if (result != 0)
    {
      err("Failed to load xFLT binary");
      goto err_out_with_open_file;
    }

  dbg("Load %s:", lib_pathname);
  dbg("  TEXT: 0x%08x-0x%08x",
      load_info.ispace, load_info.ispace + load_info.ispace_size);
  dbg("  DATA: 0x%08x-0x%08x",
      load_info.dspace, load_info.dspace + load_info.data_size);
  dbg("  BSS:  0x%08x-0x%08x",
      load_info.dspace + load_info.data_size,
      load_info.dspace + load_info.data_size + load_info.bss_size);

  /* Allocate and initialize module structure */
      
  module = xflat_allocate_module();
  if (!module)
    {
      err("Could not allocate module structure");
      goto err_out_with_open_file;
    }

  xflat_initialize_module(module, lib_pathname, XFLAT_LIBRARY,
			  load_info.ispace, load_info.ispace_size,
			  load_info.dspace, load_info.dspace_size);

  /* Add the module to the loaded module list */

  xflat_add_module(module);

  /* Closing the module will NOT release the resources set up for the
   * file by mmap.
   */

 err_out_with_open_file:
  (void)xflat_close(in_fd);

 err_out:
  return module;
}

/***********************************************************************
 * Public Functions
 ***********************************************************************/

/***********************************************************************
 * xflat_get_module_path_list
 ***********************************************************************/

int
xflat_get_module_path_list(struct xflat_module *import_module,
			   char **path_list, u_int16_t *npathes)
{
  u_int32_t  ispace  = import_module->ispace_start;
  u_int16_t  src_npathes;
  u_int32_t *src_list;
  int        retval = 0;
  int        i;

  /* The xFLT header lies at the beginning of the module ISpace.
   * Extract the TEXT section offset to list of pathes from the
   * header. Then convert the list offset to a TEXT section
   * offset.
   */

  src_list = (u_int32_t*)(GET_LIB_PATHES(ispace) + ispace);

  /* Fetch the library path count from the header */

  src_npathes = GET_LPATH_COUNT(ispace);

  /* Sanity check the number of pathes.  The current xFLT definition
   * imposes a limit on the number of pathes that may be provided
   * in the binary.
   */

  if (src_npathes > XFLAT_MAXLIB_PATHES)
    {
      err("Too many pathes in list (%d)", src_npathes);
      retval = -EINVAL;
    }
  else
    {
      dbg("ispace=0x%x src_path_list=0x%p npathes=%d",
	  ispace, src_list, src_npathes);

      /* Then fetch each path and relocate it */

      for (i = 0; i < src_npathes; i++)
	{
	  /* Fetch the offset to the next path string, relocate
	   * it into the ispace load position, and return it
	   * into the caller-provided array.
	   */

	   path_list[i] = (char*)(src_list[i] + ispace + XFLAT_HDR_SIZE);
	   dbg("Path %d: offset=0x%x address=0x%p \"%s\"",
	       i+1, src_list[i], path_list[i], path_list[i]);
	}

      /* Return the number of pathes. */

      *npathes = src_npathes;

    }
  return retval;
}
  
/***********************************************************************
 * xflat_get_module_name_list
 ***********************************************************************/

int
xflat_get_module_name_list(struct xflat_module *import_module,
			   char **name_list, u_int16_t *nnames)
{
  u_int32_t  ispace  = import_module->ispace_start;
  u_int16_t  src_nnames;
  u_int32_t *src_list;
  int        retval = 0;
  int        i;

  /* The xFLT header lies at the beginning of the module ISpace.
   * Extract the TEXT section offset to list of names from the
   * header. Then convert the list offset to a TEXT section
   * offset.
   */

  src_list = (u_int32_t*)(GET_LIB_NAMES(ispace) + ispace);

  /* Fetch the library name count from the header */

  src_nnames = GET_LNAME_COUNT(ispace);

  /* Sanity check the number of names.  The current xFLT definition
   * imposes a limit on the number of names that may be provided
   * in the binary.
   */

  if (src_nnames > XFLAT_MAXLIB_NAMES)
    {
      err("Too many library filenames in list (%d)", src_nnames);
      retval = -EINVAL;
    }
  else
    {
      dbg("ispace=0x%x src_name_list=0x%p nnames=%d",
	  ispace, src_list, src_nnames);

      /* Then fetch each name and relocate it */

      for (i = 0; i < src_nnames; i++)
	{
	  /* Fetch the offset to the next name string, relocate
	   * it into the ispace load position, and return it
	   * into the caller-provided array.
	   */

	   name_list[i] = (char*)(src_list[i] + ispace + XFLAT_HDR_SIZE);
	   dbg("Name %d: offset=0x%x address=0x%p \"%s\"",
	       i+1, src_list[i], name_list[i], name_list[i]);
	}

      /* Return the number of names. */

      *nnames = src_nnames;

    }
  return retval;
}
  
/***********************************************************************
 * xflt_load_shared_library
 ***********************************************************************/

struct xflat_module *
xflat_load_shared_library(const char *full_pathname,
			  char **path_list, int npathes)
{
  struct xflat_module *new_module;
  const char *lib_name;

  /* Get the basename from the full library name.  We assume that 
   * the basename starts immediately after the last '/'
   */

  lib_name = xflat_basename(full_pathname);
  dbg("Searching for library: \"%s\"", lib_name);

  /* If the filename has any '/', try it just as provided. */

  if (lib_name != full_pathname)
    {
      dbg("Trying raw pathname");

      new_module = xflat_load_xFLT_shared_library(full_pathname);
      if (new_module)
	{
	  return new_module;
	}
      goto err_out;
    }

  /* The easy way failed.  We will be calling xflat_find_shared_libray.
   * Ensure that its sprintf buffer doesn't overflow.  Don't 
   * allow lib_name or any directory to be longer than 1024.
   */

  if (xflat_strlen(lib_name) > 1024)
    goto err_out;

  /* Try the path list that we were provided in the argument list */

  if ((path_list) && (npathes > 0))
    {
      dbg("Searching user path list");

      new_module = xflat_find_library(lib_name, (const char * const *)path_list, npathes);
      if (new_module != NULL) 
	{
	  return new_module;
	}
    }

  /* Lastly, search the standard list of paths for the library.
     This list must exactly match the list in uClibc/ldso/util/ldd.c */

  dbg("Searching default path list");

  new_module = xflat_find_library(lib_name, default_path_list,
				  DEFAULT_NUM_PATHES);
  if (new_module != NULL) 
    {
      return new_module;
    }

 err_out:

  warn("Could not find \"%s\"", lib_name);
  return NULL;
}

/***********************************************************************
 * xflat_load_dependent_libraries
 ***********************************************************************/

int
xflat_load_dependent_libraries(struct xflat_module *import_module)
{
  struct xflat_module *new_module;
  char                *path_list[XFLAT_MAXLIB_PATHES];
  char                *name_list[XFLAT_MAXLIB_NAMES];
  u_int16_t            npathes;
  u_int16_t            nlibs;
  int                  i;

  /* Get the module path list */

  (void)xflat_get_module_path_list(import_module, path_list, &npathes);

  /* Get the library name list */

  (void)xflat_get_module_name_list(import_module, name_list, &nlibs);

  /* Traverse the list of shared libraries, loading each one
   * using the common path list.
   */

  for (i = 0; i < nlibs; i ++)
    {
      /* Load the shared library.  If successfull, then a pointer
       * to the newly loaded library's module structure is
       * returned.
       */

      new_module = xflat_load_shared_library(name_list[i], path_list, npathes);
      if (new_module == NULL)
	{
	  err("Could not load \"%s\"", name_list[i]);
	  return -ELIBACC;
	}
      else
	{
	  dbg("Loaded \"%s\"", name_list[i]);
	}
    }
  return 0;
}
