/***********************************************************************
 * xflat/ldso/xflat-ldso/xflatlib_ldso.h
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#ifndef _XFLATLIB_LDSO_H_
#define _XFLATLIB_LDSO_H_

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include <errno.h>              /* ENOEXEC */
#include "xflat_util.h"         /* xflat_printf, xflat_memset, etc. */

/***********************************************************************
 * Definitions
 ***********************************************************************/

#define XFLAT_PRINT xflat_printf
#define XFLAT_NTOHS xflat_ntohs
#define XFLAT_NTOHL xflat_ntohl

/* Fudge linux/kernel.h */

#define KERN_DEBUG
#define KERN_ERR
#define KERN_INFO
#define KERN_WARNING

/* Fudge asm/uaccess.h (linux 2.4) or asm/segment.h (linux 2.0) */

#define XFLAT_PUT_USER(x,p) (*(u_int32_t*)(p) = (u_int32_t)x)
#define XFLAT_GET_USER(x,p) (x = *((char*)(p)))

/***********************************************************************
 * Public Types
 ***********************************************************************/

/***********************************************************************
 * Public Functions
 ***********************************************************************/

#endif /* _XFLATLIB_LDSO_H_ */
