/***********************************************************************
 * xflat/ldso/simldso/xflat_sysdeps.h
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#ifndef _XFLAT_SYSDEPS_H_
#define _XFLAT_SYSDEPS_H_

/***********************************************************************
 * Compilation switches
 ***********************************************************************/

/* Defines behavior of ntohl and ntohs. */

/* #define NETWORK_IS_HOST_ORDER */

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include "xflatlib.h"   /* For XFLAT_DATA_OFFSET  */
#include "xflat_util.h" /* info() */

/***********************************************************************
 * Definitions
 ***********************************************************************/

/* Get a pointer to the argv array.  On many platforms this can be just
 * the address if the first argument, on other platforms we need to
 * do something a little more subtle here.
 */

#define GET_ARGV(ARGVP, ARGS) ARGVP = ((u_int32_t*)ARGS)

/* Transfer control to the user's application, once the dynamic loader
 * is done.  This routine has to exit the current function returning
 * the program entry point (_start?) which which will be called via
 * assembly language logic.
 */

#define START(e,d) return xflat_program_start;      

/* This is the address of the entry point to the linux resolver.
 * We just stick a dummy value here since this is never called in
 * the simulation.
 */

#define xflat_linux_resolve 0xbeefface

/***********************************************************************
 * Inline Functions
 ***********************************************************************/

/* We insist that the DSpace section be linked to virtual address zero.
 * as a consequence, the raw dspace address is very close to the
 * PIC base address -- it just off by the memory that we reserved
 * for use by the loader (XFLAT_DATA_OFFSET).
 */

static inline void
xflat_set_dspace(u_int32_t dspace)
{
  u_int32_t adjusted_dspace = dspace + XFLAT_DATA_OFFSET;
  info("Using dspace=0x%08x, PIC base would be set to 0x%08x",
       dspace, adjusted_dspace);
}

static inline u_int32_t
xflat_get_dspace(void)
{
  info("Returning dummy PIC base=0x00000000");
  return 0;
}

/* And here are the obvious conversions */

static inline u_int32_t
xflat_dspace_to_picbase(u_int32_t dspace)
{
  return dspace + XFLAT_DATA_OFFSET;
}

static inline u_int32_t
xflat_picbase_to_dspace(u_int32_t picbase)
{
  return picbase - XFLAT_DATA_OFFSET;
}

/***********************************************************************
 * Global Functions
 ***********************************************************************/

#endif /* _XFLAT_SYSDEPS_H_ */
