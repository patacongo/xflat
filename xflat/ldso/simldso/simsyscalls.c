/***********************************************************************
 * xflat/ldso/simldso/simsyscalls.c
 * Simulated linux system calls
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/stat.h>

#include "xflat_syscall.h"
#include "xflat_util.h"   /* err, dbg, etc. */

/***********************************************************************
 * Private Functions
 ***********************************************************************/

/***********************************************************************
 * Global Functions
 ***********************************************************************/

void xflat_exit(int status)
{
  info("xflat_exit() has been called with status=%d", status);
  exit(status);
}
int xflat_close(int fd)
{
  return close(fd);
}

void *xflat_mmap_frontend(void *addr, u_int32_t size, int prot,
			  int flags, int fd, u_int32_t offset)
{
  void          *alloc  = (void*)-1;

  info("addr   = 0x%p",   addr);
  info("size   = %d",     size);
  info("prot   = 0x%04x", prot);
  info("flags  = 0x%04x", flags);
  info("fd     = 0x%08x", fd);
  info("offset = 0x%08x", offset);

  if (addr != 0)
    {
      err("Mapping to non-NULL address is not supported");
    }
  else
    {
      alloc  = (void*)malloc(size);
      if (!alloc)
	{
	  err("Failed to allocate %d bytes", size);
	  alloc = (void*)-1;
	}
      else
	{
	  memset(alloc, 0, size);

	  if (fd != -1)
	    {
	      /* Seek to the specified file offset. */
	      off_t actual = lseek(fd, offset, SEEK_SET);
	      if (actual == (off_t)-1)
		{
		  err("lseek to offset %d of fd=%d failed, errno=%d",
		      offset, fd, errno);
		  free(alloc);
		  alloc = (void*)-1;
		}
	      else
		{
		  int status = read(fd, (void*)alloc, size);
		  if (status != size)
		    {
		      err("read from fd=%d failed, "
			  "status=%d, size=%d, errno=%d",
			  fd, status, size, errno);
		      free(alloc);
		      alloc = (void*)-1;
		    }
		}
	    }
	}
    }
  return alloc;
}

int xflat_open(const char *fn, int flags)
{
  info("xflat_open: Opening \"%s,\" flags=%d", fn, flags);
  return open(fn,flags);
}

u_int32_t xflat_write(int fd, const void *buf, u_int32_t count)
{
  int status = write(fd, buf, count);
  return status;
}

u_int32_t xflat_read(int fd, const void *buf, u_int32_t count)
{
  int status = read(fd, (void*)buf, count);
  return status;
}

u_int32_t
xflat_lseek(int fd, u_int32_t offset, int whence)
{
  int status = lseek(fd, offset, whence);
  return status;
}

#if 0
int xflat_mprotect(const void *addr, u_int32_t len, int prot)
{
  info("xflat_mprotect: addr=%p, len=%d, prot=%04x",
       addr, len, prot);
  err("NOT SUPPORTED");
  xflat_exit(1);
  return 1;
}

int xflat_stat(const char *file_name, struct stat *buf)
{
  int status = stat(file_name, buf);
  info("stat on \"%s\" returned %d, errno=%d",
       file_name, status, errno);
  return status;
}
#endif

int xflat_munmap(void *start, u_int32_t length)
{
    return 0;
}

#if 0
uid_t xflat_getuid(void)
{
    return 0;
}
uid_t xflat_geteuid(void)
{
    return 0;
}
gid_t xflat_getgid(void)
{
    return 0;
}
gid_t xflat_getegid(void)
{
    return 0;
}
#endif
