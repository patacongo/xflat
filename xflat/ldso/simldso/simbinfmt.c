/***********************************************************************
 * xflat/ldso/simldso/binfmt_sim.c
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "xflatlib.h"
#include "xflat_loader.h" /* xflat_loader() */
#include "xflat_util.h"   /* err, dbg, etc. */

/***********************************************************************
 * Definitions
 ***********************************************************************/

#define FAKE_ARGC 3
#define FAKE_ENVC 2

/***********************************************************************
 * Private Data
 ***********************************************************************/

static char *program_name   = NULL;
static char *xflat_filename = NULL;
 
/***********************************************************************
 * Private Data
 ***********************************************************************/

static const char fake_parms[] =
   "parm1\0parm2\0parm3\0env1\0env2";

/***********************************************************************
 * Private Functions
 ***********************************************************************/
 
/***********************************************************************
 * These are the functions that are exported to the xflatlib
 ***********************************************************************/

static void *
xflat_map(bin_handle_t file_handle, u_int32_t nbytes)
{
  FILE *in_stream = (FILE*)file_handle;
  char *fmap;

  info("xflat_map: nbytes=%d", nbytes);

  if (!in_stream)
    {
      err("xflat_map: handle is NULL");
      return (void*)-EINVAL;
    }

  fmap = (char*)malloc(nbytes);
  if (!fmap)
    {
      err("xflat_map: Allocation failure");
      return (void*)-ENOMEM;
    }

  rewind(in_stream);
  if (fread(fmap, nbytes, 1, in_stream) != 1)
    {
      err("xflat_map: Failed to read from file, errno=%d",
	      errno);
      return (void*)-errno;
    }
  return fmap;
}

static void xflat_unmap
(void *address, u_int32_t nbytes)
{
  info("xflat_alloc: address=0x%p nbytes=%d", address, nbytes);
  free(address);
}

static void *
xflat_alloc(u_int32_t nbytes)
{
  char *falloc;

  info("xflat_alloc: nbytes=%d", nbytes);

  falloc = (char*)malloc(nbytes);
  if (!falloc)
    {
      err("xflat_alloc: Allocation failure");
    }
  return falloc;
}

static file_handle_t 
xflat_open(bin_handle_t bin_handle, const char *filename)
{
  FILE *in_stream;

  info("xflat_open: bin_handle=0x%p filename=\"%s\"",
       bin_handle, filename);

  in_stream = fopen(filename, "rb");
  if (!in_stream)
    {
      err("xflat_open: Failed to open %s, errno=%d",
	  filename, errno);
    }
  info("xflat_open: returning file_handle=0x%p", in_stream);
  return (void*)in_stream;
}

static int
xflat_read(bin_handle_t bin_handle, file_handle_t file_handle,
	   char *dest, u_int32_t nbytes, u_int32_t fpos)
{
  FILE *in_stream = (FILE*)file_handle;
  int nitems;

  info("xflat_read: bin_handle=0x%p file_handle=0x%p",
	 bin_handle, file_handle);
  info("            dest=0x%p nbytes=%d fpos=%d",
	 dest, nbytes, fpos);

  if (fseek(in_stream, fpos, SEEK_SET) < 0)
    {
      err("xflat_read: Failed to fseek to %d, errno=%d",
	      fpos, errno);
      return -errno;
    }

  nitems = fread(dest, nbytes, 1, in_stream);
  if (nitems != 1)
    {
      err("xflat_read: Failed to read from file, "
	      " nitems=%d errno=%d",
	      nitems, errno);
      return -1;
    }
  return 0;
}

static void
xflat_close(file_handle_t file_handle)
{
  FILE *in_stream = (FILE*)file_handle;
  fclose(in_stream);
}

static const struct xflat_vtbl sim_vtbl =
{
  xflat_map,
  xflat_unmap, 
  xflat_alloc,
  xflat_unmap,
  xflat_open,
  xflat_read,
  xflat_close
};

/***********************************************************************
 * dump_init_stack
 ***********************************************************************/

void dump_init_stack(u_int32_t begin_stack, u_int32_t end_stack)
{
  u_int32_t *p = (u_int32_t*)(begin_stack & ~3);
  u_int32_t *pend = (u_int32_t*)((end_stack+3) & ~3);

  if (p > pend)
    {
      err("We have a push up stack????");
      exit(1);
    }

  info("INITIAL STACK:");
  info("  ADDRESS    VALUE");
  for (; p != pend; p++)
    {
      info("  0x%p 0x%08x", p, *p);
    }
}

/***********************************************************************
 * dump_load_info
 ***********************************************************************/

static void
dump_load_info(struct xflat_load_info *load_info)
{
  u_int32_t dspace_size =
    XFLAT_DATA_OFFSET +
    load_info->data_size +
    load_info->bss_size +
    load_info->stack_size;

  info("LOAD_INFO:");
  info("  ISPACE:");
  info("    ispace       = 0x%08x", load_info->ispace);
  info("    entry_offset = 0x%08x", load_info->entry_offset);
  info("    ispace_size  = 0x%08x", load_info->ispace_size);

  info("  DSPACE:");
  info("    dspace       = 0x%08x", load_info->dspace);
  info("      (ldso)     = 0x%08x", XFLAT_DATA_OFFSET);
  info("    data_size    = 0x%08x", load_info->data_size);
  info("    bss_size     = 0x%08x", load_info->bss_size);
  info("      (pad)      = 0x%08x", load_info->dspace_size - dspace_size);
  info("    stack_size   = 0x%08x", load_info->stack_size);
  info("    dspace_size  = 0x%08x", load_info->dspace_size);

  info("  ARGUMENTS:");
  info("    arg_start    = 0x%08x", load_info->arg_start);
  info("    env_start    = 0x%08x", load_info->env_start);
  info("    env_end      = 0x%08x", load_info->env_end);

  info("  RELOCS:");
  info("    reloc_start  = 0x%08x", load_info->reloc_start);
  info("    reloc_count  = 0x%08x", load_info->reloc_count);

  info("  HANDLES:");
  info("    bin_handle   = 0x%p",   load_info->bin_handle);
  info("    file_handle  = 0x%p",   load_info->file_handle);

  info("  xFLT HEADER:");
  info("    header       = 0x%p",   load_info->header);

  info("  ALLOCATIONS:");
  info("    alloc_start  = 0x%08x", load_info->alloc_start);
  info("    alloc_size   = 0x%08x", load_info->alloc_size);
}

/***********************************************************************
 * show_usage
 ***********************************************************************/

static void
show_usage(void)
{
  info("Usage: %s [options] <xflat-filename>",
	  program_name);
#if 0
  info("Where options are one or more of the following.  Note");
  info("that a space is always required between the option and");
  info("any following arguments");
  info("  -L <library-path-name>");
  info("      Path to FLAT shared libraries.  A maximum of %d",
       MAX_LIB_PATHES);
  info("      library path names may be provided");
  info("  -d Use dynamic symbol table [symtab]");
  info("  -l <abbrev>");
  info("      Include shared library of name lib<abbrev>.so");
  info("      A maximum of %d library abbreviations may be provided.",
       MAX_LIB_NAMES);
  info("  -o <out-filename>");
  info("     Output to <out-filename> [stdout]");
  info("  -v Verbose output [no output]");
#endif
  exit(1);
}

/***********************************************************************
 * parse_args
 ***********************************************************************/

static void
parse_args(int argc, char **argv)
{
#if 0
  int opt;
#endif

  /* Save our name (for show_usage) */

  program_name = argv[0];

  if (argc < 2)
    {
      err("ERROR:  Missing required arguments");
      show_usage();
    }

#if 0
  /* Get miscellaneous options from the command line. */

  while ((opt = getopt(argc, argv, "L:dl:o:v")) != -1)
    {
      switch (opt)
	{

	case 'L':
	  if (number_lib_pathes >= MAX_LIB_PATHES)
	    {
	      err("Too many shared library pathes");
	      show_usage();
	    }
	  else
	    {
	      lib_pathes[number_lib_pathes] = optarg;
	      number_lib_pathes++;
	    }
	  break;

	case 'd':
	  dsyms++;
	  break;

	case 'l':
	  if (number_lib_names >= MAX_LIB_NAMES)
	    {
	      err("Too many shared library names");
	      show_usage();
	    }
	  else
	    {
	      char lib_name[64];
	      sprintf(lib_name, "lib%s.so", optarg);
	      lib_names[number_lib_names] = strdup(lib_name);
	      number_lib_names++;
	    }
	  break;

	case 'o':
	  out_filename = optarg;
	  break;

	case 'v':
	  verbose++;
	  break;

	default:
	  err("%s Unknown option", argv[0]);
	  show_usage();
	  break;
	}
    }
#endif

  /* Get the name of the input xFLT file. */

  xflat_filename = argv[argc-1];

}
 
/***********************************************************************
 * Public Functions
 ***********************************************************************/
 
/***********************************************************************
 * main
 ***********************************************************************/
 
int 
main(int argc, char **argv, char **envp)
{
  struct xflat_hdr prog_header;
  struct xflat_hdr ldr_header;
  struct xflat_load_info prog_info;
  struct xflat_load_info ldr_info;
  start_entry_t prog_entry;
  FILE *in_stream;
  u_int32_t parmlen;
  u_int32_t wparmlen;
  u_int32_t stack_bottom;
  u_int32_t parm_start;
  u_int32_t stack_top;
  int result;

  /* Get the input parameters */

  parse_args(argc, argv);
  info("Sim started -- xflat_name=%s", xflat_filename);

  /* Open the xFLT file */

  in_stream = fopen(xflat_filename,"rb");
  if (NULL == in_stream)
    {
      err("Cannot open file %s for reading",
	  xflat_filename);
      exit(1);
    }

  if ( 1 != fread(&prog_header, sizeof(struct xflat_hdr), 1, in_stream))
    {
      err("Error reading xFLT program header, errno=%d", errno);
      exit(1);
    }

  /* Initialize the xflat library to load the program binary. */

  result = xflat_init(NULL, (file_handle_t)in_stream,
		      &prog_header, &sim_vtbl, &prog_info);
  dump_load_info(&prog_info);
  if (result != 0)
    {
      err("Failed to initialize for load of xFLT program, result=%d",
	      result);
      exit(1);
    }

  /* Determine the size of the strings */

  parmlen  = sizeof(fake_parms) + 1;
  wparmlen = (parmlen + 3) & ~3;

  /* Adjust the stack size to include space for the
   * parameters, environment, etc.
   */

  xflat_adjust_stack_size(&prog_info, FAKE_ARGC, FAKE_ENVC, wparmlen);

  /* Load the program binary */

  result = xflat_load(&prog_info);
  dump_load_info(&prog_info);
  if (result != 0)
    {
      err("Failed to load xFLT program, result=%d",
	      result);
      xflat_uninit(&prog_info);
      exit(1);
    }

  /* Initialize the xflat library to load the program binary. */

  result = xflat_init_interpreter(NULL, &prog_info, &ldr_info,
				  &ldr_header, &sim_vtbl);
  dump_load_info(&ldr_info);
  if (result != 0)
    {
      err("Failed to initialize for load of xFLT interpreter, result=%d",
	      result);
      xflat_unload(&prog_info);
      xflat_uninit(&prog_info);
      exit(1);
    }

  /* Make sure that there is not stack associated with the interpreter. */

  ldr_info.stack_size = 0;

  /* Load the intrepeter binary */

  result = xflat_load(&ldr_info);
  dump_load_info(&ldr_info);
  if (result != 0)
    {
      err("Failed to load xFLT interpreter, result=%d",
	      result);
      xflat_unload(&prog_info);
      xflat_uninit(&prog_info);
      xflat_uninit(&ldr_info);
      exit(1);
    }

  /* Move the parms into the stack. */

  stack_bottom  = prog_info.alloc_start + ((prog_info.alloc_size + 3) & ~3);
  info("stack_bottom: 0x%x", stack_bottom);

  parm_start = stack_bottom - wparmlen;
  memcpy((void*)parm_start, fake_parms, parmlen);
  info("parm_start:   0x%x", parm_start);

  /* Create the loader stack frame */

  stack_top = xflat_init_stack(&prog_info, &ldr_info, 
			       FAKE_ARGC, FAKE_ENVC,
			       (char*)parm_start);
  info("stack_top:    0x%x", stack_top);
  dump_load_info(&prog_info);
  xflat_uninit(&prog_info);
  xflat_uninit(&ldr_info);

  /* The "real" binfmt would set up the SWI return regs to vector
   * to the loader entry point here.  This simulation will
   * simply call the loader directly.
   */

  dump_init_stack(stack_top, stack_bottom);
  fflush(stdout);
  fclose(in_stream);

  prog_entry = xflat_loader(stack_top);
  info("prog_entry:   0x%p", prog_entry);

  info("End of simulation");
  return 0;
}

