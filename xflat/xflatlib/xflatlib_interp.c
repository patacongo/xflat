/***********************************************************************
 * xflat/xflatlib/xflatlib_interp.c
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

/***********************************************************************
 * Included Files
 ***********************************************************************/

/* This file of utility functions was designed to be used in three
 * different contexts:
 *
 * SIMULATION_BUILD - A user test test harness.
 * LDSO_BUILD       - ls.so
 * BINFMT_BUILD     - kernel binfmt module
 */

#if defined(SIMULATION_BUILD)
# include "xflatlib_sim.h"
#elif defined(BINFMT_BUILD)
# include "xflatlib_binfmt.h"
#elif defined(LDSO_BUILD)
# error "Not intended for use in ldso"
#else
# error "Build context not specified"
#endif

#include "xflatlib.h"           /* struct load_info */

/***********************************************************************
 * Compilation Switches
 ***********************************************************************/

/***********************************************************************
 * Definitions
 ***********************************************************************/

#undef dbg
#ifdef CONFIG_XFLAT_DEBUG

# define dbg(format, arg...) \
   XFLAT_PRINT(XFLAT_FD KERN_DEBUG "XFLATLIB: " format "\n" , ## arg)

#else

# define dbg(format, arg...) \
   do {} while (0)

#endif

#undef err
#define err(format, arg...) \
   XFLAT_PRINT(XFLAT_FD KERN_ERR "XFLATLIB: " format "\n" , ## arg)

#undef info
#define info(format, arg...) \
   XFLAT_PRINT(XFLAT_FD KERN_INFO "XFLATLIB: " format "\n" , ## arg)

#undef warn
#define warn(format, arg...) \
   XFLAT_PRINT(XFLAT_FD KERN_WARNING "XFLATLIB: " format "\n" , ## arg)

#define XFLT_HDR_SIZE   sizeof(struct xflat_hdr)

#ifndef MAX
#define MAX(x,y) ((x) > (y) ? (x) : (y))
#endif

/* Macros to access elements of the xFLT header given the load address
 * of the module's ISpace.  This works because the xFLT header resides
 * at the beginning of the module's ISpace.
 */

#define GET_LOADER(i) \
   XFLAT_NTOHL(((struct xflat_hdr*)(i))->loader)
#define GET_LIB_PATHES(i) \
   XFLAT_NTOHL(((struct xflat_hdr*)(i))->lib_pathes)
#define GET_LPATH_COUNT(i) \
   XFLAT_NTOHS(((struct xflat_hdr*)(i))->lpath_count)

/* This value, if defined, will be added to the loaders added additional
 * pathes to the loader default pathes.
 */

#ifdef XFLAT_TARGET_PREFIX
# define DEFAULT_NUM_PATHES 4
#else
# define DEFAULT_NUM_PATHES 2
#endif

#define XFLAT_HDR_SIZE   sizeof(struct xflat_hdr)

/***********************************************************************
 * Private Constant Data
 ***********************************************************************/

const char xflat_default_loader_name[] = XFLAT_DEFAULT_LOADER_NAME;

#ifdef XFLAT_TARGET_PREFIX
static const char lib_path1[] = XFLAT_TARGET_PREFIX "/usr/lib";
static const char lib_path2[] = XFLAT_TARGET_PREFIX "/lib";
#endif

static const char lib_path3[] = "/usr/lib";
static const char lib_path4[] = "/lib";

static const char *default_path_list[DEFAULT_NUM_PATHES]  =
{
#ifdef XFLAT_TARGET_PREFIX
  lib_path1, lib_path2,
#endif
  lib_path3, lib_path4
};

/***********************************************************************
 * Private Functions
 ***********************************************************************/

/***********************************************************************
 * Private Functions
 ***********************************************************************/

/***********************************************************************
 * xflat_get_module_path_list
 ***********************************************************************/

static int
xflat_get_path_list(u_int32_t ispace,
		    char **path_list, u_int16_t *npathes)
{
  u_int16_t  src_npathes;
  u_int32_t *src_list;
  int        retval = 0;
  int        i;

  /* The xFLT header lies at the beginning of the module ISpace.
   * Extract the TEXT section offset to list of pathes from the
   * header. Then convert the list offset to a TEXT section
   * offset.
   */

  src_list = (u_int32_t*)(GET_LIB_PATHES(ispace) + ispace);

  /* Fetch the library path count from the header */

  src_npathes = GET_LPATH_COUNT(ispace);

  /* Sanity check the number of pathes.  The current xFLT definition
   * imposes a limit on the number of pathes that may be provided
   * in the binary.
   */

  if (src_npathes > XFLAT_MAXLIB_PATHES)
    {
      err("Too many pathes in list (%d)\n", src_npathes);

      src_npathes = 0;
      retval      = -EINVAL;
    }
  else
    {
      dbg("ispace=0x%x src_path_list=0x%p npathes=%d",
	  ispace, src_list, src_npathes);

      /* Then fetch each path and relocate it */

      for (i = 0; i < src_npathes; i++)
	{
	  /* Fetch the offset to the next path string, relocate
	   * it into the ispace load position, and return it
	   * into the caller-provided array.
	   */

	   path_list[i] = (char*)(src_list[i] + ispace + XFLAT_HDR_SIZE);
	   dbg("Path %d: offset=0x%x address=0x%p \"%s\"",
	       i+1, src_list[i], path_list[i], path_list[i]);
	}
    }

  /* Return the number of pathes. */

  *npathes = src_npathes;
  return retval;
}
  
/***********************************************************************
 * xflat_read_header
 ***********************************************************************/

static file_handle_t
xflat_read_header(bin_handle_t bin_handle,
		  const struct xflat_vtbl *vtbl,
		  const char *ldr_name,
		  struct xflat_hdr *header)
{
  file_handle_t file_handle;

  /* Open the loader binary and read the xFLT header, leaving the
   * file open if successful and returning the file_handle if successful.
   * If we fail to open the file, it is no necessarily an error... we
   * make have to search a few pathes before we can conclude
   * that there is an error.
   */

  file_handle = vtbl->open(bin_handle, ldr_name);
  if (file_handle)
    {
      int status = vtbl->read(bin_handle, file_handle,
			      (char*)header, sizeof(struct xflat_hdr), 0);

      /* I suppose a failure to read from the file might not
       * necessarily be an error either, we could have the wrong
       * file.  Hmmm... I doubt it.
       */

      if (status != 0)
	{
	  err("Failed to read from \"%s\"", ldr_name);
	  vtbl->close(file_handle);
	  file_handle = NULL;
	}

      /* Verify that this is a valid loader shared library */

      else if (xflat_verify_library(header) != 0)
	{
	  err("Invalid xFLT library header");
	  vtbl->close(file_handle);
	  file_handle = NULL;
	}
    }
  return file_handle;
}

/***********************************************************************
 * xflat_interp_search
 ***********************************************************************/

static file_handle_t
xflat_interp_search(bin_handle_t bin_handle,
		    const struct xflat_vtbl *vtbl,
		    const char *ldr_name,
		    const char **path_list, u_int16_t npathes,
		    struct xflat_hdr *header)
{
  file_handle_t file_handle;
  char full_pathname[1024];
  int  namlen = XFLAT_STRLEN(ldr_name);
  int  i;

  for (i = 0; i < npathes; i++)
    {
      /* Create the full file name (if it will fit) */

      int len = XFLAT_STRLEN(path_list[i]) + namlen + 2;
      if (len > 1024)
	{
	  /* Complain, but keep looking */

	  err("Loader pathname name is too long");
	}
      else
	{
	  /* Create the full loader path */

	  XFLAT_STRCPY(full_pathname, path_list[i]);
	  XFLAT_STRCAT(full_pathname, "/");
	  XFLAT_STRCAT(full_pathname, ldr_name);

	  /* And read the xFLT header from the file */

	  file_handle = xflat_read_header(bin_handle, vtbl,
					  full_pathname, header);
	  if (file_handle)
	    {
	      dbg("Read xFLT header for \"%s\"", full_pathname);
	      return file_handle;
	    }
	}
    }
  return NULL;
}

/***********************************************************************
 * Public Functions
 ***********************************************************************/

/***********************************************************************
 * xflat_init_interpreter
 ***********************************************************************/

int
xflat_init_interpreter(bin_handle_t bin_handle,
		       struct xflat_load_info *prog_info,
		       struct xflat_load_info *ldr_info,
		       struct xflat_hdr *ldr_header,
		       const struct xflat_vtbl *vtbl)
{
  file_handle_t file_handle = NULL;
  u_int32_t     prog_ispace = prog_info->ispace;
  const char   *ldr_name;
  u_int32_t     file_offset;
  int           retval = 0;

  /* Check if a loader name is specified in the program object */

  file_offset = GET_LOADER(prog_ispace);
  if (file_offset)
    {
      /* Yes.  Then that is the name we will use. */

      ldr_name = (const char*)(file_offset + prog_ispace);
    }
  else
    {
      /* Otherwise, we'll use a default name for the loader. */

      ldr_name = xflat_default_loader_name;
    }

  /* Does the ldr_name begin with '/'? If it does, we'll assume
   * that it is an absolute pathe to the library and we will
   * try no other.
   */

  if (ldr_name[0] == '/')
    {
      file_handle = xflat_read_header(bin_handle, vtbl,
				      ldr_name, ldr_header);
    }
  else
    {
      char      *path_list[XFLAT_MAXLIB_PATHES];
      u_int16_t  npathes;

      /* Get the program module's path list */

      xflat_get_path_list(prog_ispace, path_list, &npathes);

      /* Search the program's path for for the loader object. */

      file_handle = xflat_interp_search(bin_handle, vtbl, ldr_name,
					(const char**)path_list, npathes,
					ldr_header);

      /* Search the default path for the loader object */

      if (!file_handle)
	{
	  file_handle = xflat_interp_search(bin_handle, vtbl, ldr_name,
					    default_path_list,
					    DEFAULT_NUM_PATHES,
					    ldr_header);
	}
    }

  /* Did we get the interpreter? */

  if (!file_handle)
    {
      err("Could not find interpreter \"%s\"", ldr_name);
      retval = -ENOEXEC;
    }
  else
    {
      /* Yes... Initialize for loading the interpreter. */

      retval = xflat_init(bin_handle, file_handle,
			  ldr_header, vtbl, ldr_info);
      if (retval != 0)
	{
	  err("Failed to initialize for interpreter load");
	  vtbl->close(file_handle);
	  file_handle = NULL;
	  retval = -ENOEXEC;
	}
    }

  return retval;
}
