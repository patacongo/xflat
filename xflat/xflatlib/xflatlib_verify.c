/***********************************************************************
 * xflat/xflatlib/xflatlib_verify.c
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

/***********************************************************************
 * Included Files
 ***********************************************************************/

/* This file of utility functions was designed to be used in three
 * different contexts:
 *
 * SIMULATION_BUILD - A user test test harness.
 * LDSO_BUILD       - ls.so
 * BINFMT_BUILD     - kernel binfmt module
 */

#if defined(SIMULATION_BUILD)
# include "xflatlib_sim.h"
#elif defined(BINFMT_BUILD)
# include "xflatlib_binfmt.h"
#elif defined(LDSO_BUILD)
# include "xflatlib_ldso.h"
#else
# error "Build context not specified"
#endif

#include "xflatlib.h"           /* struct load_info */

/***********************************************************************
 * Compilation Switches
 ***********************************************************************/

#define V_MAP   (load_info->vtbl->map)
#define V_UNMAP (load_info->vtbl->unmap)
#define V_ALLOC (load_info->vtbl->alloc)
#define V_FREE  (load_info->vtbl->free)
#define V_OPEN  (load_info->vtbl->open)
#define V_READ  (load_info->vtbl->read)
#define V_CLOSE (load_info->vtbl->close)

/***********************************************************************
 * Definitions
 ***********************************************************************/

#define XFLT_HDR_SIZE   sizeof(struct xflat_hdr)

#ifndef MAX
#define MAX(x,y) ((x) > (y) ? (x) : (y))
#endif

/***********************************************************************
 * Private Constant Data
 ***********************************************************************/

#ifndef dbg
#ifdef CONFIG_XFLAT_DEBUG

# define dbg(format, arg...) \
   XFLAT_PRINT(XFLAT_FD KERN_DEBUG "XFLATLIB: " format "\n" , ## arg)

#else

# define dbg(format, arg...) \
   do {} while (0)

#endif
#endif

#ifndef err
# define err(format, arg...) \
   XFLAT_PRINT(XFLAT_FD KERN_ERR "XFLATLIB: " format "\n" , ## arg)
#endif

#ifndef info
# define info(format, arg...) \
   XFLAT_PRINT(XFLAT_FD KERN_INFO "XFLATLIB: " format "\n" , ## arg)
#endif

#ifndef warn
# define warn(format, arg...) \
   XFLAT_PRINT(XFLAT_FD KERN_WARNING "XFLATLIB: " format "\n" , ## arg)
#endif

/***********************************************************************
 * Private Functions
 ***********************************************************************/

/***********************************************************************
 * Public Functions
 ***********************************************************************/

/***********************************************************************
 * xflat_verify_header
 ***********************************************************************/

int xflat_verify_header(const struct xflat_hdr *header)
{
  u_int16_t revision;
  u_int16_t bin_type;

  if (!header)
    {
      dbg("NULL xFLT header!");
      return -ENOEXEC;
    }

  /* Check the FLT header -- magic number and revision.
   * 
   * If the the magic number does not match.  Just return
   * silently.  This is not our binary.
   */
  
  if (xflat_strncmp(header->magic, "xFLT", 4))
    {
      dbg("Unrecognized magic=\"%c%c%c%c\"",
	  header->magic[0], header->magic[1],
	  header->magic[2], header->magic[3]);
      return -ENOEXEC;
    }

  /* Complain a little more if the version does not match. */

  revision = XFLAT_NTOHS(header->rev);
  if (revision != XFLAT_VERSION_CURRENT)
    {
      warn("Unsupported xFLT version=%d", revision);
      return -ENOEXEC;
    }

  /* Complain if this is not marked as an executable or a shared library. */

  bin_type = XFLAT_NTOHS(header->type);
  if ((bin_type != XFLAT_BINARY_TYPE_EXEC) &&
      (bin_type != XFLAT_BINARY_TYPE_DYN))
    {
      warn("xFLT binary not a recognized executable (type=%d)", bin_type);
      return -ENOEXEC;
    }
  return 0;
}

/***********************************************************************
 * xflat_verify_program
 ***********************************************************************/

int xflat_verify_program(const struct xflat_hdr *header)
{
  int retval = -ENOEXEC;

  /* Verify that it looks like a usable xFLT header and -- if the
   * header is good, that it refers to an executable
   */

  if (xflat_verify_header(header) == 0)
    {
      u_int16_t bin_type = XFLAT_NTOHS(header->type);
      if (bin_type != XFLAT_BINARY_TYPE_EXEC)
	{
	  warn("xFLT binary not a program file (type=%d)", bin_type);
	}
      else
	{
	  retval = 0;
	}
    }
  return retval;
}

/***********************************************************************
 * xflat_verify_library
 ***********************************************************************/

int xflat_verify_library(const struct xflat_hdr *header)
{
  int retval = -ENOEXEC;

  /* Verify that it looks like a usable xFLT header and -- if the
   * header is good, that it refers to an executable
   */

  if (xflat_verify_header(header) == 0)
    {
      u_int16_t bin_type = XFLAT_NTOHS(header->type);
      if (bin_type != XFLAT_BINARY_TYPE_DYN)
	{
	  warn("xFLT binary not a library (type=%d)", bin_type);
	}
      else
	{
	  retval = 0;
	}
    }
  return retval;
}
