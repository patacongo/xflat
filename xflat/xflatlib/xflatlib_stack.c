/***********************************************************************
 * xflat/xflatlib/xflatlib_stack.c
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

/***********************************************************************
 * Included Files
 ***********************************************************************/

/* This file of utility functions was designed to be used in three
 * different contexts:
 *
 * SIMULATION_BUILD - A user test test harness.
 * LDSO_BUILD       - ls.so
 * BINFMT_BUILD     - kernel binfmt module
 */

#if defined(SIMULATION_BUILD)
# include "xflatlib_sim.h"
#elif defined(BINFMT_BUILD)
# include "xflatlib_binfmt.h"
#elif defined(LDSO_BUILD)
# error "Not intended for use in ldso"
#else
# error "Build context not specified"
#endif

#include "xflatlib.h"           /* struct load_info */

/***********************************************************************
 * Compilation Switches
 ***********************************************************************/

/***********************************************************************
 * Definitions
 ***********************************************************************/

/***********************************************************************
 * Private Constant Data
 ***********************************************************************/

#undef dbg
#ifdef CONFIG_XFLAT_DEBUG

# define dbg(format, arg...) \
   XFLAT_PRINT(XFLAT_FD KERN_DEBUG "XFLATLIB: " format "\n" , ## arg)

#else

# define dbg(format, arg...) \
   do {} while (0)

#endif

#undef err
#define err(format, arg...) \
   XFLAT_PRINT(XFLAT_FD KERN_ERR "XFLATLIB: " format "\n" , ## arg)

#undef info
#define info(format, arg...) \
   XFLAT_PRINT(XFLAT_FD KERN_INFO "XFLATLIB: " format "\n" , ## arg)

#undef warn
#define warn(format, arg...) \
   XFLAT_PRINT(XFLAT_FD KERN_WARNING "XFLATLIB: " format "\n" , ## arg)

/***********************************************************************
 * Private Functions
 ***********************************************************************/

/***********************************************************************
 * Public Functions
 ***********************************************************************/

/***********************************************************************
 * xflat_adjust_stack_size
 ***********************************************************************/

void
xflat_adjust_stack_size(struct xflat_load_info *load_info,
			int argc, int envc, int system_usage)
{
  u_int32_t total_usage = system_usage;

  /* For argc, we will store the array (argc elements), the argc
   * value itself, plus a null pointer.
   */

  total_usage += (argc + 2) * sizeof(u_int32_t);

  /* For envc, we will store the array (envc elements) plus a null
   * pointer.
   */

  total_usage += (envc + 1) * sizeof(u_int32_t);

  /* And we will store six additional words described the memory
   * layout.
   */

  total_usage += 6 * sizeof(u_int32_t);

  /* Add this to the previously determined stack size */

  load_info->stack_size += total_usage;
}

/***********************************************************************
 * xflat_init_stack
 ***********************************************************************/

/* When we enter the xFLT loader, it will expect to see a stack frame
 * like the following.  NOTE: This logic assumes a push down stack
 * (i.e., we decrement the stack pointer to go from the "BOTTOM" to
 * the "TOP").
 *
 *    TOP->argc                Argument count    (integer)
 *         argv[0...(argc-1)]  Program arguments (pointers)
 *         NULL                Marks end of arguments
 *         env[0...N]          Environment variables (pointers)
 *         NULL                Marks end of environment variables
 *         loader ispace       Address of loader ISpace (xFLT header)
 *         loader dspace       Address of loader DSpace
 *         loader dspace size  Size of the allocated loader DSpace
 *         program ispace      Address of program ISpace (xFLT header)
 *         program dspace      Address of program DSpace
 * BOTTOM->program dspace size Size of the allocated program DSpace
 *
 */

u_int32_t
xflat_init_stack(struct xflat_load_info *prog_load_info,
		 struct xflat_load_info *lib_load_info,
		 int argc, int envc, char *p)
{
  u_int32_t *argv;
  u_int32_t *envp;
  u_int32_t *sp;
  char       dummy;

  /* p points to the beginning of the array of arguments;
   * sp points to the "bottom" of a push down stack.
   */

  sp = (u_int32_t*)((-(u_int32_t)sizeof(char*))&(u_int32_t) p);

  /* Place program information on the stack */

  if (prog_load_info)
    {
      --sp; XFLAT_PUT_USER((u_int32_t)prog_load_info->dspace_size, sp);
      --sp; XFLAT_PUT_USER((u_int32_t)prog_load_info->dspace, sp);
      --sp; XFLAT_PUT_USER((u_int32_t)prog_load_info->ispace, sp);
    }
  else
    {
      err("No program load info provided");
      return -EINVAL;
    }

  /* Place loader information on the stack */

  if (lib_load_info)
    {
      --sp; XFLAT_PUT_USER((u_int32_t)lib_load_info->dspace_size, sp);
      --sp; XFLAT_PUT_USER((u_int32_t)lib_load_info->dspace, sp);
      --sp; XFLAT_PUT_USER((u_int32_t)lib_load_info->ispace, sp);
    }
  else
    {
      --sp; XFLAT_PUT_USER((u_int32_t)0, sp);
      --sp; XFLAT_PUT_USER((u_int32_t)0, sp);
      --sp; XFLAT_PUT_USER((u_int32_t)0, sp);
    }

  /* Allocate space on the stack for the envp array contents
   * (including space for a null terminator).
   */

  sp  -= envc+1;
  envp = sp;

  /* Allocate space on the stack for the argv array contents
   * (including space for a null terminator).
   */

  sp  -= argc+1;
  argv = sp;

  /* Put argc on the stack. sp now points to the "top" of the
   * stack as it will be received by the new task.
   */

  --sp; XFLAT_PUT_USER((u_int32_t)argc, sp);

  /* Copy argv pointers into the stack frame (terminated with
   * a null pointer).
   */

  prog_load_info->arg_start = (u_int32_t)p;
  while (argc-->0)
    {
      /* Put the address of the beginning of the string */

      XFLAT_PUT_USER((u_int32_t)p, argv); argv++;

      /* Search for the end of the string */

      do
	{
	  XFLAT_GET_USER(dummy, p); p++;
	}
      while (dummy);
    }
  XFLAT_PUT_USER((u_int32_t)NULL,argv);

  /* Copy envp pointers into the stack frame (terminated with
   * a null pointer).
   */

  prog_load_info->env_start = (u_int32_t)p;
  while (envc-->0)
    {
      /* Put the address of the beginning of the string */

      XFLAT_PUT_USER((u_int32_t)p, envp); envp++;

      /* Search for the end of the string */

      do
	{
	  XFLAT_GET_USER(dummy, p); p++;
	}
      while (dummy);
    }
  XFLAT_PUT_USER((u_int32_t)NULL, envp);

  prog_load_info->env_end = (u_int32_t)p;

  return (u_int32_t)sp;
}

