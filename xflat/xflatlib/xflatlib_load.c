/***********************************************************************
 * xflat/xflatlib/xflatlib_load.c
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

/***********************************************************************
 * Included Files
 ***********************************************************************/

/* This file of utility functions was designed to be used in three
 * different contexts:
 *
 * SIMULATION_BUILD - A user test test harness.
 * LDSO_BUILD       - ls.so
 * BINFMT_BUILD     - kernel binfmt module
 */

#if defined(SIMULATION_BUILD)
# include "xflatlib_sim.h"
#elif defined(BINFMT_BUILD)
# include "xflatlib_binfmt.h"
#elif defined(LDSO_BUILD)
# include "xflatlib_ldso.h"
#else
# error "Build context not specified"
#endif

#include "xflatlib.h"           /* struct load_info */

/***********************************************************************
 * Compilation Switches
 ***********************************************************************/

/***********************************************************************
 * Definitions
 ***********************************************************************/

#define V_MAP   (load_info->vtbl->map)
#define V_UNMAP (load_info->vtbl->unmap)
#define V_ALLOC (load_info->vtbl->alloc)
#define V_FREE  (load_info->vtbl->free)
#define V_OPEN  (load_info->vtbl->open)
#define V_READ  (load_info->vtbl->read)
#define V_CLOSE (load_info->vtbl->close)

#define XFLAT_HDR_SIZE sizeof(struct xflat_hdr)

#ifndef MAX
#define MAX(x,y) ((x) > (y) ? (x) : (y))
#endif

#undef dbg
#ifdef CONFIG_XFLAT_DEBUG

# define dbg(format, arg...) \
   XFLAT_PRINT(XFLAT_FD KERN_DEBUG "XFLATLIB: " format "\n" , ## arg)

#else

# define dbg(format, arg...) \
   do {} while (0)

#endif

#undef err
#define err(format, arg...) \
   XFLAT_PRINT(XFLAT_FD KERN_ERR "XFLATLIB: " format "\n" , ## arg)

#undef info
#define info(format, arg...) \
   XFLAT_PRINT(XFLAT_FD KERN_INFO "XFLATLIB: " format "\n" , ## arg)

#undef warn
#define warn(format, arg...) \
   XFLAT_PRINT(XFLAT_FD KERN_WARNING "XFLATLIB: " format "\n" , ## arg)

/***********************************************************************
 * Private Constant Data
 ***********************************************************************/

#ifdef CONFIG_XFLAT_DEBUG
static const char text_segment[] = "TEXT";
static const char data_segment[] = "DATA";
static const char bss_segment[]  = "BSS";
static const char unknown[]      = "UNKNOWN";

static const char *segment[] =
{
  text_segment,
  data_segment,
  bss_segment,
  unknown
};
#endif

/***********************************************************************
 * Private Functions
 ***********************************************************************/

/***********************************************************************
 * xflat_swap32
 ***********************************************************************/

#ifdef __BIG_ENDIAN
static inline u_int32_t xflat_swap32(u_int32_t little)
{
    u_int32_t big =
	((little >> 24) & 0xff) | 
	(((little >> 16) & 0xff) << 8) |
	(((little >> 8) & 0xff) << 16) |
	((little & 0xff) << 24);
    return big;
}
#endif

/***********************************************************************
 * xflat_reloc
 ***********************************************************************/

static void xflat_reloc(struct xflat_load_info *load_info, u_int32_t rl)
{
  union
  {
    u_int32_t     l;
    struct xflat_reloc s;
  } reloc;
  u_int32_t *ptr;
  u_int32_t data_start;

  /* Force the long value into a union so that we can strip off some
   * bit-encoded values.
   */

  reloc.l = rl;

  /* We only support relocations in the data sections.
   * Verify that the the relocation address lies in the data
   * section of the file image.
   */

  if (reloc.s.offset > load_info->data_size)
    {
      err("ERROR: Relocation at 0x%08x invalid -- "
	  "does not lie in the data segment, size=0x%08x",
	  reloc.s.offset, load_info->data_size);
      err("       Relocation not performed!");
    }
  else if ((reloc.s.offset & 0x00000003) != 0)
    {
      err("ERROR: Relocation at 0x%08x invalid -- "
	  "Improperly aligned",
	  reloc.s.offset);
    }
  else
    {
      /* Get a reference to the "real" start of data.  It is
       * offset slightly from the beginning of the allocated
       * DSpace to hold information needed by ld.so at run time.
       */

      data_start = load_info->dspace + XFLAT_DATA_OFFSET;

      /* Get a pointer to the value that needs relocation in
       * DSpace.
       */
      
      ptr = (u_int32_t*)(data_start + reloc.s.offset);

      dbg("Relocation of variable at DATASEG+0x%08x "
	  "(address 0x%p, currently 0x%08x) into segment %s",
	  reloc.s.offset, ptr, *ptr, segment[reloc.s.type]);
	
      switch (reloc.s.type)
	{
	  /* TEXT is located at an offset of XFLAT_HDR_SIZE from
	   * the allocated/mapped ISpace region.
	   */

	case XFLAT_RELOC_TYPE_TEXT:
	  *ptr += load_info->ispace + XFLAT_HDR_SIZE;
	  break;

	  /* DATA and BSS are always contiguous regions.  DATA
	   * begins at an offset of XFLAT_DATA_OFFSET from
	   * the beginning of the allocated data segment.
	   * BSS is positioned after DATA, unrelocated references
	   * to BSS include the data offset.
	   *
	   * In other contexts, is it necessary to add the data_size
	   * to get the BSS offset like:
	   *
	   *   *ptr += data_start + load_info->data_size;
	   */

	case XFLAT_RELOC_TYPE_DATA:
	case XFLAT_RELOC_TYPE_BSS:
	  *ptr += data_start;
	  break;

	  /* This case happens normally if the symbol is a weak
	   * undefined symbol.  We permit these.
	   */

	case XFLAT_RELOC_TYPE_NONE:
	  dbg("NULL relocation!");
	  break;

	default:
	  err("ERROR: Unknown relocation type=%d", reloc.s.type);
	  break;
	}

      dbg("Relocation became 0x%08x", *ptr);
    }
}

/***********************************************************************
 * Public Functions
 ***********************************************************************/

/***********************************************************************
 * xflat_init
 ***********************************************************************/

int xflat_init(bin_handle_t bin_handle,
	       file_handle_t file_handle,
	       const struct xflat_hdr *header,
	       const struct xflat_vtbl *vtbl,
	       struct xflat_load_info *load_info)
{
  u_int32_t  data_start;
  u_int32_t  data_end;
  u_int32_t  bss_start;
  u_int32_t  bss_end;

  dbg("bin_handle=0x%p header=0x%p load_info=0x%p",
      bin_handle, header, load_info);

  /* Clear the load info structure */

  xflat_memset(load_info, 0, sizeof(struct xflat_load_info));

  /* Verify the xFLT header */

  if (xflat_verify_header(header) != 0)
    {
      /* This is not an error because we will be called
       * to attempt loading EVERY binary.  Returning -ENOEXEC
       * simply informs the system that the file is not
       * an xFLT file.  Besides, if there is something worth
       * complaining about, xflat_verify_header() has already
       * done so.
       */

      dbg("Bad xFLT header");
      return -ENOEXEC;
    }

  /* Save all of the input values in the load_info structure */

  load_info->bin_handle  = bin_handle;
  load_info->file_handle = file_handle;
  load_info->header      = header;
  load_info->vtbl        = vtbl;

  /* And extract some additional information from the xflat
   * header.  Note that the information in the xflat header is in
   * network order.
   */

  data_start             = XFLAT_NTOHL(header->data_start);
  data_end               = XFLAT_NTOHL(header->data_end);
  bss_start              = data_end;
  bss_end                = XFLAT_NTOHL(header->bss_end);

  /* And put this information into the load_info structure as well.
   *
   * Note that:
   *
   *   ispace_size = the address range from 0 up to data_start.
   *   data_size   = the address range from data_start up to data_end
   *   bss_size    = the address range from data_end up to bss_end.
   */

  load_info->entry_offset = XFLAT_NTOHL(header->entry);
  load_info->ispace_size  = data_start;

  load_info->data_size    = data_end - data_start;
  load_info->bss_size     = bss_end - data_end;
  load_info->stack_size   = XFLAT_NTOHL(header->stack_size);

  /* This is the initial dspace size.  We'll recaculate this later
   * after the memory has been allocated.  So that the caller can feel
   * free to modify dspace_size values from now until then.
   */

  load_info->dspace_size  =      /* Total DSpace Size is: */
    (XFLAT_DATA_OFFSET +         /*   Memory set aside for ldso */
     bss_end - data_start +      /*   Data and bss segment sizes */
     load_info->stack_size);     /*   (Current) stack size */

  /* Get the offset to the start of the relocations (we'll relocate
   * this later).
   */

  load_info->reloc_start  = XFLAT_NTOHL(header->reloc_start);
  load_info->reloc_count  = XFLAT_NTOHL(header->reloc_count);

  return 0;
}

/***********************************************************************
 * xflat_uninit
 ***********************************************************************/

int xflat_uninit(struct xflat_load_info *load_info)
{
  if (load_info->file_handle)
    {
      V_CLOSE(load_info->file_handle);
    }
  return 0;
}
		      
/***********************************************************************
 * xflat_load
 ***********************************************************************/

int xflat_load(struct xflat_load_info *load_info)
{
  u_int32_t dspace_read_size;
  u_int32_t data_offset;
  u_int32_t *reloc_tab;
  u_int32_t result;
  int           i;

  /* Calculate the extra space we need to map in.  This region
   * will be the BSS segment and the stack.  It will also be used
   * temporarily to hold relocation information.  So the size of this
   * region will either be the size of the BSS section and the
   * stack OR, it the size of the relocation entries, whichever
   * is larger
   */

  {
    u_int32_t extra_alloc;
    u_int32_t reloc_size;

    /* This is the amount of memory that we have to have to hold
     * the relocations.
     */

    reloc_size  = load_info->reloc_count * sizeof(u_int32_t);

    /* In the file, the relocations should lie at the same offset
     * as BSS.  The additional amount that we allocate have to
     * be either (1) the BSS size + the stack size, or (2) the
     * size of the relocation records, whicher is larger.
     */

    extra_alloc = MAX(load_info->bss_size + load_info->stack_size,
		      reloc_size);

    /* Use this addtional amount to adjust the total size of the
     * dspace region. */

    load_info->dspace_size =
      XFLAT_DATA_OFFSET +      /* Memory used by ldso */
      load_info->data_size +   /* Initialized data */
      extra_alloc;             /* bss+stack/relocs */

    /* The number of bytes of data that we have to read from the
     * file is the data size plus the size of the relocation table.
     */

    dspace_read_size = load_info->data_size + reloc_size;
  }

  /* We'll need this a few times as well. */

  data_offset = load_info->ispace_size;

  /* We will make two mmap calls create an address space for
   * the executable.  We will attempt to map the file to get
   * the ISpace address space and to allocate RAM to get the
   * DSpace address space.  If the system does not support
   * file mapping, the V_MAP() implementation should do the
   * right thing.
   */

  /* The following call will give as a pointer to the mapped
   * file ISpace.  This may be in ROM, RAM, Flash, ...
   * We don't really care where the memory resides as long
   * as it is fully initialized and ready to execute.
   * However, the memory should be share-able between processes;
   * otherwise, we don't really have shared libraries.
   */

  load_info->ispace = (u_int32_t)V_MAP(load_info->file_handle,
				       load_info->ispace_size);
      
  if (load_info->ispace >= (u_int32_t) -4096)
    {
      err("Failed to map xFLT ISpace, error=%d", -load_info->ispace);
      return load_info->ispace;
    }

  dbg("Mapped ISpace (%d bytes) at 0x%08x",
      load_info->ispace_size, load_info->ispace);

  /* The following call will give a pointer to the allocated
   * but uninitialized ISpace memory.
   */

  load_info->dspace = (u_int32_t)V_ALLOC(load_info->dspace_size);

  if (load_info->dspace >= (u_int32_t) -4096)
    {
      err("Failed to allocate DSpace, error=%d",
	  -load_info->ispace);
      (void)xflat_unload(load_info);
      return load_info->ispace;
    }

  dbg("Allocated DSpace (%d bytes) at 0x%08x",
      load_info->dspace_size, load_info->dspace);

  /* Now, read the data into allocated DSpace at an offset into
   * the allocated DSpace memory.  This offset provides a small
   * amount of BSS for use by the loader.
   */

  result = V_READ(load_info->bin_handle,
		  load_info->file_handle,
		  (char*)(load_info->dspace + XFLAT_DATA_OFFSET),
		  dspace_read_size,
		  data_offset);

  if (result >= (u_int32_t) -4096)
    {
      err("Unable to read DSpace, errno %d", -result);
      (void)xflat_unload(load_info);
      return result;
    }

  /* Save information about the allocation. */

  load_info->alloc_start = load_info->dspace;
  load_info->alloc_size  = load_info->dspace_size;

  dbg("TEXT=0x%x Entry point offset=0x%08x, data_start is 0x%08x\n",
      load_info->ispace, load_info->entry_offset, data_offset);

  /* Resolve the address of the relocation table.  In the file, the
   * relocations should lie at the same offset as BSS.  The current
   * value of reloc_start is the offset from the beginning of the file.
   * The following adjustment will convert it to an address in DSpace.
   */

  reloc_tab = (u_int32_t*)
    (load_info->reloc_start     /* File offset to reloc records */
     + load_info->dspace        /* + Allocated DSpace memory */
     + XFLAT_DATA_OFFSET        /* + Offset for ldso usage */
     - load_info->ispace_size); /* - File offset to DSpace */

  dbg("Relocation table at 0x%p, reloc_count=%d",
      reloc_tab, load_info->reloc_count);

  /* Now run through the relocation entries. */

  for (i=0; i < load_info->reloc_count; i++)
    {
#ifdef __BIG_ENDIAN
      xflat_reloc(load_info, xflat_swap32(reloc_tab[i]));
#else
      xflat_reloc(load_info, reloc_tab[i]);
#endif
    }

  /* Zero the BSS, BRK and stack areas, trashing the relocations
   * that lived in the corresponding space in the file. */

  xflat_memset((void*)(load_info->dspace + XFLAT_DATA_OFFSET +
		       load_info->data_size),
	       0,
	       (load_info->dspace_size - XFLAT_DATA_OFFSET -
		load_info->data_size));

  return 0;
}

/***********************************************************************
 * xflat_unload
 ***********************************************************************/

/* This function unloads the object from memory. This essentially
 * undoes the actions of xflat_load.
 */

int
xflat_unload(struct xflat_load_info *load_info)
{
  /* Reset the contents of the info structure. */

  /* Nothing is allocated */

  load_info->alloc_start = 0;
  load_info->alloc_size  = 0;

  /* Release the memory segments */

  if (load_info->ispace)
    {
      V_UNMAP((void*)load_info->ispace, load_info->ispace_size);
      load_info->ispace = 0;
    }

  if (load_info->dspace)
    {
      V_FREE((void*)load_info->dspace, load_info->dspace_size);
      load_info->dspace = 0;
    }

  return 0;
}
		      
