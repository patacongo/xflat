/***********************************************************************
 * xflat/arch-include/xflat_accessors.h
 *
 * Copyright (C) 2002, 2006 Cadenux, LLC.  All rights reserved.
 * Copyright (C) 2002, 2006 Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please report all bugs/problems to the author or <info@cadenux.com>
 ***********************************************************************/

#ifndef _XFLAT_ACCESSORS_H_
#define _XFLAT_ACCESSORS_H_

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include <stdio.h> /* FILE */

/***********************************************************************
 * Compilation Switches
 ***********************************************************************/

#define XFLAT_HAVE_OPENLIST     1 /* Added in 0.9.15 */
#define XFLAT_HAVE_USER_LOCKING 1 /* Added in 0.9.28 */
#define XFLAT_HAVE_RES          1 /* Referenced in recent busybox */

/***********************************************************************
 * Definitions
 ***********************************************************************/

enum xflat_varid
{
  XFLAT_STDIN,
  XFLAT_STDOUT,
  XFLAT_STDERR,
  XFLAT_SYS_ERRLIST,
  XFLAT_ERRNO,
  XFLAT_H_ERRNO,
  XFLAT_ENVIRON,
  XFLAT_OPTARG,
  XFLAT_OPTIND,
#ifdef XFLAT_HAVE_OPENLIST
  XFLAT_STDIO_OPENLIST,
#endif /* XFLAT_HAVE_OPENLIST */
#ifdef XFLAT_HAVE_USER_LOCKING
  XFLAT_STDIO_USER_LOCKING,
#endif /* XFLAT_HAVE_USER_LOCKING */
#ifdef XFLAT_HAVE_RES
  XFLAT_RES,
#endif /* XFLAT_HAVE_RES */
};

/***********************************************************************
 * Global Function Prototypes
 ***********************************************************************/

extern void *
xflat_varptr(enum xflat_varid varid);

#endif /* _XFLAT_ACCESSORS_H_ */
