/***********************************************************************
 * File: xflat/getopt.h
 *
 * Copyright (C) 2002, 2006 Cadenux, LLC.  All rights reserved.
 * Copyright (C) 2002, 2006 Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please report all bugs/problems to the author or <info@cadenux.com>
 ***********************************************************************/

#ifndef _XFLAT_GETOPT_H_
#define _XFLAT_GETOPT_H_

#include_next <getopt.h> /* For optarg and optind */
#include      <xflat_accessors.h>

#undef optarg /* In case optarg is already a macro */
#define optarg (*(char**)xflat_varptr(XFLAT_OPTARG))

#undef optind /* In case optind is already a macro */
#define optind (*(int*)xflat_varptr(XFLAT_OPTIND))

#endif /* _XFLAT_GETOPT_H_ */

