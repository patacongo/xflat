/***********************************************************************
 * File: xflat/ctype.h
 *
 * Copyright (C) 2005, 2006 Cadenux, LLC.  All rights reserved.
 * Copyright (C) 2005, 2006 Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please report all bugs/problems to the author or <info@cadenux.com>
 ***********************************************************************/

#ifndef _XFLAT_CTYPE_H_
#define _XFLAT_CTYPE_H_

#include <features.h>

/* Only do something special is this is uClibc-2.9.26 (or above) */

#if ((__UCLIBC_MAJOR__ == 0) && (__UCLIBC_MINOR__ == 9) && (__UCLIBC_SUBLEVEL__ >= 26))

/* Include the "real" ctype.h */
# include_next <ctype.h> /* For ctype stuff */

/* If uClibc supports locales, then everything is okay because the
 * ctype tables are already defined to be accessor functions.
 */

#ifndef __UCLIBC_HAS_XLOCALE__

/* These are the prototypes of the uClibc accessor functions */

extern __const __ctype_mask_t **__ctype_b_loc (void)
     __attribute__ ((__const));
extern __const __ctype_touplow_t **__ctype_tolower_loc (void)
     __attribute__ ((__const));
extern __const __ctype_touplow_t **__ctype_toupper_loc (void)
     __attribute__ ((__const));

/* Subsequent references to the global variables will refer to the
 * accessor functions.
 */

#define __ctype_b               (*__ctype_b_loc())
#define __ctype_tolower         (*__ctype_tolower_loc())
#define __ctype_toupper         (*__ctype_toupper_loc())

#undef  __UCLIBC_CTYPE_B
#define __UCLIBC_CTYPE_B        (*__ctype_b_loc())
#undef  __UCLIBC_CTYPE_TOLOWER
#define __UCLIBC_CTYPE_TOLOWER  (*__ctype_tolower_loc())
#undef  __UCLIBC_CTYPE_TOUPPER
#define __UCLIBC_CTYPE_TOUPPER  (*__ctype_toupper_loc())

#endif

#else

/* Don't do anything special for older versions of uClibc */
# include_next <ctype.h> /* For ctype stuff */

#endif

#endif /* _XFLAT_CTYPE_H_ */

