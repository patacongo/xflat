/***********************************************************************
 * File: xflat/bits/getopt.h
 *
 * Copyright (C) 2005, 2006 Cadenux, LLC.  All rights reserved.
 * Copyright (C) 2005, 2006 Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please report all bugs/problems to the author or <info@cadenux.com>
 ***********************************************************************/

#ifndef _XFLAT_BITS_GETOPT_H_
#define _XFLAT_BITS_GETOPT_H_

#include <features.h>

/* Only do something special is this is uClibc-2.9.26 (or above) */

#if ((__UCLIBC_MAJOR__ == 0) && (__UCLIBC_MINOR__ == 9) && (__UCLIBC_SUBLEVEL__ >= 26))

# undef __need_getopt
# include_next <bits/getopt.h> /* For optarg and optind */
# include      <xflat_accessors.h>

# undef optarg /* In case optarg is already a macro */
# define optarg (*(char**)xflat_varptr(XFLAT_OPTARG))

# undef optind /* In case optind is already a macro */
# define optind (*(int*)xflat_varptr(XFLAT_OPTIND))

#else
# error "bits/getopt.h does not exist in this uClibc version"
#endif

#endif /* _XFLAT_BITS_GETOPT_H_ */

