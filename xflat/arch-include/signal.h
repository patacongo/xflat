/***********************************************************************
 * File: xflat/signal.h
 *
 * Copyright (C) 2002, 2006 Cadenux, LLC.  All rights reserved.
 * Copyright (C) 2002, 2006 Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please report all bugs/problems to the author or <info@cadenux.com>
 ***********************************************************************/

#ifndef _XFLAT_SIGNAL_H_

/* See uClibc 0.9.15's signal.h.  It may not define all of the critical
 * types (depending upon certain conditioning definitions).  The clue for
 * us that the critical types are defined will be that _SIGNAL_H has
 * been defined.
 */

#include_next <signal.h> /* For sigset_t only */

#ifdef _SIGNAL_H
#define _XFLAT_SIGNAL_H_

/* This function will intercept all calls to signal(): */

extern __sighandler_t
xflat_signal (int signo, __sighandler_t handler);

/* This function will intercept all calls to sigaction(): */

extern int
xflat_sigaction(int signo,  const struct  sigaction  *act,
		struct sigaction *oact);

/* This macro will redirect all subsequent calls to signal() to
 * xflat_signal().
 */

#undef signal
#define signal(signo,handler) xflat_signal(signo,handler)

/* This macro will redirect all subsequent calls to sigaction() to
 * xflat_sigaction().
 */

#undef sigaction
#define sigaction(signo,act,oact) xflat_sigaction(signo,act,oact)

#endif /* _SIGNAL_H */
#endif /* _XFLAT_SIGNAL_H_ */

