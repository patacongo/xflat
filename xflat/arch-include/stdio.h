/***********************************************************************
 * File: xflat/stdio.h
 *
 * Copyright (C) 2002, 2006 Cadenux, LLC.  All rights reserved.
 * Copyright (C) 2002, 2006 Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please report all bugs/problems to the author or <info@cadenux.com>
 ***********************************************************************/

#ifndef _XFLAT_STDIO_H_
#define _XFLAT_STDIO_H_

#include_next <stdio.h> /* For stdin, stdout, stderr, etc */
#include_next <errno.h> /* In case sys_errlist is defined here */
#include      <xflat_accessors.h>

#undef stdin /* In case stdin is already a macro */
#define stdin (*((FILE**)xflat_varptr(XFLAT_STDIN)))

#undef stdout /* In case stdout is already a macro */
#define stdout (*((FILE**)xflat_varptr(XFLAT_STDOUT)))

#undef stderr /* In case stderr is already a macro */
#define stderr (*((FILE**)xflat_varptr(XFLAT_STDERR)))

/* This may be defined in either stdio.h or errno.h */

#undef sys_errlist /* In case sys_errlist is already a macro */
#define sys_errlist ((__const char *__const*)xflat_varptr(XFLAT_SYS_ERRLIST))

/* In uClibc 0.9.28 (and later), the following optimized macros
 * were defined.  Just defining the macros will revert to correct
 * functions.
 */

#undef fgetc
#undef fputc
#undef fgetc_unlocked
#undef fputc_unlocked
#undef getchar
#undef putchar
#undef getchar_unlocked
#undef putchar_unlocked
#undef clearerr
#undef feof
#undef ferror
#undef clearerr_unlocked
#undef feof_unlocked
#undef ferror_unlocked

#endif /* _XFLAT_STDIO_H_ */

