/***********************************************************************
 * File: xflat/stdlib.h
 *
 * Copyright (C) 2002, 2006 Cadenux, LLC.  All rights reserved.
 * Copyright (C) 2002, 2006 Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please report all bugs/problems to the author or <info@cadenux.com>
 ***********************************************************************/

#ifndef _XFLAT_STDLIB_H_
#define _XFLAT_STDLIB_H_

#include_next <stdlib.h> /* For on_exit() and atexit() */

/* This function will intercept all calls to atexit: */

extern int
xflat_atexit (void (*fn)(void)) __THROW;

/* This macro will redirect all subsequent calls to atexit
 * to xflat_atexit.
 */

#undef atexit
#define atexit(fn) xflat_atexit(fn)

/* This function will intercept all calls to on_exit: */

extern int
xflat_on_exit (void (*fn)(int status, void *arg), void *arg) __THROW;

/* This macro will redirect all subsequent calls to on_exit
 * to xflat_on_exit.
 */

#undef on_exit
#define on_exit(fn,arg) xflat_on_exit(fn,arg)

#endif /* _XFLAT_STDLIB_H_ */

