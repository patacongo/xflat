/***********************************************************************
 * File: xflat/dlfcn.h
 *
 * Copyright (C) 2002, 2006 Cadenux, LLC.  All rights reserved.
 * Copyright (C) 2002, 2006 Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please report all bugs/problems to the author or <info@cadenux.com>
 ***********************************************************************/

#ifndef _DLFCN_H_
#define _DLFCN_H_

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include <stdlib.h> /* For malloc, free */

/***********************************************************************
 * Definitions
 ***********************************************************************/

/* For dlopen -- These are, however, ignored. */

#define RTLD_LAZY	1
#define RTLD_NOW	2
#define RTLD_GLOBAL	0x100

/* For dlsym  -- Also ignored. */

#define RTLD_NEXT	((void *)-1)

/***********************************************************************
 * Public Type Declarations
 ***********************************************************************/

/* The following defines the generic type of the an inter-module
 * function pointer.  The type is intended to be generic but does
 * impose some restrictions on the kinds of function signatures
 * that can be exported:
 *
 * 1. The size of the return type must be less than or equal to
 *    sizeof(void*)
 * 2. The size of each parameter must also be less than or equal to
 *    sizeof(void*)
 * 3. No more than four parameters may be passed.
 *
 * dlfcncall will call the function pointer as though it were
 * the following type:
 */

typedef void *
(*dlfcnptr_t)(void *arg1, void *arg2, void *arg3, void *arg4);

/* The following structure defines a inter-module function pointer.
 * It is used by the dlfcncall function prototype below (as well as
 * by the inline helper functions).
 */

struct dlfcndesc
{
  dlfcnptr_t fcnptr; /* The address of the function */
  void      *module; /* Handle to the module that exports the function */
};

/***********************************************************************
 * Public Function Prototypes
 ***********************************************************************/

/*** STANDARD DLCFN.H FUNCTIONS ****************************************/
/* dlopen loads a dynamic library from the file named by the null
 * terminated string filename and returns an opaque "handle" for the
 * dynamic library. If the library is already loaded, dlopen returns
 * a reference to the existing library.  If filename is not an absolute
 * path (i.e., it does not contain a '/), then the file is searched
 * for in a default* set of pathes.  The flags (RTLD_LAZY, RTLD_GLOBAL,
 * and RTLD_NOW are ignored).
 */

extern void *dlopen(const char *filename, int flag);

/* If dlopen fails for any reason, it returns NULL. A human readable
 * string describing the most recent error that occurred from any
 * of the dl routines (dlopen, dlsym or dlclose) can be extracted with
 * dlerror(). dlerror returns NULL if no errors have occurred since
 * initialization or since it was last called. (Calling dlerror()
 * twice consecutively, will always result in the second call
 * returning NULL.)
 */

/* Not supported at present.
 *
 * const char *dlerror(void);
 */

#define dlerror() "dlerror is not supported"

/* dlsym takes a "handle" of a dynamic library returned by dlopen and
 * the null terminated symbol name, returning the address where
 * that symbol is loaded. If the symbol is not found, dlsym returns
 * NULL.
 */

extern void *dlsym(void *handle, const char *symbol);

/* dlclose decrements the reference count on the dynamic library handle
 * handle (and on all other libraries loaded by this library). If the
 * reference count drops to zero and no other loaded libraries use
 * symbols in it (how would I know that?), then the dynamic library is
 * unloaded. If the dynamic library exports a routine named _fini, then
 * that routine is called just before the library is unloaded.
 */

/* Not supported at present.
 *
 * extern int dlclose (void *handle);
 */

#define dlclose(h)

/*** NON-STANDARD DLCFN.H FUNCTIONS ************************************/
/* Given the raw address of a function (a function pointer), dlmodule
 * returns a handle to the module that exports the symbol.  If the
 * module containing 'function_address' is not found, dlmodule
 * returns NULL.
 */

extern void *dlmodule(void *fcnptr);

/* Give a instance of the dlfcndesc structure, dlfcncall will perform
 * the intermodule function call.  The return value of dlfcncall is the
 * return value of the called called function (dlfnccall will assert
 * any failures.  dlfcncall can only be used with pointers to functions
 * that conform to the requirements of type dlfcnptr_t (see above).
 */

extern void *dlfcncall(struct dlfcndesc *fcndesc, void *arg, ...);

/***********************************************************************
 * Inline Functions
 ***********************************************************************/

/* Given a static reference to a struct dlfncptr and a function address,
 * this helper function will initialize the structure suitable for use
 * with dlfcncall.
 *
 * Given:
 * 1. A function called foo() that receives a function pointer like:
 *    void foo(int (*fcndesc)(int arg));
 * 2. A function to be passed, bar(), like:
 *    int bar(int arg)
 *
 * Then a call to foo() passing a reference to bar could look like:
 *    struct dlfcndesc foothunk;
 *    dlinitdesc(bar, &foothunk);
 *    foo((void*)foothunk);
 *
 * Warning: dlinitdesc may return a NULL value for the struct dlfcndesc
 * module.
 */

static inline void
dlinitdesc(void *fcnptr, struct dlfcndesc *fcndesc)
{
  if (fcndesc)
    {
      fcndesc->fcnptr = fcnptr;
      fcndesc->module  = dlmodule(fcnptr);
    }
}

/* This helper function will allocate a function pointer structure
 * (using malloc), initialize it, and return a reference to the
 * structure.  This inline function could be used as follows.
 *
 * Given:
 * 1. A function called foo() that receives a function pointer like:
 *    void foo(int (*fcndesc)(int arg));
 * 2. A function to be passed, bar(), like:
 *    int bar(int arg)
 *
 * Then a call to foo() passing a reference to bar could look like:
 *    foo((void*)dlmkdesc(bar));
 *
 * dlmkdesc returns NULL on any failures.
 */

static inline struct dlfcndesc *
dlmkdesc(void *fcnptr)
{
  struct dlfcndesc *fcndesc = NULL;
  void *module = dlmodule(fcnptr);
  if (module)
    {
      fcndesc = (struct dlfcndesc*)malloc(sizeof(struct dlfcndesc));
      if (fcndesc)
	{
	  fcndesc->fcnptr = fcnptr;
	  fcndesc->module = module;
	}
    }
  return fcndesc;
}

/* dlfreedesc releases a function description structure allocated
 * by dlmkdesc.
*/

static inline void
dlfreedesc(struct dlfcndesc *fcndesc)
{
  if (fcndesc) free(fcndesc);
}


#endif /* _DLFCN_H_ */
