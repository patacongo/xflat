/***********************************************************************
 * xflat/libc/xflat_libc_thunk.c
 * This file contains some simple accessor functions to facilitate
 * access to global variables exported from libc.
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include <stdlib.h>          /* For atexit() and on_exit() */
#include <signal.h>          /* For signal(), sigaction(), etc. */
#include <sched.h>           /* For clone in bits/sched.h */
#include <malloc.h>          /* For malloc() and free() */
#include <errno.h>           /* For ENOMEM */

#include "xflat-internals.h"

/***********************************************************************
 * Definitions
 ***********************************************************************/

/* #define CONFIG_XFLAT_DEBUG 1 */

#ifdef CONFIG_XFLAT_DEBUG

# include <stdio.h>
# define dbg(format, arg...) \
   printf("LIBC: " format "\n" , ## arg)

#else

# define dbg(format, arg...) \
   do {} while (0)

#endif

/***********************************************************************
 * Private Types
 ***********************************************************************/

struct xflat_callback_s
{
  union
  {
    void (*fn0)(void);                   /* For atexit() callback */
    int  (*fn1)(void *arg);              /* For clone() start-up */
    void (*fn2)(int status, void *arg);  /* For on_exit() callback */
    void *fn;                            /* Generic pointer form */
  } cb;                                  /* Callback */
  void *arg;                             /* Callback argument */
  u_int32_t pic_base;                    /* Callee's PIC base */
};

struct xflat_sigaction_s
{
  union
  {
    void (*handler)(int signo);
    void (*action)(int signo, siginfo_t *siginfo, void *arg);
  } sa;                                  /* Signal handler address */
  u_int32_t pic_base;                    /* Signal handler's PIC base */
};

/***********************************************************************
 * Private Data
 ***********************************************************************/

static struct xflat_sigaction_s xflat_sigaction[NSIG];

/***********************************************************************
 * Private Functions
 ***********************************************************************/

/***********************************************************************
 * xflat_sighandler
 ***********************************************************************/

static void
xflat_sighandler(int signo, siginfo_t *siginfo, void *arg)
{
  u_int32_t my_pic_base;
  struct xflat_sigaction_s my_sigaction;

  dbg("signo=%d siginfo=%p arg=%p", signo, siginfo, arg);

  /* Get a stack reference to the struct xflat_sigaction associated
   * with this signal.  After this, we may not be able reference
   * global variables.
   */

  my_sigaction = xflat_sigaction[signo];

  /* Check if we have a valid sigaction and pic_base */

  if ((my_sigaction.sa.action != NULL) &&
      (my_sigaction.pic_base != 0))
    {
      /* Save our PIC base and select the user's PIC base.  After this, we
       * we cannot access global variables until we restore our PIC base.
       */

      my_pic_base = xflat_get_pic_base();
      dbg("my_pic_base=0x%08lx my_sigaction(sa.action=%p pic_base=0x%08lx)",
	  my_pic_base, my_sigaction.sa.action, my_sigaction.pic_base);

      xflat_set_pic_base(my_sigaction.pic_base);

      /* And call the real, user function */

      my_sigaction.sa.action(signo, siginfo, arg);

      /* Restore our PIC base when the user function returns */

      xflat_set_pic_base(my_pic_base);
      dbg("start_routine returned, restored my_pic_base=0x%08lx",
	  my_pic_base);

    }
}

/***********************************************************************
 * xflat_do_clone_thunk
 ***********************************************************************/

static int
xflat_do_clone_thunk(void *arg)
{
  u_int32_t my_pic_base;
  int retval;

  /* Get a stack copy of the argument structure created by
   * xflat_real_clone */

  struct xflat_callback_s newarg = *(struct xflat_callback_s*)arg;

  /* Release the original, allocated structure */

  free(arg);

  /* Save our PIC base and select the user's PIC base.  After this, we
   * cannot access global variables until we restore our PIC base.
   */

  my_pic_base = xflat_get_pic_base();
  dbg("my_pic_base=0x%08lx newarg(cb.fn1=%p pic_base=0x%08lx)",
      my_pic_base, newarg.cb.fn1, newarg.pic_base);

  xflat_set_pic_base(newarg.pic_base);

  /* And call the real, user clone start_routine */

  retval = newarg.cb.fn1(newarg.arg);

  /* Restore our PIC base when the user function returns */

  xflat_set_pic_base(my_pic_base);
  dbg("thread returned, restored my_pic_base=0x%08lx",
      my_pic_base);

  return retval;
}

/***********************************************************************
 * xflat_do_atexit_thunk
 ***********************************************************************/

static void
xflat_do_atexit_thunk(int status, void *arg)
{
  u_int32_t my_pic_base;

  /* Get a stack copy of the argument structure created by
   * xflat_real_atexit */

  struct xflat_callback_s newarg = *(struct xflat_callback_s*)arg;

  /* Release the original, allocated structure */

  free(arg);

  /* Save our PIC base and select the user's PIC base.  After this, we
   * cannot access global variables until we restore our PIC base.
   */

  my_pic_base = xflat_get_pic_base();
  dbg("my_pic_base=0x%08lx newarg(cb.fn0=%p pic_base=0x%08lx)",
      my_pic_base, newarg.cb.fn0, newarg.pic_base);

  xflat_set_pic_base(newarg.pic_base);

  /* And call the real, user function */

  newarg.cb.fn0();

  /* Restore our PIC base when the user function returns */

  xflat_set_pic_base(my_pic_base);
  dbg("exit function returned, restored my_pic_base=0x%08lx",
      my_pic_base);
}

/***********************************************************************
 * xflat_do_on_exit_thunk
 ***********************************************************************/

static void
xflat_do_on_exit_thunk(int status, void *arg)
{
  u_int32_t my_pic_base;

  /* Get a stack copy of the argument structure created by
   * xflat_real_on_exit */

  struct xflat_callback_s newarg = *(struct xflat_callback_s*)arg;

  /* Release the original, allocated structure */

  free(arg);

  /* Save our PIC base and select the user's PIC base.  After this, we
   * cannot access global variables until we restore our PIC base.
   */

  my_pic_base = xflat_get_pic_base();
  dbg("my_pic_base=0x%08lx newarg(cb.fn2=%p pic_base=0x%08lx)",
      my_pic_base, newarg.cb.fn2, newarg.pic_base);

  xflat_set_pic_base(newarg.pic_base);

  /* And call the real, user function */

  newarg.cb.fn2(status, newarg.arg);

  /* Restore our PIC base when the user function returns */

  xflat_set_pic_base(my_pic_base);
  dbg("exit function returned, restored my_pic_base=0x%08lx",
      my_pic_base);
}

/***********************************************************************
 * xflat_make_callback_thunk
 ***********************************************************************/

static struct xflat_callback_s *
xflat_make_callback_thunk(void *fn, void *arg, u_int32_t pic_base)
{
  struct xflat_callback_s *newarg;

  /* Allocate a structure to hold an extended argument list */

  newarg = (struct xflat_callback_s*)malloc(sizeof(struct xflat_callback_s));
  if (!newarg)
    {
      /* The allocation failed */

      dbg("Failed to allocate newarg");
      return NULL;
    }
  else
    {
      /* Copy the necessary info into the extended argument list */

      newarg->arg      = arg;
      newarg->cb.fn    = fn;
      newarg->pic_base = pic_base;
    }
  return newarg;
}

/***********************************************************************
 * Global Functions 
 ***********************************************************************/

/***********************************************************************
 * xflat_real_signal
 ***********************************************************************/

__sighandler_t xflat_real_signal (int signo, __sighandler_t handler,
				  u_int32_t pic_base)
{
  __sighandler_t retval = SIG_ERR; /* Assume failure */

  dbg("signo=%d handler=%p pic_base=0x%08lx", signo, handler, pic_base);

  /* Is the signal number with the range that we can handle. */

  if (signo < NSIG)
    {
      /* Save the previous contents.  (We might have to restore it) */

      struct xflat_sigaction_s my_sigaction = xflat_sigaction[signo];

      /* Is this a request to ignor the signal? or to use the
       * default hanlder?  If so, then we will be removing the
       * present signal handler.
       */

      if ((handler == SIG_IGN) ||
	  (handler == SIG_DFL))
	{
	  dbg("Handler is SIG_IGN or SIG_DFL");

	  if (signal(signo, handler) != SIG_ERR)
	    {
	      /* It succeeded.  Reset our copy. */

	      xflat_sigaction[signo].sa.action = NULL;
	      xflat_sigaction[signo].pic_base = 0;
	      retval = my_sigaction.sa.handler;
	    }
	}
      /* This is a request to establish a new handler. */

      else
	{
	  /* Set up the new signal info BEFORE we set call signal()
	   * (to avoid any potential race conditions).
	   */

	  xflat_sigaction[signo].sa.handler = handler;
	  xflat_sigaction[signo].pic_base = pic_base;

	  if (signal(signo, (__sighandler_t)xflat_sighandler) == SIG_ERR)
	    {
	      /* It failed, restore the previous contents and
	       * return SIG_ERR.
	       */

	      dbg("signal failed");
	      xflat_sigaction[signo] = my_sigaction;
	    }
	  else
	    {
	      /* It succeeded.  Return the previous handler address */

	      retval = my_sigaction.sa.handler;
	    }
	}
    }
  else
    {
      /* Let the real signal() do the error handling */

      retval = signal(signo, handler);
    }
  return retval;
}

/***********************************************************************
 * xflat_real_sigaction
 ***********************************************************************/

int xflat_real_sigaction(int signo,  const struct  sigaction  *act,
			 struct sigaction *oact, u_int32_t pic_base)
{
  struct xflat_sigaction_s my_sigaction = xflat_sigaction[signo];
  int retval;

  dbg("signo=%d act=%p oact=%p", signo, act, oact);

  /* Is the signal number with the range that we can handle. */

  if (signo < NSIG)
    {
      /* Case 1:  act is NULL.  This is just a request for the currently
       * installed signal handler.
       */

      if (act == NULL)
	{
	  /* Get the currently installed signal handler */

	  dbg("act is NULL");
	  retval = sigaction(signo, act, oact);
	}

      /* Case 2:  act is non-NULL, but sa_handler is SIGN_IGN or SIG_DEFL.
       * This is a request to remove the currently installed handler.
       */

      else if ((act->sa_handler == SIG_IGN) ||
	       (act->sa_handler == SIG_DFL))
	{
	  dbg("sa_handler is SIG_IGN or SIG_DFL");

	  retval = sigaction(signo, act, oact);
	  if (retval == 0)
	    {
	      /* It succeeded.  Reset our copy. */

	      xflat_sigaction[signo].sa.action = NULL;
	      xflat_sigaction[signo].pic_base = 0;
	    }
	}

      /* Case 3:  Otherwise, it is a request to establish a new handler. */

      else
	{
	  /* Set up a new action to direct signal handling to our
	   * handler.
	   */

	  struct sigaction my_act = *act;
	  my_act.sa_sigaction = xflat_sighandler;

	  /* Set up the new signal info BEFORE we set call signal()
	   * (to avoid any potential race conditions.
	   */

	  xflat_sigaction[signo].sa.handler = act->sa_handler;
	  xflat_sigaction[signo].pic_base = pic_base;

	  retval = sigaction(signo, &my_act, oact);
	  if (retval != 0)
	    {
	      /* It failed, restore the previous contents */

	      dbg("sigaction failed");
	      xflat_sigaction[signo] = my_sigaction;
	    }
	}
    }
  else
    {
      /* Let the real signal() do the error handling */

      retval = sigaction(signo, act, oact);
    }

  /* Did the caller provide a location to return the handler? */

  if (oact)
    {
      /* Yes, set the correct handler address */

      oact->sa_handler = my_sigaction.sa.handler;
    }
  return retval;
}

/***********************************************************************
 * xflat_real_clone
 ***********************************************************************/

int
xflat_real_clone (int (*fn)(void *arg), void *child_stack,
                  int flags, void *arg, u_int32_t pic_base)
{
  struct xflat_callback_s *newarg;
  int retval;

  dbg("fn=%p pic_base=0x%08lx", fn, pic_base);

  /* Create a structure to hold an extended argument list */

  newarg = xflat_make_callback_thunk(fn, arg, pic_base);
  if (!newarg)
    {
      /* The allocation failed */

      dbg("Failed to allocate newarg");
      retval = ENOMEM;
    }
  else
    {
      /* Then call clone, intercepting the user start_routine with
       * our thunk routine.
       */

      retval = clone(xflat_do_clone_thunk, child_stack, flags, newarg);
      if (retval == -1)
	{
	  dbg("clone failed");
	  free(newarg);
	}
    }
  return retval;
}

/***********************************************************************
 * xflat_real_atexit
 ***********************************************************************/

int
xflat_real_atexit (void (*fn)(void), u_int32_t pic_base)
{
  struct xflat_callback_s *newarg;
  int retval;

  dbg("fn=%p pic_base=0x%08lx", fn, pic_base);

  /* Create a structure to hold an extended argument list */

  newarg = xflat_make_callback_thunk(fn, NULL, pic_base);
  if (!newarg)
    {
      /* The allocation failed */

      dbg("Failed to allocate newarg");
      retval = ENOMEM;
    }
  else
    {
      /* Then call on_exit instead of atexit so that we can
       * pass newarg as an argument.  Our thunk routine will intercept
       * the user atexit callback, set the pic_base correctly, and
       * then call the user callback function.
       */

      retval = on_exit((void(*)(int,void*))xflat_do_atexit_thunk, newarg);
      if (retval != 0)
	{
	  dbg("on_exit failed");
	  free(newarg);
	}
    }
  return retval;
}

/***********************************************************************
 * xflat_real_on_exit
 ***********************************************************************/

/* This function will intercept all calls to on_exit: */

int
xflat_real_on_exit (void (*fn)(int status, void *arg),
		    void *arg, u_int32_t pic_base)
{
  struct xflat_callback_s *newarg;
  int retval;

  dbg("fn=%p arg=%ppic_base=0x%08lx", fn, arg, pic_base);

  /* Create a structure to hold an extended argument list */

  newarg = xflat_make_callback_thunk(fn, arg, pic_base);
  if (!newarg)
    {
      /* The allocation failed */

      dbg("Failed to allocate newarg");
      retval = ENOMEM;
    }
  else
    {
      /* Then call on_exit, intercepting the user start_routine with
       * our thunk routine.
       */

      retval = on_exit(xflat_do_on_exit_thunk, newarg);
      if (retval != 0)
	{
	  dbg("on_exit failed");
	  free(newarg);
	}
    }
  return retval;
}

