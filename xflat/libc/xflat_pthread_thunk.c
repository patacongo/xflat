/***********************************************************************
 * xflat/libc/xflat_pthread_thunk.c
 * This file contains some simple accessor functions to facilitate
 * access to global variables exported from libc.
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include <pthread.h>         /* For create_pthread and struct's */
#include <signal.h>          /* For struct sigaction */
#include <malloc.h>          /* For malloc() and free() */
#include <errno.h>           /* For ENOMEM */

#include <sys/types.h>       /* For u_int32_t */

#include "xflat-internals.h"

/***********************************************************************
 * Definitions
 ***********************************************************************/

/* #define CONFIG_XFLAT_DEBUG 1 */

#ifdef CONFIG_XFLAT_DEBUG

# include <stdio.h>
# define dbg(format, arg...) \
   printf("LIBPTHREAD: " format "\n" , ## arg)

#else

# define dbg(format, arg...) \
   do {} while (0)

#endif

/***********************************************************************
 * Private Types
 ***********************************************************************/

struct xflat_arg_s
{
  void *arg;
  void *(*start_routine)(void *arg);
  u_int32_t pic_base;
};

struct xflat_sigaction_s
{
  union
  {
    void (*handler)(int signo);
    void (*action)(int signo, siginfo_t *siginfo, void *arg);
  } sa;                                  /* Signal handler address */
  u_int32_t pic_base;                    /* Signal handler's PIC base */
};

/***********************************************************************
 * Private Data
 ***********************************************************************/

static struct xflat_sigaction_s xflat_sigactions[NSIG];

/***********************************************************************
 * Private Functions
 ***********************************************************************/

static void *
xflat_start_routine(void *arg)
{
  u_int32_t my_pic_base;
  void *retval;

  /* Get a stack copy of the argument structure created by
   * xflat_real_pthread_create */

  struct xflat_arg_s newarg = *(struct xflat_arg_s*)arg;

  /* Release the original, allocated structure */

  free(arg);

  /* Save our PIC base and select the user's PIC base.  After this, we
   * cannot access global variables until we restore our PIC base.
   */

  my_pic_base = xflat_get_pic_base();

  dbg("my_pic_base=0x%08lx newarg(start_routine=%p pic_base=0x%08lx)",
      my_pic_base, newarg.start_routine, newarg.pic_base);

  xflat_set_pic_base(newarg.pic_base);

  /* And call the real, user start function */

  retval = newarg.start_routine(newarg.arg);

  /* Restore our PIC base when the user function returns */

  xflat_set_pic_base(my_pic_base);

  dbg("start_routine returned, restored my_pic_base=0x%08lx",
      my_pic_base);

  return retval;
}

/***********************************************************************
 * xflat_sighandler
 ***********************************************************************/

static void
xflat_sighandler(int signo, siginfo_t *siginfo, void *arg)
{
  u_int32_t my_pic_base;
  struct xflat_sigaction_s my_sigaction;

  dbg("signo=%d siginfo=%p arg=%p", signo, siginfo, arg);

  /* Get a stack reference to the struct xflat_sigaction associated
   * with this signal.  After this, we may not be able reference
   * global variables.
   */

  my_sigaction = xflat_sigactions[signo];

  /* Check if we have a valid sigaction and pic_base */

  if ((my_sigaction.sa.action != NULL) &&
      (my_sigaction.pic_base != 0))
    {
      /* Save our PIC base and select the user's PIC base.  After this, we
       * we cannot access global variables until we restore our PIC base.
       */

      my_pic_base = xflat_get_pic_base();
      dbg("my_pic_base=0x%08lx my_sigaction(sa.action=%p pic_base=0x%08lx)",
	  my_pic_base, my_sigaction.sa.action, my_sigaction.pic_base);

      xflat_set_pic_base(my_sigaction.pic_base);

      /* And call the real, user function */

      my_sigaction.sa.action(signo, siginfo, arg);

      /* Restore our PIC base when the user function returns */

      xflat_set_pic_base(my_pic_base);
      dbg("start_routine returned, restored my_pic_base=0x%08lx",
	  my_pic_base);

    }
}

/***********************************************************************
 * Global Functions 
 ***********************************************************************/

int
xflat_real_pthread_create(pthread_t *thread, pthread_attr_t *attr,
			  void *(*start_routine)(void*), void *arg,
			  u_int32_t pic_base)
{
  struct xflat_arg_s *newarg;

  dbg("start_routine=%p pic_base=0x%08lx", start_routine, pic_base);

  /* Allocate a structure to hold an extended argument list */

  newarg = (struct xflat_arg_s*)malloc(sizeof(struct xflat_arg_s));
  if (!newarg)
    {
      /* The allocation failed */

      dbg("Failed to allocate newarg");
      return ENOMEM;
    }
  else
    {
      /* Copy the necessary info into the extended argument list */

      newarg->arg           = arg;
      newarg->start_routine = start_routine;
      newarg->pic_base      = pic_base;

      /* Then create a thread that will be intercepted by our
       * start routine.
       */

      dbg("creating xflat_start_routine=%p", xflat_start_routine);
      return pthread_create(thread, attr, xflat_start_routine, newarg);
    }
}


/***********************************************************************
 * xflat_real_sigaction
 ***********************************************************************/

/* libpthread provides a wrapper around libc's sigaction().  So we must
 * provide a wrapper around libpthread's wrapper.
 */

int xflat_real_sigaction(int signo,  const struct  sigaction  *act,
			 struct sigaction *oact, u_int32_t pic_base)
{
  struct xflat_sigaction_s my_sigaction = xflat_sigactions[signo];
  int retval;

  dbg("signo=%d act=%p oact=%p", signo, act, oact);

  /* Is the signal number with the range that we can handle. */

  if (signo < NSIG)
    {
      /* Case 1:  act is NULL.  This is just a request for the currently
       * installed signal handler.
       */

      if (act == NULL)
	{
	  /* Get the currently installed signal handler */

	  dbg("act is NULL");
	  retval = sigaction(signo, act, oact);
	}

      /* Case 2:  act is non-NULL, but sa_handler is SIGN_IGN or SIG_DEFL.
       * This is a request to remove the currently installed handler.
       */

      else if ((act->sa_handler == SIG_IGN) ||
	       (act->sa_handler == SIG_DFL))
	{
	  dbg("sa_handler is SIG_IGN or SIG_DFL");

	  retval = sigaction(signo, act, oact);
	  if (retval == 0)
	    {
	      /* It succeeded.  Reset our copy. */

	      xflat_sigactions[signo].sa.action = NULL;
	      xflat_sigactions[signo].pic_base = 0;
	    }
	}

      /* Case 3:  Otherwise, it is a request to establish a new handler. */

      else
	{
	  /* Set up a new action to direct signal handling to our
	   * handler.
	   */

	  struct sigaction my_act = *act;
	  my_act.sa_sigaction = xflat_sighandler;

	  /* Set up the new signal info BEFORE we set call signal()
	   * (to avoid any potential race conditions.
	   */

	  xflat_sigactions[signo].sa.handler = act->sa_handler;
	  xflat_sigactions[signo].pic_base = pic_base;

	  retval = sigaction(signo, &my_act, oact);
	  if (retval != 0)
	    {
	      /* It failed, restore the previous contents */

	      dbg("sigaction failed");
	      xflat_sigactions[signo] = my_sigaction;
	    }
	}
    }
  else
    {
      /* Let the real signal() do the error handling */

      retval = sigaction(signo, act, oact);
    }

  /* Did the caller provide a location to return the handler? */

  if (oact)
    {
      /* Yes, set the correct handler address */

      oact->sa_handler = my_sigaction.sa.handler;
    }
  return retval;
}

/***********************************************************************
 * __libc_sigaction
 ***********************************************************************/

/* libpthread calls the internal __libc_sigaction which should never
 * be exported.  Catch that call here and redirect it to xflat_sigaction.
 */

extern int
xflat_libc_sigaction(int signo, const struct sigaction  *act,
		     struct sigaction *oact);

int
__libc_sigaction(int signo,  const struct  sigaction  *act,
		 struct sigaction *oact)
{
  return xflat_libc_sigaction(signo, act, oact);
}
