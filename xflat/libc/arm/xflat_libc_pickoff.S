/***********************************************************************
 * xflat/tools/xflat_libc_pickoff.S
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#define XFLAT_ASSEMBLY 1
#include "xflat-internals.h"

/* Redirect calls to xflat_signal to xflat_real_signal.
 * Add an additional parameter:	The caller's PIC base address.
 */

	pickoff2 xflat_signal, xflat_real_signal

/* Redirect calls to xflat_sigaction to xflat_real_sigaction.
 * Add an additional parameter:	The caller's PIC base address.
 * (xflat_libc_sigaction if for integration with libpthread's
 * version of sigaction).
 */

	pickoff3 xflat_sigaction, xflat_real_sigaction
	pickoff3 xflat_libc_sigaction, xflat_real_sigaction

/* Redirect calls to xflat_clone to xflat_clone.
 * Add an additional parameter: The caller's PIC base address.
 */

	pickoff4 xflat_clone, xflat_real_clone

/* Redirect calls to xflat_atexit to xflat_real_atexit.
 * Add an additional parameter: The caller's PIC base address.
 */
	pickoff1 xflat_atexit, xflat_real_atexit

/* Redirect calls to xflat_on_exit to xflat_real_on_exit.
 * Add an additional parameter: The caller's PIC base address.
 */
	pickoff2 xflat_on_exit, xflat_real_on_exit
	.end
