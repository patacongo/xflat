/***********************************************************************
 * xflat/libc/xflat_accessors.c
 * This file contains some simple accessor functions to facilitate
 * access to global variables exported from libc.
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include <stdio.h>           /* For stdin, stdout, stderr, sys_errlist  */
#include <errno.h>           /* For errno */
#include <unistd.h>          /* For environ */
#include <getopt.h>          /* For optind and optarg */

#ifdef HAVE_RES              /* 1=_res is defined */
#  include <resolv.h>        /* For struct __res_state _res */
#endif

#include "xflat_accessors.h" /* enum xflat_varid */

/***********************************************************************
 * Private variables
 ***********************************************************************/

#ifdef XFLAT_HAVE_USER_LOCKING  /* 1=enumeration value defined */
#  ifdef HAVE_USER_LOCKING      /* 1=_stdio_user_locking declared */
     extern int _stdio_user_locking;
#  else
     static int __dummy_user_locking = 2;
#  endif
#endif

/***********************************************************************
 * Global variables
 ***********************************************************************/

/* #include <bit/netdb.h> */
extern int h_errno;

#ifdef XFLAT_HAVE_OPENLIST
/* #include <bits/uClibc_stdio.h> */
 extern FILE *_stdio_openlist;
#endif /* XFLAT_HAVE_OPENLIST */

/***********************************************************************
 * Global Functions 
 ***********************************************************************/

/***********************************************************************
 * xflat_varptr
 ***********************************************************************/

void *
xflat_varptr(enum xflat_varid varid)
{
  void *retval = NULL;
  switch (varid)
    {
    case XFLAT_STDIN:
      retval = (void*)&stdin;           /* stdio.h: returns FILE ** */
      break;
    case XFLAT_STDOUT:
      retval = (void*)&stdout;          /* stdio.h: returns FILE ** */
      break;
    case XFLAT_STDERR:
      retval = (void*)&stderr;          /* stdio.h: returns FILE ** */
      break;
#ifdef __UCLIBC_HAS_SYS_ERRLIST__
    case XFLAT_SYS_ERRLIST:
      retval = (void*)sys_errlist;      /* stdio.h: returns char ** */
      break;
#endif
    case XFLAT_ERRNO:
      retval = (void*)&errno;           /* errno.h: returns int * */
      break;
    case XFLAT_H_ERRNO:
      retval = (void*)&h_errno;         /* errno.h: returns int * */
      break;
    case XFLAT_ENVIRON:
#ifdef __USE_GNU
      retval = (void*)&environ;         /* unistd.h: returns char *** */
#else
      retval = (void*)&__environ;       /* unistd.h: returns char *** */
#endif
      break;
    case XFLAT_OPTARG:
      retval = (void*)&optarg;          /* getopt.h: returns char** */
      break;
    case XFLAT_OPTIND:
      retval = (void*)&optind;          /* errno.h: returns int * */
      break;

#ifdef XFLAT_HAVE_OPENLIST              /* 1=enumeration defined */
    case XFLAT_STDIO_OPENLIST:          /* bits/uClibc_stdio.h: */
#  ifdef HAVE_OPENLIST                  /* 1=__stdio_openlist declared */
      retval = (void*)_stdio_openlist;	/* returns FILE* */
#  else
      retval = NULL;
#  endif /* HAVE_OPENLIST */
#endif /* XFLAT_HAVE_OPENLIST */
      break;

#ifdef XFLAT_HAVE_USER_LOCKING          /* 1=enumeration defined */
    case XFLAT_STDIO_USER_LOCKING:      /* bits/uClibc_stdio.h: */
#  ifdef HAVE_USER_LOCKING              /* 1=_stdio_user_locking defined */
      retval = (void*)&_stdio_user_locking;  /* returns int* */
#  else
      retval = (void*)&__dummy_user_locking; /* returns int* */
#  endif /* HAVE_USER_LOCKING */
#endif /* XFLAT_HAVE_USER_LOCKING */
      break;

#ifdef XFLAT_HAVE_RES                   /* 1=enumeration defined */
    case XFLAT_RES:                     /* resolv.h */
#  ifdef HAVE_RES                       /* 1=_res is defined */
      retval = (void*)&_res;            /* returns struct __res_state */
#  else
      retval = NULL;                    /* returns struct __res_state */
#  endif /* HAVE_RES */
#endif /* XFLAT_HAVE_RES */
      break;

    default:
      break;
    }
  return retval;
}

/***********************************************************************
 * This is a weak import that we need to make stronger so that it
 * can be accessed by libpthread-xflat.so.  This was needed in the
 * the uClibc 0.9.15 timeframe, but not after around 0.9.26
 ***********************************************************************/

#if defined(XFLAT_NEED_LIBC_FORK)
pid_t __libc_fork(void) {return fork();}
#endif /* XFLAT_NEED_LIBC_FORK */

