/***********************************************************************
 * xflat/sys-include/xflat_accessors.h
 *
 * Copyright (C) 2002, 2006 Cadenux, LLC.  All rights reserved.
 * Copyright (C) 2002, 2006 Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#ifndef _XFLAT_ACCESSORS_H_
#define _XFLAT_ACCESSORS_H_

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include <stdio.h> /* FILE */

/***********************************************************************
 * Compilation Switches
 ***********************************************************************/

#define XFLAT_HAVE_OPENLIST     1 /* Added in 0.9.15 */
#define XFLAT_HAVE_USER_LOCKING 1 /* Added in 0.9.28 */
#define XFLAT_HAVE_RES          1 /* Referenced in recent busybox */

/***********************************************************************
 * Definitions
 ***********************************************************************/

enum xflat_varid
{
  XFLAT_STDIN,
  XFLAT_STDOUT,
  XFLAT_STDERR,
  XFLAT_SYS_ERRLIST,
  XFLAT_ERRNO,
  XFLAT_H_ERRNO,
  XFLAT_ENVIRON,
  XFLAT_OPTARG,
  XFLAT_OPTIND,
#ifdef XFLAT_HAVE_OPENLIST
  XFLAT_STDIO_OPENLIST,
#endif /* XFLAT_HAVE_OPENLIST */
#ifdef XFLAT_HAVE_USER_LOCKING
  XFLAT_STDIO_USER_LOCKING,
#endif /* XFLAT_HAVE_USER_LOCKING */
#ifdef XFLAT_HAVE_RES
  XFLAT_RES,
#endif /* XFLAT_HAVE_RES */
};

/***********************************************************************
 * Global Function Prototypes
 ***********************************************************************/

extern void *
xflat_varptr(enum xflat_varid varid);

#endif /* _XFLAT_ACCESSORS_H_ */
