/***********************************************************************
 * xflat/sys-include/xflat.h
 *
 * Copyright (C) 2002, 2006 Cadenux, LLC.  All rights reserved.
 * Copyright (C) 2002, 2006 Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please report all bugs/problems to the author or <info@cadenux.com>
 ***********************************************************************/

#ifndef _XFLAT_H
#define _XFLAT_H

/***********************************************************************
 * Included Files
 ***********************************************************************/

/* If we are building in the kernel, then we may be either big- or
 * little-endian.  The following include will tell us which.
 */

#ifdef __KERNEL__
#  include <asm/byteorder.h>
#  include <linux/types.h>
#else
#  include <sys/types.h>
#  if !defined(__BYTE_ORDER) || (__BYTE_ORDER == __LITTLE_ENDIAN)
#    undef  __LITTLE_ENDIAN
#    undef  __BIG_ENDIAN
#    define __LITTLE_ENDIAN          1234
#    undef  __LITTLE_ENDIAN_BITFIELD
#    undef  __BIG_ENDIAN_BITFIELD
#    define __LITTLE_ENDIAN_BITFIELD 1
#    undef  __BYTE_ORDER
#    define __BYTE_ORDER __LITTLE_ENDIAN
#  else
#    undef  __LITTLE_ENDIAN
#    undef  __BIG_ENDIAN
#    define __BIG_ENDIAN             4321
#    undef  __LITTLE_ENDIAN_BITFIELD
#    undef  __BIG_ENDIAN_BITFIELD
#    define __BIG_ENDIAN_BITFIELD    1
#    define __BYTE_ORDER __BIG_ENDIAN
#  endif
#endif

/***********************************************************************
 * Maximum Sizes
 ***********************************************************************/

/* To simplify the xFLT implementations, fixed maximum sizes of certain
 * things are hard-coded.  This eliminates certain memory management
 * operations that are necessary by the xFLT loader.
 */

#define XFLAT_MAX_STRING_SIZE 128 /* Largest size of string (incl. Z) */
#define XFLAT_MAXLIB_PATHES     8 /* Maximum number of search pathes  */
#define XFLAT_MAXLIB_NAMES     16 /* Maximum number of shared libraries
				   * per program. */

/***********************************************************************
 * The FLAT file header
 *
 * The elements within this structure are stored in network order (i.e.,
 * ntohs() and ntohl() should be used to access fields within the
 * header.
 ***********************************************************************/

struct xflat_hdr
{
  /* The "magic number identifying the FLT type.  For this FLT variant
   * this field should contain "xFLT"
   */

  char magic[4];

  /* Object file type and FLT revision number number. */

  u_int16_t rev;
  u_int16_t type;

  /* The following fields provide the memory map for the flat binary.
   *
   * entry      - Offset to the the first executable insruction from
   *              the beginning of the file.  For a program this is
   *              normally the entry point, _start.  For a shared
   *              library, this field may be NULL.  If it is non-NULL
   *              then it refers to the library initialization
   *              entry point and will be called by the loader after
   *              the library has been loaded.
   * data_start - Offset to the beginning of the data segment from
   *              the beginning of the file.  This field can also
   *              interpreted as the size of the ISpace segment.
   * data_end   - Offset to the end of the data segment from the
   *              beginning of  the file.
   * bss_end    - Offset to the end of bss segment from the beginning
   *              of the file.
   *
   * The text segment can be considered to be the contiguous (unrelocated)
   * address space range from address zero through (but not including)
   * data_start.
   *
   * The size of the data/bss segment includes (as a minimum) the data
   * and bss regions (bss_end - data_start) as well as the size of the
   * stack.  At run time, this region will also include program arguments
   * and environement variables.
   * 
   * The bss segment is data_end through bss_end.
   */

  u_int32_t entry;
  u_int32_t data_start;
  u_int32_t data_end;
  u_int32_t bss_end;

  /* Size of stack, in bytes */

  u_int32_t stack_size;

  /* Relocation entries
   * reloc_start - Offset to the beginning of an array of relocation
   *               records (struct flat_reloc).  The offset is
   *               relative to the start of the file
   * reloc_count - The number of relocation records in the arry
   */

  u_int32_t reloc_start;       /* Offset of relocation records */
  u_int32_t reloc_count;       /* Number of relocation records */

  /* Loader Path
   *
   * This provides the full pathname of the xFLT loader that should
   * be used to load this file.  This name is used by binfmt_xflat
   * to start the process.  If this name is not present, the
   * binfmt_xflat will attempt to use a default loader path.
   */

  u_int32_t loader;            /* Offset to loader pathname */

  /* Imported and exported symbol tables
   *
   * import_symbols - Offset to the beginning of an array of imported
   *                  symbol structures (struct flat_import).  The
   *                  import_symbols offset is relative to the
   *                  beginning of the file.  Each entry of the
   *                  array contains an u_int32_t offset (again from
   *                  the beginning of the file) to the name of
   *                  a symbol string.  This string is null-terminated.
   * import_count   - The number of records in the export_symbols array.
   * export_symbols - Offset to the beginning of an array of export
   *                  symbol structures (struct flat_export).  The
   *                  import_symbols offset is relative to the
   *                  beginning of the file.  Each entry of the
   *                  array contains an u_int32_t offset (again from
   *                  the beginning of the file) to the name of
   *                  a symbol string.  This string is null-terminated.
   * export_count   - The number of records in the export_symbols array.
   *
   * NOTE:  All of the arrays referenced in the header reside in the
   * the .text section.  This is possible because these arrays are
   * read-only and are only referenced by the load.  Residing in text
   * also guarantees that only one copy of the array is required.
   *
   * An exception is the import_import symbols array with will lie
   * in .data.  This array contains write-able data and must have
   * a single instance per process.  NOTE:  The string offset contained
   * within flat_import still refers to strings residing in the text
   * section.
   */

  u_int32_t  import_symbols;   /* Offset to list of imported symbols */
  u_int32_t  export_symbols;   /* Offset to list of exported symbols */
  u_int16_t  import_count;     /* Number of imported symbols */
  u_int16_t  export_count;     /* Number of imported symbols */

  /* Shared ibrary paths and file names.
   *
   * lib_pathes     - Offset to the beginning of an array of library
   *                  pathname offsets (u_int32_t).  The lib_pathes
   *                  offset is relative to the beginning of the file
   *                  as is each pathname offset in the array.  The
   *                  pathname offset points to a null-terminated
   *                  string that provides library pathname. These
   *                  are the pathes that should be search by the
   *                  loader to locate the library files identified
   *                  by lib_names.
   * lpath_count    - The number of offsets in the lib_pathes array.
   * lib_names      - Offset to the beginning of an array of library
   *                  filename offsets (u_int32_t).  The lib_names
   *                  offset is relative to the beginning of the file
   *                  as is each filename offset in the array.  The
   *                  filename offset points to a null-terminated
   *                  string that provides library filesname. These
   *                  are the shared library files that should be
   *                  loaded by the loader when the process is started.
   * lname_count    - The number of offsets in the lib_names array.
   */

  u_int32_t  lib_pathes;       /* Offset to library search pathes */
  u_int32_t  lib_names;        /* Offset to library names */
  u_int16_t  lpath_count;      /* Number of library pathes */
  u_int16_t  lname_count;      /* Number of library names */
};

/* Possible values for the type field: */

#define XFLAT_BINARY_TYPE_NONE    0       /* No file type */
#define XFLAT_BINARY_TYPE_REL     1       /* Relocatable file */
#define XFLAT_BINARY_TYPE_EXEC    2       /* Executable file */
#define XFLAT_BINARY_TYPE_DYN     3       /* Shared object file */
#define XFLAT_BINARY_TYPE_CORE    4       /* Core file */
#define XFLAT_BINARY_TYPE_NUM     5       /* Number of defined types */
#define XFLAT_BINARY_TYPE_LOOS    0xfe00  /* OS-specific range start */
#define XFLAT_BINARY_TYPE_HIOS    0xfeff  /* OS-specific range end */
#define XFLAT_BINARY_TYPE_LOPROC  0xff00  /* Processor-specific range start */
#define XFLAT_BINARY_TYPE_HIPROC  0xffff  /* Processor-specific range end */

/* Legal values for the version field.  */

#define XFLAT_VERSION_NONE         0      /* Invalid ELF version */
#define XFLAT_VERSION_CURRENT      1      /* Current version */
#define XFLAT_VERSION_NUM          2

/***********************************************************************
 * Entry Point Types
 ***********************************************************************/

/* The "entry" file offset points to the entry point of the module.
 * The prototype of the entry point differs with the type of the
 * module.  As typedef'ed below:
 */

typedef int  (*program_entry_t)(int argc, char **argv, char **envp);
typedef void (*library_entry_t)(void);
typedef void (*loader_entry_t) (u_int32_t args);
typedef int  (*start_entry_t)(u_int32_t args);

/***********************************************************************
 * Relocation types.
 *
 * The relocation records are an array of the following type.  The fields
 * in each element are stored in native machine order.
 ***********************************************************************/

#define XFLAT_RELOC_TYPE_NONE 0
#define XFLAT_RELOC_TYPE_TEXT 1
#define XFLAT_RELOC_TYPE_DATA 2
#define XFLAT_RELOC_TYPE_BSS  3
#define XFLAT_RELOC_TYPE_NUM  4

struct xflat_reloc
{
#ifdef __LITTLE_ENDIAN_BITFIELD
    int32_t offset : 30;
  u_int32_t type   : 2; 
#else
  u_int32_t type   : 2; 
    int32_t offset : 30;
#endif
};

/***********************************************************************
 * Imported symbol type 
 *
 * The imported symbols are an array of the following type.  The fields
 * in each element are stored in native machine order.
 ***********************************************************************/

struct xflat_import
{
  u_int32_t function_name;    /* Offset to name of imported function */
  u_int32_t function_address; /* Resolved address of imported function */
  u_int32_t data_segment;     /* Data segment value for imported func */
};

/***********************************************************************
 * Exported symbol type
 *
 * The exported symbols are an array of the following type.  The fields
 * in each element are stored in native machine order.
 ***********************************************************************/

struct xflat_export
{
  u_int32_t function_name;    /* Offset to name of exported function */
  u_int32_t function_address; /* Address of exported function */
};

#endif /* _XFLAT_H */
