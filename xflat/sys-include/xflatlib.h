/***********************************************************************
 * xflat/sys-include/xflatlib.h
 *
 * Copyright (C) 2002, 2006 Cadenux, LLC.  All rights reserved.
 * Copyright (C) 2002, 2006 Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#ifndef _XFLATLIB_H_
#define _XFLATLIB_H_

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include "xflat.h"

/***********************************************************************
 * Definitions
 ***********************************************************************/

/* If the loader path is not specified in the xFLT file, this is the
 * default loader that will be used.
 */

#define XFLAT_DEFAULT_LOADER_PATH "/lib"
#define XFLAT_DEFAULT_LOADER_NAME "ld-xflat.so.1"
#define XFLAT_DEFAULT_LOADER \
   XFLAT_DEFAULT_LOADER_PATH "/" XFLAT_DEFAULT_LOADER_NAME

/***********************************************************************
 * Public Types
 ***********************************************************************/

/* When DSpace is allocated, space is reserved at the beginning to
 * hold ldso-specific information.  The following structure defines
 * that information.  This structure can be referenced at run-time
 * using a negative offset from the PID base address.
 */

struct xflat_ldso_info
{
  u_int32_t dspace; /* The beginning of ldso DSpace */
};
#define XFLAT_DATA_OFFSET sizeof(struct xflat_ldso_info)

/* An "opaque" handle that describes the xflat binary to be loaded. */

typedef void *bin_handle_t;

/* An "opaque" handle that describes an open file */

typedef void *file_handle_t;

/* This is a call table that is used by the xflat library to call
 * obtain system information.  The use of this call table allows the
 * library to be designed in a platform independent way.
 */

struct xflat_vtbl
{
  /* Allocators.  These imports keep the xflat library independent of
   * the memory mapping and memory management facilities of the host
   * system.
   *
   *   map/unmap will map/unmap a program file onto an address;
   *   alloc/free will allocate/deallocate program memory.
   */

  void *(*map)(file_handle_t file_handle, u_int32_t nbytes);
  void  (*unmap)(void *address, u_int32_t nbytes);
  void *(*alloc)(u_int32_t nbytes);
  void  (*free)(void *address, u_int32_t nbytes);

  /* File access utilities.  These imports keep the xflat libary independent
   * of the host system's file system.
   */

  file_handle_t (*open)(bin_handle_t bin_handle, const char *filename);
  int   (*read)(bin_handle_t bin_handle, file_handle_t file_handle,
		char *dest, u_int32_t nbytes,
		u_int32_t fpos);
  void  (*close)(file_handle_t file_handle);
};

/* This struct provides a desciption of the currently loaded
 * instantiation of an xflat binary.
 */

struct xflat_load_info
{
  /* Instruction Space (ISpace):  This region contains the flat
   * file header plus everything from the text section.  Ideally,
   * will have only one text section instance in the system.
   */

  u_int32_t ispace;       /* Address where hdr/text is loaded */
                          /* 1st: struct xflat_hdr */
                          /* 2nd: text section */
  u_int32_t entry_offset; /* Offset from ispace to entry point */
  u_int32_t ispace_size;  /* Size of ispace. */

  /* Data Space (DSpace): This region contains all information that
   * in referenced as data.  There will be a unique instance of
   * DSpace for each instance of a process.
   */

  u_int32_t dspace;       /* Address where data/bss/stack/etc. is loaded */
                          /* 1st: Memory set aside for ldso */
  u_int32_t data_size;    /* 2nd: Size of data segment in dspace */
  u_int32_t bss_size;     /* 3rd: Size of bss segment in dspace */
                          /* 4th: Potential padding from relocs/mm/etc. */
  u_int32_t stack_size;   /* 5th: Size of stack in dspace */
  u_int32_t dspace_size;  /* Size of dspace (may be large than parts) */

  /* Program arguments (addresses in dspace) */

  u_int32_t arg_start;    /* Beginning of program arguments */
  u_int32_t env_start;    /* End of argments, beginning of env */
  u_int32_t env_end;      /* End(+4) of env */

  /* This is temporary memory where relocation records will be loaded. */

  u_int32_t reloc_start;  /* Start of array of struct flat_reloc */
  u_int32_t reloc_count;  /* Number of elements in reloc array */

  /* These are hooks stored by xflat_init for subsequent use.
   * These constitute all points of contact between the flat
   * library and the rest of the world.  These allows the flat
   * library to opperate in a variey of contexts without change.
   */

  bin_handle_t  bin_handle;   /* Like a "this" pointer.  Retains
			       * calling context information in callbacks */
  file_handle_t file_handle;  /* Describes an open file */

  const struct xflat_hdr  *header; /* A reference to the flat file header */
  const struct xflat_vtbl *vtbl;   /* Systam callback vtbl */

  /* At most one memory allocation will be made.  These describe that
   * allocation.
   */

  u_int32_t alloc_start; /* Start of the allocation */
  u_int32_t alloc_size;  /* Size of the allocation */
};

/***********************************************************************
 * Exported Functions
 *
 * These are the public functions exported by the xFLT library for 
 * use by the binfmt logic.
 ***********************************************************************/

/* Given the header from a possible xFLT executable, verify that it
 * is any kind of valid xFLT executable.
 */

extern int
xflat_verify_header(const struct xflat_hdr *header);

/* Given the header from a possible xFLT executable, verify that the
 * refers to what we think it is -- either a program or a library.
 */

extern int
xflat_verify_program(const struct xflat_hdr *header);

extern int
xflat_verify_library(const struct xflat_hdr *header);

/* This function is called to configure xflatlib to process an xFLT
 * program binary.  Upon return, the controlling logic has the opportunity
 * to adjust the contents of the load_info structure.
 */

extern int
xflat_init(bin_handle_t bin_handle, file_handle_t file_handle,
	   const struct xflat_hdr *header,
	   const struct xflat_vtbl *vtbl,
	   struct xflat_load_info *load_info);

/* This function is called to configure xflatlib to process an xFLT
 * interpreter binary.
 */

extern int
xflat_init_interpreter(bin_handle_t bin_handle,
		       struct xflat_load_info *prog_info,
		       struct xflat_load_info *ldr_info,
		       struct xflat_hdr *ldr_header,
		       const struct xflat_vtbl *vtbl);

/* This function unloads the object from memory. This essentially
 * undoes the actions of xflat_load.
 */

extern int
xflat_unload(struct xflat_load_info *load_info);

/* Releases any resources committed by xflat_init().  This essentially
 * undoes the actions of xflat_init or xflat_init_interpreter. */

extern int
xflat_uninit(struct xflat_load_info *load_info);

/* Loads the binary specified by xflat_init into memory,
 * Completes all relocations, and clears BSS.
 */

extern int
xflat_load(struct xflat_load_info *load_info);

/* Adjust stack size to include argc, envc, xFLT internal usage and
 * system internal usage. */

extern void
xflat_adjust_stack_size(struct xflat_load_info *load_info,
			int argc, int envc, int system_usage);

/* Initialize stack frame for execution */

extern u_int32_t
xflat_init_stack(struct xflat_load_info *prog_load_info,
		 struct xflat_load_info *lib_load_info,
		 int argc, int envc, char *p);

/* Releases any resources committed by xflat_init(). */

extern int
xflat_uninit(struct xflat_load_info *load_info);

#endif /* _XFLATLIB_H_ */
