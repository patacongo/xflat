<html>
<head>
<title>uClinux, File Mapping, and Shared Libraries</title>
</head>
<body background="backgd.gif">
</body>
<center><a name="toc"><h1>uClinux, File mapping, and Shared Libraries</h1></a><br>
<b>by<br>Gregory E. Nutt</b></center>

<p>&lt;&lt;<a href="XFLATTableOfContents.html">Index</a>&gt;&gt;</p>

<center><h2>Table of Contents</h2></center>
<li><a href="#FileMapping">File Mapping</a></li>
<li><a href="#SharedLibrariesAndFileMapping">Shared Libraries and File Mapping</a></li>
<li><a href="#uClinuxAndFileMapping">uClinux and File Mapping</a></li>
<li><a href="#SharedLibrariesAndXIP">Shared Libraries and XIP</a></li>
<li><a href="#XFLATCadenuxBSP">XFLAT and the Cadenux uClinux BSP</a></li>

<a name="FileMapping">
<h2>File Mapping</h2>
</a>

<p><b>Defined.</b>
File mapping is the association of a file's contents with a portion
of the virtual address space of a process.</p>

<p><b><tt>mmap()</tt>.</b>
In Linux, file mapping is accomplished with the <tt>mmap()</tt> system
call:</p>

<ul><pre>
void  *mmap(void *start, size_t length, int prot, int flags,
            int fd, off_t offset);
</pre></ul>

<p>The <tt>mmap()</tt> function asks the system to map <tt>length</tt>
bytes starting at <tt>offset</tt> from the file (or other object)
specified by the file descriptor <tt>fd</tt> into memory, preferably
at address <tt>start</tt>.
This latter <tt>address</tt> is a hint only, and is usually specified as 0.
The actual place where the object is mapped is returned by <tt>mmap</tt>.
The <tt>prot</tt> argument describes the desired memory protection.</p>

<blockquote><small><b>NOTE:</b>
You can actually do much more with <tt>mmap()</tt>.
Using the <tt>flags</tt> and <tt>prot</tt> options, you can get other
behaviors from <tt>mmap()</tt>.
Mappings do not have to be shared and mappings can be made to allocated
memory instead of files.
<tt>mmap()</tt> is often used simply to allocate memory.
In this paper, we will restrict our consideration to the use of
<tt>mmap()</tt> to provide shared file mappings.
</small></blockquote>

<p><b>Why <tt>mmap()</tt>?</b>
File mapping may be done for a number of reasons:</p>
<ol>
<li>Faster and easier file access,</li>
<li>Shared memory between two or more applications, or</li>
<li>Just because a flat memory representation of a file is a better
    design decision</li>
</ol>

<a name="SharedLibrariesAndFileMapping">
<h2>Shared Libraries and File Mapping</h2>
</a>

<p><b>Loading Binary Files</b>
File mapping is also used within the Linux kernel.
In particular, file mapping is used the by the binary loaders (see
<tt>linux/fs/binfmt_elf.c</tt>, <tt>linux/fs/binfmt_aout.c</tt>, etc.).
These are the kernel components that load an executable file, either
a program or a shared library, into memory in preparation for their
execution.
They do this by simply calling <tt>mmap()</tt> to map the executable
file into the user memory address space.</p>

<p><b>Shared and Private Program Segments.</b>
A Linux program consists of two classes of memory regions:  (1) Read-only
regions that can be shared by all executing copies of a program, and (2)
Write-able regions which are unique to each copy of the program.
The most important read-only region is the <tt>text</tt> section of a
program that contains the executable instructions comprising the program.
The <tt>text</tt> section is share-able by all executing instances of a
program;
many programs may be running, but they can all share the same in-memory
copy of the program <tt>text</tt> section.</p>

<blockquote><small>
<b><i>busybox</i>.</b>
<a href="http://www.busybox.org"><i>busybox</i></a> is a wonderful example
that exploits these shared <tt>text</tt> sections to conserve
memory in an embedded system.
<i>busybox</i> combines small versions of most, standard Unix utilities
together in one, relatively big executable file.
Although the single busybox executable is large, it is much smaller than
the combined sizes of all of the individual utilities.
And, since the <tt>text</tt> is shared via file mapping, there is never
more instance of the busybox.
The memory savings from <i>busybox</i> is one of most important enablers
of embedded Linux today.
(in addition to <a href="http://uclibc.org/"><i>uClibc</i></a>, both
from <b>Eric Anderson</b>.
The embedded Linux community owes a great debt to Eric and other key
collaborators for all of their efforts!)
</small></blockquote>

<p>In addition to the share-able, read-only memory region, each program
instance must have a private memory region where write-able, data is stored.
Such private memory regions are necessary for program variables
(for example, in <tt>.data</tt> and <tt>.bss</tt> sections), for program
stack, and for privately allocated memory (such as from <tt>malloc()</tt>).
These private sections allow each copy of a program to behave independently.
The incremental cost for each copy of a program in memory is only this
private data section (plus other, smaller kernel allocations).</p>

<p><b>Libraries.</b>
are collections of precompiled functions that have been written to be
reusable.
 Typically, they consist of sets of related functions to perform a
common task.</p>

<p><b>Static Libraries.</b>
The simplest library is a <i>static library</i>.
A static library is a type of <i>archive</i>.
An archive is a single file holding a collection of other files in a
format that makes it possible to retrieve the original individual files
(called members of the archive).
A static library is an archive whose members are object files.<p>

<p><b>Disadvantages of Static Libraries.</b>
One disadvantage of static libraries is that many programs may use
objects from the same library and, as a result, there may be many
copies of the same objects.
Multiple copies of the same objects consume a large amount of valuable
storage resources (RAM, ROM, disk space, etc.).
Also, when a static library is updated, all programs that use the
library must be recompiled in order to take advantage of the updated logic.
<i>Shared libraries</i> can overcome both of these disadvantages.</p>

<blockquote><small>
<b>NOTE:</b> There is a third, non-technical problem with the use of
static shared libraries:
The licensing for certain software packages (including GPL licensed
software) may compromise the legal status of intellectual properties
if software containing that property is linked directly with such
software packages.
</small></blockquote>

<p><b>Shared Libraries.</b>
A Linux shared library is similar to a Linux program in its memory usage:
The shared library's read-only segment (in particular, its <tt>.text</tt>
section) can be shared among all processes; while its write-able sections
(such as <tt>.data</tt> and <tt>.bss</tt>) can be allocated uniquely for
each executing process.</p>

<a name="uClinuxAndFileMapping">
<h2>uClinux and File Mapping</h2>
</a>

<p><b>uClinux.</b>
<a href="http://www.uclinux.org/">uClinux</a> is a special version of
Linux that is available for processors that do not have a memory management
unit (MMU).
Unfortunately, uClinux (2.4) does not support file mapping.
This limitation is because the MMU is necessary to implement true file mapping!
The MMU is required to map the user-space memory to the file position.
The MMU is necessary to perform updates to file when writes are made to
the mapped file memory region.
The MMU is necessary to enforce protections on the memory (such as the
read-only nature of a program mapping).</p>

<p>So without an MMU there can be no traditional mapping.
But without file mapping, there can be no file sharing.
There can be no sharing of program code sections.
There can be no shared libraries.
Busybox loses all of its appeal because multiple copies of busybox in
memory actually consume more memory than the individual utilities may
have.</p>

<p>But that can't be the case!
How does the MMU-less embedded Linux overcome this limitation of uClinux?</p>

<a name="SharedLibrariesAndXIP">
<h2>Shared Libraries and XIP</h2>
</a>

<p><b>XIP.</b>
The traditional solution to the file mapping limitation of uClinux (2.4)
is to use an <i>e<b>X</b>ecute <b>I</b>n <b>P</b>lace</i> (XIP) file system.
With an XIP file system, programs do not have to be copied from the file
system into system memory; rather, the program can execute <i>directly</i>
on the file system image.
The Linux file system subsystem supports special <i>hooks</i> into the
file system (the <tt>MAGIC_ROM_PTR</tt>) that allows certain files
systems function as XIP file system.</p>

<p><b>XIP and Shared <tt>TEXT</tt> Section.</b>
With XIP, there is no need to support file mapping to achieve
<tt>text</tt> section sharing as described above.
With XIP, full support for shared program sections and for shared
libraries is obtained because the program/library image on the XIP file
system is always unique.
The program/library is never copied to RAM, therefore, there is only one
shared copy of the read-only portions of the program/library.</p>

<p><b>XIP File System Limitations.</b>
Of course, the XIP file system must have special properties if it is
going to be used in this way:  The file system image must be provided
on an underlying medium that will support execution, i.e., it must be
a ROM, RAM, or FLASH based file system.  Also, the file system probably
must be read-only; you cannot permit modification of the contiguous
program images.  Keeping the program image contiguous precludes many
write-able file system strategies.
The popular <tt>romfs</tt> file system is an XIP file system that
has these properties.</p>

<a name="XFLATCadenuxBSP">
<h2>XFLAT and the Cadenux uClinux BSP</h2>
</a>

<p><b>The Cadenux uClinux 2.4 BSP.</b>
Cadenux, LLC, has recently incorporated a limited file mapping
capability into the uClinux kernel based on some initial uClinux file mapping
ideas published on the uClinux-dev mailing list.</p>

<blockquote><small>
<b>Credit Where Credit Is Due.</b>
The original patch set for uClinux 2.5 was developed by 
<a href="http://www.codewiz.org">Bernardo Innocenti</a> and appeared
in the uClinux-dev mailing list in June of 2003 (see
<a href="http://mailman.uclinux.org/pipermail/uclinux-dev/2003-June/018302.html">[uClinux-dev] PATCH: strip dead swap and file mapping code</a> and again
<a href="http://mailman.uclinux.org/pipermail/uclinux-dev/2003-June/019120.html">[uClinux-dev] PATCH: enable read-only filemapping for !CONFIG_MMU (2.5.73-uc0)</a>) but were not incorporated into uClinux 2.6.
</small></blockquote>

<p>Diego Dompe of Cadenux, LLC, used these initial ideas as a spring board,
back-ported the logic to the 2.4 kernel, made substantial improvements and
fully tested them.
The implementation is limited, of course, in that:  (1) the entire file
must be mapped into memory (rather than on demand),(2) writes to
the file image will not be reflected in the stored file, and (3) no
protections can be enforced.</p>

<p><b>uClinux File Mapping and Shared <tt>TEXT</tt> Sections.</b>
Although the file mapping provided in the uClinux (2.4) kernel is limited,
it is sufficient to support read-only, shared file mappings on a non-XIP
file system.
This means that program <tt>TEXT</tt> sections are always shared, no matter
what file system or block driver you are using.
<tt>TEXT</tt> sharing will work with <tt>ext2</tt> file systems,
<tt>FAT</tt> file systems, <tt>JFFS</tt> file systems, whatever file system
you choose to use.
And <tt>TEXT</tt> sharing will work no matter what block driver you are using or what the underlying media is:
It will work with RAM, ROM, IDE devices (such as compact flash or a rotating disk), MMC/SD,... whatever device you are using.</p>

<p><b>XFLAT Shared Library Technology.</b>
Cadenux, LLC, also develped an open-source shared library technology, called
XFLAT, that provides shared library support for uClinux on MMU-less processors.
At present, XFLAT is provided for the ARM processor family, however,
the XFLAT solution is easily ported to any processor without changes to the core toolchain (GCC, LD).</p>

<p>Combined with the file mapping changes to the uClinux (2.4) kernel, XFLAT
makes a very powerful solutions.
The Cadenux uClinux 2.4 kernel now supports true shared libraries on all file systems and on all block drivers.</p>

<p>&lt;&lt;<a href="#toc">Table of Contents</a>&gt;&gt;<br>
   &lt;&lt;<a href="XFLATTableOfContents.html">Index</a>&gt;&gt;</p>
</body>
</html>
