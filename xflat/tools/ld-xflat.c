/***********************************************************************
 * xflat/tools/ld-xflat.c
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>      /* For basename */

#include <sys/types.h>
#include <sys/wait.h>

#include "ld-options.h"
#include "ld-strings.h"

/***********************************************************************
 * Verify that we are fully configured
 ***********************************************************************/

#ifndef ARCH_LIB_DIR
# error ARCH_LIB_DIR not defined
#endif

#ifndef ARCHCC
# error ARCHCC not defined
#endif

#ifndef ARCH_CRT0XFLAT_PATH
# error ARCH_CRT0XFLAT_PATH not defined
#endif

#ifndef ARCH_CRT0NOLIBC_PATH
# error ARCH_CRT0NOLIBC_PATH not defined
#endif

#ifndef ARCH_SCRIPT_PATH
# error ARCH_SCRIPT_PATH not defined
#endif

#ifndef ARCH_INC_DIR
# error ARCH_INC_DIR not defined
#endif

#ifndef ARCH_LDELFLIB
# error ARCH_LDELFLIB not defined
#endif

#ifndef ARCH_LDELF2XFLT
# error ARCH_LDELF2XFLT not defined
#endif

#ifndef ARCH_CPU_FLAGS
# error ARCH_CPU_FLAGS not defined
#endif

#if defined(ARCHCXX_VER2X)

# ifndef ARCH_CRT1XFLAT_PATH
#  error ARCH_CRT1XFLAT_PATH not defined
# endif

# ifndef ARCH_CTORSXFLAT_PATH
#  error ARCH_CTORSXFLAT_PATH not defined
# endif

# ifndef ARCH_XFLATDUMMY_PATH
#  error ARCH_XFLATDUMMY_PATH not defined
# endif

#elif defined(ARCHCXX_VER3X)

# ifndef ARCH_CRT1XFLAT_PATH
#  error ARCH_CRT1XFLAT_PATH not defined
# endif

# ifndef ARCH_CRTIXFLAT_PATH
#  error ARCH_CRTIXFLAT_PATH not defined
# endif

# ifndef ARCH_CRTNXFLAT_PATH
#  error ARCH_CRTNXFLAT_PATH not defined
# endif

# ifndef ARCH_CRTBEGIN_PATH
#  error ARCH_CRT1BEGIN_PATH not defined
# endif

# ifndef ARCH_CRTEND_PATH
#  error ARCH_CRTEND_PATH not defined
# endif

# endif

/***********************************************************************
 * Defintions
 ***********************************************************************/

#define MAX_EXTRA_ARGS 64

/***********************************************************************
 * Private Variables
 ***********************************************************************/

static const char *tool_name       = NULL;
static const char *entry_name      = NULL;
static const char *exit_name       = NULL;
static const char *loader_name     = NULL;
static const char *output_name     = NULL;
static const char *output_basename = NULL;
static char *intermediate1         = NULL;
static char *intermediate2         = NULL;
static char *intermediate3         = NULL;
static char *intermediate4         = NULL;
static  char *output_debug_name    = NULL;

static char **objects              = NULL;
static char **dynamic_libraries    = NULL;
static char **static_libraries     = NULL;
static char **xflat_libpathes      = NULL;
static char **gcc_libpathes        = NULL;

static char arch_cpu_flags[]       = ARCH_CPU_FLAGS;
static char **arch_cpu_option_list = NULL;

static char *ldelflib_extra[MAX_EXTRA_ARGS];
static char *ldelf2xflt_extra[MAX_EXTRA_ARGS];

static int    verbose              = 0;
static int    link_cxx             = 0;
static int    is_shared_library    = 0;
static int    libc_is_included     = 0;
static int    link_static          = 0;
static int    gen_debug_file       = 0;

static int    nobjects             = 0;
static int    ndynamic_libraries   = 0;
static int    nstatic_libraries    = 0;
static int    nlibpathes           = 0;
static int    ncpuflags            = 0;
static int    nldelflib_extra      = 0;
static int    nldelf2xflt_extra    = 0;

static u_int32_t stack_size        = 0;

/***********************************************************************
 * Private Constants
 ***********************************************************************/

static const char archcc[]               = ARCHCC;
static const char crt0xflat_path[]       = ARCH_CRT0XFLAT_PATH;
static const char crt0nolibc_path[]      = ARCH_CRT0NOLIBC_PATH;
static const char arch_script_path[]     = "-Wl,-T," ARCH_SCRIPT_PATH;
static const char arch_inc_dir[]         = "-I"ARCH_INC_DIR;
static const char rpath[]                = "-Wl,-rpath," ARCH_LIB_DIR;
static const char arch_lib_path[]        = ARCH_LIB_DIR;
static const char gcc_arch_lib_path[]    = "-L" ARCH_LIB_DIR;

#if defined(ARCHCXX_VER2X)

static const char crt1xflat_path[]       = ARCH_CRT1XFLAT_PATH;
static const char ctorsxflat_path[]      = ARCH_CTORSXFLAT_PATH;
static const char xflatdummy_path[]      = ARCH_XFLATDUMMY_PATH;

#elif defined(ARCHCXX_VER3X)

static const char crt1xflat_path[]       = ARCH_CRT1XFLAT_PATH;
static const char crtixflat_path[]       = ARCH_CRTIXFLAT_PATH;
static const char crtnxflat_path[]       = ARCH_CRTNXFLAT_PATH;
static const char crtbegin_path[]        = ARCH_CRTBEGIN_PATH;
static const char crtend_path[]          = ARCH_CRTEND_PATH;

#endif

static const char arch_ldelflib[]        = ARCH_LDELFLIB;
static const char arch_ldelf2xflt[]      = ARCH_LDELF2XFLT;

static const char default_outname[]      = "a.out";
static const char intermediate1_suffix[] = ".r1";
static const char intermediate2_suffix[] = "-thunk.S";
static const char intermediate3_suffix[] = "-thunk.o";
static const char intermediate4_suffix[] = ".r2";
static const char debug_suffix[]         = ".debug";

/***********************************************************************
 * Private Functions
 ***********************************************************************/

/***********************************************************************
 * my_malloc
 ***********************************************************************/

static void *
my_malloc(u_int32_t size)
{
  void *retval = malloc(size);
  if (!retval)
    {
      fprintf(stderr, "Failed to malloc %d bytes\n", size);
      exit(1);
    }
  return retval;
}

/***********************************************************************
 * fork_exec_and_wait
 ***********************************************************************/

static int
fork_exec_and_wait(const char *path, char *const argv[])
{
  pid_t pid;
  int   status;
  int   i;

  /* Let the world no just what we are going to do */

  if (verbose)
    {
      for ( i = 0 ; argv[i] ; i++ )
	{
	  printf("arg[%2i] = %s\n", i, argv[i]);
	}
      fflush(stdout);
    }

  pid = fork();
  if (pid == -1)
    {
      fprintf(stderr, "fork failed\n");
      exit(1);
    }
  else if (pid == 0)
    {
      /* We are the child... exec the requested process. */

      status = execvp(path, argv);
      fprintf(stderr, "execvp returned\n");
      exit(1);
    }
  else
    {
      /* We are the parent.  Wait for the exec'ed process
       * to complete.
       */

      int waitstatus = waitpid(pid, &status, 0);
      if (waitstatus < 0)
	{
	  fprintf(stderr, "waitpid failed\n");
	  exit(1);
	}

      /* Was the chile processed terminated by a signal which
       * was not caught?
       */

      else if (WIFSIGNALED(status) != 0)
	{
	  status = 1;
	}

      /* Assume that the child process exited via a call to exit */

      else
	{
	  status = WEXITSTATUS(status);
	}
    }
  return status;
}

/***********************************************************************
 * parse_flags
 ***********************************************************************/

static void
parse_flags(void)
{
  char *ptr;
  int i;

  /* First, count options in the cpu flags */

  for (ncpuflags = 0, ptr = arch_cpu_flags; *ptr != '\0';)
    {
      /* Search for the beginning of the next option */

      for (; (*ptr == ' '); ptr++);

      /* Is there another option in the list? */

      if (*ptr != '\0')
	{
	  /* Yes... bump the option count */

	  ncpuflags++;

	  /* Search for the end of the option */

	  for (; ((*ptr != ' ') && (*ptr != '\0')); ptr++);
	}
    }

  /* Then allocate an array to hold references to the individual
   * options.
   */

  arch_cpu_option_list = my_malloc(sizeof(char*) * (ncpuflags));

  /* Now, pass through the CPU options one more time, adding
   * each option to the option list.
   */

  for (i = 0, ptr = arch_cpu_flags; *ptr != '\0';)
    {
      /* Search for the beginning of the next option */

      for (; (*ptr == ' '); ptr++) *ptr = '\0';

      /* Is there another option in the list? */

      if (*ptr != '\0')
	{
	  /* Yes... Save the reference to the option. */

	  arch_cpu_option_list[i++] = ptr;

	  /* Search for the end of the option */

	  for (; ((*ptr != ' ') && (*ptr != '\0')); ptr++);
	}
    }
}

/***********************************************************************
 * show_usage
 ***********************************************************************/

static void 
show_usage(void)
{
  printf("\nSYNOPSIS\n");
  printf("%s [options] objfile [objfile [...]]\n", tool_name);
  printf("\nOPTIONS\n");

  printf("\n-Bdynamic\n");
  printf("-dy\n");
  printf("-call_shared\n");
  printf("   Link against dynamic libraries.  This  option is the default.\n");
  printf("   You may use this option multiple times on the command line:\n");
  printf("   It affects library searching for \"-l\" options which follow\n");
  printf("   it.\n");

  printf("\n--cxx\n");
  printf("   Assume C++ conventions when linking (default: C conventions)\n");

  printf("\n--dynamic-linker <file>\n");
  printf("   Set the name of the dynamic linker.  This is only meaningful\n");
  printf("   when generating  dynamically  linked xFLT executables.  The\n");
  printf("   default dynamic linker is normally correct; don't use this\n");
  printf("   unless you know what you are doing.\n");

  printf("\n-e <name>\n");
  printf("--entry=<name>\n");
  printf("-init <name>");
  printf("   Use <name> as the explicit symbol for beginning execution of\n");
  printf("   your program or for initialization of your shared library.\n");
  printf("   If omitted, the default program entry point is \"start;\"\n");
  printf("   for shared libraries, there is no default initialization.\n");
       
  printf("\n-fini <name>\n");
  printf("   When creating an xFLT executable or shared object, call <name>\n");
  printf("   when the executable or shared  object is unloaded.  There\n");
  printf("   is no default\n");

  printf("\n-g\n");
  printf("   In addition to the xflat binary, generate a special ELF format\n");
  printf("   binary for use with a remote GDB debugger.  This special debug\n");
  printf("   binary has the same name as the xflat binary but with the added\n");
  printf("   extension, \".debug.\"  Note:  the \"-g\" is recognized by the\n");
  printf("   GNU ld, but ignored.\n");

  printf("\n--help\n");
  printf("   Print a summary of the command-line options on the standard\n");
  printf("   output and exit.\n");

  printf("\n-L<searchdir>\n");
  printf("--library-path=<searchdir>\n");
  printf("-rpath <searchdir>\n");
  printf("   Add path <searchdir> to the list of paths that will be\n");
  printf("   searched for shared libraries.  You may use these options\n");
  printf("   any number of times. The directories are searched in the\n");
  printf("   order in which they are specified on the command line.\n");
  printf("   Directories specified on the command line are searched\n");
  printf("   before the default directories.  All \"-L\" and \"-rpath\"\n");
  printf("   options apply to all \"-l\" options, regardless of the order\n");
  printf("   in which the options appear.\n");

  printf("\n-l<archive>\n");
  printf("--library=<archive>\n");
  printf("   Add a shared library file to the list of files to link.  This\n");
  printf("   option may be used any number of times.  %s will search its\n",
	 tool_name);
  printf("   path-list for occurrences of `lib<archive>.so' for every shared\n");
  printf("   library specified\n");

  printf("\n-o <output>\n");
  printf("--output=<output>\n");
  printf("   Use <output> as the name for the program or shared library\n");
  printf("   produced by %s.  If this option is not specified, the name\n",
	 tool_name);
  printf("   a.out is used by default.\n");

  printf("\n-shared\n");
  printf("-Bshareable\n");
  printf("   Create a shared library.\n");

  printf("\n--stack <reserve>\n");
  printf("   Specify the amount of memory to allocate for use as stack\n");
  printf("   for this program.  The default is 4Kb.\n");

  printf("\n-Bstatic\n");
  printf("-dn\n");
  printf("-non_shared\n");
  printf("-static\n");
  printf("   Do  not  link  against shared libraries. You  may  use\n"); 
  printf("   this  option  multiple  times  on the command line: it\n");
  printf("   affects library searching for \"-l\" options which  follow\n");
  printf("   it.\n");

  printf("\n--dll-verbose\n");
  printf("--verbose\n");
  printf("   Pass the corresponding verbose option on to all %s\n",
	 tool_name);
  printf("   sub-processes.\n");

  printf("\n-W1,<arg[,arg[...]]\n");
  printf("   Pass the specified argument(s) to the ldelflib sub-process\n");

  printf("\n-W2,<arg[,arg[...]]\n");
  printf("   Pass the specified argument(s) to the ldelf2xflt sub-process\n");
}

/***********************************************************************
 * parse_extra_args
 ***********************************************************************/

static void
parse_extra_args(char **arglist, int *nargs, char *option_argument)
{
  char *ptr  = option_argument;
  int   len  = strlen(option_argument);
  char *pend = ptr + len;
  int   n    = *nargs;

  /* Each argument begins with a comma */

  while (ptr < pend)
    {
      if (n < MAX_EXTRA_ARGS)
	{
	  arglist[n++] = ptr++;
	  for (; ((*ptr != ',') && (*ptr != '\0')); ptr++);
	  *ptr++ = '\0';
	}
      else
	{
	  fprintf(stderr, "Too many pass-through parameters!\n");
	  exit(1);
	}
    }
  *nargs = n;
}

/***********************************************************************
 * process_supported_option
 ***********************************************************************/

static void
process_supported_option(const struct supported_option_desc *desc,
			 char *option_argument)
{
  switch (desc->xflat_type)
    {
    case XFLAT_CXX:
      link_cxx++;
      break;

    case XFLAT_ENTRY_NAME:
      entry_name = option_argument;
      break;

    case XFLAT_EXIT_NAME:
      exit_name = option_argument;
      break;

    case XFLAT_LOADER_NAME:
      loader_name = option_argument;
      break;

    case XFLAT_LIBRARY_NAME:
      /* Ignore libgcc.a */

      if ((option_argument) && (strcmp(option_argument, "gcc") != 0))
	{
	  /* Check if libc is included in the link */

	  if (strcmp(option_argument, "c") == 0)
	    {
	      libc_is_included++;
	      option_argument = "c-xflat";
	    }
	  else if (strcmp(option_argument, "c-xflat") == 0)
	    {
	      libc_is_included++;
	    }

	  /* Save the library for later use. */
	  if (link_static)
	    {
	      /* Save the argument as -l<libname> for gcc */

	      int libsize = strlen(option_argument) + 3;
	      char *tmp = my_malloc(libsize);

	      sprintf(tmp, "-l%s", option_argument);
	      static_libraries[nstatic_libraries++] = tmp;
	    }
	  else
	    {
	      /* Just save the libname for ldelf2xflt */

	      dynamic_libraries[ndynamic_libraries++] = option_argument;
	    }
	}
      break;

    case XFLAT_LIBRARY_PATH:
      {
	/* Save the argument as -L<libpath> for gcc*/

	int libsize = strlen(option_argument) + 3;
	char *tmp = my_malloc(libsize);

	sprintf(tmp, "-L%s", option_argument);
	gcc_libpathes[nlibpathes] = tmp;

	/* AND save the straight path name for ldelf2xflt */

	xflat_libpathes[nlibpathes++] = option_argument;
      }
      break;

    case XFLAT_OUTPUT_NAME:
      output_name = option_argument;
      break;

    case XFLAT_SHARED_LIBRARY:
      is_shared_library++;
      break;

    case XFLAT_STACK_SIZE:
      stack_size = (u_int32_t)atol(option_argument);
      break;

    case XFLAT_VERBOSE:
      verbose++;
      break;

    case XFLAT_HELP:
      show_usage();
      exit(0);
      break;

    case XFLAT_LDELFLIB_ARGS:
      parse_extra_args(ldelflib_extra, &nldelflib_extra, option_argument);
      break;

    case XFLAT_LDELF2XFLT_ARGS:
      parse_extra_args(ldelf2xflt_extra, &nldelf2xflt_extra, option_argument);
      break;

    case XFLAT_STATIC:
      link_static = 1;
      break;

    case XFLAT_DYNAMIC:
      link_static = 0;
      break;

    case XFLAT_GEN_DEBUG:
      gen_debug_file++;
      break;

    default:
      fprintf(stderr, "Internal Error -- Illegal xflat option type\n");
      exit(1);
    }
}
  
/***********************************************************************
 * check_unsupported_option
 ***********************************************************************/

static const struct unsupported_option_desc *
check_unsupported_option(int *argndx, char **argv)
{
  const struct unsupported_option_desc *retdesc = NULL;
  int ndx = *argndx;

  /* Check if this is an option (must begin with "-") */

  if (argv[ndx][0] == '-')
    {
      const struct unsupported_option_desc *desc;
      int nunsupported_options;
      char *arg;
      int cndx;
      int len;
      int i;

      if (argv[ndx][1] == '-')
	{
	  desc = unsupported_options2;
	  nunsupported_options = nunsupported_options2;
	  cndx = 2;
	}
      else
	{
	  desc = unsupported_options1;
	  nunsupported_options = nunsupported_options1;
	  cndx = 1;
	}

      arg = &argv[ndx][cndx];

      for (i = 0; i < nunsupported_options; i++)
	{
	  switch ((enum ld_type_e)desc[i].ld_type)
	    {
	    case MATCH_WHOLE: /* Option is just the provided string. */
	      if (strcmp(desc[i].option, arg) == 0)
		{
		  fprintf(stderr,
			  "Unsupported option \"%s\" -- Ignored\n",
			  argv[ndx]);
		  retdesc = &desc[i];
		}
	      break;

	    case MATCH_TO_LENGTH: /* Option is <string><something> */
	      len = strlen(desc[i].option);
	      if (strncmp(desc[i].option, arg, len) == 0)
		{
		  fprintf(stderr,
			  "Unsupported option \"%s\" -- Ignored\n",
			  argv[ndx]);
		  retdesc = &desc[i];
		}
	      break;

	    case MATCH_TO_EQUAL: /* Option is <string>=<something> */
	      len = strlen(desc[i].option);
	      if ((strncmp(desc[i].option, arg, len) == 0) &&
		  (arg[len] == '='))
		{
		  fprintf(stderr,
			  "Unsupported option \"%s\" -- Ignored\n",
			  argv[ndx]);
		  retdesc = &desc[i];
		}
	      break;

	    case MATCH_PLUS_ARG:       /* Option is <string> <something> */
	      if (strcmp(desc[i].option, arg) == 0)
		{
		  fprintf(stderr,
			  "Unsupported option \"%s %s\" -- Ignored\n",
			  argv[ndx], argv[ndx+1]);
		  *argndx = ++ndx;
		  retdesc = &desc[i];
		}
	      break;

	    case MATCH_SPECIAL:        /* Option is something else */
	    default:
	      fprintf(stderr,
		      "Internal Error -- Illegal option type\n");
	      exit(1);
	    }
	}
    }
  return retdesc;
}

/***********************************************************************
 * check_supported_option
 ***********************************************************************/

static const struct supported_option_desc *
check_supported_option(int *argndx, char **argv, char **option_argument)
{
  const struct supported_option_desc *retdesc = NULL;
  int ndx = *argndx;

  /* Assume no optional argument */

  *option_argument = NULL;

  /* Check if this is an option (must begin with "-") */

  if (argv[ndx][0] == '-')
    {
      const struct supported_option_desc *desc;
      int nsupported_options;
      char *arg;
      int cndx;
      int len;
      int i;

      if (argv[ndx][1] == '-')
	{
	  desc = supported_options2;
	  nsupported_options = nsupported_options2;
	  cndx = 2;
	}
      else
	{
	  desc = supported_options1;
	  nsupported_options = nsupported_options1;
	  cndx = 1;
	}

      arg = &argv[ndx][cndx];

      for (i = 0; ((i < nsupported_options) && (retdesc == NULL)); i++)
	{
	  switch ((enum ld_type_e)desc[i].ld_type)
	    {
	    case MATCH_WHOLE: /* Option is just the provided string. */
	      if (strcmp(desc[i].option, arg) == 0)
		{
		  retdesc = &desc[i];
		}
	      break;

	    case MATCH_TO_LENGTH: /* Option is <string><something> */
	      len = strlen(desc[i].option);
	      if (strncmp(desc[i].option, arg, len) == 0)
		{
		  if (arg[len] != '\0')
		    {
		      *option_argument = &arg[len];
		    }
		  else
		    {
		      fprintf(stderr,
			      "Missing value for option \"%s\"\n",
			      argv[ndx]);
		    }
		  retdesc = &desc[i];
		}
	      break;
	    case MATCH_TO_EQUAL: /* Option is <string>=<something> */
	      len = strlen(desc[i].option);
	      if ((strncmp(desc[i].option, arg, len) == 0) &&
		  (arg[len] == '='))
		{
		  if (arg[len+1] != '\0')
		    {
		      *option_argument = &arg[len+1];
		    }
		  else
		    {
		      fprintf(stderr,
			      "Missing value for option \"%s\"\n",
			      argv[ndx]);
		    }
		  retdesc = &desc[i];
		}
	      break;
	    case MATCH_PLUS_ARG:       /* Option is <string> <something> */
	      if (strcmp(desc[i].option, arg) == 0)
		{
		  ndx++;
		  if ((argv[ndx] != NULL) && (argv[ndx][0] != '\0'))
		    {
		      *option_argument = argv[ndx];
		      *argndx = ndx;
		    }
		  else
		    {
		      fprintf(stderr,
			      "Missing value for option \"%s\"\n",
			      argv[ndx-1]);
		    }
		  retdesc = &desc[i];
		}
	      break;
	    case MATCH_SPECIAL:        /* Option is something else */
	    default:
	      fprintf(stderr, "Internal Error -- Illegal option type\n");
	      exit(1);
	    }
	}
    }
  return retdesc;
}

/***********************************************************************
 * parse_args
 ***********************************************************************/

/* SYNOPSIS: ld [ options ] objfile... */

static void
parse_args(int argc, char **argv)
{
  char *option_argument;
  int i;

  /* Allocate space to hold library names.  The maximum that we
   * could possibly have is argc.
   */

  ndynamic_libraries = 0;
  dynamic_libraries = my_malloc(sizeof(char*) * (argc));

  nstatic_libraries = 0;
  static_libraries = my_malloc(sizeof(char*) * (argc));

  /* Allocate space to hold library pathes.  The maximum that we
   * could possibly have is argc.
   */

  nlibpathes = 0;
  xflat_libpathes = my_malloc(sizeof(char*) * (argc));
  gcc_libpathes = my_malloc(sizeof(char*) * (argc));

  /* Allocate space to source files that are simply passed through
   * to gcc from the command line.
   */

  nobjects = 0;
  objects = my_malloc(sizeof(char*) * (argc+1));

  /* Now examine each argument.  If the argument is recognized and
   * captured in this loop, its argv pointer will be nullified.
   * The remaining non-null pointers will be passed through to
   * gcc.
   */

  for ( i = 1 ; i < argc ; i++ )
    {
      /* If it begins with '-', it must be an argument */

      if (argv[i][0] == '-')
	{
	  const struct unsupported_option_desc *udesc = NULL;
	  const struct supported_option_desc   *sdesc = NULL;

	  sdesc =  check_supported_option(&i, argv, &option_argument);
	  if (sdesc)
	    {
	      process_supported_option(sdesc, option_argument);
	    }
	  else
	    {
	      udesc = check_unsupported_option(&i, argv);
	      if (!udesc)
		{
		  fprintf(stderr,
			  "Unrecognized option \"%s\" -- Ignored\n",
			  argv[i]);
		}
	    }
	}
      else
	{
	  /* Otherwise, it must be an object file */

	  objects[nobjects++] = argv[i];
	}
    }

  /* No do some final sanity checking. */

  if (nobjects == 0)
    {
      fprintf(stderr, "No object files provided\n");
      exit(1);
    }

  if (output_name == NULL)
    {
      output_name = default_outname;

    }
  output_basename = basename((char*)output_name);

  /* Create the name of the first partial link */

  intermediate1 = my_malloc(strlen(output_basename)
			    + strlen(intermediate1_suffix)
			    + 1);
  sprintf(intermediate1, "%s%s", output_basename, intermediate1_suffix);

  /* Create the name of the thunk source file */

  intermediate2 = my_malloc(strlen(output_basename)
			    + strlen(intermediate2_suffix)
			    + 1);
  sprintf(intermediate2, "%s%s", output_basename, intermediate2_suffix);

  /* Create the name of the thunk object file */

  intermediate3 = my_malloc(strlen(output_basename)
			    + strlen(intermediate3_suffix)
			   + 1);
  sprintf(intermediate3, "%s%s", output_basename, intermediate3_suffix);

  /* Create the name of the second partial link */

  intermediate4 = my_malloc(strlen(output_basename)
			    + strlen(intermediate4_suffix)
			    + 1);
  sprintf(intermediate4, "%s%s", output_basename, intermediate4_suffix);

  /* Create the name of the (optional) debug output file */

  output_debug_name = my_malloc(strlen(output_name)
			    + strlen(debug_suffix)
			    + 1);
  sprintf(output_debug_name, "%s%s", output_name, debug_suffix);
}

/***********************************************************************
 * parse_tool_name
 ***********************************************************************/

static void parse_tool_name(char *program_name)
{
  tool_name = basename(program_name);
  if (tool_name[0] == '-')
    {
      tool_name++;
    }
}

/***********************************************************************
 * exec_ldelf2xflt
 ***********************************************************************/

/* Usage: ./ldelf2xflt [options] <bfd-filename>
 * 
 * Where options are one or more of the following.  Note
 * that a space is always required between the option and
 * any following arguments
 * 
 *   -d Use dynamic symbol table [symtab]
 *   -e <entry-point>
 *      Entry point to module [_start for executable];
 *      NULL for shared library
 *   -l Build a shared library binary [executable]
 *   -o <out-filename>
 *      Output to <out-filename> [<bfd-filename>.xflt]
 *   -s <stack-size>
 *      Set stack size to <stack-size>.  Ignored if -l also     
 *      selected. [4096]
 *   -v Verbose output [no verbose output]
 */
/* /home/gnutt/projects/xflat/tests/hello/../../tools/ldelf2xflt -v -o hello hello.r2 */

static int
exec_ldelf2xflt(void)
{
  char stack_size_string[25];
  char **ldelf2xflt_argv;
  int nargs = 0;
  int retval;
  int i;

  /* Create the arguments that we will pass to ldelflib */

  ldelf2xflt_argv =
    my_malloc(sizeof(char*) * (nldelf2xflt_extra + 12));

  /* The version of ldelf2xflt that we are going to be using. */

  ldelf2xflt_argv[nargs++] = (char*)arch_ldelf2xflt;

  /* Generate verbose output */

  if (verbose)
    {
      ldelf2xflt_argv[nargs++] = "-v";
    }

  /* Generate a shared library */

  if (is_shared_library > 0)
    {
      ldelf2xflt_argv[nargs++] = "-l";
    }

  /* Select stack size */

  if (stack_size > 0)
    {
      sprintf(stack_size_string, "%d", stack_size);
      ldelf2xflt_argv[nargs++] = "-s";
      ldelf2xflt_argv[nargs++] = stack_size_string;
    }

  /* Add the entry point symbol */
  if (entry_name)
    {
      ldelf2xflt_argv[nargs++] = "-s";
      ldelf2xflt_argv[nargs++] = (char*)entry_name;
    }

  /* Add any extra arguments */

  for (i = 0; i < nldelf2xflt_extra; i++)
    {
      ldelf2xflt_argv[nargs++] = ldelf2xflt_extra[i];
    }

  /* Add the output file name */

  ldelf2xflt_argv[nargs++] = "-o";
  ldelf2xflt_argv[nargs++] = (char*)output_name;

  /* Add the input intermediate file */

  ldelf2xflt_argv[nargs++] = intermediate4;
  ldelf2xflt_argv[nargs++] = NULL;

  retval = fork_exec_and_wait(arch_ldelf2xflt, ldelf2xflt_argv);
  free(ldelf2xflt_argv);
  return retval;
}

/***********************************************************************
 * exec_ldelflib
 ***********************************************************************/

/* Usage: ./ldelflib [options] <bfd-filename>
 * 
 * Where options are one or more of the following.  Note
 * that a space is always required between the option and
 * any following arguments
 * 
 *   -L <library-path-name>
 *       Path to FLAT shared libraries.  A maximum of 8
 *   -W Do NOT export weakly defined functions, i.e.,
 *       weakly defined functions will NOT be available to
 *       modules load-time [exported]
 *   -X <symbol_name>
 *       Export the symbol <symbol_name> from this module.
 *       The -X option may be specified numerous times to
 *       specified multiple export symbols (up to 1024 times).
 *       Default behavior to export [all global functions].
 *   -Y <symbol_name>
 *       Do NOT export the symbol <symbol_name> from this module.
 *       module. The -Y option may be specified numerous times
 *       to specify multiple export symbols (up to 1024 times).
 *       Note: -Y cannot be specified with -X.
 *       Default behavior to export [all global functions].
 *   -d Use dynamic symbol table [symtab]
 *   -f <cmd-filename>
 *       Take next commands from <cmd-filename> [cmd-line]
 *   -l <abbrev>
 *       Include shared library of name lib<abbrev>.so
 *       A maximum of 16 library abbreviations may be provided.
 *   -o <out-filename>
 *       Output to <out-filename> [stdout]
 *   -p <max_threads>
 *      The maximum number of threads that can make simultaneous
 *      make calls into a shared library [6]
 *   -t Enable tracing of outbound shared library function
 *      calls. [no tracing]
 *   -v Verbose output [no output]
 *   -w Import weakly declared functions, i.e., weakly
 *      declared functions are expected to be provided at
 *      load-time [not imported]
 *   -x <loader-pathname>
 *      This provides the full pathname of the xFLT loader
 *      that should be used to load this file. If this
 *      name is not present, the system will use a default
 *      loader pathname [default loader].
 */

static int
exec_ldelflib(void)
{
  char **ldelflib_argv;
  int nargs = 0;
  int retval;
  int i;

  /* Create the arguments that we will pass to ldelflib */

  ldelflib_argv =
    my_malloc(sizeof(char*) *
	      (2*ndynamic_libraries + 2*nlibpathes + nldelflib_extra + 20));

  /* The version of ldelflib that we are going to be using. */

  ldelflib_argv[nargs++] = (char*)arch_ldelflib;

  /* Generate verbose output */

  if (verbose)
    {
      ldelflib_argv[nargs++] = "-v";
    }

  /* Add the name of the loader to use */

  if (loader_name)
    {
      ldelflib_argv[nargs++] = "-x";
      ldelflib_argv[nargs++] = (char*)loader_name;
    }
    
  /* Add any extra arguments */

  for (i = 0; i < nldelflib_extra; i++)
    {
      ldelflib_argv[nargs++] = ldelflib_extra[i];
    }

  /* Add all of the library pathes */

  for (i = 0; i < nlibpathes; i++)
    {
      if (xflat_libpathes[i])
	{
	  ldelflib_argv[nargs++] = "-L";
	  ldelflib_argv[nargs++] = xflat_libpathes[i];
	}
    }

  /* Add all of the libraries */

  for (i = 0; i < ndynamic_libraries; i++)
    {
      if (dynamic_libraries[i])
	{
	  ldelflib_argv[nargs++] = "-l";
	  ldelflib_argv[nargs++] = dynamic_libraries[i];

#ifdef BUNDLE_XFLAT_PTHREADS
	  if (!strcmp("c-xflat",dynamic_libraries[i]))
	    {
	      /* Always add libpthreads to the list, since libc weak functions will not
	       * work without it
	       */

	      ldelflib_argv[nargs++] = "-l";
	      ldelflib_argv[nargs++] = "pthread-xflat";
	    }
#endif	  
	}
    }

  /* Add the output file */

  ldelflib_argv[nargs++] = "-o";
  ldelflib_argv[nargs++] = intermediate2;

  /* And the input file */

  ldelflib_argv[nargs++] = intermediate1;

  /* And don't forget to stick a NULL at the end of the argument
   * list.
   */

  ldelflib_argv[nargs++] = NULL;

  retval = fork_exec_and_wait(arch_ldelflib, ldelflib_argv);
  free(ldelflib_argv);
  return retval;
}

/***********************************************************************
 * exec_gcc3
 ***********************************************************************/

static int
exec_gcc3(void)
{
  char **gcc_argv = my_malloc(sizeof(char*) * 10);
  int nargs = 0;
  int retval;

  /* The version of GCC that we are going to be using. */

  gcc_argv[nargs++] = (char*)archcc;

  /* Generate verbose output */

  if (verbose)
    {
      gcc_argv[nargs++] = "-v";
    }

  /* Provide the script file that tells GCC how we must
   * organize memory.
   */

  gcc_argv[nargs++] = (char*)arch_script_path;

  /* These things are already taken care of in intermediate4 */

  gcc_argv[nargs++] = "-nostdlib";
  gcc_argv[nargs++] = "-nostartfiles";
  gcc_argv[nargs++] = "-Wl,--no-check-sections";

  /* The file we are going to generate. */

  gcc_argv[nargs++] = "-o";
  gcc_argv[nargs++] = output_debug_name;

  /* This is the single input file */

  gcc_argv[nargs++] = intermediate4;
  gcc_argv[nargs++] = NULL;

  retval = fork_exec_and_wait(archcc, gcc_argv);
  free(gcc_argv);
  return retval;
}

/***********************************************************************
 * exec_gcc2
 ***********************************************************************/

static int
exec_gcc2(void)
{
  char **gcc_argv = my_malloc(sizeof(char*) * (ncpuflags + 10));
  int nargs = 0;
  int retval;
  int i;

  /* The version of GCC that we are going to be using. */

  gcc_argv[nargs++] = (char*)archcc;

  /* Generate verbose output */

  if (verbose)
    {
      gcc_argv[nargs++] = "-v";
    }

  /* Add each of the CPU flags to the arguments */

  for (i = 0; i < ncpuflags; i++)
    {
      gcc_argv[nargs++] = arch_cpu_option_list[i];
    }
    
  /* Compile only */

  gcc_argv[nargs++] = "-c";

  /* The file we are going to generate. */

  gcc_argv[nargs++] = "-o";
  gcc_argv[nargs++] = intermediate3;

  /* This is the single input file */

  gcc_argv[nargs++] = intermediate2;
  gcc_argv[nargs++] = NULL;

  retval = fork_exec_and_wait(archcc, gcc_argv);
  free(gcc_argv);
  return retval;
}

/***********************************************************************
 * exec_gcc1
 ***********************************************************************/

/* Execute GCC to generate a single, partially linked object.  This is
 * the object that ldelflib will examine to generate the shared library
 * thunk.
 *
 * pass==1 : This is the initial link and does not include the auto-
 *           generated thunk layer
 * pass==2 : This is the final link (before ldelf2xflat) and th
 *           object list does include the thunk layer.
 */

static int
exec_gcc1(char *outintermediate, int pass)
{
  char **gcc_argv;
  int startup_needed = 1;
  int nargs = 0;
  int alloc_size;
  int retval;
  int i;

  alloc_size = sizeof(char*) * (nobjects + nstatic_libraries + 20);
  if (nstatic_libraries > 0)
    {
      alloc_size += sizeof(char*) * nlibpathes;
    }

  gcc_argv = my_malloc(alloc_size);

  /* If we are linking a shared library, we won't need a startup file */

  if (is_shared_library > 0)
    {
      startup_needed = 0;
    }
  else
    {
      /* We are linking an executable.  Check if a crt0/1 file is already
       * in the object list */

      for (i = 0; ((i < nobjects) && (startup_needed != 0)); i++)
	{
	  char *objfile = basename(objects[i]);
	  if ((strncmp(objfile, "crt0", 4) == 0) ||
	      (strncmp(objfile, "crt1", 4) == 0))
	    {
	      /* The user has specified a crt0* or crt1* file.  We will assume
	       * that the user knows what they are doing and we won't bother
	       * with startup files.
	       */
	  
	      startup_needed = 0;
	    }
	}
    }

  /* The version of GCC that we are going to be using. */

  gcc_argv[nargs++] = (char*)archcc;

  /* Generate verbose output */

  if (verbose)
    {
      gcc_argv[nargs++] = "-v";
    }
    
  /* The file we are going to generate. */

  gcc_argv[nargs++] = "-o";
  gcc_argv[nargs++] = (char*)outintermediate;

  /* -r = generate relocatable output (partial link)
   * -d = assign common symbols in relocatable output.
   * -warn-common = Warn  when a common symbol is combined with
   *      another common symbol or with a symbol definition
   */

  gcc_argv[nargs++] = "-Wl,-r";
  gcc_argv[nargs++] = "-Wl,-d";
  gcc_argv[nargs++] = "-Wl,-warn-common";

  /* We are going to include libgcc.a */

  gcc_argv[nargs++] = (char*)gcc_arch_lib_path;

  /* If we going to link in any other shared libraries,
   * then add all of the other library pathes that we
   * may have.
   */

  if (nstatic_libraries > 0)
    {
      for (i = 0; i < nlibpathes; i++)
	{
	  gcc_argv[nargs++] = gcc_libpathes[i];
	}
    }

  /* Well do everything ourselves. */

  gcc_argv[nargs++] = "-nostdlib";
  gcc_argv[nargs++] = "-nostartfiles";

  /* We never want to link in dynamic libraries! */

  gcc_argv[nargs++] = "-static";

  /* Provide the script file that tells GCC how we must
   * organize memory.
   */

  gcc_argv[nargs++] = (char*)arch_script_path;

  /* Add a startup file(s) at the begining of the object list */

  if (startup_needed)
    {
      /* C++ convertions require crt1 at the beginning and..
       * well the rest depends on the GNU compiler version
       */

      if (link_cxx)
	{
#if defined(ARCHCXX_VER2X)
	  gcc_argv[nargs++] = (char*)crt1xflat_path;
#elif defined(ARCHCXX_VER3X)
	  gcc_argv[nargs++] = (char*)crt1xflat_path;
	  gcc_argv[nargs++] = (char*)crtixflat_path;
	  gcc_argv[nargs++] = (char*)crtbegin_path;
#endif
	}

      /* C just needs crt0 but we'll handle the case where
       * is no libc as well.
       */

      else if (libc_is_included)
	{
	  gcc_argv[nargs++] = (char*)crt0xflat_path;
	}
      else
	{
	  gcc_argv[nargs++] = (char*)crt0nolibc_path;
	}
    }

  /* Then add all of the objects */

  for (i = 0; i < nobjects; i++)
    {
      if (objects[i])
	{
	  gcc_argv[nargs++] = objects[i];
	}
    }

  /* Always add libgcc.a to the list.  If we don't need anything
   * from it, then there is no harm done.
   */

  gcc_argv[nargs++] = "-lgcc";

  /* Then add all of the static libraries */

  if (nstatic_libraries)
    {
      for (i = 0; i < nstatic_libraries; i++)
	{
	  gcc_argv[nargs++] = static_libraries[i];
	}

      /* And add libgcc again in case we picked up some new 
       * requirements.
       */

      gcc_argv[nargs++] = "-lgcc";
    }

  /* If we are linking for C++ then add libgcc_eh.a to the list
   * as well.
   */

#if defined(ARCHCXX_VER3X)
#if 0 /* Don't think this is really necessary */
  if (link_cxx)
    {
      gcc_argv[nargs++] = "-lgcc_eh";
    }
#endif
#endif

  /* If we are linking for C++, then we will need to include
   * some terminating stuff which depends on the GNU compiler
   * version.
   */

  if (startup_needed && link_cxx)
    {
#if defined(ARCHCXX_VER2X)
      if (pass == 1)
	{
	  gcc_argv[nargs++] = (char*)xflatdummy_path;
	}
      else
	{
	  gcc_argv[nargs++] = (char*)ctorsxflat_path;
	}
#elif defined(ARCHCXX_VER3X)
        gcc_argv[nargs++] = (char*)crtend_path;
	gcc_argv[nargs++] = (char*)crtnxflat_path;
#endif
    }

  /* And don't forget to stick a NULL at the end of the argument
   * list.
   */

  gcc_argv[nargs++] = NULL;

  retval = fork_exec_and_wait(archcc, gcc_argv);
  free(gcc_argv);
  return retval;
}

/***********************************************************************
 * Public Functions
 ***********************************************************************/

/***********************************************************************
 * main
 ***********************************************************************/

int main(int argc, char **argv, char **envp)
{
  int status = 0;

  /* Find out which tool we are going to emulate */

  parse_tool_name(argv[0]);

  /* Then parse the incoming arguments */

  parse_args(argc, argv);

  /* Break up the CPU flags into individual elementes */

  parse_flags();

  /* Are there multiple object files?  If so, we will
   * have to run gcc to create one partially linked file
   */

  /* Execute gcc -- generating partially linked intermediate1 */

  status = exec_gcc1(intermediate1, 1);
  if (status != 0)
    {
      /* If gcc fails, so do we */

      goto exit_with_status;
    }

  /* Then execute ldelflib -- generating source file intermediate2 */

  status = exec_ldelflib();
  if (status != 0)
    {
      /* If ldelflib fails, so do we */

      goto exit_removing_intermediate1;
    }

  /* Execute gcc again to compile the thunk and link the whole mess.
   * This generates object file intermediate3 */

  status = exec_gcc2();
  if (status != 0)
    {
      /* If gcc fails, so do we */

      goto exit_removing_intermediate2;
    }

  /* Add intermediate3 at the end of the list of objects */

  objects[nobjects++] = intermediate3;

  /* Execute gcc -- generating partially linked intermediate4 */

  status = exec_gcc1(intermediate4, 2);
  if (status != 0)
    {
      /* If gcc fails, so do we */

      goto exit_removing_intermediate3;
    }

  /* Then execute ldelf2xflt to produce the final xflat binary
   * from intermediate4. */

  status = exec_ldelf2xflt();
  if (status != 0)
    {
      /* If gcc fails, so do we */

      goto exit_removing_intermediate4;
    }

  /* Finally, if a debug output is selected, link one more time
   * to produce the debug output file.
   */

  if (gen_debug_file)
    {
      status = exec_gcc3();
      if (status != 0)
	{
	  /* If gcc fails, so do we */

	  goto exit_removing_output_files;
	}
    }
  goto exit_removing_intermediate4;

  /* Clean up temporary files */

 exit_removing_output_files:
  fprintf(stderr, "Removing %s\n", output_name);
  (void)remove(output_name);
  (void)remove(output_debug_name);
 exit_removing_intermediate4:
  (void)remove(intermediate4);
 exit_removing_intermediate3:
  (void)remove(intermediate3);
 exit_removing_intermediate2:
  (void)remove(intermediate2);
 exit_removing_intermediate1:
  (void)remove(intermediate1);
 exit_with_status:
  return status;
}
