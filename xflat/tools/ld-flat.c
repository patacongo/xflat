/***********************************************************************
 * xflat/tools/ld-flat.c
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>      /* For basename */

#include <sys/types.h>
#include <sys/wait.h>

#include "ld-options.h"
#include "ld-strings.h"

/***********************************************************************
 * Verify that we are fully configured
 ***********************************************************************/

#ifndef ARCH_LIB_DIR
# error ARCH_LIB_DIR not defined
#endif

#ifndef ARCHCC
# error ARCHCC not defined
#endif

#ifndef ARCH_CRT0FLAT_PATH
# error ARCH_CRT0FLAT_PATH not defined
#endif

#ifndef ARCH_CRT0NOLIBC_PATH
# error ARCH_CRT0NOLIBC_PATH not defined
#endif

#ifndef ARCH_SCRIPT_FLAT
# error ARCH_SCRIPT_FLAT not defined
#endif

#ifndef ARCH_INC_DIR
# error ARCH_INC_DIR not defined
#endif

#ifndef ARCH_LDELF2FLT
#error ARCH_LDELF2FLT not defined
#endif

#ifndef ARCH_CPU_FLAGS
#error ARCH_CPU_FLAGS not defined
#endif

/***********************************************************************
 * Defintions
 ***********************************************************************/

#define MAX_EXTRA_ARGS 64

/***********************************************************************
 * Private Variables
 ***********************************************************************/

static const char *tool_name       = NULL;
static const char *entry_name      = NULL;
static const char *output_name     = NULL;
static const char *output_basename = NULL;
static char *intermediate          = NULL;
static char *output_debug_name     = NULL;

static char **objects              = NULL;
static char **dynamic_libraries    = NULL;
static char **static_libraries     = NULL;
static char **flat_libpathes       = NULL;
static char **gcc_libpathes        = NULL;

static char arch_cpu_flags[]       = ARCH_CPU_FLAGS;
static char **arch_cpu_option_list = NULL;

static char *ldelf2flt_extra[MAX_EXTRA_ARGS];

static int    verbose              = 0;
static int    libc_is_included     = 0;
static int    gen_debug_file       = 0;

static int    nobjects             = 0;
static int    ndynamic_libraries   = 0;
static int    nstatic_libraries    = 0;
static int    nlibpathes           = 0;
static int    ncpuflags            = 0;
static int    nldelf2flt_extra     = 0;

static u_int32_t stack_size        = 0;

/***********************************************************************
 * Private Constants
 ***********************************************************************/

static const char archcc[]               = ARCHCC;
static const char crt0flat_path[]        = ARCH_CRT0FLAT_PATH;
static const char crt0nolibc_path[]      = ARCH_CRT0NOLIBC_PATH;
static const char arch_script_path[]     = "-Wl,-T," ARCH_SCRIPT_FLAT;
static const char arch_inc_dir[]         = "-I"ARCH_INC_DIR;
static const char rpath[]                = "-Wl,-rpath," ARCH_LIB_DIR;
static const char arch_lib_path[]        = ARCH_LIB_DIR;
static const char gcc_arch_lib_path[]    = "-L" ARCH_LIB_DIR;

static const char arch_ldelf2flt[]       = ARCH_LDELF2FLT;

static const char default_outname[]      = "a.out";
static const char intermediate_suffix[]  = ".r";
static const char debug_suffix[]         = ".debug";

/***********************************************************************
 * Private Functions
 ***********************************************************************/

/***********************************************************************
 * my_malloc
 ***********************************************************************/

static void *
my_malloc(u_int32_t size)
{
  void *retval = malloc(size);
  if (!retval)
    {
      fprintf(stderr, "Failed to malloc %ld bytes\n", size);
      exit(1);
    }
  return retval;
}

/***********************************************************************
 * fork_exec_and_wait
 ***********************************************************************/

static int
fork_exec_and_wait(const char *path, char *const argv[])
{
  pid_t pid;
  int   status;
  int   i;

  /* Let the world no just what we are going to do */

  if (verbose)
    {
      for ( i = 0 ; argv[i] ; i++ )
	{
	  printf("arg[%2i] = %s\n", i, argv[i]);
	}
      fflush(stdout);
    }

  pid = fork();
  if (pid == -1)
    {
      fprintf(stderr, "fork failed\n");
      exit(1);
    }
  else if (pid == 0)
    {
      /* We are the child... exec the requested process. */

      status = execvp(path, argv);
      fprintf(stderr, "execvp returned\n");
      exit(1);
    }
  else
    {
      /* We are the parent.  Wait for the exec'ed process
       * to complete.
       */

      int waitstatus = waitpid(pid, &status, 0);
      if (waitstatus < 0)
	{
	  fprintf(stderr, "waitpid failed\n");
	  exit(1);
	}

      /* Was the chile processed terminated by a signal which
       * was not caught?
       */

      else if (WIFSIGNALED(status) != 0)
	{
	  status = 1;
	}

      /* Assume that the child process exited via a call to exit */

      else
	{
	  status = WEXITSTATUS(status);
	}
    }
  return status;
}

/***********************************************************************
 * parse_flags
 ***********************************************************************/

static void
parse_flags(void)
{
  char *ptr;
  int i;

  /* First, count options in the cpu flags */

  for (ncpuflags = 0, ptr = arch_cpu_flags; *ptr != '\0';)
    {
      /* Search for the beginning of the next option */

      for (; (*ptr == ' '); ptr++);

      /* Is there another option in the list? */

      if (*ptr != '\0')
	{
	  /* Yes... bump the option count */

	  ncpuflags++;

	  /* Search for the end of the option */

	  for (; ((*ptr != ' ') && (*ptr != '\0')); ptr++);
	}
    }

  /* Then allocate an array to hold references to the individual
   * options.
   */

  arch_cpu_option_list = my_malloc(sizeof(char*) * (ncpuflags));

  /* Now, pass through the CPU options one more time, adding
   * each option to the option list.
   */

  for (i = 0, ptr = arch_cpu_flags; *ptr != '\0';)
    {
      /* Search for the beginning of the next option */

      for (; (*ptr == ' '); ptr++) *ptr = '\0';

      /* Is there another option in the list? */

      if (*ptr != '\0')
	{
	  /* Yes... Save the reference to the option. */

	  arch_cpu_option_list[i++] = ptr;

	  /* Search for the end of the option */

	  for (; ((*ptr != ' ') && (*ptr != '\0')); ptr++);
	}
    }
}

/***********************************************************************
 * show_usage
 ***********************************************************************/

static void 
show_usage(void)
{
  printf("\nSYNOPSIS\n");
  printf("%s [options] objfile [objfile [...]]\n", tool_name);
  printf("\nOPTIONS\n");

  printf("\n-e <name>\n");
  printf("--entry=<name>\n");
  printf("   Use <name> as the explicit symbol for beginning execution of\n");
  printf("   your program.  If omitted, the default program entry point is \"start;\"\n");
       
  printf("\n-g\n");
  printf("   In addition to the flat binary, generate a special ELF format\n");
  printf("   binary for use with a remote GDB debugger.  This special debug\n");
  printf("   binary has the same name as the flat binary but with the added\n");
  printf("   extension, \".debug.\"  Note:  the \"-g\" is recognized by the\n");
  printf("   GNU ld, but ignored.\n");

  printf("\n--help\n");
  printf("   Print a summary of the command-line options on the standard\n");
  printf("   output and exit.\n");

  printf("\n-L<searchdir>\n");
  printf("--library-path=<searchdir>\n");
  printf("-rpath <searchdir>\n");
  printf("   Add path <searchdir> to the list of paths that will be\n");
  printf("   searched for static libraries.  You may use these options\n");
  printf("   any number of times. The directories are searched in the\n");
  printf("   order in which they are specified on the command line.\n");
  printf("   Directories specified on the command line are searched\n");
  printf("   before the default directories.  All \"-L\" and \"-rpath\"\n");
  printf("   options apply to all \"-l\" options, regardless of the order\n");
  printf("   in which the options appear.\n");

  printf("\n-larchive\n");
  printf("--library=archive\n");
  printf("   Add a static library file to the list of files to link.  This\n");
  printf("   option may be used any number of times.  %s will search its\n",
	 tool_name);
  printf("   path-list for occurrences of `libarchive.a' for every static\n");
  printf("   library specified\n");

  printf("\n-o output\n");
  printf("--output=output\n");
  printf("   Use output as the name for the program produced by %s.  If this\n",
	 tool_name);
  printf("   option is not specified, the name a.out is used by default.\n");

  printf("\n--stack reserve\n");
  printf("   Specify the amount of memory to allocate for use as stack\n");
  printf("   for this program.  The default is 4Kb.\n");

  printf("\n-Bstatic\n");
  printf("-dn\n");
  printf("-non_shared\n");
  printf("-static\n");
  printf("   Do  not  link  against shared libraries. You  may  use\n"); 
  printf("   this  option  multiple  times  on the command line: it\n");
  printf("   affects library searching for \"-l\" options which  follow\n");
  printf("   it.\n");

  printf("\n--dll-verbose\n");
  printf("--verbose\n");
  printf("   Pass the corresponding verbose option on to all %s\n",
	 tool_name);
  printf("   sub-processes.\n");

  printf("\n-W1,<arg[,arg[...]]\n");
  printf("   Pass the specified argument(s) to the ldelf2flt sub-process\n");
}

/***********************************************************************
 * parse_extra_args
 ***********************************************************************/

static void
parse_extra_args(char **arglist, int *nargs, char *option_argument)
{
  char *ptr  = option_argument;
  int   len  = strlen(option_argument);
  char *pend = ptr + len;
  int   n    = *nargs;

  /* Each argument begins with a comma */

  while (ptr < pend)
    {
      if (n < MAX_EXTRA_ARGS)
	{
	  arglist[n++] = ptr++;
	  for (; ((*ptr != ',') && (*ptr != '\0')); ptr++);
	  *ptr++ = '\0';
	}
      else
	{
	  fprintf(stderr, "Too many pass-through parameters!\n");
	  exit(1);
	}
    }
  *nargs = n;
}

/***********************************************************************
 * process_supported_option
 ***********************************************************************/

static void
process_supported_option(const struct supported_option_desc *desc,
			 char *option_argument)
{
  switch (desc->xflat_type)
    {
    case XFLAT_ENTRY_NAME:
      entry_name = option_argument;
      break;

    case XFLAT_LIBRARY_NAME:
      /* Ignore libgcc.a */

      if ((option_argument) && (strcmp(option_argument, "gcc") != 0))
	{
	  /* Check if libc is included in the link */

	  if (strcmp(option_argument, "c") == 0)
	    {
	      libc_is_included++;
	    }

	  /* Save the library for later use. */
	  {
	    /* Save the argument as -l<libname> for gcc */

	    int libsize = strlen(option_argument) + 3;
	    char *tmp = my_malloc(libsize);

	    sprintf(tmp, "-l%s", option_argument);
	    static_libraries[nstatic_libraries++] = tmp;
	  }
	}
      break;

    case XFLAT_LIBRARY_PATH:
      {
	/* Save the argument as -L<libpath> for gcc*/

	int libsize = strlen(option_argument) + 3;
	char *tmp = my_malloc(libsize);

	sprintf(tmp, "-L%s", option_argument);
	gcc_libpathes[nlibpathes] = tmp;

	/* AND save the straight path name for ldelf2flt */

	flat_libpathes[nlibpathes++] = option_argument;
      }
      break;

    case XFLAT_OUTPUT_NAME:
      output_name = option_argument;
      break;

    case XFLAT_STACK_SIZE:
      stack_size = (u_int32_t)atol(option_argument);
      break;

    case XFLAT_VERBOSE:
      verbose++;
      break;

    case XFLAT_HELP:
      show_usage();
      exit(0);
      break;

    case XFLAT_LDELF2XFLT_ARGS:
      parse_extra_args(ldelf2flt_extra, &nldelf2flt_extra, option_argument);
      break;

    case XFLAT_STATIC:
      break;

    case XFLAT_GEN_DEBUG:
      gen_debug_file++;
      break;

    case XFLAT_DYNAMIC:
    case XFLAT_EXIT_NAME:
    case XFLAT_LOADER_NAME:
    case XFLAT_SHARED_LIBRARY:
    default:
      fprintf(stderr, "Internal Error -- Illegal flat option type\n");
      exit(1);
    }
}
  
/***********************************************************************
 * check_unsupported_option
 ***********************************************************************/

static const struct unsupported_option_desc *
check_unsupported_option(int *argndx, char **argv)
{
  const struct unsupported_option_desc *retdesc = NULL;
  int ndx = *argndx;

  /* Check if this is an option (must begin with "-") */

  if (argv[ndx][0] == '-')
    {
      const struct unsupported_option_desc *desc;
      int nunsupported_options;
      char *arg;
      int cndx;
      int len;
      int i;

      if (argv[ndx][1] == '-')
	{
	  desc = unsupported_options2;
	  nunsupported_options = nunsupported_options2;
	  cndx = 2;
	}
      else
	{
	  desc = unsupported_options1;
	  nunsupported_options = nunsupported_options1;
	  cndx = 1;
	}

      arg = &argv[ndx][cndx];

      for (i = 0; i < nunsupported_options; i++)
	{
	  switch ((enum ld_type_e)desc[i].ld_type)
	    {
	    case MATCH_WHOLE: /* Option is just the provided string. */
	      if (strcmp(desc[i].option, arg) == 0)
		{
		  fprintf(stderr,
			  "Unsupported option \"%s\" -- Ignored\n",
			  argv[ndx]);
		  retdesc = &desc[i];
		}
	      break;

	    case MATCH_TO_LENGTH: /* Option is <string><something> */
	      len = strlen(desc[i].option);
	      if (strncmp(desc[i].option, arg, len) == 0)
		{
		  fprintf(stderr,
			  "Unsupported option \"%s\" -- Ignored\n",
			  argv[ndx]);
		  retdesc = &desc[i];
		}
	      break;

	    case MATCH_TO_EQUAL: /* Option is <string>=<something> */
	      len = strlen(desc[i].option);
	      if ((strncmp(desc[i].option, arg, len) == 0) &&
		  (arg[len] == '='))
		{
		  fprintf(stderr,
			  "Unsupported option \"%s\" -- Ignored\n",
			  argv[ndx]);
		  retdesc = &desc[i];
		}
	      break;

	    case MATCH_PLUS_ARG:       /* Option is <string> <something> */
	      if (strcmp(desc[i].option, arg) == 0)
		{
		  fprintf(stderr,
			  "Unsupported option \"%s %s\" -- Ignored\n",
			  argv[ndx], argv[ndx+1]);
		  *argndx = ++ndx;
		  retdesc = &desc[i];
		}
	      break;

	    case MATCH_SPECIAL:        /* Option is something else */
	    default:
	      fprintf(stderr,
		      "Internal Error -- Illegal option type\n");
	      exit(1);
	    }
	}
    }
  return retdesc;
}

/***********************************************************************
 * check_supported_option
 ***********************************************************************/

static const struct supported_option_desc *
check_supported_option(int *argndx, char **argv, char **option_argument)
{
  const struct supported_option_desc *retdesc = NULL;
  int ndx = *argndx;

  /* Assume no optional argument */

  *option_argument = NULL;

  /* Check if this is an option (must begin with "-") */

  if (argv[ndx][0] == '-')
    {
      const struct supported_option_desc *desc;
      int nsupported_options;
      char *arg;
      int cndx;
      int len;
      int i;

      if (argv[ndx][1] == '-')
	{
	  desc = supported_options2;
	  nsupported_options = nsupported_options2;
	  cndx = 2;
	}
      else
	{
	  desc = supported_options1;
	  nsupported_options = nsupported_options1;
	  cndx = 1;
	}

      arg = &argv[ndx][cndx];

      for (i = 0; ((i < nsupported_options) && (retdesc == NULL)); i++)
	{
	  switch ((enum ld_type_e)desc[i].ld_type)
	    {
	    case MATCH_WHOLE: /* Option is just the provided string. */
	      if (strcmp(desc[i].option, arg) == 0)
		{
		  retdesc = &desc[i];
		}
	      break;

	    case MATCH_TO_LENGTH: /* Option is <string><something> */
	      len = strlen(desc[i].option);
	      if (strncmp(desc[i].option, arg, len) == 0)
		{
		  if (arg[len] != '\0')
		    {
		      *option_argument = &arg[len];
		    }
		  else
		    {
		      fprintf(stderr,
			      "Missing value for option \"%s\"\n",
			      argv[ndx]);
		    }
		  retdesc = &desc[i];
		}
	      break;
	    case MATCH_TO_EQUAL: /* Option is <string>=<something> */
	      len = strlen(desc[i].option);
	      if ((strncmp(desc[i].option, arg, len) == 0) &&
		  (arg[len] == '='))
		{
		  if (arg[len+1] != '\0')
		    {
		      *option_argument = &arg[len+1];
		    }
		  else
		    {
		      fprintf(stderr,
			      "Missing value for option \"%s\"\n",
			      argv[ndx]);
		    }
		  retdesc = &desc[i];
		}
	      break;
	    case MATCH_PLUS_ARG:       /* Option is <string> <something> */
	      if (strcmp(desc[i].option, arg) == 0)
		{
		  ndx++;
		  if ((argv[ndx] != NULL) && (argv[ndx][0] != '\0'))
		    {
		      *option_argument = argv[ndx];
		      *argndx = ndx;
		    }
		  else
		    {
		      fprintf(stderr,
			      "Missing value for option \"%s\"\n",
			      argv[ndx-1]);
		    }
		  retdesc = &desc[i];
		}
	      break;
	    case MATCH_SPECIAL:        /* Option is something else */
	    default:
	      fprintf(stderr, "Internal Error -- Illegal option type\n");
	      exit(1);
	    }
	}
    }
  return retdesc;
}

/***********************************************************************
 * parse_args
 ***********************************************************************/

/* SYNOPSIS: ld [ options ] objfile... */

static void
parse_args(int argc, char **argv)
{
  char *option_argument;
  int i;

  /* Allocate space to hold library names.  The maximum that we
   * could possibly have is argc.
   */

  ndynamic_libraries = 0;
  dynamic_libraries = my_malloc(sizeof(char*) * (argc));

  nstatic_libraries = 0;
  static_libraries = my_malloc(sizeof(char*) * (argc));

  /* Allocate space to hold library pathes.  The maximum that we
   * could possibly have is argc.
   */

  nlibpathes = 0;
  flat_libpathes = my_malloc(sizeof(char*) * (argc));
  gcc_libpathes = my_malloc(sizeof(char*) * (argc));

  /* Allocate space to source files that are simply passed through
   * to gcc from the command line.
   */

  nobjects = 0;
  objects = my_malloc(sizeof(char*) * (argc+1));

  /* Now examine each argument.  If the argument is recognized and
   * captured in this loop, its argv pointer will be nullified.
   * The remaining non-null pointers will be passed through to
   * gcc.
   */

  for ( i = 1 ; i < argc ; i++ )
    {
      /* If it begins with '-', it must be an argument */

      if (argv[i][0] == '-')
	{
	  const struct unsupported_option_desc *udesc = NULL;
	  const struct supported_option_desc   *sdesc = NULL;

	  sdesc =  check_supported_option(&i, argv, &option_argument);
	  if (sdesc)
	    {
	      process_supported_option(sdesc, option_argument);
	    }
	  else
	    {
	      udesc = check_unsupported_option(&i, argv);
	      if (!udesc)
		{
		  fprintf(stderr,
			  "Unrecognized option \"%s\" -- Ignored\n",
			  argv[i]);
		}
	    }
	}
      else
	{
	  /* Otherwise, it must be an object file */

	  objects[nobjects++] = argv[i];
	}
    }

  /* No do some final sanity checking. */

  if (nobjects == 0)
    {
      fprintf(stderr, "No object files provided\n");
      exit(1);
    }

  if (output_name == NULL)
    {
      output_name = default_outname;

    }
  output_basename = basename((char*)output_name);

  /* Create the name of the first partial link */

  intermediate = my_malloc(strlen(output_basename)
			   + strlen(intermediate_suffix)
			   + 1);
  sprintf(intermediate, "%s%s", output_basename, intermediate_suffix);

  /* Create the name of the (optional) debug output file */

  output_debug_name = my_malloc(strlen(output_name)
			    + strlen(debug_suffix)
			    + 1);
  sprintf(output_debug_name, "%s%s", output_name, debug_suffix);
}

/***********************************************************************
 * parse_tool_name
 ***********************************************************************/

static void parse_tool_name(char *program_name)
{
  tool_name = basename(program_name);
  if (tool_name[0] == '-')
    {
      tool_name++;
    }
}

/***********************************************************************
 * exec_ldelf2flt
 ***********************************************************************/

/* Usage: ./ldelf2flt [options] <bfd-filename>
 * 
 * Where options are one or more of the following.  Note
 * that a space is always required between the option and
 * any following arguments
 * 
 *   -e <entry-point>
 *      Entry point to module [_start for executable];
 *      NULL for shared library
 *   -o <out-filename>
 *      Output to <out-filename> [<bfd-filename>.flt]
 *   -s <stack-size>
 *      Set stack size to <stack-size>.  Ignored if -l also     
 *      selected. [4096]
 *   -v Verbose output [no verbose output]
 */

static int
exec_ldelf2flt(void)
{
  char stack_size_string[25];
  char **ldelf2flt_argv;
  int nargs = 0;
  int retval;
  int i;

  /* Create the arguments that we will pass to ldelf2flt */

  ldelf2flt_argv =
    my_malloc(sizeof(char*) * (nldelf2flt_extra + 12));

  /* The version of ldelf2flt that we are going to be using. */

  ldelf2flt_argv[nargs++] = (char*)arch_ldelf2flt;

  /* Generate verbose output */

  if (verbose)
    {
      ldelf2flt_argv[nargs++] = "-v";
    }

  /* Select stack size */

  if (stack_size > 0)
    {
      sprintf(stack_size_string, "%ld", stack_size);
      ldelf2flt_argv[nargs++] = "-s";
      ldelf2flt_argv[nargs++] = stack_size_string;
    }

  /* Add the entry point symbol */
  if (entry_name)
    {
      ldelf2flt_argv[nargs++] = "-s";
      ldelf2flt_argv[nargs++] = (char*)entry_name;
    }

  /* Add any extra arguments */

  for (i = 0; i < nldelf2flt_extra; i++)
    {
      ldelf2flt_argv[nargs++] = ldelf2flt_extra[i];
    }

  /* Add the output file name */

  ldelf2flt_argv[nargs++] = "-o";
  ldelf2flt_argv[nargs++] = (char*)output_name;

  /* Add the input intermediate file */

  ldelf2flt_argv[nargs++] = intermediate;
  ldelf2flt_argv[nargs++] = NULL;

  retval = fork_exec_and_wait(arch_ldelf2flt, ldelf2flt_argv);
  free(ldelf2flt_argv);
  return retval;
}

/***********************************************************************
 * exec_gcc_debug
 ***********************************************************************/

static int
exec_gcc_debug(void)
{
  char **gcc_argv = my_malloc(sizeof(char*) * 10);
  int nargs = 0;
  int retval;

  /* The version of GCC that we are going to be using. */

  gcc_argv[nargs++] = (char*)archcc;

  /* Generate verbose output */

  if (verbose)
    {
      gcc_argv[nargs++] = "-v";
    }

  /* Provide the script file that tells GCC how we must
   * organize memory.
   */

  gcc_argv[nargs++] = (char*)arch_script_path;

  /* These things are already taken care of in the intermediate */

  gcc_argv[nargs++] = "-nostdlib";
  gcc_argv[nargs++] = "-nostartfiles";

  /* The file we are going to generate. */

  gcc_argv[nargs++] = "-o";
  gcc_argv[nargs++] = output_debug_name;

  /* This is the single input file */

  gcc_argv[nargs++] = intermediate;
  gcc_argv[nargs++] = NULL;

  retval = fork_exec_and_wait(archcc, gcc_argv);
  free(gcc_argv);
  return retval;
}

/***********************************************************************
 * exec_gcc_intermediate
 ***********************************************************************/

/* Execute GCC to generate a single, partially linked object. */

static int
exec_gcc_intermediate(void)
{
  char **gcc_argv;
  int crt0_needed = 1;
  int nargs = 0;
  int alloc_size;
  int retval;
  int i;

  alloc_size = sizeof(char*) * (nobjects + nstatic_libraries + 20);
  if (nstatic_libraries > 0)
    {
      alloc_size += sizeof(char*) * nlibpathes;
    }

  gcc_argv = my_malloc(alloc_size);

  /* We are linking an executable.  Check if a crt0 file is already
   * in the object list */

  for (i = 0; ((i < nobjects) && (crt0_needed != 0)); i++)
    {
      char *objfile = basename(objects[i]);
      if (strncmp(objfile, "crt0", 4) == 0)
	{
	  /* The user has specified a crt0* file.  We won't bother */

	  crt0_needed = 0;
	}
    }

  /* The version of GCC that we are going to be using. */

  gcc_argv[nargs++] = (char*)archcc;

  /* Generate verbose output */

  if (verbose)
    {
      gcc_argv[nargs++] = "-v";
    }
    
  /* The file we are going to generate. */

  gcc_argv[nargs++] = "-o";
  gcc_argv[nargs++] = (char*)intermediate;

  /* -r = generate relocatable output (partial link)
   * -d = assign common symbols in relocatable output.
   * -warn-common = Warn  when a common symbol is combined with
   *      another common symbol or with a symbol definition
   */

  gcc_argv[nargs++] = "-Wl,-r";
  gcc_argv[nargs++] = "-Wl,-d";
  gcc_argv[nargs++] = "-Wl,-warn-common";

  /* We are going to include libgcc.a */

  gcc_argv[nargs++] = (char*)gcc_arch_lib_path;

  /* If we going to link in any other shared libraries,
   * then add all of the other library pathes that we
   * may have.
   */

  if (nstatic_libraries > 0)
    {
      for (i = 0; i < nlibpathes; i++)
	{
	  gcc_argv[nargs++] = gcc_libpathes[i];
	}
    }

  /* Well do everything ourselves. */

  gcc_argv[nargs++] = "-nostdlib";
  gcc_argv[nargs++] = "-nostartfiles";

  /* We never want to link in dynamic libraries! */

  gcc_argv[nargs++] = "-static";

  /* Provide the script file that tells GCC how we must
   * organize memory.
   */

  gcc_argv[nargs++] = (char*)arch_script_path;

  /* Add a startup file at the begining of the object list */

  if (crt0_needed)
    {
      if (libc_is_included)
	{
	  gcc_argv[nargs++] = (char*)crt0flat_path;
	}
      else
	{
	  gcc_argv[nargs++] = (char*)crt0nolibc_path;
	}
    }

  /* Then add all of the objects */

  for (i = 0; i < nobjects; i++)
    {
      if (objects[i])
	{
	  gcc_argv[nargs++] = objects[i];
	}
    }

  /* Always add libgcc.a to the list.  If we don't need anything
   * from it, then there is no harm done.
   */

  gcc_argv[nargs++] = "-lgcc";

  /* Then add all of the static libraries */

  if (nstatic_libraries)
    {
      for (i = 0; i < nstatic_libraries; i++)
	{
	  gcc_argv[nargs++] = static_libraries[i];
	}

      /* And add libgcc again in case we picked up some new 
       * requirements.
       */

      gcc_argv[nargs++] = "-lgcc";
    }

  /* And don't forget to stick a NULL at the end of the argument
   * list.
   */

  gcc_argv[nargs++] = NULL;

  retval = fork_exec_and_wait(archcc, gcc_argv);
  free(gcc_argv);
  return retval;
}

/***********************************************************************
 * Public Functions
 ***********************************************************************/

/***********************************************************************
 * main
 ***********************************************************************/

int main(int argc, char **argv, char **envp)
{
  int status = 0;

  /* Find out which tool we are going to emulate */

  parse_tool_name(argv[0]);

  /* Then parse the incoming arguments */

  parse_args(argc, argv);

  /* Break up the CPU flags into individual elementes */

  parse_flags();

  /* Are there multiple object files?  If so, we will
   * have to run gcc to create one partially linked file
   */

  /* Execute gcc -- generating partially linked intermediate */

  status = exec_gcc_intermediate();
  if (status != 0)
    {
      /* If gcc fails, so do we */

      goto exit_with_status;
    }

  /* Then execute ldelf2flt to produce the final flat binary
   * from intermediate. */

  status = exec_ldelf2flt();
  if (status != 0)
    {
      /* If gcc fails, so do we */

      goto exit_removing_intermediate;
    }

  /* Finally, if a debug output is selected, link one more time
   * to produce the debug output file.
   */

  if (gen_debug_file)
    {
      status = exec_gcc_debug();
      if (status != 0)
	{
	  /* If gcc fails, so do we */

	  goto exit_removing_output_files;
	}
    }
  goto exit_removing_intermediate;

  /* Clean up temporary files */

 exit_removing_output_files:
  fprintf(stderr, "Removing %s\n", output_name);
  (void)remove(output_name);
  (void)remove(output_debug_name);
 exit_removing_intermediate:
  (void)remove(intermediate);
 exit_with_status:
  return status;
}
