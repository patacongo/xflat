########################################################################
# xflat/tools/Makefile
#
# Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
# Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
# Author: Gregory Nutt <spudmonkey@racsa.co.cr>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name Cadenux nor the names of its contributors may be
#    used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
########################################################################

TOPDIR		= ${shell pwd}/..

include $(TOPDIR)/.config
include $(TOPDIR)/Make.defs

# Platform specifics
# Versions of uClibc newer that 0.9.26 should not
# define XFLAT_NEED_LIBC_FORK but should define BUNDLE_XFLAT_PTHREADS

BINARY_TARGET	= XFLAT_TARGET
TOOLDEFINES	= -D$(BINARY_TARGET)=1 -DBUNDLE_XFLAT_PTHREADS $(HOSTDEFINES)
TOOLINCLUDES	= -I$(ARCH_BFD_INC_DIR) $(HOSTINCLUDES)
TOOLWARNINGS	= -Wall

ifeq ($(XFLAT_DEBUG),y)
  TOOLOPTIMIZE	= -g -DCONFIG_XFLAT_DEBUG
else
  TOOLOPTIMIZE	= -O2
endif

TOOLCFLAGS	= $(TOOLWARNINGS) $(TOOLOPTIMIZE) $(TOOLDEFINES) $(TOOLINCLUDES)
TOOLLDFLAGS	=

LIB_BFD		= $(ARCH_BFD_LIB_DIR)/libbfd.a
LIB_IBERTY	= $(ARCH_IBERTY_LIB_DIR)/libiberty.a
LBFD		= $(LIB_BFD) $(LIB_IBERTY)

PROGS		= ldelf2xflt ldelflib readxflat xflat-ld xflat-gcc xflat-g++ flat-ld

LDELF2XFLT_OBJS	= ldelf2xflt.o
LDELFLIB_OBJS	= ldelflib.o
READXFLAT_OBJS	= readxflat.o arm/disarm.o
XFLATLD_OBJS	= ld-xflat.o ld-options.o
FLATLD_OBJS	= ld-flat.o ld-flat-options.o
XFLATGCC_OBJS	= gcc-xflat.o
XFLATCXX_OBJS	= g++-xflat.o

INTERN_NAME	= xflat-internals.h
INTERN_SRC	= arch/$(INTERN_NAME)
INTERN_DEST	=  ../sys-include/$(INTERN_NAME)

OBJS		= $(LDELF2XFLT_OBJS) $(LDELFLIB_OBJS) $(READXFLAT_OBJS) \
		  $(XFLATLD_OBJS) $(XFLATGCC_OBJS) $(XFLATCXX_OBJS)

GXX_VERSION	= ${shell $(ARCHCXX) -dumpversion | cut -d. -f1}

all:	$(PROGS) internals

arch:
	@ln -sf $(ARCHTARGET) arch

internals: arch
	@cp -f $(INTERN_SRC) $(INTERN_DEST)

$(OBJS): %.o: %.c
	$(HOSTCC) $(TOOLCFLAGS) -c $<

g++-xflat.c: gcc-xflat.c
	cat gcc-xflat.c | sed -e "s/ARCHCC/ARCHCXX/g" >g++-xflat.c

ld-flat-options.o: ld-options.c
	@rm -f ld-options.o
	@$(MAKE) ld-options.o BINARY_TARGET=FLAT_TARGET
	@mv -f ld-options.o ld-flat-options.o

ldelf2xflt: arch $(LDELF2XFLT_OBJS)
	$(HOSTCC) $(TOOLLDFLAGS) $(LDELF2XFLT_OBJS) $(LBFD) -o $@

ldelflib: arch $(LDELFLIB_OBJS)
	$(HOSTCC) $(TOOLLDFLAGS) $(LDELFLIB_OBJS) $(LBFD) -o $@

arch/libarch.a:
	$(MAKE) -C arch

readxflat: arch $(READXFLAT_OBJS) arch/libarch.a
	$(HOSTCC) $(TOOLLDFLAGS) -o $@ $(READXFLAT_OBJS) --whole-archive arch/libarch.a

gcc-strings.h:
	@echo "#ifndef _GCC_STRINGS_H_" >gcc-strings.h
	@echo "#define _GCC_STRINGS_H_" >>gcc-strings.h
	@echo "#define ARCHCC               \"$(ARCHCC)\"" >>gcc-strings.h
	@echo "#define ARCHCXX              \"$(ARCHCXX)\"" >>gcc-strings.h
	@echo "#define ARCH_INC1_DIR        \"$(ARCH_INC_DIR)\"" >>gcc-strings.h
	@echo "#define ARCH_INC2_DIR        \"$(ARCH_INC2_DIR)\"" >>gcc-strings.h
	@echo "#define ARCH_INC3_DIR        \"$(ARCH_INC3_DIR)\"" >>gcc-strings.h
	@echo "#define XFLAT_INC_DIR        \"$(ARCH_XFLAT_INC_DIR)\"" >>gcc-strings.h
	@echo "#endif /* _GCC_STRINGS_H_ */" >>gcc-strings.h

ld-strings.h:
	@echo "#ifndef _LD_STRINGS_H_" >ld-strings.h
	@echo "#define _LD_STRINGS_H_" >>ld-strings.h
	@echo "#define ARCH_LIB_DIR         \"$(ARCH_LIB_DIR)\"" >>ld-strings.h
	@echo "#define ARCHCC               \"$(ARCHCC)\"" >>ld-strings.h
	@echo "#define ARCHCXX              \"$(ARCHCXX)\"" >>ld-strings.h
	@echo "#define ARCH_CRT0FLAT_PATH   \"$(ARCH_STARTUP_FLAT)\"" >>ld-strings.h
	@echo "#define ARCH_CRT0XFLAT_PATH  \"$(ARCH_STARTUP_XFLAT)\"" >>ld-strings.h
	@echo "#define ARCH_CRT0NOLIBC_PATH \"$(ARCH_STARTUP_NOLIBC)\"" >>ld-strings.h
	@echo "#define ARCH_CRT1XFLAT_PATH  \"$(ARCH_STARTUP1_XFLAT)\"" >>ld-strings.h
ifeq ($(GXX_VERSION),2)
	@echo "#define ARCHCXX_VER2X        1" >>ld-strings.h
	@echo "#undef  ARCHCXX_VER3X" >>ld-strings.h
	@echo "#define ARCH_CRT1XFLAT_PATH  \"$(ARCH_STARTUP1_XFLAT)\"" >>ld-strings.h
	@echo "#define ARCH_CTORSXFLAT_PATH \"$(ARCH_CTORS_XFLAT)\"" >>ld-strings.h
	@echo "#define ARCH_XFLATDUMMY_PATH \"$(ARCH_DUMMY_XFLAT)\"" >>ld-strings.h
endif
ifeq ($(GXX_VERSION),3)
	@echo "#undef  ARCHCXX_VER2X" >>ld-strings.h
	@echo "#define ARCHCXX_VER3X        1" >>ld-strings.h
	@echo "#define ARCH_CRT1XFLAT_PATH  \"$(ARCH_STARTUP1_XFLAT)\"" >>ld-strings.h
	@echo "#define ARCH_CRTIXFLAT_PATH  \"$(ARCH_STARTUPI_XFLAT)\"" >>ld-strings.h
	@echo "#define ARCH_CRTNXFLAT_PATH  \"$(ARCH_STARTUPN_XFLAT)\"" >>ld-strings.h
	@echo "#define ARCH_CRTBEGIN_PATH   \"$(ARCH_CRTBEGIN)\"" >>ld-strings.h
	@echo "#define ARCH_CRTEND_PATH     \"$(ARCH_CRTEND)\"" >>ld-strings.h
endif
	@echo "#define ARCH_INC_DIR         \"$(ARCH_INC_DIR)\"" >>ld-strings.h
	@echo "#define ARCH_LDELF2FLT       \"$(ARCH_LDELF2FLT)\"" >>ld-strings.h
	@echo "#define ARCH_LDELFLIB        \"$(ARCH_LDELFLIB)\"" >>ld-strings.h
	@echo "#define ARCH_LDELF2XFLT      \"$(ARCH_LDELF2XFLT)\"" >>ld-strings.h
	@echo "#define ARCH_SCRIPT_FLAT     \"$(ARCH_SCRIPTFILE_FLAT)\"" >>ld-strings.h
	@echo "#define ARCH_SCRIPT_PATH     \"$(ARCH_SCRIPTFILE_XFLAT)\"" >>ld-strings.h
	@echo "#define ARCH_CPU_FLAGS       \"$(ARCHCPUFLAGS)\"" >>ld-strings.h
	@echo "#endif /* _LD_STRINGS_H_ */" >>ld-strings.h

xflat-ld: arch ld-strings.h $(XFLATLD_OBJS)
	$(HOSTCC) $(TOOLLDFLAGS) $(XFLATLD_OBJS) -o $@

xflat-gcc: arch gcc-strings.h $(XFLATGCC_OBJS)
	$(HOSTCC) $(TOOLLDFLAGS) $(XFLATGCC_OBJS) -o $@

xflat-g++: arch gcc-strings.h g++-xflat.c $(XFLATCXX_OBJS)
	$(HOSTCC) $(TOOLLDFLAGS) $(XFLATCXX_OBJS) -o $@

flat-ld: arch ld-strings.h $(FLATLD_OBJS)
	$(HOSTCC) $(TOOLLDFLAGS) $(FLATLD_OBJS) -o $@

user_install:

install_headers:
	echo $(TOPDIR)
	echo $(BRAND)
	rm -rf $(ARCH_XFLAT_INC_DIR)
	mkdir -m 755 $(ARCH_XFLAT_INC_DIR)
	cp -Rf $(XFLAT_INC_DIR)/* $(ARCH_XFLAT_INC_DIR)

root_install: install_headers
	install -m 755 ldelf2xflt $(ARCH_BIN_DIR)/ldelf2xflt
	install -m 755 ldelflib $(ARCH_BIN_DIR)/ldelflib
	install -m 755 readxflat $(ARCH_BIN_DIR)/readxflat
	install -m 755 xflat-ld $(ARCH_BIN_DIR)/xflat-ld
	install -m 755 xflat-gcc $(ARCH_BIN_DIR)/xflat-gcc
	install -m 755 xflat-g++ $(ARCH_BIN_DIR)/xflat-g++
	install -m 755 flat-ld $(ARCH_BIN_DIR)/flat-ld

clean:
	rm -f *.o $(PROGS) ld-strings.h gcc-strings.h a.out arch core
	rm -f $(INTERN_DEST)
	rm -f g++-xflat.c
	make -f Makefile.hello clean
