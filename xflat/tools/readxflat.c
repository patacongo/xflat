/***********************************************************************
 * xflat/tools/readxflat.c
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

/***********************************************************************
 * Compilation Flags
 ***********************************************************************/

#define SWAP_BYTES 1

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>
#include <netinet/in.h> /* ntohl and friends */
#include "xflat.h"

/***********************************************************************
 * Compilation Switches
 ***********************************************************************/

/* #define RELOCS_IN_NETWORK_ORDER */
 
/***********************************************************************
 * Definitions
 ***********************************************************************/

#define XFLAT_HDR_SIZE   sizeof(struct xflat_hdr)

/***********************************************************************
 * Private Data
 ***********************************************************************/

static const char *program_name;
static const char *xflat_filename;

static int dump_header   = 0;
static int dump_relocs   = 0;
static int dump_imports  = 0;
static int dump_exports  = 0;
static int dump_lpathes  = 0;
static int dump_lnames   = 0;
static int dump_ldrpath  = 0;
static int dump_text     = 0;
static int dump_data     = 0;
static int verbose       = 0;

static int num_errors    = 0;

#ifdef ARCH_BIG_ENDIAN
static int big_endian    = 1; /* Assume big-endian */
#else
static int big_endian    = 0; /* Assume little-endian */
#endif

/***********************************************************************
 * Private Constant Data
 ***********************************************************************/

static const char unknown[]          = "UNKNOWN";

static const char header_type_none[] = "NONE";
static const char header_type_rel[]  = "REL";
static const char header_type_exec[] = "EXEC";
static const char header_type_dyn[]  = "DYN";
static const char header_type_core[] = "CORE";

static const char *header_type_string[] =
{
  header_type_none,
  header_type_rel,
  header_type_exec,
  header_type_dyn,
  header_type_core,
  unknown
};

static const char header_reloc_none[] = "RELOC_NONE";
static const char header_reloc_text[] = "RELOC_TEXT";
static const char header_reloc_data[] = "RELOC_DATA";
static const char header_reloc_bss[]  = "RELOC_BSS";

static const char *reloc_type_string[] =
{
  header_reloc_none,
  header_reloc_text,
  header_reloc_data,
  header_reloc_bss,
  unknown
};

/***********************************************************************
 * Public Function Prototypes
 ***********************************************************************/

extern void __attribute__ ((weak)) print_insn_arm(u_int32_t pc,
						  FILE *stream,
						  u_int32_t given);

/***********************************************************************
 * Private Functions
 ***********************************************************************/

/***********************************************************************
 * swap32
 ***********************************************************************/

static inline u_int32_t xflat_swap32(u_int32_t little)
{
    u_int32_t big =
	((little >> 24) & 0xff) | 
	(((little >> 16) & 0xff) << 8) |
	(((little >> 8) & 0xff) << 16) |
	((little & 0xff) << 24);
    return big;
}

/***********************************************************************
 * get_xflat32
 ***********************************************************************/
 
static inline u_int32_t
get_xflat32(u_int32_t *addr32)
{
  return ntohl(*addr32);
}

/***********************************************************************
 * get_xflat16
 ***********************************************************************/
 
static inline u_int16_t
get_xflat16(u_int16_t *addr16)
{
  return ntohs(*addr16);
}

/***********************************************************************
 * dump_hex_data
 ***********************************************************************/

static void
dump_hex_data(FILE *in_stream, struct xflat_hdr *header)
{
  u_int32_t data_start = get_xflat32(&header->data_start);
  u_int32_t data_end   = get_xflat32(&header->data_end);
    int32_t words_left = (data_end - data_start) / sizeof(u_int32_t);
  u_int32_t addr;
  u_int32_t buffer[64];

  printf("\nXFLAT DATA SEGMENT:\n\n");

  /* Seek to the beginning of data in the file */

  if (fseek(in_stream, data_start, SEEK_SET) != 0)
    {
      fprintf(stderr,
	      "ERROR: Failed to seek to data in file: offset=0x%08x\n",
	      data_start);
      return;
    }

  /* Now dump all of the data reading 64 words at a time */

  addr = 0;
  while (words_left > 0)
    {
      size_t nread = fread(buffer, sizeof(u_int32_t), 64, in_stream);
      if (nread >= 0)
	{
	  union
	  {
	    u_int32_t l[4];
	    unsigned char b[16];
	  } row;
	  int32_t i, j, k;

	  for (i = 0; i < nread; i += 4)
	    {
	      for (j = 0; j < 4; j++)
		{
		  row.l[j] = buffer[i+j];
		}

	      printf("%08x: ", addr);

	      for (j = 0; j < 4; j++)
		{
		  printf("%08x ", row.l[j]);
		}

	      printf("  ");

	      for (j = 0;
		   j < 4*sizeof(u_int32_t);
		   j += sizeof(u_int32_t))
		{
		  for (k = 0; k < sizeof(u_int32_t); k++)
		    {
		      if (isprint(row.b[j+k]))
			putchar(row.b[j+k]);
		      else
			putchar('.');
		    }
		}

	      putchar('\n');
	      addr += 4*sizeof(u_int32_t);
	    }
	  words_left -= nread;
	}
      else break;
    }
  putchar('\n');
}

/***********************************************************************
 * disassemble_text
 ***********************************************************************/

static void
disassemble_text(FILE *in_stream, struct xflat_hdr *header)
{
  if (print_insn_arm)
    {
      u_int32_t text_start = XFLAT_HDR_SIZE;
      u_int32_t text_end   = get_xflat32(&header->data_start);
        int32_t insns_left = (text_end - text_start) / sizeof(u_int32_t);
      u_int32_t addr;
      u_int32_t buffer[64];

      printf("\nXFLAT TEXT:\n\n");

      /* Seek to the beginning of text in the file */
      if (fseek(in_stream, text_start, SEEK_SET) != 0)
	{
	  fprintf(stderr,
		  "ERROR: Failed to seek to text in file: offset=0x%08x\n",
		  text_start);
	  return;
	}

      /* Now dump all of the data reading 64 insns at a time */

      addr = text_start;
      while (insns_left > 0)
	{
	  size_t nread = fread(buffer, sizeof(u_int32_t), 64, in_stream);
	  if (nread > 0)
	    {
	      int i;
	      for (i = 0; i < nread; i++)
		{
		  u_int32_t insn = buffer[i];
		  if (big_endian)
		    {
		      insn = xflat_swap32(insn);
		    }

		  printf("%08x %08x\t", addr, insn);
		  print_insn_arm(addr, stdout, insn);
		  putchar('\n');
		  addr += sizeof(u_int32_t);
		}
	      insns_left -= nread;
	    }
	  else break;
	  putchar('\n');
	}
    }
}

/***********************************************************************
 * dump_library_names
 ***********************************************************************/

static void dump_library_names(FILE *in_stream,
			       struct xflat_hdr *header)
{
  u_int32_t *lib_names;
  u_int32_t  lname_count;
  u_int32_t  lname_offset;
  char       library_name[XFLAT_MAX_STRING_SIZE];
  int        status;
  int        i;

  printf("\nLIBRARY SEARCH NAMES:\n");
  printf("      PATHNAME\n\n");

  /* Get the number of library names */

  lname_count = get_xflat16(&header->lname_count);
  if (lname_count > 0)
    {
      /* Allocate a buffer to hold all of the library path names */

      lib_names = (u_int32_t*)malloc(lname_count*sizeof(u_int32_t));
      if (lib_names == NULL)
	{
	  fprintf(stderr, "ERROR: Failed to allocate memory "
		  "for library names\n");
	  goto err_out_nomem;
	}

      /* Seek to the beginning of the library names */

      lname_offset = get_xflat32(&header->lib_names);

      if (0 != fseek(in_stream, lname_offset, SEEK_SET))
	{
	  fprintf(stderr, "ERROR: fseek to library names failed\n");
	  fprintf(stderr, "       file offset=%d, errno=%d\n",
		  lname_offset, errno);
	  goto err_out_withmem;
	}

      /* Read the all of the library names */

      status = fread((void*)lib_names, sizeof(u_int32_t),
		     lname_count, in_stream);
      if (status != lname_count)
	{
	  fprintf(stderr, "ERROR: Read library name list failed, "
		  "errno=%d\n", errno);
	  goto err_out_withmem;
	}

      /* Then process each library name in the list */

      for ( i = 0 ; i < lname_count; i++ )
	{
	  /* Seek to the library name in the file */

	  if (big_endian)
	    {
	      lname_offset = xflat_swap32(lib_names[i]) + XFLAT_HDR_SIZE;
	    }
	  else
	    {
	      lname_offset = lib_names[i] + XFLAT_HDR_SIZE;
	    }

	  if (0 != fseek(in_stream, lname_offset, SEEK_SET))
	    {
	      fprintf(stderr, "ERROR: fseek to library name failed\n");
	      fprintf(stderr, "       file offset=%d, errno=%d\n",
		      lname_offset, errno);
	      goto err_out_withmem;
	    }

	  /* Then, read the library name (assuming it is less than
	   * XFLAT_MAX_STRING_SIZE in length).
	   */

	  status = fread((void*)library_name, XFLAT_MAX_STRING_SIZE,
			 1, in_stream);
	  if (status != 1)
	    {
	      fprintf(stderr, "ERROR: Read library name failed, "
		      "errno=%d\n", errno);
	      goto err_out_withmem;
	    }
	  library_name[XFLAT_MAX_STRING_SIZE-1] = '\0';

	  /* And print it */

	  printf("%5d %s\n", i+1, library_name);
    	}
    }
  goto no_errors;

 err_out_withmem:
  free(lib_names);
 err_out_nomem:
  num_errors++;
 no_errors:
  return;
}

/***********************************************************************
 * dump_library_search_pathes
 ***********************************************************************/

static void dump_library_search_pathes(FILE *in_stream,
				       struct xflat_hdr *header)
{
  u_int32_t *lib_pathes;
  u_int32_t  lpath_count;
  u_int32_t  lpath_offset;
  char       library_pathname[XFLAT_MAX_STRING_SIZE];
  int        status;
  int        i;

  printf("\nLIBRARY SEARCH PATHES:\n");
  printf("      PATHNAME\n\n");

  /* Get the number of library pathes */

  lpath_count = get_xflat16(&header->lpath_count);
  if (lpath_count > 0)
    {
      /* Allocate a buffer to hold all of the library path names */

      lib_pathes = (u_int32_t*)malloc(lpath_count*sizeof(u_int32_t));
      if (lib_pathes == NULL)
	{
	  fprintf(stderr, "ERROR: Failed to allocate memory "
		  "for library pathes\n");
	  goto err_out_nomem;
	}

      /* Seek to the beginning of the library pathes */

      lpath_offset = get_xflat32(&header->lib_pathes);

      if (0 != fseek(in_stream, lpath_offset, SEEK_SET))
	{
	  fprintf(stderr, "ERROR: fseek to library pathes failed\n");
	  fprintf(stderr, "       file offset=%d, errno=%d\n",
		  lpath_offset, errno);
	  goto err_out_withmem;
	}

      /* Read the all of the library pathes */

      status = fread((void*)lib_pathes, sizeof(u_int32_t),
		     lpath_count, in_stream);
      if (status != lpath_count)
	{
	  fprintf(stderr, "ERROR: Read library path list failed, "
		  "errno=%d\n", errno);
	  goto err_out_withmem;
	}

      /* Then process each library path in the list */

      for ( i = 0 ; i < lpath_count; i++ )
	{
	  /* Seek to the library pathname in the file */

	  lpath_offset = lib_pathes[i] + XFLAT_HDR_SIZE;

	  if (0 != fseek(in_stream, lpath_offset, SEEK_SET))
	    {
	      fprintf(stderr, "ERROR: fseek to library pathname failed\n");
	      fprintf(stderr, "       file offset=%d, errno=%d\n",
		      lpath_offset, errno);
	      goto err_out_withmem;
	    }

	  /* Then, read the library pathname (assuming it is less than
	   * XFLAT_MAX_STRING_SIZE in length).
	   */

	  status = fread((void*)library_pathname, XFLAT_MAX_STRING_SIZE,
			 1, in_stream);
	  if (status != 1)
	    {
	      fprintf(stderr, "ERROR: Read library pathname failed, "
		      "errno=%d\n", errno);
	      goto err_out_withmem;
	    }
	  library_pathname[XFLAT_MAX_STRING_SIZE-1] = '\0';

	  /* And print it */

	  printf("%5d %s\n", i+1, library_pathname);
    	}
    }
  goto no_errors;

 err_out_withmem:
  free(lib_pathes);
 err_out_nomem:
  num_errors++;
 no_errors:
  return;
}

/***********************************************************************
 * dump_loader_pathname
 ***********************************************************************/

static void dump_loader_pathname(FILE *in_stream, struct xflat_hdr *header)
{
  char loader_pathname[XFLAT_MAX_STRING_SIZE];
  u_int32_t loader_pathname_offset;
  int status;

  printf("\nLOADER PATHNAME:  ");
  loader_pathname_offset = get_xflat32(&header->loader);
  if (loader_pathname_offset != 0)
    {
      /* Seek to the loader path_name in the file */

      if (0 != fseek(in_stream, loader_pathname_offset, SEEK_SET))
	{
	  printf("ERROR\n");
	  fprintf(stderr, "ERROR: fseek to loader pathname failed, "
		  "file offset=%d, errno=%d\n",
		  loader_pathname_offset, errno);
	  exit(1);
	}
 
      /* Then, read the exported symbol name (assuming it is less than
       * XFLAT_MAX_STRING_SIZE in length).
       */

     status = fread((void*)loader_pathname, XFLAT_MAX_STRING_SIZE,
		     1, in_stream);
      if (status != 1)
	{
	  printf("ERROR\n");
	  fprintf(stderr, "ERROR: Read of loader pathname failed, "
		  "errno=%d\n", errno);
	  exit(1);
	}
      loader_pathname[XFLAT_MAX_STRING_SIZE-1] = '\0';

      /* And print it */

      printf("%s\n", loader_pathname);
    }
  else
    {
      printf("None (using default xFLAT loader)\n");
    }
}

/***********************************************************************
 * dump_exported_symbols
 ***********************************************************************/

static void dump_exported_symbols(FILE *in_stream, struct xflat_hdr *header)
{
  struct xflat_export export;
  u_int32_t export_offset;
  u_int32_t file_offset;
  char exported_symbol_name[XFLAT_MAX_STRING_SIZE];
  int status;
  int i;

  printf("\nEXPORTED SYMBOLS:\n");
  printf("      TEXT OFFS  SYMBOL NAME\n\n");

  export_offset = get_xflat32(&header->export_symbols);

  for ( i = 0 ; i < get_xflat16(&header->export_count); i++ )
    {
      /* Seek to the next exported symbol */

      file_offset = i*sizeof(struct xflat_export) + export_offset;

      if (0 != fseek(in_stream, file_offset, SEEK_SET))
	{
	  fprintf(stderr, "ERROR: fseek to exported symbol %d struct failed\n", i);
	  fprintf(stderr, "       file_offset=%d, errno=%d\n",
		  file_offset, errno);
	  exit(1);
	}

      /* Read the next export entry. */

      status = fread((void*)&export,
		     sizeof(struct xflat_export),
		     1, in_stream);

      if (status != 1)
	{
	  fprintf(stderr, "ERROR: Read exported symbol %d struct failed, "
		  "errno=%d\n", i+1, errno);
	  exit(1);
	}

      if (big_endian)
        {
          export.function_name    = xflat_swap32(export.function_name);
          export.function_address = xflat_swap32(export.function_address);
        }

      if (verbose)
	{
	  /* Print the raw info */

	  printf("[%4d 0x%08x 0x%08x from file offset=0x%08x]\n",
		 i+1, export.function_address,
		 export.function_name, file_offset);
	}

      /* Seek to the function name in the file */

      file_offset = export.function_name + XFLAT_HDR_SIZE;
      if (0 != fseek(in_stream, file_offset, SEEK_SET))
	{
	  fprintf(stderr, "ERROR: fseek to exported symbol %d name failed\n",
		  i);
	  fprintf(stderr, "       file_offset=%d, errno=%d\n",
		  file_offset, errno);
	  exit(1);
	}
 
      /* Then, read the exported symbol name (assuming it is less than
       * XFLAT_MAX_STRING_SIZE in length).
       */

     status = fread((void*)exported_symbol_name, XFLAT_MAX_STRING_SIZE,
		     1, in_stream);
      if (status != 1)
	{
	  fprintf(stderr, "ERROR: Read exported symbol %d name failed, "
		  "errno=%d\n", i+1, errno);
	  exit(1);
	}
      exported_symbol_name[XFLAT_MAX_STRING_SIZE-1] = '\0';

      /* And print it */

      printf("%5d 0x%08x %s\n",
	     i+1, export.function_address, exported_symbol_name);
    }
}

/***********************************************************************
 * dump_imported_symbols
 ***********************************************************************/

static void dump_imported_symbols(FILE *in_stream, struct xflat_hdr *header)
{
  struct xflat_import import;
  u_int32_t import_offset;
  u_int32_t file_offset;
  char imported_symbol_name[XFLAT_MAX_STRING_SIZE];
  int status;
  int i;

  printf("\nIMPORTED SYMBOLS:\n");
  printf("      ADDRESS    DATA SEGM  SYMBOL NAME\n\n");

  import_offset = get_xflat32(&header->import_symbols);

  for (i = 0 ; i < get_xflat16(&header->import_count); i++)
    {
      /* Seek to the next imported symbol */

      file_offset = i*sizeof(struct xflat_import) + import_offset;

      if (fseek(in_stream, file_offset, SEEK_SET) != 0)
	{
	  fprintf(stderr, "ERROR: fseek to imported symbol %d struct failed\n", i);
	  fprintf(stderr, "       file_offset=%d, errno=%d\n",
		  file_offset, errno);
	  exit(1);
	}

      /* Read the next import entry. */

      status = fread((void*)&import,
		     sizeof(struct xflat_import),
		     1, in_stream);

      if (status != 1)
	{
	  fprintf(stderr, "ERROR: Read imported symbol %d struct failed, "
		  "errno=%d\n", i+1, errno);
	  exit(1);
	}

      if (big_endian)
        {
          import.function_name    = xflat_swap32(import.function_name);
          import.function_address = xflat_swap32(import.function_address);
          import.data_segment     = xflat_swap32(import.data_segment);
        }

      if (verbose)
	{
	  /* Print the raw info */

	  printf("[%4d 0x%08x 0x%08x 0x%08x]\n",
		 i+1, import.function_address, import.data_segment,
		 import.function_name);
	}

      /* Seek to the function name in the file */

      file_offset = import.function_name + XFLAT_HDR_SIZE;
      if (0 != fseek(in_stream, file_offset, SEEK_SET))
	{
	  fprintf(stderr, "ERROR: fseek to imported symbol %d name failed\n",
		  i);
	  fprintf(stderr, "       file_offset=%d, errno=%d\n",
		  file_offset, errno);
	  exit(1);
	}

      /* Then, read the imported symbol name (assuming it is less than
       * XFLAT_MAX_STRING_SIZE in length).
       */

      status = fread((void*)imported_symbol_name, XFLAT_MAX_STRING_SIZE,
		     1, in_stream);
      if (status != 1)
	{
	  fprintf(stderr, "ERROR: Read imported symbol %d name failed, "
		  "errno=%d\n", i+1, errno);
	  exit(1);
	}
      imported_symbol_name[XFLAT_MAX_STRING_SIZE-1] = '\0';

      /* And print it */

      printf("%5d ", i+1);

      if (import.function_address)
	{
	  printf("0x%08x ", import.function_address);
	}
      else
	{
	  printf("UNKNOWN    ");
	}

      if (import.data_segment)
	{
	  printf("0x%08x ", import.data_segment);
	}
      else
	{
	  printf("UNKNOWN    ");
	}

      printf("%s\n", imported_symbol_name);
    }
}

/***********************************************************************
 * dump_relocation_entries
 ***********************************************************************/

static void dump_relocation_entries(FILE *in_stream, struct xflat_hdr *header)
{
  struct xflat_reloc reloc;
  int status;
  int i;

  /* Seek to the beginning of the relocation records. */

  if (0 != fseek(in_stream, get_xflat32(&header->reloc_start), SEEK_SET))
    {
      fprintf(stderr, "ERROR: fseek to reloc records failed, errno=%d\n",
	      errno);
      exit(1);
    }

  printf("\nRELOCATION ENTRIES:\n");
  printf("      DATA OFFS  RELOC TYPE\n\n");
	
  for ( i = 0 ; i < get_xflat32(&header->reloc_count); i++ )
    {
      /* Read the next reloction entry. */

      status = fread((void*)&reloc, sizeof(struct xflat_reloc), 1, in_stream);
      if (status != 1)
	{
	  fprintf(stderr, "Error reading reloc record %d, errno=%d\n",
		  i+1, errno);
	  exit(1);
	}

#ifdef RELOCS_IN_NETWORK_ORDER
      {
	u_int32_t *ptmp;
	ptmp = (u_int32_t*)&reloc;
	*ptmp = get_xflat32(ptmp);
      }
#endif

      if (reloc.type >= XFLAT_RELOC_TYPE_NUM)
	{
	  printf("%5d 0x%08x UNKNOWN(%d)\n",
		 i+1, reloc.offset, reloc.type);
	  fprintf(stderr, "Error eloc type out of range(%d)\n", reloc.type);
	  num_errors++;
	}
      else
	{
	  printf("%5d 0x%08x %s\n",
		 i+1, reloc.offset, reloc_type_string[reloc.type]);
	}
    }
}

/***********************************************************************
 * dump_hdr
 ***********************************************************************/

static void dump_hdr(struct xflat_hdr *header)
{
  u_int16_t hdr_type;

  /* Print the contents of the FLT header */
	
  printf("\nXFLAT HEADER:\n");
  printf("\nMagic            %c%c%c%c\n",
	 header->magic[0], header->magic[1],
	 header->magic[2], header->magic[3]);
		
  printf("Revision         %d\n", get_xflat16(&header->rev));

  printf("Binary type      ");
  hdr_type = get_xflat16(&header->type);
  if (hdr_type >= XFLAT_BINARY_TYPE_NUM)
    {
      printf("UNKNOWN(%d)\n", hdr_type);
      fprintf(stderr, "ERROR: Header type is out of range(%d)\n", hdr_type);
      num_errors++;
    }
  else
    {
      printf("XFLAT_BINARY_TYPE_%s\n", header_type_string[hdr_type]);
    }

  printf("\nMEMORY MAP:\n");
  printf("  Text start     0x%08x\n",   XFLAT_HDR_SIZE);
  printf("  Entry point    0x%08x\n",   get_xflat32(&header->entry));
  printf("  Data start     0x%08x\n",   get_xflat32(&header->data_start));
  printf("  Data end       0x%08x\n",   get_xflat32(&header->data_end)-1);
  printf("  Bss start      0x%08x\n",   get_xflat32(&header->data_end));
  printf("  Bss end        0x%08x\n",   get_xflat32(&header->bss_end)-1);
  printf("TOTAL SIZE       0x%08x\n\n", get_xflat32(&header->bss_end));
  printf("Stack size       0x%08x\n",   get_xflat32(&header->stack_size));
  printf("\nRELOCATIONS:\n");
  printf("  Reloc start    0x%08x\n",   get_xflat32(&header->reloc_start));
  printf("  reloc count    %d\n",       get_xflat32(&header->reloc_count));
  printf("\nIMPORTED SYMBOLS:\n");
  printf("  Import start   0x%08x\n",   get_xflat32(&header->import_symbols));
  printf("  Import count   %d\n",       get_xflat16(&header->import_count));
  printf("\nEXPORTED SYMBOLS:\n");
  printf("  Export start   0x%08x\n",   get_xflat32(&header->export_symbols));
  printf("  Export count   %d\n",       get_xflat16(&header->export_count));
  printf("\nLIBRARY SEARCH PATHES:\n");
  printf("  Lib path start 0x%08x\n",   get_xflat32(&header->lib_pathes));
  printf("  Lib path count %d\n",       get_xflat16(&header->lpath_count));
  printf("\nLIBRARY FILE NAMES:\n");
  printf("  Lib name start 0x%08x\n",   get_xflat32(&header->lib_names));
  printf("  Lib name count %d\n",       get_xflat16(&header->lname_count));
	
}

/***********************************************************************
 * show_usage
 ***********************************************************************/
 
static void
show_usage(void)
{  
  fprintf(stderr, "Usage: %s [options] <flat-filename>\n\n",
	  program_name);
#if 1
  fprintf(stderr, "Where options are one or more of the following:\n\n");
#else
  fprintf(stderr, "Where options are one or more of the following.  Note\n");
  fprintf(stderr, "that a space is always required between the option and\n");
  fprintf(stderr, "any following arguments\n\n");
#endif
  fprintf(stderr, "  -h Dump the XFLAT file header     [not dumped]\n");
  fprintf(stderr, "  -r Dump relocation entries        [not dumped]\n");
  fprintf(stderr, "  -i Dump the imported symbol table [not dumped]\n");
  fprintf(stderr, "  -e Dump the exported symbol table [not dumped]\n");
  fprintf(stderr, "  -L Dump the library search pathes [not dumped]\n");
  fprintf(stderr, "  -l Dump the library file names    [not dumped]\n");
  fprintf(stderr, "  -x Dump xFLT loader pathname      [not dumped]\n");
  fprintf(stderr, "  -c Disassemble the text section   [not dumped]\n");
  fprintf(stderr, "  -d Dump data section (hex)        [not dumped]\n");
  fprintf(stderr, "  -a Dump all of the above          [not dumped]\n");
#ifdef ARCH_BIG_ENDIAN
  fprintf(stderr, "  -b Assume little-endian byteorder [big endian]\n");
#else
  fprintf(stderr, "  -b Assume big-endian byteorder    [little endian]\n");
#endif
  fprintf(stderr, "  -v Output verbose debug info      [no output]\n");
  fprintf(stderr, "\n");
  exit(1);
}

/***********************************************************************
 * parse_args
 ***********************************************************************/

static void
parse_args(int argc, char **argv)
{
  int opt;

  /* Save our name (for show_usage) */

  program_name = argv[0];

  /* At least three things must appear on the program line:
   * the program name, the BFD filname, and at least one
   * option.
   */

  if (argc < 3)
    {
      fprintf(stderr, "ERROR: Missing required arguments\n\n");
      show_usage();
    }

  /* Get miscellaneous options from the command line. */

  while ((opt = getopt(argc, argv, "hrieLlxcbdav")) != -1)
    {
      switch (opt)
	{

	case 'h': /* Dump the flat file header */
	  dump_header++;
	  break;

	case 'r': /* Dump the flat file header */
	  dump_relocs++;
	  break;

	case 'i': /* Dump the imported symbol table */
	  dump_imports++;
	  break;

	case 'e': /* Dump the exported symbol table */
	  dump_exports++;
	  break;

	case 'L': /* Dump the library search pathes */
	  dump_lpathes++;
	  break;

	case 'l': /* Dump the library file names */
	  dump_lnames++;
	  break;

	case 'x': /* Dump the library file names */
	  dump_ldrpath++;
	  break;

	case 'c': /* Disassembly text */
	  if (print_insn_arm)
	    {
	      dump_text++;
	    }
	  else
	    {
	      printf("-c ignored: No disassembler available\n");
	    }
	  break;

	case 'b': /* other-endian*/
#ifdef ARCH_BIG_ENDIAN
	  big_endian = 0;
#else
	  big_endian++;
#endif
	  break;

	case 'd': /* Dump data */
	  dump_data++;
	  break;

	case 'a': /* Dump everying */
	  dump_header++;
	  dump_relocs++;
	  dump_imports++;
	  dump_exports++;
	  dump_lpathes++;
	  dump_lnames++;
	  dump_ldrpath++;
	  dump_text++;
	  dump_data++;
	  break;

	case 'v': /* Output verbose debug information */
	  verbose++;
	  break;

	default:
	  fprintf(stderr, "%s Unknown option\n\n", argv[0]);
	  show_usage();
	  break;
	}
    }

  /* Get the name of the input BFD file. */

  xflat_filename = argv[argc-1];
}
 
/***********************************************************************
 * Public Functions
 ***********************************************************************/
 
/***********************************************************************
 * main
 ***********************************************************************/
 
int 
main(int argc, char **argv, char **envp)
{
  FILE *in_stream;
  struct xflat_hdr header;

  /* Get the input parameters */

  parse_args(argc, argv);

  /* Open the FLT file */

  in_stream = fopen(xflat_filename,"rb");
  if (NULL == in_stream)
    {
      fprintf(stderr,"Cannot open file %s for reading\n",
	      xflat_filename);
      exit(1);
    }

  /* Read the FLT header */

  if ( 1 != fread((void*)&header, sizeof(struct xflat_hdr), 1, in_stream))
    {
      fprintf(stderr,"Error reading flat header\n");
      exit(1);
    }

  printf("Dumping Flat Binary File: %s\n", xflat_filename);

  /* Dump the contents of the FLT header */

  if (dump_header)
    {
      dump_hdr(&header);
    }

  /* Dump the relocation entries */

  if (dump_relocs)
    {
      dump_relocation_entries(in_stream, &header);
    }

  /* Dump all imported symbols */

  if (dump_imports)
    {
      dump_imported_symbols(in_stream, &header);
    }

  /* Dump all exported symbols */

  if (dump_exports)
    {
      dump_exported_symbols(in_stream, &header);
    }

  /* Dump library search pathes */

  if (dump_ldrpath)
    {
      dump_loader_pathname(in_stream, &header);
    }

  if (dump_lpathes)
    {
      dump_library_search_pathes(in_stream, &header);
    }

  /* Dump library names */

  if (dump_lnames)
    {
      dump_library_names(in_stream, &header);
    }

  if (dump_text)
    {
      disassemble_text(in_stream, &header);
    }

  if (dump_data)
    {
      dump_hex_data(in_stream, &header);
    }

  fclose(in_stream);

  if (num_errors > 0)
    {
      fprintf(stderr, "Finished with %d errors\n", num_errors);
    }

  return 0 ;
}
