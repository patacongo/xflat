/***********************************************************************
 * xflat/tools/ld-options.c
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include "ld-options.h"

/***********************************************************************
 * Public Constant Data
 ***********************************************************************/

/* These the gld options that are valid, but are not recognized by
 * xflat-ld.  unsupported_options1[] is the list of unsupported_options
 * that begin with a single hyphen; unsupported_options2[] is the list
 * of unsupported options that begin with two hyphens.
 */

const struct unsupported_option_desc unsupported_options1[] =
{
  {"(",				MATCH_WHOLE},
  {")",				MATCH_WHOLE},

  {"A",				MATCH_TO_LENGTH},
  {"Bgroup",			MATCH_WHOLE},
#ifdef FLAT_TARGET
  {"Bshareable",		MATCH_WHOLE},
#endif
  {"Bsymbolic",			MATCH_WHOLE},
  {"EB",			MATCH_WHOLE},
  {"EL",			MATCH_WHOLE},
  {"E",				MATCH_WHOLE},
  {"F",				MATCH_PLUS_ARG},
  {"G",				MATCH_TO_LENGTH},
  {"M",				MATCH_WHOLE},
  {"Map",			MATCH_PLUS_ARG},
  {"N",				MATCH_WHOLE},
  {"O",				MATCH_PLUS_ARG},
  {"Qy",			MATCH_WHOLE},
  {"R",				MATCH_PLUS_ARG},
  {"S",				MATCH_WHOLE},
  {"T",				MATCH_PLUS_ARG},
  {"Tbss",			MATCH_PLUS_ARG},
  {"Tdata",			MATCH_PLUS_ARG},
  {"Ttext",			MATCH_PLUS_ARG},
  {"Ur",			MATCH_WHOLE},
  {"V",				MATCH_WHOLE},
  {"X",				MATCH_WHOLE},
  {"Y",				MATCH_PLUS_ARG},

  {"assert",			MATCH_PLUS_ARG},
  {"a",				MATCH_TO_LENGTH}, /* Must come after of a's */
  {"b",				MATCH_PLUS_ARG},
  {"c",				MATCH_PLUS_ARG},
#ifdef FLAT_TARGET
  {"call_shared",		MATCH_WHOLE},
#endif
  {"dc",			MATCH_WHOLE},
  {"d",				MATCH_WHOLE},
  {"dp",			MATCH_WHOLE},
#ifdef FLAT_TARGET
  {"dy",			MATCH_WHOLE},
#endif
  {"f",				MATCH_WHOLE},
#ifdef FLAT_TARGET
  {"fini",			MATCH_PLUS_ARG},
#endif
  {"h",				MATCH_TO_LENGTH},
  {"i",				MATCH_WHOLE},
#ifdef FLAT_TARGET
  {"init",			MATCH_PLUS_ARG},
#endif
  {"m",				MATCH_TO_LENGTH},
  {"n",				MATCH_WHOLE},
  {"qmagic",			MATCH_WHOLE},
  {"q",				MATCH_WHOLE},
  {"r",				MATCH_WHOLE},
  {"rpath-link",		MATCH_PLUS_ARG},
  {"s",				MATCH_WHOLE},
#ifdef FLAT_TARGET
  {"shared",			MATCH_WHOLE},
#endif
  {"soname",			MATCH_TO_EQUAL},
  {"t",				MATCH_WHOLE},
  {"u",				MATCH_PLUS_ARG},
  {"v",				MATCH_WHOLE},
  {"x",				MATCH_WHOLE},
  {"y",				MATCH_PLUS_ARG},
  {"z ",			MATCH_PLUS_ARG},
};

const int nunsupported_options1 =
sizeof(unsupported_options1) / sizeof(struct unsupported_option_desc);

const struct unsupported_option_desc unsupported_options2[] =
{
  {"add-stdcall-alias",		MATCH_WHOLE},
  {"allow-shlib-undefined",	MATCH_WHOLE},
  {"architecture",		MATCH_TO_EQUAL},
  {"auxiliary",			MATCH_PLUS_ARG},
  {"base-file",			MATCH_PLUS_ARG},
  {"check-sections",		MATCH_WHOLE},
  {"cref",			MATCH_WHOLE},
  {"defsym",			MATCH_TO_EQUAL},
  {"demangle",			MATCH_TO_EQUAL},
  {"demangle",			MATCH_WHOLE},
  {"disable-stdcall-fixup",	MATCH_WHOLE},
  {"discard-all",		MATCH_WHOLE},
  {"discard-locals",		MATCH_WHOLE},
  {"dll",			MATCH_WHOLE},
#ifdef FLAT_TARGET
  {"dynamic-linker",		MATCH_PLUS_ARG},
#endif
  {"embedded-relocs",		MATCH_WHOLE},
  {"emit-relocs",		MATCH_WHOLE},
  {"enable-stdcall-fixup",	MATCH_WHOLE},
  {"end-group",			MATCH_WHOLE},
  {"exclude-symbols",		MATCH_PLUS_ARG},
  {"export-all-symbols",	MATCH_WHOLE},
  {"export-dynamic",		MATCH_WHOLE},
  {"fatal-warnings",		MATCH_WHOLE},
  {"file-alignment",		MATCH_WHOLE},
  {"filter",			MATCH_PLUS_ARG},
  {"force-exe-suffix",		MATCH_WHOLE},
  {"format",			MATCH_TO_EQUAL},
  {"gc-sections",		MATCH_WHOLE},
  {"gpsize",			MATCH_TO_EQUAL},
  {"heap",			MATCH_PLUS_ARG},
  {"image-base",		MATCH_PLUS_ARG},
  {"just-symbols",		MATCH_TO_EQUAL},
  {"kill-at",			MATCH_WHOLE},
  {"major-image-version",	MATCH_PLUS_ARG},
  {"major-os-version",		MATCH_PLUS_ARG},
  {"major-subsystem-version",	MATCH_PLUS_ARG},
  {"minor-image-version",	MATCH_PLUS_ARG},
  {"minor-os-version",		MATCH_PLUS_ARG},
  {"minor-subsystem-version",	MATCH_PLUS_ARG},
  {"mri-script",		MATCH_TO_EQUAL},
  {"nmagic",			MATCH_WHOLE},
  {"no-check-sections",		MATCH_WHOLE},
  {"no-demangle",		MATCH_WHOLE},
  {"no-gc-sections",		MATCH_WHOLE},
  {"noinhibit-exec",		MATCH_WHOLE},
  {"no-keep-memory",		MATCH_WHOLE},
  {"no-undefined",		MATCH_WHOLE},
  {"no-warn-mismatch",		MATCH_WHOLE},
  {"no-whole-archive",		MATCH_WHOLE},
  {"oformat",			MATCH_PLUS_ARG},
  {"omagic",			MATCH_WHOLE},
  {"output-def file",		MATCH_PLUS_ARG},
  {"print-map",			MATCH_WHOLE},
  {"relax",			MATCH_WHOLE},
  {"relocateable",		MATCH_WHOLE},
  {"retain-symbols-file",	MATCH_PLUS_ARG},
  {"script",			MATCH_TO_EQUAL},
  {"section-alignment",		MATCH_WHOLE},
  {"section-start",		MATCH_PLUS_ARG},
  {"sort-common",		MATCH_WHOLE},
  {"split-by-file",		MATCH_PLUS_ARG},
  {"split-by-reloc",		MATCH_PLUS_ARG},
  {"start-group",		MATCH_WHOLE},
  {"stats",			MATCH_WHOLE},
  {"strip-all",			MATCH_WHOLE},
  {"strip-debug",		MATCH_WHOLE},
  {"subsystem",			MATCH_PLUS_ARG},
  {"target-help",		MATCH_WHOLE},
  {"trace",			MATCH_WHOLE},
  {"trace-symbol",		MATCH_TO_EQUAL},
  {"traditional-format",	MATCH_WHOLE},
  {"undefined",			MATCH_TO_EQUAL},
  {"unique",			MATCH_TO_EQUAL},
  {"unique",			MATCH_WHOLE},
  {"version",			MATCH_WHOLE},
  {"version-script",		MATCH_TO_EQUAL},
  {"warn-common",		MATCH_WHOLE},
  {"warn-constructors",		MATCH_WHOLE},
  {"warn-multiple-gp",		MATCH_WHOLE},
  {"warn-once",			MATCH_WHOLE},
  {"warn-section-align",	MATCH_WHOLE},
  {"whole-archive",		MATCH_WHOLE},
  {"wrap symbol",		MATCH_WHOLE},
};
const int nunsupported_options2 =
sizeof(unsupported_options2) / sizeof(struct unsupported_option_desc);

const struct supported_option_desc supported_options1[] =
{
  {"Bdynamic",		MATCH_WHOLE,		XFLAT_DYNAMIC},
#ifndef FLAT_TARGET
  {"Bshareable",	MATCH_WHOLE,		XFLAT_SHARED_LIBRARY},
#endif
  {"Bstatic",		MATCH_WHOLE,		XFLAT_STATIC},
  {"L",			MATCH_PLUS_ARG,		XFLAT_LIBRARY_PATH},
  {"L",			MATCH_TO_LENGTH,	XFLAT_LIBRARY_PATH},
#ifndef FLAT_TARGET
  {"W1,",		MATCH_TO_LENGTH,	XFLAT_LDELFLIB_ARGS},
  {"W2,",		MATCH_TO_LENGTH,	XFLAT_LDELF2XFLT_ARGS},
#else
  {"W1,",		MATCH_TO_LENGTH,	XFLAT_LDELF2XFLT_ARGS},
#endif
#ifndef FLAT_TARGET
  {"call_shared",	MATCH_WHOLE,		XFLAT_DYNAMIC},
#endif
  {"dn",		MATCH_WHOLE,		XFLAT_STATIC},
#ifndef FLAT_TARGET
  {"dy",		MATCH_WHOLE,		XFLAT_DYNAMIC},
#endif
  {"e",			MATCH_PLUS_ARG,		XFLAT_ENTRY_NAME},
#ifndef FLAT_TARGET
  {"fini",		MATCH_PLUS_ARG,		XFLAT_EXIT_NAME},
#endif
  {"g",			MATCH_WHOLE,		XFLAT_GEN_DEBUG},
#ifndef FLAT_TARGET
  {"init",		MATCH_PLUS_ARG,		XFLAT_ENTRY_NAME},
#endif
  {"l",			MATCH_TO_LENGTH,	XFLAT_LIBRARY_NAME},
  {"non_shared",	MATCH_WHOLE,		XFLAT_STATIC},
  {"o",			MATCH_PLUS_ARG,		XFLAT_OUTPUT_NAME},
  {"rpath",		MATCH_PLUS_ARG,		XFLAT_LIBRARY_PATH},
#ifndef FLAT_TARGET
  {"shared",		MATCH_WHOLE,		XFLAT_SHARED_LIBRARY},
#endif
  {"static",		MATCH_WHOLE,		XFLAT_STATIC},
};
const int nsupported_options1 =
sizeof(supported_options1) / sizeof(struct supported_option_desc);

const struct supported_option_desc supported_options2[] =
{
  {"cxx",		MATCH_WHOLE,		XFLAT_CXX},
  {"entry",		MATCH_TO_EQUAL,		XFLAT_ENTRY_NAME},
#ifndef FLAT_TARGET
  {"dynamic-linker",	MATCH_PLUS_ARG,		XFLAT_LOADER_NAME},
#endif
  {"dll-verbose",	MATCH_WHOLE,		XFLAT_VERBOSE},
  {"help",		MATCH_WHOLE,		XFLAT_HELP},
  {"library",		MATCH_TO_EQUAL,		XFLAT_LIBRARY_NAME},
  {"library-path",	MATCH_TO_EQUAL,		XFLAT_LIBRARY_PATH},
  {"output",		MATCH_TO_EQUAL,		XFLAT_OUTPUT_NAME},
  {"stack",		MATCH_PLUS_ARG,		XFLAT_STACK_SIZE},
  {"verbose",		MATCH_WHOLE,		XFLAT_VERBOSE},
};
const int nsupported_options2 =
  sizeof(supported_options2) / sizeof(struct supported_option_desc);
