/***********************************************************************
 * xflat/tools/ld-options.h
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#ifndef _LD_OPTIONS_H_
#define _LD_OPTIONS_H_

/***********************************************************************
 * Public Type Declarations
 ***********************************************************************/

enum ld_type_e
{
  MATCH_WHOLE,		/* Option is just the provided string. */
  MATCH_TO_LENGTH,      /* Option is <string><something> */
  MATCH_TO_EQUAL,	/* Option is <string>=<something> */
  MATCH_PLUS_ARG,	/* Option is <string> <something> */
  MATCH_SPECIAL		/* Option is something else */
};

enum xflat_type_e
{
  XFLAT_CXX,
  XFLAT_ENTRY_NAME,
  XFLAT_EXIT_NAME,
  XFLAT_LOADER_NAME,
  XFLAT_LIBRARY_NAME,
  XFLAT_LIBRARY_PATH,
  XFLAT_OUTPUT_NAME,
  XFLAT_SHARED_LIBRARY,
  XFLAT_STACK_SIZE,
  XFLAT_VERBOSE,
  XFLAT_HELP,
  XFLAT_LDELFLIB_ARGS,
  XFLAT_LDELF2XFLT_ARGS,
  XFLAT_STATIC,
  XFLAT_DYNAMIC,
  XFLAT_GEN_DEBUG
};

struct unsupported_option_desc
{
  const char     *option;
  unsigned short ld_type;
};

struct supported_option_desc
{
  const char     *option;
  unsigned short ld_type;
  unsigned short xflat_type;
};

/***********************************************************************
 * Public Constant Data
 ***********************************************************************/

/* These the gld options that are valid, but are not recognized by
 * xflat-ld.  unsupported_options1[] is the list of unsupported_options
 * that begin with a single hyphen; unsupported_options2[] is the list
 * of unsupported options that begin with two hyphens.
 */

extern const struct unsupported_option_desc unsupported_options1[];
extern const int nunsupported_options1;

extern const struct unsupported_option_desc unsupported_options2[];
extern const int nunsupported_options2;

/* These the gld options that are recognized by xflat-ld.
 * supported_options1[] is the list of supported_options
 * that begin with a single hyphen; uupported_options2[] is the list
 * of supported options that begin with two hyphens.
 */

extern const struct supported_option_desc supported_options1[];
extern const int nsupported_options1;

extern const struct supported_option_desc supported_options2[];
extern const int nsupported_options2;

#endif /* _LD_OPTIONS_H_ */
