/***********************************************************************
 * xflat/tools/xflat-gcc.c
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

/***********************************************************************
 * Included Files
 ***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>

#include <sys/types.h>
#include <sys/wait.h>

#include "gcc-strings.h"

/***********************************************************************
 * Verify that we are fully configured
 ***********************************************************************/

#ifndef ARCHCC
# error ARCHCC not defined
#endif
#ifndef ARCH_INC1_DIR
# error ARCH_INC1_DIR not defined
#endif
#ifndef XFLAT_INC_DIR
# error XFLAT_INC_DIR not defined
#endif

/***********************************************************************
 * Compilation Switches
 ***********************************************************************/

/* xflat-gcc should not normally be used to compile final executables;
 * In the xflat usage model, that is done by xflat-ld to produce an
 * xflat executable (vs. an ELF executable).  But the following switch
 * will permit this behavior:
 */

/* #define ALLOW_FINAL_TARGETS 1 */

/***********************************************************************
 * Private Variables
 ***********************************************************************/

static const char *tool_name      = NULL;

static int    link_module         = 1;
static int    verbose             = 0;
static int    use_stdinc          = 1;

static char **user_include_path   = NULL;
static char **std_include_path    = NULL;

static int    nuser_includes      = 0;
static int    nstd_includes       = 0;

/***********************************************************************
 * Private Constants
 ***********************************************************************/

static const char archcc[]             = ARCHCC;
static const char arch_inc1_dir[]      = "-I"ARCH_INC1_DIR;
#ifdef ARCH_INC2_DIR
static const char arch_inc2_dir[]      = "-I"ARCH_INC2_DIR;
#endif
#ifdef ARCH_INC3_DIR
static const char arch_inc3_dir[]      = "-I"ARCH_INC3_DIR;
#endif
static const char xflat_inc_dir[]      = "-I"XFLAT_INC_DIR;

static char nostdinc[]                 = "-nostdinc";

/***********************************************************************
 * Private Functions
 ***********************************************************************/

/***********************************************************************
 * my_malloc
 ***********************************************************************/

static void *
my_malloc(unsigned long size)
{
  void *retval = malloc(size);
  if (!retval)
    {
      fprintf(stderr, "Failed to malloc %ld bytes\n", size);
      exit(1);
    }
  return retval;
}

/***********************************************************************
 * fork_exec_and_wait
 ***********************************************************************/

static int
fork_exec_and_wait(const char *path, char *const argv[])
{
  pid_t pid;
  int   status;
  int   i;

  /* Let the world know just what we are going to do */

  if (verbose)
    {
      for ( i = 0 ; argv[i] ; i++ )
	{
	  printf("arg[%2i] = %s\n", i, argv[i]);
	}
      fflush(stdout);
    }

  pid = fork();
  if (pid == -1)
    {
      fprintf(stderr, "fork failed\n");
      exit(1);
    }
  else if (pid == 0)
    {
      /* We are the child... exec the requested process. */

      status = execvp(path, argv);
      fprintf(stderr, "execvp returned\n");
      exit(1);
    }
  else
    {
      /* We are the parent.  Wait for the exec'ed process
       * to complete.
       */

      int waitstatus = waitpid(pid, &status, 0);
      if (waitstatus < 0)
	{
	  fprintf(stderr, "waitpid failed\n");
	  exit(1);
	}

      /* Was the chile processed terminated by a signal which
       * was not caught?
       */

      else if (WIFSIGNALED(status) != 0)
	{
	  status = 1;
	}

      /* Assume that the child process exited via a call to exit */

      else
	{
	  status = WEXITSTATUS(status);
	}
    }
  return status;
}

/***********************************************************************
 * parse_args
 ***********************************************************************/

static void
parse_args(int argc, char **argv)
{
  int std_include = 0;
  int i;

  /* Allocate arrays to hold references to include files pathes */

  user_include_path  = (char**)my_malloc(sizeof(char*) * argc);
  std_include_path   = (char**)my_malloc(sizeof(char*) * argc);
  nuser_includes     = 0;
  nstd_includes      = 0;

  /* Now examine each argument.  If the argument is recognized and
   * captured in this loop, its argv pointer will be nullified.
   * The remaining non-null pointers will be passed through to
   * gcc.
   */

  for ( i = 1 ; i < argc ; i++ )
    {
      /* If it begins with '-', it must be an argument */

      if (argv[i][0] == '-')
	{
	  switch (argv[i][1])
	    {
	    case 'E': /* preprocess only */
	    case 'M': /* generate dependencies */
	    case 'S': /* generate assembler code */
	    case 'c': /* compile or assemble */
	    case 'r': /* partial link */
	      link_module = 0;
	      break;

	    case 'd': /* partial link */
	      if ((strcmp("dumpversion", &argv[i][1]) == 0) ||
		  (strcmp("dumpmachine", &argv[i][1]) == 0))
		{
		  link_module = 0;
		}
	      break;

	    case 'v': /* verbose */
	      if (argv[i][2] == 0)
		{
		  verbose = 1;
		}
	      break;

	    case 'I':
	      if (argv[i][2] == '-')
		{
		  std_include++;
		}
	      else if (std_include)
		{
		  std_include_path[nstd_includes++] = argv[i];
		}
	      else
		{
		  user_include_path[nuser_includes++] = argv[i];
		}
	      argv[i] = NULL;
	      break;

	    case 'n': /* -nostdinc */
	      if (strcmp(nostdinc, argv[i]) == 0)
		{
		  use_stdinc = 0;
		  argv[i] = NULL;
		}
	      break;

	    case '-':
	      if (strcmp("help", &argv[i][2]) == 0)
		{
		  link_module = 0;
		}
	      break;
	    }
	}
    }

  /* No do some final sanity checking. */

#ifndef ALLOW_FINAL_TARGETS
  if (link_module)
    {
      fprintf(stderr, "%s cannot be used to produce final modules (programs\n",
	      tool_name);
      fprintf(stderr, "or shared libraries).  Use xflat-ld to produce these.\n");
      fprintf(stderr, "To use %s, you must specify on of the following on\n",
	      tool_name);
      fprintf(stderr, "command line:  -E, -M, -S, -c, -r, -dumpversion, -dumpmachine,\n");
      fprintf(stderr, "or --help\n");
      exit(1);
    }
#endif /* ALLOW_FINAL_TARGETS */
}

/***********************************************************************
 * exec_gcc
 ***********************************************************************/
static int
exec_gcc(int argc, char **argv)
{
  char **gcc_argv;
  int status;
  int nargs = 0;
  int i;

  /* Now, create the arguments that we will pass to gcc */

  gcc_argv = my_malloc(sizeof(char*) * (argc + 10));

  i = 0;

  /* The program name is the first argument */

  gcc_argv[nargs++] = (char*)archcc;

  /* No matter what is going on, we will not use the standard
   * include pathes.
   */

  gcc_argv[nargs++] = (char*)nostdinc;

  /* A side effect of specifying -I- on the command line is that
   * -I. is removed from the "user" include pathes.  If we are
   * going to add -I- without the user's knowledge, then make
   * sure that we restore this default behavior.
   *
   * This happens when the user did not specify -I- but we
   * are going to replace the standard include pathes which
   * we will do whenever -nostdinc is not specified.
   */

  if ((std_include_path == 0) || (use_stdinc))
    {
      /* Note, this is not exactly the same thing... this is the
       * directory that was current when the compiler was invoked
       * (not necessarily the current directory).  This will have
       * to do.
       */

      gcc_argv[nargs++] = "-I.";
    }

  /* Add all of the "user" include pathes */

  for (i = 0; i < nuser_includes; i++)
    {
      gcc_argv[nargs++] = user_include_path[i];
    }

  /* If the user has specified "standard" include pathes
   * or has NOT specified -nostdinc, then we will add
   * some "standard" pathes.
   */

  if ((std_include_path > 0) || (use_stdinc))
    {
      gcc_argv[nargs++] = "-I-";

      /* If the user has not suppressed standard includes,
       * then add the xflat include directory as the first
       * path.
       */

      if (use_stdinc)
	{
	  gcc_argv[nargs++] = (char*)xflat_inc_dir;
	}

      /* Add all of the user specified "standard" include pathes */

      for (i = 0; i < nstd_includes; i++)
	{
	  gcc_argv[nargs++] = std_include_path[i];
	}

      /* Finally, put the compiler's standard include path at the
       * end.
       */

      if (use_stdinc)
	{
	  gcc_argv[nargs++] = (char*)arch_inc1_dir;
#ifdef ARCH_INC2_DIR
	  gcc_argv[nargs++] = (char*)arch_inc2_dir;
#endif
#ifdef ARCH_INC3_DIR
	  gcc_argv[nargs++] = (char*)arch_inc3_dir;
#endif
	}
    }

  /* Now add all of the other command line parameters (unless
   * the parameter was nullified in parse_args() and, of course,
   * skipping argv[0] which contains this programs name). */

  for (i = 1; i < argc; i++)
    {
      if (argv[i])
	{
	  gcc_argv[nargs++] = argv[i];
	}
    }

  gcc_argv[nargs++] = NULL;

  /* Fire off gcc with the selected arguments */

  status = fork_exec_and_wait(archcc, gcc_argv);
  free(gcc_argv);
  return status;
}

/***********************************************************************
 * Public Functions
 ***********************************************************************/

/***********************************************************************
 * main
 ***********************************************************************/

int main(int argc, char **argv, char **envp)
{
  int status;

  /* Get our name */

  tool_name = basename(argv[0]);

  /* Then parse the incoming arguments */

  parse_args(argc, argv);

  /* Execute gcc */

  status = exec_gcc(argc, argv);
  if (status != 0)
    {
      /* If gcc fails, so do we */

      exit(status);
    }
  return 0;
}
