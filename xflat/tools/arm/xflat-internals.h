/***********************************************************************
 * xflat/tools/arm/xflat-internals.h
 *
 * Copyright (c) 2002, 2006, Cadenux, LLC.  All rights reserved.
 * Copyright (c) 2002, 2006, Gregory Nutt.  All rights reserved.
 * Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Cadenux nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#ifndef __XFLAT_INTERNALS_H_
#define __XFLAT_INTERNALS_H_

/***********************************************************************
 * Included Files
 ***********************************************************************/

#ifndef XFLAT_ASSEMBLY
# include "xflat.h"  /* For u_int32_t */
#endif

/***********************************************************************
 * Definitions
 ***********************************************************************/

/* Register definitions.  These must exactly match those provided
 * in dyncall_skeleton.def: */

#define PIC_REG  r10 /* Current PIC base register value */
#define RINFO    r4  /* A pointer to the DYN info structure */
#define HISPIC   r5  /* Callers PIC base register value */
#define TOS      r6  /* Top of the DYN call stack */
#define WK1      r7  /* Available for use without saving */
#define WK2      r8  /* Available for use without saving */

#define PIC_REG_STRING "r10"

/*******************************************************************
 *  C Macros/Inline functions
 *******************************************************************/

#ifndef XFLAT_ASSEMBLY

static inline u_int32_t
xflat_get_pic_base(void)
{
  u_int32_t retval;
  __asm__ __volatile__ ("\tmov %0, " PIC_REG_STRING "\n\t":"=r"(retval):);
  return retval;
}

static inline void
xflat_set_pic_base(u_int32_t pic_base)
{
  __asm__ __volatile__ ("\tmov " PIC_REG_STRING ", %0\n\t"::"r"(pic_base));
}    

#endif /* XFLAT_ASSEMBLY */

/*******************************************************************
 * Assembly Language Macros
 *******************************************************************/

#ifdef XFLAT_ASSEMBLY

/* These macros defines generic assembly functions to pick off the
 * XFLAT info from protected registers and to forward this info
 * to a C function.  This logic must complement the logic in
 * dyncall_skeleton.def.  On entry, the is assumed:
 *
 *      1. [RINFO]  = Points to the dyncall info data
 *      3. [HISPIC]  = Points to the PIC base of the caller
 *      4. [TOS]  = Points to the top of the dyncall stack
 *      5. WK1-WK2, and lr are available for use.
 *
 * These macro save registers, put the callers PIC base in the
 * argument list, then call the specified function.  Then performs
 * the necessary cleanup when that function returns.
 */
	.macro pickoff0 fname1, fname2
	.global	\fname1
	.type	\fname1, %function
	.global	\fname2
	.type	\fname2, %function
\fname1:
	mov	r0, HISPIC		@ ARG1 = Callers PIC
	b	\fname2			@ Branch to the function with ARG1
	.size	\fname1, .-\fname1
	.endm

	.macro pickoff1 fname1, fname2
	.global	\fname1
	.type	\fname1, %function
	.global	\fname2
	.type	\fname2, %function
\fname1:
	mov	r1, HISPIC		@ ARG2 = Callers PIC
	b	\fname2			@ Branch to the function with ARG1-2
	.size	\fname1, .-\fname1
	.endm

	.macro pickoff2 fname1, fname2
	.global	\fname1
	.type	\fname1, %function
	.global	\fname2
	.type	\fname2, %function
\fname1:
	mov	r2, HISPIC		@ ARG3 = Callers PIC
	b	\fname2			@ Branch to the function with ARG1-3
	.size	\fname1, .-\fname1
	.endm

	.macro pickoff3 fname1, fname2
	.global	\fname1
	.type	\fname1, %function
	.global	\fname2
	.type	\fname2, %function
\fname1:
	mov	r3, HISPIC		@ ARG4 = Callers PIC
	b	\fname2			@ Branch to the function with ARG1-4
	.size	\fname1, .-\fname1
	.endm

	.macro pickoff4 fname1, fname2
	.global	\fname1
	.type	\fname1, %function
	.global	\fname2
	.type	\fname2, %function
\fname1:
	str	lr, [sp, #-4]!		@ Save regs
	sub	sp, sp, #4		@ Make space for ARG5 on stack
	str	HISPIC, [sp]		@ ARG5 = Callers PIC is on the stack
	bl	\fname2			@ Call the function with ARG1-5
	add	sp, sp, #4		@ Release ARG5
	ldmia	sp!, {pc}		@ Return
	.size	\fname1, .-\fname1
	.endm

	.macro pickoff5 fname1, fname2
	.global	\fname1
	.type	\fname1, %function
	.global	\fname2
	.type	\fname2, %function
\fname1:
	str	lr, [sp, #-4]!		@ Save regs
	sub	sp, sp, #8		@ Make space for ARG5-6 on stack
	ldr	r12, [sp, #12]		@ Fetch old ARG5
	str	HIPIC, [sp, #4]		@ ARG6 = Callers PIC is on the stack
	str	r12, [sp]		@ Copy old ARG5
	bl	\fname2			@ Call the function with ARG1-6
	add	sp, sp, #8		@ Release ARG5-6
	ldmia	sp!, {pc}		@ Return
	.size	\fname1, .-\fname1
	.endm

	.macro pickoff6 fname1, fname2
	.global	\fname1
	.type	\fname1, %function
	.global	\fname2
	.type	\fname2, %function
\fname1:
	str	lr, [sp, #-4]!		@ Save regs
	sub	sp, sp, #12		@ Make space for ARG5-7 on stack
	add	r12, sp, #16		@ Fetch old ARG5-6
	ldmia	r12, {r12, lr}		@
	stmia	sp, {r12, lr}		@ Copy old ARG5-6
	str	r0, [sp, #8]		@ ARG7 = Callers PIC is on the stack
	bl	\fname2			@ Call the function with ARG1-7
	add	sp, sp, #12		@ Release ARG5-7
	ldmia	sp!, {pc}		@ Return
	.size	\fname1, .-\fname1
	.endm

	.macro pickoff7 fname1, fname2
	.global	\fname1
	.type	\fname1, %function
	.global	\fname2
	.type	\fname2, %function
\fname1:
	str	lr, [sp, #-4]!		@ Save regs
	sub	sp, sp, #16		@ Make space for ARG5-8 on stack
	add	r12, sp, #24		@ Fetch old ARG5-6
	ldmia	r12, {r12, lr}		@
	ldr	WK1, [sp, #32]		@ Fetch old ARG7
	stmia	sp, {r12, lr}		@ Save old ARG5-6
	str	WK1, [sp, #8]		@ Save old ARG7
	str	HISPIC, [sp, #12]	@ ARG8 = Callers PIC is on the stack
	bl	\fname2			@ Call the function with ARG1-8
	add	sp, sp, #16		@ Release ARG5-8
	ldmia	sp!, {pc}		@ Return
	.size	\fname1, .-\fname1
	.endm
#endif /* XFLAT_ASSEMBLY */

#endif /* __XFLAT_INTERNALS_H_ */
