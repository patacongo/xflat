#######################################################################
# File: toolchain/config/rosedale/xflat.config
# Description:
#
#   This config file is included by XFLAT make files.  The variable
#   TOPDIR will be defined when this make fragment is included.
#   This configuration file includes target-specific settings.
#   In principle, it should only be necessary of modify this file
#   build the target binaries with a new toolchain.
#
# Created 2006, Copyright (C) 2002 Cadenux, LLC.  All rights reserved.
# Author: Gregory Nutt <spudmonkey@racsa.co.cr>
#
# Please report all bugs/problems to the author or <info@cadenux.com>
#######################################################################

#######################################################################
# General
#######################################################################
# Set the following to y to enable XFLAT debug options

XFLAT_DEBUG	        = n

# This defines the target architecture family (e.g., arm) and
# endian-ness

ARCHTARGET		= arm
ARCH_BIG_ENDIAN		= y

# The XFLAT binary loader may be built in the kernel source tree
# (see xflat/kernel/patches) or may be built outside of the kernel
# source tree as an installable kernel module (only).  Select the
# the following as y to build the loader outside of the kernel source
# tree

XFLATMODULE		 = n

#######################################################################
# Buildroot Environment.  These values will be provided by the
# buildroot toolchain/xflat makefile at build time
#######################################################################
# See also ARCH_BFD_*_DIR

STAGING_DIR		=
TARGET_DIR		=
BASE_DIR		=

#######################################################################
# Describe the host development tools
#######################################################################
# HOSTCC		Name of the host C compiler (usually gcc)
# HOSTCXX		Name of the host C++ compiler (usually g++)
# HOSTAR		Name of the host archiver (usually ar rc)
# HOSTWARNINGS		-Wall
# HOSTOPTIMIZE		-O2
# HOSTDEBUG		-g
# HOSTLD		Use $(HOSTCC)
# HOSTLDFLAGS		Usually empty

HOSTCC			= gcc
HOSTCXX			= g++
HOSTAR			= ar rc
HOSTWARNINGS		= -Wall
HOSTCOPTIMIZE		= -O2
HOSTDEBUG		= -g
HOSTLD			= $(HOSTCC)
HOSTLDFLAGS		= 

#######################################################################
# Describe the linux kernel
#######################################################################
# KERNEL_VERSION	eg. 2.4, 2.6
# KERNEL_DIR		Path to the kernel sources

KERNEL_VERSION		= 2.6
KERNEL_DIR		= $(BASE_DIR)/../linux-2.6.14

#######################################################################
# Describe the target root filesystem
#######################################################################
# ROOTFS_DIR		The top of the root file system image
# MODULE_DIR		The kernel-specific directory where modules
#			are installed

ROOTFS_DIR		= $(TARGET_DIR)
MODULE_DIR		= $(TARGET_DIR)/lib/modules/2.6.14

#######################################################################
# Describe the target cross development tools
#######################################################################
# ARCH_BIN_DIR		Path to the tool cross development tool binaries
# ARCH_LIB_DIR		Path to the tool cross development libraries
# ARCH_LIB2_DIR		Path to the hiddent compiler internal libraries
# ARCH_INC_DIR		Path to the target C header files
# ARCH_INC2_DIR		Path to the target C++-specific header files
# ARCH_INC3_DIR		Path to the hidden compiler internal header files

ARCH_BIN_DIR		= $(STAGING_DIR)/bin
ARCH_LIB_DIR		= $(STAGING_DIR)/lib
ARCH_LIB2_DIR		= $(STAGING_DIR)/lib/gcc/armeb-linux-uclibc/3.4.5
ARCH_INC_DIR		= $(STAGING_DIR)/include
ARCH_INC2_DIR		= $(ARCH_INC_DIR)/g++-3
ARCH_INC3_DIR		= $(STAGING_DIR)/lib/gcc/armeb-linux-uclibc/3.4.5/include/

# Settings that will be used to setup CFLAGS

ARCHDEFINES		= 
ARCHWARNINGS		= -Wall -Wstrict-prototypes -Wshadow -Wno-trigraphs

ifeq ($(XFLAT_DEBUG),y)
  ARCHOPTIMIZATION	= -g
else
  ARCHOPTIMIZATION	= -pipe -Os -fno-strict-aliasing -fno-common \
			  -fno-strength-reduce -fomit-frame-pointer
endif

ARCHPICFLAGS		= -fpic -msingle-pic-base -membedded-pic
ARCHCPUFLAGS		= -mapcs-32 -march=armv5te -mtune=arm9tdmi \
			  -malignment-traps -msoft-float -mbig-endian

# Full pathes to tools

ARCH_TOOL_PREFIX	= $(ARCH_BIN_DIR)/armeb-linux-uclibc-
ARCHAS			= $(ARCH_TOOL_PREFIX)as
ARCHLD			= $(ARCH_TOOL_PREFIX)ld
ARCHCC			= $(ARCH_TOOL_PREFIX)gcc
ARCHCXX			= $(ARCH_TOOL_PREFIX)g++
ARCHCPP			= $(ARCHCC) -E
ARCHAR			= $(ARCH_TOOL_PREFIX)ar
ARCHNM			= $(ARCH_TOOL_PREFIX)nm
ARCHSTRIP		= $(ARCH_TOOL_PREFIX)strip
ARCHOBJCOPY		= $(ARCH_TOOL_PREFIX)objcopy
ARCHOBJDUMP		= $(ARCH_TOOL_PREFIX)objdump

# Access to the cross BFD is necessary.  These are dummy settings; the
# actual values will be applied by the buildroot makefile at build time
#
# ARCH_BFD_LIB_DIR	Path to the BFD library (libbfd.a)
# ARCH_IBERTY_LIB_DIR	Path to the BFD iberty library (libiberty.a)
# ARCH_BFD_INC_DIR	Path to the BFD header files (bfd.h)

ARCH_BFD_LIB_DIR	=
ARCH_IBERTY_LIB_DIR	=
ARCH_BFD_INC_DIR	=

#######################################################################
# Describe build options
#######################################################################
# libc and libpthread share symbols that are designated as ELF "weak"
# symbols.  "weak" symbols are not handled by the XFLAT loader.  So if
# libpthread is going to be used, then it must be "bundled" with libc.
# This should only be disabled in a non-threaded environment (no
# libpthread)

CONFIG_XFLAT_BUNDLE	= y

# Describe the uClibc version

CONFIG_UCLIBC_VERSION	= uClibc-0.9.28

