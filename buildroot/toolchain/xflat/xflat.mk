#############################################################
#
# XFLAT
#
#############################################################

XFLAT_DIR:=$(TOOL_BUILD_DIR)/xflat
XFLAT_INCLUDE_DIR:=$(XFLAT_DIR)/arch-include
XFLAT_CONFIG_FILE:=toolchain/xflat/xflat.config

xflat-unpacked: $(XFLAT_DIR)/.unpacked
$(XFLAT_DIR)/.unpacked:
	rm -rf $(XFLAT_DIR)
	mkdir -p $(XFLAT_DIR)
	cp -a $(BR2_XFLAT_DIR)/. $(XFLAT_DIR)/.
	find $(XFLAT_DIR) -name CVS -exec rm -rf {} \; || true
	touch $(XFLAT_DIR)/.unpacked

xflat-configured: $(XFLAT_DIR)/.configured
$(XFLAT_DIR)/.configured: $(XFLAT_DIR)/.unpacked
	cp -f $(XFLAT_CONFIG_FILE) $(XFLAT_DIR)/.config
	$(SED) 's,^STAGING_DIR[[:space:]]*=.*,STAGING_DIR	= $(STAGING_DIR),g' \
		-e 's,^TARGET_DIR[[:space:]]*=.*,TARGET_DIR	= $(TARGET_DIR),g' \
		-e 's,^BASE_DIR[[:space:]]*=.*,BASE_DIR	= $(BASE_DIR),g' \
		-e 's,^ARCH_BFD_LIB_DIR[[:space:]]*=.*,ARCH_BFD_LIB_DIR	= $(BINUTILS_DIR1)/bfd,g' \
		-e 's,^ARCH_IBERTY_LIB_DIR[[:space:]]*=.*,ARCH_IBERTY_LIB_DIR	= $(BINUTILS_DIR1)/libiberty,g' \
		-e 's,^ARCH_BFD_INC_DIR[[:space:]]*=.*,ARCH_BFD_INC_DIR	= $(BINUTILS_DIR1)/bfd,g' \
		$(XFLAT_DIR)/.config
	ln -sf ${XFLAT_DIR}/arch-include/xflat.h ${XFLAT_DIR}/sys-include/xflat.h
	ln -sf ${XFLAT_DIR}/arch-include/xflat_accessors.h ${XFLAT_DIR}/sys-include/xflat_accessors.h
	touch $(XFLAT_DIR)/.configured

xflat-built: $(XFLAT_DIR)/.built
$(XFLAT_DIR)/.built: $(XFLAT_DIR)/.configured
	$(MAKE1) -C $(XFLAT_DIR) build
	touch $(XFLAT_DIR)/.built

xflat-staged: $(XFLAT_DIR)/.staged
$(XFLAT_DIR)/.staged: $(XFLAT_DIR)/.built
	$(MAKE1) -C $(XFLAT_DIR) root_install
	touch $(XFLAT_DIR)/.staged

xflat-installed: $(XFLAT_DIR)/.installed
$(XFLAT_DIR)/.installed: $(XFLAT_DIR)/.staged
	install -D --mode 755 $(STAGING_DIR)/lib/ld-xflat.so.1 $(TARGET_DIR)/lib/ld-xflat.so.1
	ln -sf ld-xflat.so.1 $(TARGET_DIR)/lib/ld-xflat.so
	install -D --mode 755 $(STAGING_DIR)/lib/libc-xflat.so.1 $(TARGET_DIR)/lib/libc-xflat.so.1
	ln -sf libc-xflat.so.1 $(TARGET_DIR)/lib/libc-xflat.so
	install -D --mode 755 $(STAGING_DIR)/lib/libpthread-xflat.so.1 $(TARGET_DIR)/lib/libpthread-xflat.so.1
	ln -sf libpthread-xflat.so.1 $(TARGET_DIR)/lib/libpthread-xflat.so

	touch $(XFLAT_DIR)/.installed

xflat: gcc-xflat-headers xflat-installed

xflat-clean:
	-$(MAKE1) -C $(XFLAT_DIR) clean

xflat-dirclean:
	rm -rf $(XFLAT_DIR)

ifeq ($(strip $(BR2_XFLAT)),y)
TARGETS+=xflat
endif
