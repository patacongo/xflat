#############################################################
#
# uClibc (the C library) tailored for use with XFLAT
#
#############################################################

ifeq ($(BR2_ENABLE_LOCALE),y)
UCLIBC_XFLAT_CONFIG_FILE=toolchain/uClibc-xflat/uClibc-xflat.config-locale
else
UCLIBC_XFLAT_CONFIG_FILE=toolchain/uClibc-xflat/uClibc-xflat.config
endif

UCLIBC_XFLAT_DIR:=$(TOOL_BUILD_DIR)/uClibc-xflat
UCLIBC_XFLAT_INCLUDE_DIR:=$(UCLIBC_XFLAT_DIR)/include/xflat

UCLIBC_XFLAT_TMPDIR:=$(TOOL_BUILD_DIR)/uClibc-xflat-TMP
ifeq ($(BR2_UCLIBC_VERSION_SNAPSHOT),y)
UCLIBC_XFLAT_TMP:=$(UCLIBC_XFLAT_TMPDIR)/uClibc
UCLIBC_XFLAT_PATCHDIR:=toolchain/uClibc-xflat/uClibc-$(BR2_USE_UCLIBC_SNAPSHOT)
else
UCLIBC_XFLAT_TMP:=$(UCLIBC_XFLAT_TMPDIR)/uClibc-$(UCLIBC_VER)
UCLIBC_XFLAT_PATCHDIR:=toolchain/uClibc-xflat/uClibc-$(UCLIBC_VER)
endif

uclibc-xflat-unpacked: $(UCLIBC_XFLAT_DIR)/.unpacked
$(UCLIBC_XFLAT_DIR)/.unpacked: $(DL_DIR)/$(UCLIBC_SOURCE)
	rm -rf $(UCLIBC_XFLAT_TMPDIR)
	mkdir -p $(UCLIBC_XFLAT_TMPDIR)
	bzcat $(DL_DIR)/$(UCLIBC_SOURCE) | tar -C $(UCLIBC_XFLAT_TMPDIR) $(TAR_OPTIONS) -
	mv $(UCLIBC_XFLAT_TMP) $(UCLIBC_XFLAT_DIR)
	rm -rf $(UCLIBC_XFLAT_TMPDIR)
	toolchain/patch-kernel.sh $(UCLIBC_XFLAT_DIR) $(UCLIBC_XFLAT_PATCHDIR) \*.patch
	touch $(UCLIBC_XFLAT_DIR)/.unpacked

uclibc-xflat-headers: $(UCLIBC_XFLAT_DIR)/.xflat-headers
$(UCLIBC_XFLAT_DIR)/.xflat-headers: xflat-unpacked $(UCLIBC_XFLAT_DIR)/.unpacked
	rm -rf $(UCLIBC_XFLAT_INCLUDE_DIR)
	cp -a $(XFLAT_INCLUDE_DIR) $(UCLIBC_XFLAT_INCLUDE_DIR)
	touch $(UCLIBC_XFLAT_DIR)/.xflat-headers

uclibc-xflat-configured: $(UCLIBC_XFLAT_DIR)/.configured
$(UCLIBC_XFLAT_DIR)/.configured: $(UCLIBC_XFLAT_DIR)/.unpacked
	cp $(UCLIBC_XFLAT_CONFIG_FILE) $(UCLIBC_XFLAT_DIR)/.config
	$(SED) 's,^CROSS_COMPILER_PREFIX=.*,CROSS_COMPILER_PREFIX="$(TARGET_CROSS)",g' \
		-e 's,# TARGET_$(UCLIBC_TARGET_ARCH) is not set,TARGET_$(UCLIBC_TARGET_ARCH)=y,g' \
		-e 's,^TARGET_ARCH="none",TARGET_ARCH=\"$(UCLIBC_TARGET_ARCH)\",g' \
		-e 's,^KERNEL_SOURCE=.*,KERNEL_SOURCE=\"$(LINUX_HEADERS_DIR)\",g' \
		-e 's,^RUNTIME_PREFIX=.*,RUNTIME_PREFIX=\"/\",g' \
		-e 's,^DEVEL_PREFIX=.*,DEVEL_PREFIX=\"/usr/\",g' \
		-e 's,^SHARED_LIB_LOADER_PREFIX=.*,SHARED_LIB_LOADER_PREFIX=\"/lib\",g' \
		$(UCLIBC_XFLAT_DIR)/.config
ifeq ($(UCLIBC_TARGET_ARCH),arm)
	$(SED) 's,^.*CONFIG_$(shell echo $(BR2_ARM_TYPE)).*,CONFIG_$(shell echo $(BR2_ARM_TYPE))=y,g' \
	$(UCLIBC_XFLAT_DIR)/.config
endif
ifneq ($(UCLIBC_TARGET_ENDIAN),)
	$(SED) '/^# ARCH_$(UCLIBC_TARGET_ENDIAN)_ENDIAN /{s,# ,,;s, is not set,=y,g}' \
		-e '/^# ARCH_$(UCLIBC_NOT_TARGET_ENDIAN)_ENDIAN /{s,# ,,;s, is not set,=n,g}' \
		$(UCLIBC_XFLAT_DIR)/.config
endif
ifeq ($(BR2_LARGEFILE),y)
	$(SED) 's,^.*UCLIBC_HAS_LFS.*,UCLIBC_HAS_LFS=y,g' $(UCLIBC_XFLAT_DIR)/.config
else
	$(SED) 's,^.*UCLIBC_HAS_LFS.*,UCLIBC_HAS_LFS=n,g' $(UCLIBC_XFLAT_DIR)/.config
endif
	$(SED) 's,.*UCLIBC_HAS_WCHAR.*,UCLIBC_HAS_WCHAR=y,g' $(UCLIBC_XFLAT_DIR)/.config
ifeq ($(BR2_SOFT_FLOAT),y)
	$(SED) 's,.*UCLIBC_HAS_FPU.*,UCLIBC_HAS_FPU=n\nUCLIBC_HAS_FLOATS=y\nUCLIBC_HAS_SOFT_FLOAT=y,g' $(UCLIBC_XFLAT_DIR)/.config
endif
ifneq ($(BR2_PTHREADS_NONE),y)
	$(SED) 's,# UCLIBC_HAS_THREADS is not set,UCLIBC_HAS_THREADS=y,g' $(UCLIBC_XFLAT_DIR)/.config
	$(SED) 's,# PTHREADS_DEBUG_SUPPORT is not set,PTHREADS_DEBUG_SUPPORT=y,g' $(UCLIBC_XFLAT_DIR)/.config
endif
ifeq ($(BR2_PTHREADS),y)
	$(SED) 's,# LINUXTHREADS is not set,LINUXTHREADS=y,g' $(UCLIBC_XFLAT_DIR)/.config
endif
ifeq ($(BR2_PTHREADS_OLD),y)
	$(SED) 's,# LINUXTHREADS_OLD is not set,LINUXTHREADS_OLD=y,g' $(UCLIBC_XFLAT_DIR)/.config
endif
ifeq ($(BR2_PTHREADS_NATIVE),y)
	$(SED) 's,# UCLIBC_HAS_THREADS_NATIVE is not set,UCLIBC_HAS_THREADS_NATIVE=y,g' $(UCLIBC_XFLAT_DIR)/.config
endif
	mkdir -p $(TOOL_BUILD_DIR)/uClibc_dev/usr/include
	mkdir -p $(TOOL_BUILD_DIR)/uClibc_dev/usr/lib
	mkdir -p $(TOOL_BUILD_DIR)/uClibc_dev/lib
	$(MAKE1) -C $(UCLIBC_XFLAT_DIR) \
		PREFIX=$(TOOL_BUILD_DIR)/uClibc_dev/ \
		DEVEL_PREFIX=/usr/ \
		RUNTIME_PREFIX=$(TOOL_BUILD_DIR)/uClibc_dev/ \
		HOSTCC="$(HOSTCC)" \
		pregen install_dev && \
	touch $(UCLIBC_XFLAT_DIR)/.configured

$(UCLIBC_XFLAT_DIR)/lib/libc.a: $(UCLIBC_XFLAT_DIR)/.configured $(LIBFLOAT_TARGET)
	$(MAKE1) -C $(UCLIBC_XFLAT_DIR) \
		PREFIX= \
		DEVEL_PREFIX=/ \
		RUNTIME_PREFIX=/ \
		HOSTCC="$(HOSTCC)" \
		all
	touch -c $(UCLIBC_XFLAT_DIR)/lib/libc.a

$(STAGING_DIR)/lib/libc-xflat.a: $(UCLIBC_XFLAT_DIR)/.xflat-headers $(UCLIBC_XFLAT_DIR)/lib/libc.a
	install -D --mode=644 $(UCLIBC_XFLAT_DIR)/lib/libc.a $(STAGING_DIR)/lib/libc-xflat.a
	install -D --mode=644 $(UCLIBC_XFLAT_DIR)/lib/libpthread.a $(STAGING_DIR)/lib/libpthread-xflat.a
	touch -c $(STAGING_DIR)/lib/libc-xflat.a

uclibc-xflat-configured: $(UCLIBC_XFLAT_DIR)/.configured

uclibc-xflat: $(STAGING_DIR)/bin/$(REAL_GNU_TARGET_NAME)-gcc $(STAGING_DIR)/lib/libc-xflat.a

uclibc-xflat-clean:
	-$(MAKE1) -C $(UCLIBC_XFLAT_DIR) clean
	rm -f $(UCLIBC_XFLAT_DIR)/.config

uclibc-xflat-dirclean:
	rm -rf $(UCLIBC_XFLAT_DIR)

ifeq ($(strip $(BR2_XFLAT)),y)
TARGETS+=uclibc-xflat
endif
